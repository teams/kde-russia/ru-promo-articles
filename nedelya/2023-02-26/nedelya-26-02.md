# На этой неделе в KDE: многоэкранные успехи

Нечто забавное происходит, когда вы берёте что-то очень сломанное и значительно его усовершенствуете: им начинают больше пользоваться! А затем вас заваливают отчётами об ошибках в необычных, непредвиденных и забытых сценариях использования. В моменте кажется, что всё стало хуже, но на самом деле продукт улучшается, а отчёты об ошибках со временем описывают всё более и более экзотические ситуации.

Я наблюдал это 2 года назад с сеансом Plasma Wayland (который с тех пор стал вполне себе надёжным), а теперь это происходит снова с поддержкой нескольких экранов. Наконец-то мы разобрались с основами, поэтому все снова начали пробовать настроить систему без «костылей» вроде скриптов, вызывающих `xrandr`, и прислали нам кучу отчётов о проблемах в их диких конфигурациях 🙂. И это здорово! На прошедшей неделе мы потратили уйму времени, работая над исправлением таких специфичных ошибок, чтобы взаимодействие с несколькими мониторами было ещё надёжнее. С солидным фундаментом искоренять недочёты не так уж и сложно!

И хотя основная команда Plasma была всецело поглощена этим, другие участники не теряли времени зря! На этой неделе есть на что посмотреть:

> 19–26 февраля, основное — прим. переводчика

## Новые возможности

Пункт контекстного меню для установки изображения в качестве обоев, доступный в диспетчере файлов Dolphin и на рабочем столе, теперь также позволяет менять обои и на экране блокировки! (Julius Zint, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2458)):

![0](https://pointieststick.files.wordpress.com/2023/02/setting-wallpapers-from-the-context-menu.jpg)

*Не обращайте внимание на дублирование на снимке пункта меню для настройки обоев, в итоговом выпуске такого не будет*

## Улучшения пользовательского интерфейса

Текстовые редакторы Kate и KWrite теперь вскоре после открытия сохраняют список открытых документов, так что если они аварийно завершатся или будут вытеснены из памяти из-за её нехватки, при повторном открытии их вкладки будут восстановлены (Waqar Ahmed, Kate и KWrite 23.04. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=437337))

Просмотрщик документов Okular теперь изменяет масштаб плавно, а не пошагово, при прокрутке сенсорной панелью или колёсиком мыши высокого разрешения с зажатой клавишей Ctrl (Friso Smit, Okular 23.04. [Ссылка](https://invent.kde.org/graphics/okular/-/merge_requests/692))

Помощник первого запуска стал более похожим на другие приложения KDE: кнопки действий теперь расположены снизу, а индикатор из точек, соответствующих страницам, показывает текущий шаг (Oliver Beard, Plasma 6.0. [Ссылка 1](https://invent.kde.org/plasma/plasma-welcome/-/merge_requests/41) и [ссылка 2](https://invent.kde.org/plasma/plasma-welcome/-/merge_requests/64)):

![1](https://pointieststick.files.wordpress.com/2023/02/more-kde-ish-welcome-center.jpg)

Страницы приложений в Discover прошли через ещё одну итерацию внешнего вида, так что теперь они эффективнее используют пространство и в целом лучше выглядят (Nate Graham, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/discover/-/merge_requests/465)):

* ![2](https://pointieststick.files.wordpress.com/2023/02/discover-showing-krita.jpg)
* ![3](https://pointieststick.files.wordpress.com/2023/02/discover-showing-openscad.jpg)

На странице «Права доступа пакетов Flatpak» в Параметрах системы теперь есть строка поиска для фильтрации приложений по имени, а над опциями появился красивый заголовок со сведениями о программе (Иван Ткаченко, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/flatpak-kcm/-/merge_requests/49)):

![4](https://pointieststick.files.wordpress.com/2023/02/flatpak-kcm-with-search-and-header.jpg)

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Исправлено недавнее ухудшение, которое могло привести к сбою Plasma при пробуждении системы в многомониторной конфигурации (Aleix Pol Gonzalez, Plasma 5.27.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=466095))

Исправлено недавнее ухудшение, из-за которого в сеансе Plasma Wayland вокруг панелей появлялись артефакты в виде линий при использовании дробного коэффициента глобального масштабирования (Arjen Hiemstra, Plasma 5.27.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=464526))

Исправлена ошибка, из-за которой диспетчер окон KWin мог аварийно завершить работу при воспроизведении видео в проигрывателе VLC (Влад Загородний, Plasma 5.27.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=466186))

Исправлена ошибка, из-за которой KWin мог аварийно завершить работу при выходе из сеанса Wayland, оставив вас в подвешенном состоянии (Влад Загородний, Plasma 5.27.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=465456))

При использовании недавно выпущенной версии 1.8.11 библиотеки `fwupd` Discover больше не сталкивается с ошибкой при запуске (Adam Williamson, Plasma 5.27.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=466318))

Исправлено недавнее ухудшение, из-за которого служба `powerdevil` могла аварийно завершиться в многомониторной конфигурации, нарушив управление питанием (Aleix Pol Gonzalez, Plasma 5.27.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=466181))

Исправлена ошибка, из-за которой Параметры системы могли аварийно завершать работу при применении или отмене изменений в расположении экранов (Arjen Hiemstra, Plasma 5.27.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=464707))

Исправлено серьёзное ухудшение в том, как оформления окон формата Aurorae отрисовывались в сеансе Wayland (David Edmundson, Plasma 5.27.2. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=465790) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=452730))

Решена проблема в сеансе Wayland, из-за которой размер рабочего стола вычислялся слегка неправильно при использовании дробного коэффициента масштабирования, что вызывало различные визуальные и функциональные сбои (David Edmundson, Plasma 5.27.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=465850))

Сравнительно новая QML-версия эффекта «Все окна» теперь правильно работает с клавиатурой, когда вызывается для показа окон одного приложения (Влад Загородний, Plasma 5.27.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=466120))

При использовании дробного коэффициента масштабирования в сеансе Wayland курсор мыши теперь отображается правильно в приложениях, использующих слой совместимости XWayland (Xaver Hugl, Plasma 5.27.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=466094))

Устройства с несколькими подключёнными экранами от одного поставщика, серийный номер которых отличается только в последнем символе (представьте большую компанию, оптом закупившую мониторы), больше не путаются при входе в систему (David Redondo, Plasma 5.27.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=466136))

Исправлено недавнее ухудшение в сеансе Wayland, которое могло приводить к частому сбою службы индексирования файлов Baloo (David Redondo, Frameworks 5.104. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=465801))

При получении новых дополнительных материалов через стандартный диалог загрузки, список выбора версии теперь правильно прокручивается, если он не помещается в видимую область (Иван Ткаченко, Frameworks 5.104. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=448800))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Известно о 16 ошибках в Plasma с очень высоким приоритетом (на прошлой неделе было лишь 12). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma стало 44 (на прошлой неделе было 43). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 152 ошибки было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-02-17&chfieldto=2023-02-24&chfieldvalue=RESOLVED&list_id=2286503&query_format=advanced&resolution=FIXED)

## Автоматизация и систематизация

[Учебник по разработке приложений на основе библиотеки Kirigami](https://develop.kde.org/docs/use/kirigami/) был переписан и теперь куда полезнее и понятнее! (Thiago Sueto, [ссылка](https://invent.kde.org/documentation/develop-kde-org/-/merge_requests/235))

Для каждого изменения в просмотрщике изображений Gwenview и приложении для захвата с веб-камеры Kamoso теперь автоматически собирается тестовый пакет Flatpak (Neelaksh Singh, [ссылка 1](https://invent.kde.org/graphics/gwenview/-/merge_requests/184) и [ссылка 2](https://invent.kde.org/multimedia/kamoso/-/merge_requests/15))

## Изменения вне KDE, затрагивающие KDE

У пользователей дистрибутивов Neon и Fedora KDE в сеансе Plasma Wayland снова работает управление питанием экранов, подключённых через DisplayPort. Оказалось, что они собирали библиотеку KIdleTime без поддержки Wayland (Jonathan Riddell и Marc Deop, [ссылка](https://bugs.kde.org/show_bug.cgi?id=462695)).

> …Да, да, технически, Neon входит в KDE, но было бы странно из-за этого копировать абзац. В обоих дистрибутивах была одна и та же первопричина проблемы, и её решили одинаковым исправлением.

## Как вы можете помочь

Если вы пользователь, обновитесь до [Plasma 5.27](https://www.kde.org/announcements/plasma/5/5.27.0/)! Если обновление в вашем дистрибутиве не доступно и не предвидится, подумайте о переходе на другой дистрибутив, поставляющий программное обеспечение ближе к графикам разработчиков.

Разработчики, помогите нам исправить [известные проблемы в Plasma 5.27](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&keywords=regression%2C%20&keywords_type=allwords&list_id=2278517&query_format=advanced&version=5.26.90&version=5.27.0&version=5.27.1&version=5.27.2&version=5.27.3&version=5.27.4&version=5.27.5). Также посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этих списков — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

<!-- Временная ссылка! -->

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/02/24/this-week-in-kde-even-better-multi-monitor/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: get new [thing] → «Загрузить \[что-то\]» -->
<!-- 💡: icon → значок -->
<!-- 💡: task manager → панель задач -->
