# На этой неделе в KDE: одиночные модификаторы как сочетания клавиш

Многие годы в диспетчере окон KWin указать в качестве сочетания клавиш только клавишу-модификатор можно было исключительно посредством редактирования файла `kwinrc`, что было неочевидно и не было описано в документации. Наконец ситуация изменилась: в Plasma 6.1 соответствующие инструменты KDE позволяют указывать клавиши-модификаторы без символьных клавиш! Теперь будет намного проще, например, изменить работу клавиши Meta и назначить её на открытие эффекта «Обзор», строки поиска и запуска KRunner и т. д. Это исправление потребовало обширной сквозной перестройки в программном стеке KDE и было реализовано разработчиком Yifan Zhu. При этом удалось закрыть несколько отчётов об ошибках на Bugzilla (некоторые из них ждали своего часа уже давно). Огромное спасибо, Yifan! ([Ссылка 1](https://invent.kde.org/plasma/kglobalacceld/-/merge_requests/44), [ссылка 2](https://invent.kde.org/plasma/kwin/-/issues/205), [3](https://bugs.kde.org/show_bug.cgi?id=481173), [4](https://bugs.kde.org/show_bug.cgi?id=470257), [5](https://bugs.kde.org/show_bug.cgi?id=464805) и [6](https://bugs.kde.org/show_bug.cgi?id=470256))

Но это ещё не всё! Исправление ошибок — процесс бесконечный, однако нам удалось разрешить основные проблемы в Plasma 6.0, и в последнее время фокус сообщества сместился на новые функции и улучшение интерфейса. Итак, начнём с самого востребованного…

> 31 марта – 7 апреля, основное — прим. переводчика

## Новые возможности

В утилиту для создания снимков экрана Spectacle вернулся инструмент «Кадрирование», но теперь это встроенная функция, а не часть сторонней библиотеки работы со снимками экрана. При этом ещё и была исправлена одна ошибка (Noah Davis, Spectacle 24.05. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=467590) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=481435)):

![0](https://pointieststick.files.wordpress.com/2024/04/image.png)

На ноутбуках с RGB-подсветкой клавиатуры Plasma теперь может синхронизировать цвет подсветки с активным цветом выделения! Скоро появится и поддержка настраиваемых цветов (Natalie Clarius, Plasma 6.1. [Ссылка 1](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/560) и [ссылка 2](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4093#note_914254)):

<https://i.imgur.com/zlsaJlX.mp4?_=1>

Новый автоматический обработчик ошибок теперь может скачивать отладочные символы в фоновом режиме, что повысит качество генерируемых отчётов о сбоях (Harald Sitter, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/drkonqi/-/merge_requests/223))

Продолжая тему одиночных клавиш-модификаторов: теперь неиспользуемую кнопку мыши можно заставить имитировать нажатие Alt, Shift и т. д. (David Redondo, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=475876))

Обычно пакеты Snap обновляются автоматически, но теперь в центре приложений Discover есть возможность обновить их вручную (Kevin Ottens, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=476654))

## Улучшения пользовательского интерфейса

Диспетчер файлов Dolphin научился отображать сообщение об ошибке, если использовать сочетание клавиш для запуска действия, которое невозможно выполнить в текущем контексте — например, если попытаться вырезать файл в папке, доступной только для чтения (Jin Liu, Dolphin 24.05. [Ссылка](https://invent.kde.org/system/dolphin/-/merge_requests/738)):

![2](https://pointieststick.files.wordpress.com/2024/04/image-1.png)

В музыкальном проигрывателе Elisa теперь можно переключаться между режимами просмотра списком и сеткой (Jack Hill, Elisa 24.05. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=411952)):

![3](https://pointieststick.files.wordpress.com/2024/04/image-2-1.png)

Также в Elisa появилась возможность искать альбомы на странице «Дорожки» (Karl Hoffman, Elisa 24.05. [Ссылка](https://invent.kde.org/multimedia/elisa/-/merge_requests/561))

В диалоговом окне Plasma, которое позволяет добавить новые обои, теперь можно выбрать несколько файлов (Sahil Arora, Plasma 6.0.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=484770))

Переключатели и флажки в стиле Breeze теперь используют цветовую роль «Кнопка» (как и большинство других интерактивных элементов интерфейса). Это позволило не только улучшить единообразие внешнего вида, но и устранить проблему в ПО на основе QtQuick, когда соответствующие элементы иногда отрисовывались неправильным цветом (Akseli Lahtinen, Frameworks 6.1 и Plasma 6.1. [Ссылка 1](https://invent.kde.org/frameworks/qqc2-desktop-style/-/merge_requests/389), [ссылка 2](https://invent.kde.org/plasma/breeze/-/merge_requests/450) и [3](https://invent.kde.org/plasma/breeze-gtk/-/merge_requests/88))

Заголовки списков в меню запуска приложений теперь используют стандартный стиль из приложений KDE на основе QtQuick. Для этого потребовалось создать новую версию этого компонента, подходящую для Plasma, поэтому можно ожидать его появления и в других местах (Nate Graham, Plasma 6.1. [Ссылка 1](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2155) и [ссылка 2](https://invent.kde.org/plasma/libplasma/-/merge_requests/1085)):

![4](https://pointieststick.files.wordpress.com/2024/04/image-2-2.png)

## Исправления ошибок

Просмотрщик документов Okular больше не запрашивает пароль для открытия зашифрованных PDF-документов, если шифрование на открытие не распространяется (Nicolas Fella при поддержке Технического университета Дрездена, Okular 24.02.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=474888))

Устранён ряд связанных между собой сбоев в Plasma, оказавшихся неочевидным побочным эффектом недавнего исправления, благодаря которому приложения, удалённые из сетки избранных в меню запуска приложений, стали исчезать сразу же (Fushan Wen, Plasma 6.0.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482887))

Исправлена ещё одна ошибка, из-за которой Plasma могла аварийно завершать работу при подключении нового экрана (Fushan Wen, Plasma 6.0.4. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=483432) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=483916))

Исправлен редкий сбой Plasma, который мог происходить при нажатии на определённые значки в системном лотке (Fushan Wen, Plasma 6.0.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=478059))

Исправлена ошибка, из-за которой утилита Spectacle могла аварийно завершать работу по окончании записи экрана, если использовались определённые видеокарты (Fabian Vogt, Plasma 6.0.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=484620))

Исправлена ошибка, из-за которой в течение 60 секунд после входа в сеанс выйти было нельзя, если для запуска Plasma использовался systemd (Harald Sitter, Plasma 6.0.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=484864))

Исправлена проблема, из-за которой при касании определённых сенсорных панелей (скажем так, «класса ниже премиум») двумя пальцами над панелью задач её контекстное меню не открывалось (Fushan Wen, Plasma 6.0.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482813))

Исправлен код в отношении ещё четырёх сбоев, обнаруженных автоматическим обработчиком ошибок KDE, — как видите, он оказывается весьма полезен! (И снова Fushan Wen — такой вот он крутой! Plasma 6.0.4. [Ссылка 1](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4150), [ссылка 2](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4175), [3](https://invent.kde.org/frameworks/kirigami/-/merge_requests/1497) и [4](https://invent.kde.org/plasma/libplasma/-/merge_requests/1094))

Исправлена ошибка, из-за которой сведения о дате в подсказках различных виджетов с часами локализовались неправильно (Albert Astals Cid, Plasma 6.0.4. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=467325) и [ссылка 2](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/572))

Исправлено ещё множество странных проблем с курсорами мыши в видеоиграх под Wayland (Xaver Hugl, Plasma 6.1. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=482629) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=482476))

Исправлены некоторые визуальные сбои, которые можно было наблюдать в сеансе Wayland при изменении размера окон определённого типа, открывающихся в распахнутом состоянии (Ser Freeman, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=483309))

Другие сведения об ошибках:

* Осталось 2 ошибки в Plasma с очень высоким приоритетом (на прошлой неделе было 4). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 34 (на прошлой неделе было 35). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 145 ошибок было исправлено на этой неделе во всём программном обеспечении KDE! [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-3-29&chfieldto=2024-4-05&chfieldvalue=RESOLVED&list_id=2673419&query_format=advanced&resolution=FIXED)

## Производительность и технические улучшения

Spectacle теперь делает снимки экрана быстрее при её использовании вне Plasma и KWin под X11 (Константин Харламов, Spectacle 24.02.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=484652))

Исправлена утечка памяти в виджете Plasma «Сети» (Fushan Wen, Plasma 6.0.4. [Ссылка](https://invent.kde.org/plasma/plasma-nm/-/merge_requests/342))

Сторонние макеты экрана блокировки на QML в составе оформлений рабочей среды больше не поддерживаются: они создают слишком большую угрозу безопасности (Marco Martin, Plasma 6.1. [Ссылка 1](https://invent.kde.org/plasma/kscreenlocker/-/merge_requests/209), [ссылка 2](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2136), [3](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4112) и [4](https://invent.kde.org/plasma/libplasma/-/merge_requests/1081))

Эффект «Затемнение основного окна» (когда открыто дочернее диалоговое окно) теперь работает и в сеансе Wayland (Carlos Garnacho и David Redondo, Plasma 6.1 с Qt 6.8. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460819))

Существенно пересмотрен код виджетов Plasma «Цифровые часы» и «Календарь»: исправлено множество потенциальных источников нестабильности, предупреждений и пара мелких сбоев; работа продолжается (Иван Ткаченко, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4177))

## Автоматизация и систематизация

Написано руководство по [публикации совместимых с Android приложений KDE в Google Play](https://develop.kde.org/docs/packaging/android/googleplay/) (Ingo Klöcker, [ссылка](https://invent.kde.org/documentation/develop-kde-org/-/merge_requests/383))

## Как вы можете помочь

Сообщество KDE стало важным явлением в мире — благодаря вашему труду и вложенному времени! Однако по мере роста проекта важно обеспечить стабильность работы над ним, а это значит, что труд должен *оплачиваться*. Сегодня KDE по большей части использует работу людей, которым фонд KDE e.V. не платит, и это проблема. Мы предприняли шаги, которые позволят изменить ситуацию, и наняли [платных технических подрядчиков](https://ev.kde.org/corporate/staffcontractors/), однако из-за ограниченности финансовых ресурсов этого всё ещё мало. Если вы хотите помочь, [сделайте пожертвование](https://kde.org/ru/community/donations/)!

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* коммерческий  
*Источник:* <https://pointieststick.com/2024/04/05/this-week-in-kde-real-modifier-only-shortcuts-and-cropping-in-spectacle/>  
