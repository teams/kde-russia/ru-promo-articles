# На этой неделе в KDE: всё дело в приложениях

Пару недель назад мы обнаружили, что приложения KDE выглядят просто ужасно при запуске в GNOME. Последовало [длинное обсуждение темирования значков](https://gitlab.gnome.org/GNOME/adwaita-icon-theme/-/issues/288), из которого вышли различные улучшения с обеих сторон. Со стороны KDE усилия возглавил Christoph Cullmann, о чём он уже рассказал [в своём блоге](https://cullmann.io/posts/kde-applications-and-icons/). В двух словах, приложения KDE, задействующие новый механизм, при запуске вне Plasma всегда будут иметь оформление и значки Breeze, если это не переопределено системой или пользователем. Среди таких приложений уже [текстовый редактор Kate](https://invent.kde.org/utilities/kate/-/merge_requests/1482), [эмулятор терминала Konsole](https://invent.kde.org/utilities/konsole/-/merge_requests/996) и [диспетчер файлов Dolphin](https://invent.kde.org/system/dolphin/-/merge_requests/773). Вы можете помочь включить это и в других приложениях, используя приведённые изменения в качестве вдохновения!

> 12–19 мая, основное — прим. переводчика

## Новые возможности

Dolphin теперь позволяет вам включить миниатюры для папок в удалённых расположениях. Учитывайте, что это может вызывать замедления, о чём вас предупреждает интерфейс (Сергей Катунин, Dolphin 24.08. [Ссылка](https://invent.kde.org/system/dolphin/-/merge_requests/768)):

![0](https://pointieststick.com/wp-content/uploads/2024/05/image-6.png)

Центр приложений Discover теперь обрабатывает случай, когда одно из ваших Flatpak-приложений было помечено как более не поддерживаемое и заменено другим; он предлагает вам перейти на новое или остаться на старом (Harald Sitter, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=485886)):

![1](https://pointieststick.com/wp-content/uploads/2024/05/image-7.png)

## Улучшения пользовательского интерфейса

Поддержка файловых операций от имени администратора в Dolphin (требует `kio-admin`) была улучшена: теперь при входе в режим администратора отображается предупреждение о возможности нанесения вреда системе по неосторожности, а во время работы от имени администратора отображается баннер, напоминающий об этом (Felix Ernst, Dolphin 24.08. [Ссылка](https://invent.kde.org/system/dolphin/-/merge_requests/487)):

* ![2](https://pointieststick.com/wp-content/uploads/2024/05/act.jpeg)
* ![3](https://pointieststick.com/wp-content/uploads/2024/05/understand.jpeg)
* ![4](https://pointieststick.com/wp-content/uploads/2024/05/its-dangerous.jpeg)

В Dolphin улучшено взаимодействие с папками, доступными только для чтения (Jin Liu, Dolphin 24.08. [Ссылка](https://invent.kde.org/system/dolphin/-/issues/55))

Неизменяемые вкладки панели инструментов в утилите для создания снимков экрана Spectacle переведены на стиль, используемый другими Kirigami-приложениями (Nate Graham, Spectacle 24.08. [Ссылка](https://invent.kde.org/graphics/spectacle/-/merge_requests/368)):

* ![5](https://pointieststick.com/wp-content/uploads/2024/05/no-screenshot-view.jpeg)
* ![6](https://pointieststick.com/wp-content/uploads/2024/05/main-view.jpeg)

На значках, отображаемых в наших диалоговых окнах, больше не изображены диалоги; теперь это обычные цветные значки (Nate Graham, Frameworks 6.3. [Ссылка](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/364)):

![7](https://pointieststick.com/wp-content/uploads/2024/05/image-8.png)

## Исправления ошибок

Попытка открыть несколько диалогов «Создание папки» в медленном сетевом расположении больше не приводит к сбою Dolphin (Akseli Lahtinen, Dolphin 24.08. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=481401))

Очень маленькие изображения SVG стали правильно отображаться на миниатюрах (Méven Car, kio-extras 24.08. [Ссылка](https://invent.kde.org/network/kio-extras/-/merge_requests/350))

Исправлен случай, когда наша система аутентификации могла завершиться аварийно и оставить приложения неспособными запрашивать аутентификацию (Nate Graham, Plasma 6.0.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=485407))

Включение режима HDR больше не делает цвета экрана неправильными при использовании ночной цветовой схемы (Xaver Hugl, Plasma 6.0.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=483801))

Внизу экранов, использующих масштабирование с дробным коэффициентом, больше нет странного ряда пикселей, сохраняющего цвет ранее открытых окон (Xaver Hugl, Plasma 6.0.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482987))

Браузеры на основе Chromium, запущенные в режиме нативной поддержки Wayland, больше не зависают и не вылетают при перетаскивании файлов на веб-сайты. Это была сложная проблема, вызванная необычной логикой в Chromium, но диспетчер окон KWin теперь правильно обрабатывает её (David Edmundson, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482142))

Переход на страницу «Поиск файлов» Параметров системы больше не вызывает иногда длительное подвисание при высокой загруженности индексатора файлов (Janet Blackquill, Frameworks 6.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=487064))

Элементы `KSvg` и `Kirigami.Icon`, используемые в Plasma, теперь правильно раскрашивают изображения SVG при использовании светлой цветовой схемы для приложений и тёмной для Plasma. Так, котёнок из виджета [CatWalk](https://store.kde.org/p/2137844) теперь выглядит как надо с Breeze Twilight (Marco Martin, Frameworks 6.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=485801)):

![8](https://pointieststick.com/wp-content/uploads/2024/05/screenshot_20240517_100033.jpeg)

Другие сведения об ошибках:

* Остаётся 3 ошибки в Plasma с очень высоким приоритетом (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma — 39 (на прошлой неделе было 38). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 106 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-5-10&chfieldto=2024-5-17&chfieldvalue=RESOLVED&list_id=2716671&query_format=advanced&resolution=FIXED)

## Как вы можете помочь

Сообщество KDE стало важным явлением в мире — благодаря вашему труду и вложенному времени! Однако по мере роста проекта важно обеспечить стабильность работы над ним, а это значит, что труд должен *оплачиваться*. Сегодня KDE по большей части использует работу людей, которым фонд KDE e.V. не платит, и это проблема. Мы предприняли шаги, которые позволят изменить ситуацию, и наняли [платных технических подрядчиков](https://ev.kde.org/corporate/staffcontractors/), однако из-за ограниченности финансовых ресурсов этого всё ещё мало. Если вы хотите помочь, [сделайте пожертвование](https://kde.org/ru/community/donations/)!

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2024/05/17/this-week-in-kde-all-about-those-apps/>  
