# На этой неделе в KDE: автопрокрутка

> 30 июня – 7 июля, основное — прим. переводчика

## Новые возможности

Теперь можно включить функцию «автоматической прокрутки» драйвера Libinput, которая позволяет листать любое прокручиваемое представление, зажав среднюю кнопку мыши и перемещая всю мышь (Евгений Чесноков, Plasma 6.2.0. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2357))

## Улучшения пользовательского интерфейса

В просмотрщике документов Okular при изменении масштаба документа прокруткой с зажатым Ctrl он теперь меняется в направлении положения курсора, а не центра страницы (Alexis Murzeau, Okular 24.08.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=421484))

Okular теперь масштабирует переключатели и флажки в соответствии с размером содержащих их полей формы, что выглядит лучше в формах с огромными или крошечными элементами (Pratham Gandhi, Okular 24.08.0. [Ссылка](https://invent.kde.org/graphics/okular/-/merge_requests/1015))

Диспетчер файлов Dolphin теперь учитывает общесистемный выключатель плавной прокрутки (Nathan Misner, Dolphin 24.08.0 [Ссылка](https://invent.kde.org/system/dolphin/-/merge_requests/795))

Ярлыки `.desktop` в подпапках рабочего стола теперь выглядят так же, как и ярлыки на самом рабочем столе (Alexander Wilms, Plasma 6.2.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=458400))

На странице «Специальные возможности» Параметров системы диалоговое окно выбора файла звука системного сигнала теперь принимает файлы `.oga`, а также перечисляет все поддерживаемые типы файлов (Nate Graham, Plasma 6.2.0. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2354))

На странице «Эффекты рабочего стола» Параметров системы «внутренние» эффекты больше не отображаются вообще (даже в скрытом по умолчанию состоянии), что дополнительно усложняет случайную поломку системы, а также предотвращает неожиданный сброс настроек внутренних эффектов, изменённых в других местах, по нажатию кнопки «По умолчанию». При необходимости вы можете найти внутренние эффекты в отладочной консоли диспетчера окон KWin (Влад Загородний, Plasma 6.2.0. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/6010))

В страницы Параметров системы внесён ряд небольших изменений, приводящих их в большее соответствие с [новыми Рекомендациями по разработке интерфейсов (HIG)](https://pointieststick.com/2024/06/09/new-human-interface-guidelines/) (Nate Graham, Plasma 6.2.0. [Ссылка 1](https://invent.kde.org/plasma/krdp/-/merge_requests/50), [ссылка 2](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4503), [3](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4502) и [4](https://invent.kde.org/plasma/print-manager/-/merge_requests/182))

Улучшена видимость текста на кнопках панелей навигации (`NavigationTabBar`) из библиотеки Kirigami, особенно на экранах с невысокой плотностью пикселей (Nate Graham, Frameworks 6.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=489573))

## Исправления ошибок

Исправлено ​​недавнее ухудшение, приводившее к случайному сбою службы управления питанием PowerDevil на устройствах с мониторами, поддерживающими управление яркостью через DDC (Jakob Petsovits, Plasma 6.1.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=489169))

На недавно переработанной странице «Клавиатура» Параметров системы ширина столбцов таблицы раскладок снова настраивается, а по умолчанию более уместная (Wind He, Plasma 6.1.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=488957))

Исправлена одна из причин недавней проблемы, из-за которой некоторые страницы Параметров системы могли ломаться при открытии — в данном случае после настройки сенсорных панелей или сетей. Мы всё ещё разбираемся с другими возможными причинами, но, честно говоря, пока они не поддаются логике. Некоторые из них могут быть ухудшениями в Qt (Marco Martin, Plasma 6.1.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=488326))

Значки на кнопках панели инструментов нового режима редактирования больше не размыты (Akseli Lahtinen, Plasma 6.1.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=488920))

Опция KWin «Размещение нового окна: под мышью» теперь действительно работает и игнорирует активный экран, если указатель мыши не на нём (Xaver Hugl, Plasma 6.1.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=488110))

Исправлено несколько недавних ухудшений и давних проблем виджетов Системного монитора на панелях (Arjen Hiemstra, Plasma 6.2.0):

* Подписи маленьких круговых диаграмм больше не переносятся так неуклюже на следующую строку ([ссылка](https://bugs.kde.org/show_bug.cgi?id=488863))
* Соседние круговые диаграммы больше не перекрываются при определённой толщине панели ([ссылка](https://bugs.kde.org/show_bug.cgi?id=489145))
* Графики теперь занимают достаточно места на толстых панелях ([ссылка](https://bugs.kde.org/show_bug.cgi?id=489307))

При включённой широкой цветовой гамме или использовании цветового профиля ICC прозрачные окна больше не *слишком* прозрачные (Xaver Hugl, Plasma 6.2.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=476868))

Отображение и скрытие заголовков и рамок окон слоя совместимости XWayland на экранах с масштабированием больше не приводит к тому, что окна каждый раз сдвигаются где-то на пиксель по диагонали (Влад Загородний, Plasma 6.2.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=489016))

Благодаря значительной переработке кода, исправлено множество недочётов плавающих панелей (Marco Martin, Plasma 6.2.0. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=480550), [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=485648) и [ссылка 3](https://bugs.kde.org/show_bug.cgi?id=483346))

Исправлено ​​недавнее ухудшение в Qt, из-за которого при отключении экранов иногда происходил сбой Plasma (David Edmundson, Qt 6.7.3. [Ссылка 1](https://codereview.qt-project.org/c/qt/qtwayland/+/574934) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=489180))

Исправлено ухудшение в Qt, из-за которого веб-страницы, отображаемые с помощью QtWebEngine (особенно в окне просмотра HTML-сообщений почтового клиента KMail), выглядели нечётко или пикселизованно (David Edmundson, Qt 6.8.0. [Ссылка](https://bugreports.qt.io/browse/QTBUG-113574))

Другие сведения об ошибках:

* Осталось 3 ошибки в Plasma с очень высоким приоритетом (на прошлой неделе было 4). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma — 31 (на прошлой неделе было 29). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 98 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-6-28&chfieldto=2024-7-05&chfieldvalue=RESOLVED&list_id=2768267&query_format=advanced&resolution=FIXED)

## Производительность и технические улучшения

Библиотеку `pam_kwallet` теперь можно собирать с `libgcrypt` версии 1.11, что во многих дистрибутивах восстанавливает возможность автоматической разблокировки бумажника KDE (хранилища паролей) при входе в сеанс (Daniel Exner, Plasma 6.1.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=489303))

## Как вы можете помочь

Если у вас несколько устройств или вы любите приключения, вы можете серьёзно помочь нам, устанавливая бета-версии Plasma и [сообщая нам об ошибках](https://kde.ru/bugs). Для этого хорошо подходят такие дистрибутивы, как Arch, Fedora и openSUSE Tumbleweed. Ну а если вы совсем рисковые, переезжайте на ночные сборки. Я вот пользуюсь ими на постоянной основе на своём единственном компьютере уже 5 лет, и всё на удивление стабильно.

Звучит слишком страшно? Тогда [рассмотрите возможность пожертвования](https://kde.org/ru/community/donations/)! Они тоже важны.

Ну или прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку действительно важного проекта. Для KDE крайне важен каждый участник; здесь вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2024/07/05/this-week-in-kde-autoscrolling/>  
