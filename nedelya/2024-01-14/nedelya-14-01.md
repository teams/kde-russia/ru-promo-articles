# На этих неделях в KDE: идёт, грядёт, близится…

Ух, как летит время! Несмотря на праздники, участники KDE немало успели: запланированная дата мегавыпуска, 28 февраля, уже близко, и мы вовсю заняты устранением недочётов.

В целом, мы справляемся. Несмотря на кажущееся большим число открытых отчётов об ошибках, большинство из них несущественны, и я уверен, что оставшиеся серьёзные проблемы до итогового выпуска мы решим. Но, конечно, лучший способ это обеспечить — это помочь нам!

## Мегавыпуск KDE 6

> (Сюда входит всё ПО, которое будет выпущено 28 февраля: Plasma 6, библиотеки Frameworks 6 и приложения из состава Gear 24.02)

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 238](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

### Улучшения пользовательского интерфейса

Обои рабочего стола теперь можно применить ко всем экранам одновременно (Prajna Sariputra, [ссылка](https://bugs.kde.org/show_bug.cgi?id=436001)):

![0](https://invent.kde.org/plasma/plasma-workspace/uploads/5fffebe97a3d07756a47d45d020d1bb7/image.png)

Теперь можно настроить скорость указателя мыши, как это уже можно было делать для сенсорной панели (Денис Жданов, [ссылка](https://bugs.kde.org/show_bug.cgi?id=431751))

Индикатор центра приложений Discover больше не отображается в системном лотке (даже в расширенном списке), если ему не о чем вас уведомить. Это означает, что вы можете полностью его скрыть, просто отключив автоматическую проверку обновлений на странице «Обновление программ» Параметров системы (Yifan Zhu, [ссылка](https://bugs.kde.org/show_bug.cgi?id=413053))

В программном обеспечении KDE был улучшен алгоритм поиска символьных значков (то есть тех, у которых названия заканчиваются на `-symbolic`): если конкретный символьный значок в текущем наборе не найден и происходит откат к более общему значку, теперь и для него используется символьная версия, если она существует (Joshua Goins, [ссылка](https://invent.kde.org/frameworks/kiconthemes/-/merge_requests/118))

Теперь можно настроить Plasma на выключение экрана в тот же момент, когда он блокируется (Jakob Petsovits, [ссылка 1](https://bugs.kde.org/show_bug.cgi?id=477641) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=476962))

### Исправления ошибок

> **Важное примечание:** я не упоминаю здесь исправления ошибок, не успевших дойти до пользователей; посреди большого цикла разработки Plasma их слишком много, чтобы про них писать (а вам — читать), да и вообще мало кто их видел. Тем не менее огромное спасибо всем, кто плотно занимается их исправлением!

Архиватор Ark теперь сохраняет расширенные атрибуты архивов при их редактировании (Kristen McWilliam, [ссылка](https://bugs.kde.org/show_bug.cgi?id=435001))

Устранена ​​утечка файловых дескрипторов в Plasma, которая могла приводить к их исчерпанию и сбою процесса оболочки (Moody Liu, [ссылка](https://bugs.kde.org/show_bug.cgi?id=478831))

Решена ​​проблема, которая могла препятствовать аутентификации в VPN OpenConnect через GlobalProtect SAML (Rahul Rameshbabu, [ссылка](https://bugs.kde.org/show_bug.cgi?id=444500))

Ночная цветовая схема и цветокоррекция с помощью профилей ICC теперь могут сосуществовать — по крайней мере, в сеансе Wayland (Xaver Hugl, [ссылка](https://bugs.kde.org/show_bug.cgi?id=413134))

Виджет погоды больше не показывает иногда неадекватно высокие значения температуры при использовании немецкой службы погоды в качестве источника (Ismael Asensio, [ссылка](https://bugs.kde.org/show_bug.cgi?id=462548))

Вкладки виджета погоды в некоторых случаях больше не накладываются друг на друга (Ismael Asensio, [ссылка](https://bugs.kde.org/show_bug.cgi?id=472171))

Степень прозрачности всплывающего окна «Варианты виджета» теперь соответствует родительской панели Plasma (Niccolò Venerandi, [ссылка](https://bugs.kde.org/show_bug.cgi?id=440275))

Предпросмотр выбранной визуализации переключения окон (Alt+Tab) теперь, как и ожидалось, закрывается щелчком вне него в сеансе Wayland (Ismael Asensio, [ссылка](https://bugs.kde.org/show_bug.cgi?id=374971))

Другие сведения об ошибках:

* Остаётся 4 ошибки в Plasma с очень высоким приоритетом (три недели назад было 3). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma остаётся 37 (три недели назад было 35). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 262 ошибки были исправлены за прошедшие три недели во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-12-22&chfieldto=2024-1-12&chfieldvalue=RESOLVED&list_id=2577479&query_format=advanced&resolution=FIXED)

### Производительность и технические улучшения

Снижена фоновая нагрузка на процессор при перемещении указателя мыши (Xaver Hugl, [ссылка](https://bugs.kde.org/show_bug.cgi?id=479126))

Различные свойства текущего состояния приложений на основе QtWidgets теперь хранятся в отдельном файле описания состояния, а не в конфигурационном файле, предназначенном для пользовательских настроек (Alexander Lohnau, [ссылка](https://invent.kde.org/frameworks/kxmlgui/-/merge_requests/211/diffs))

## Как вы можете помочь

Спасибо всем, кто поучаствовал в [сборе средств, приуроченном к выпуску Plasma 6](https://kde.org/fundraisers/plasma6member/) — он достиг цели! Я думал, что 500 новых спонсоров — это слишком оптимистичная цель для фонда KDE e.V., но вы показали мне, что я, к счастью, был неправ. Сейчас их уже **627**, и это число продолжает расти. Если вам нравится наша работа, пожертвование — это отличный способ нас отблагодарить. 🙂

Разработчики, пожалуйста, помогите исправить проблемы в Qt 6, KDE Frameworks 6 и Plasma 6! Plasma 6 уже вполне годится для повседневного использования, хоть и всё ещё требует работы, чтобы стать готовой к выпуску в феврале.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2024/01/12/the-last-few-weeks-in-kde-its-coming-its-coming-its-coming/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
