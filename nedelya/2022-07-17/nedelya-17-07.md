# На этой неделе в KDE: приятные доработки

> 10–17 июля, основное — прим. переводчика

## Исправленные «15-минутные ошибки»

Текущее число ошибок: **52, вместо бывших 53**. 0 новых и 1 исправлена:

Plasma [больше не завершает работу аварийно у многих пользователей при входе в систему или выходе из неё](https://bugs.kde.org/show_bug.cgi?id=443706) (David Edmundson, Frameworks 5.97)

[Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)

## Новые возможности

[Диспетчер файлов Dolphin](https://invent.kde.org/system/dolphin/-/merge_requests/385), [просмотрщик изображений Gwenview](https://invent.kde.org/graphics/gwenview/-/merge_requests/143) и [утилита для создания снимков экрана Spectacle](https://invent.kde.org/graphics/spectacle/-/merge_requests/134) теперь используют интерфейс порталов XDG для перетаскиваемых файлов, что позволяет им успешно передавать файлы в изолированные приложения, не пробивая в песочнице дыру вроде предоставления приложениям доступа к вашей домашней папке или временной папке системы (Harald Sitter, версия 22.08 перечисленных приложений)

Теперь можно [установить размер бумаги по умолчанию для печати](https://bugs.kde.org/show_bug.cgi?id=420928) (Akseli Lahtinen, Plasma 5.26):

![0](https://pointieststick.files.wordpress.com/2022/07/paper-size.png)

Страница «О системе» [теперь поддерживает отображение данных о более широком спектре оборудования и прошивок, включая Apple M1](https://invent.kde.org/plasma/kinfocenter/-/merge_requests/104) (James Calligeros, Plasma 5.26):

![1](https://pointieststick.files.wordpress.com/2022/07/m1-in-about.png)

## Wayland

В сеансе Plasma Wayland устранена [проблема, из-за которой внешний монитор, подключённый по USB-C, при выключении и повторном включении переставал отображать сигнал до перезагрузки компьютера](https://bugs.kde.org/show_bug.cgi?id=456369). Это [также исправляет полное зависание сеанса при включении экрана телевизора, подключённого к вашему компьютеру, когда подсоединена гарнитура виртуальной реальности](https://bugs.kde.org/show_bug.cgi?id=456298) (Xaver Hugl, Plasma 5.25.3)

В сеансе Wayland устранена [проблема, из-за которой система могла не проснуться у пользователей с видеокартами NVIDIA](https://bugs.kde.org/show_bug.cgi?id=455814) (Xaver Hugl, Plasma 5.25.3)

В сеансе Wayland [курсор больше не становится иногда невидимым при использовании некоторых очень сломанных устаревших графических драйверов](https://bugs.kde.org/show_bug.cgi?id=453860) (Xaver Hugl, Plasma 5.25.4)

В сеансе Wayland [оформления окон с видимыми границами больше не обрезаются с правой стороны при использовании глобального коэффициента масштабирования более 100%](https://bugs.kde.org/show_bug.cgi?id=453745) (David Edmundson, Plasma 5.26)

В сеансе Wayland [включение внешнего монитора больше не приводит к немедленному сбою приложений, которые в это время отображают уведомление с ходом выполнения задания](https://bugs.kde.org/show_bug.cgi?id=450325) (Michael Pyne, Frameworks 5.97)

## Улучшения пользовательского интерфейса

Опция Dolphin «Показывать строку состояния» [теперь дополнительно доступна в меню «Настройка»](https://invent.kde.org/system/dolphin/-/merge_requests/418), где подобные параметры для конкретных представлений обычно можно найти в приложениях KDE на основе QtWidgets (Kai Uwe Broulik, Dolphin 22.08):

![2](https://pointieststick.files.wordpress.com/2022/07/show-statusbar-in-menu.png)

Многие виджеты Plasma [получили улучшенные характеристики доступности](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests?scope=all&state=merged&label_name[]=Accessibility&author_username=fusionfuture) после их использования со средством чтения с экрана (Fushan Wen, Plasma 5.25.4 и 5.26)

[Теперь вы можете найти Системный монитор по различным связанным поисковым запросам, таким как «диспетчер», «задача», «процессор» и «память»](https://invent.kde.org/plasma/plasma-systemmonitor/-/merge_requests/186) (Tom Knuf, Plasma 5.26)

Диалог выбора обоев [теперь пытается извлекать из изображений и отображать доступные метаданные](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1852) (Fushan Wen, Plasma 5.26):

![3](https://pointieststick.files.wordpress.com/2022/07/metadata.png)

Виджет и комбинация клавиш «Показать рабочий стол» [были переименованы во «Взглянуть на рабочий стол»](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1036), чтобы было понятнее, что они на самом деле делают, и чтобы уменьшить путаницу с альтернативным действием «Свернуть все окна» (Nate Graham, Plasma 5.26):

![4](https://pointieststick.files.wordpress.com/2022/07/peek_at_desktop.png)

## Исправления ошибок и улучшения производительности

Значок виджета «Словарь» [больше не выглядит сломанным](https://bugs.kde.org/show_bug.cgi?id=399568) (Иван Ткаченко, Plasma 5.24.6)

Переключение между виджетами меню запуска приложений (например, современным и классическим) [больше не приводит к повторному заполнению списка избранных приложений набором по умолчанию, если вы удалили какие-либо из них](https://bugs.kde.org/show_bug.cgi?id=456411) (Fushan Wen, Plasma 5.24.6)

Виджет «Переключение рабочих столов» [теперь всегда переключается на рабочий стол, на который вы наводите курсор при перетаскивании на него чего-либо](https://bugs.kde.org/show_bug.cgi?id=416878). Его [визуализация окон стала плавнее](https://bugs.kde.org/show_bug.cgi?id=456488), а [среди его настроек больше нет групп переключателей, где ни один из вариантов не выбран](https://bugs.kde.org/show_bug.cgi?id=456525) (Иван Ткаченко, Plasma 5.24.6)

Plasma [больше не падает при удалении панели, на которой есть разделители](https://bugs.kde.org/show_bug.cgi?id=450663) (Aleix Pol Gonzalez, Plasma 5.25.3)

Параметры системы [больше не вылетают иногда при смене набора курсоров](https://bugs.kde.org/show_bug.cgi?id=456526) (David Edmundson, Plasma 5.25.3)

Щелчок средней кнопкой мыши по значкам приложений в системном лотке [снова работает](https://bugs.kde.org/show_bug.cgi?id=456466) (Chris Holland, Plasma 5.25.3)

Plasma [больше не падает иногда при перетаскивании чего-либо из Firefox на рабочий стол](https://bugs.kde.org/show_bug.cgi?id=454747) (David Edmundson, Frameworks 5.97)

Исправлена [распространённая причина зависаний в приложениях, использующих библиотеку Kirigami, с прокручиваемыми страницами](https://bugs.kde.org/show_bug.cgi?id=448784) (Marco Martin, Frameworks 5.97)

Файлы изображений RAW формата `.rw2` [снова отображают миниатюры](https://bugs.kde.org/show_bug.cgi?id=453480) (Alexander Lohnau, Frameworks 5.97)

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2022/07/15/this-week-in-kde-some-nice-improvements/>  

<!-- 💡: Kicker → классическое меню запуска приложений -->
<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: thumbnail → миниатюра -->
