# На этой неделе в KDE: ввод диакритических и альтернативных символов

В Plasma на этой неделе была добавлена большая новая функция, и я очень рад представить её!

Теперь, когда вы удерживаете какую-либо клавишу, [появляется небольшая палитра, предлагающая альтернативные символы, похожие на тот, что вы удерживаете](https://invent.kde.org/plasma/plasma-integration/-/merge_requests/2). Она делает невероятно простым ввод диакритических знаков из других языков и редких символов вашего языка, без необходимости переключать раскладку или запоминать последовательность клавиш Compose. Это очень интуитивно и приятно в использовании:

<https://i.imgur.com/7Lw2geR.mp4?_=1>

Вы также можете применять эту возможность для ввода альтернативных символов, таких как символы валют, редких в вашем регионе. Это полезно, если, например, вы живёте в США, но много работаете с денежными суммами, выраженными в евро, или наоборот:

<https://i.imgur.com/NF4WgDc.mp4?_=2>

Естественно, эта функция может быть выключена (на странице настройки клавиатуры Параметров системы), если вы предпочитаете текущее поведение с повтором удерживаемой клавиши.

Реализация в большей степени закончена и добавлена, но остались некоторые моменты, такие как полная поддержка GTK-приложений и совместимость с другими методами ввода. Пока авантюристы, использующие пакеты из ветки git master, могут опробовать её, установив переменную окружения `QT_IM_MODULE=plasmaim`.

Эта работа была проделана Carson Black и будет по умолчанию включена с Plasma 5.21. Спасибо, Carson!

## Другие новые возможности

Просмотрщик изображений Gwenview [теперь позволяет вам вводить произвольные значения масштаба, используя интерактивный двунаправленный счётчик, заменивший статическую надпись](https://bugs.kde.org/show_bug.cgi?id=157274) (Antonio Prcela, Gwenview 21.04):

![2](https://pointieststick.files.wordpress.com/2020/12/screenshot_20201201_130857.jpeg)

Особые параметры окон (правила KWin) [теперь можно клонировать](https://bugs.kde.org/show_bug.cgi?id=429588) (Ismael Asensio, Plasma 5.21)

Стандартный диалог перезаписи файлов [получил опцию автоматической перезаписи более старых файлов](https://bugs.kde.org/show_bug.cgi?id=236884) (Méven Car, Frameworks 5.77)

## Исправления ошибок и улучшения производительности

Эмулятор терминала Konsole [снова может правильно отображать жирный красный текст](https://bugs.kde.org/show_bug.cgi?id=405345) (Carlos Alves, Konsole 20.12)

Поддержка просмотра документов FB2 в Okular [теперь включает возможность показа таблиц](https://invent.kde.org/graphics/okular/-/merge_requests/325) (Марат Радченко, Okular 21.04)

Страница управления пользователями Параметров системы [теперь отображает правильное содержимое при первом открытии](https://bugs.kde.org/show_bug.cgi?id=429314) (David Edmundson, Plasma 5.20.5)

Исправлены [многие визуальные глюки кнопок в области заголовка окон приложений на основе GTK 3](https://invent.kde.org/plasma/kde-gtk-config/-/merge_requests/17) (Михаил Золотухин, Plasma 5.20.5)

В сеансе Plasma Wayland [всплывающие подсказки панели задач теперь отображают миниатюры окон приложений на основе Electron и Java](https://bugs.kde.org/show_bug.cgi?id=427245) (Aleix Pol Gonzalez, Plasma 5.21)

Улучшена [производительность центра приложений Discover в сеансе Plasma Wayland](https://bugs.kde.org/show_bug.cgi?id=418082) (Aleix Pol Gonzalez, Plasma 5.21)

Параметры системы [больше не падают при нажатии на кнопку «Загрузить новые сценарии» на странице сценариев KWin](https://bugs.kde.org/show_bug.cgi?id=416328) (Alexander Lohnau, Frameworks 5.77)

Удаление элементов, установленных через диалоги «Загрузить \[что-то\]», [стало надёжнее](https://invent.kde.org/frameworks/knewstuff/-/merge_requests/78) (Aleix Pol Gonzalez, Frameworks 5.77)

Переход к другим папкам через адресную строку с помощью клавиатуры [снова работает](https://bugs.kde.org/show_bug.cgi?id=428226) (Ahmad Samir, Frameworks 5.77)

Полосы прокрутки Plasma [теперь правильного цвета при использовании светлой темы Plasma с тёмной темой приложений](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/159) (Mikel Johnson, Frameworks 5.77)

Общие папки Samba [снова отображают правильный значок при просмотре во вкладке диспетчера файлов Dolphin](https://bugs.kde.org/show_bug.cgi?id=429530) (Méven Car, Plasma 5.77)

## Улучшения пользовательского интерфейса

Кнопка «Показать список воспроизведения» в музыкальном проигрывателе Elisa [больше не отображается в режиме вечеринки](https://pointieststick.files.wordpress.com/2020/11/shared-folders.jpeg?w=1024) (он же развёрнутый вид), так как в этом режиме то, чем она управляет, всё равно скрыто (Nate Graham, Elisa 20.12)

Dolphin теперь отображает текст-заглушку для [пустых представлений](https://bugs.kde.org/show_bug.cgi?id=429248) и [результатов поиска](https://bugs.kde.org/show_bug.cgi?id=429248), похожий на аналогичный текст в разных виджетах Plasma, на страницах Параметров системы и в различных приложениях на основе QML (Nate Graham, Dolphin 21.04):

![3](https://pointieststick.files.wordpress.com/2020/11/search-results.jpeg)

![4](https://pointieststick.files.wordpress.com/2020/11/empty-folder.jpeg)

![5](https://pointieststick.files.wordpress.com/2020/11/filter.jpeg)

![6](https://pointieststick.files.wordpress.com/2020/11/empty-trash.jpeg)

![7](https://pointieststick.files.wordpress.com/2020/11/shared-folders.jpeg)

Эффект выделения в Dolphin [теперь плавно затухает после отпускания, как при просмотре папки в Plasma](https://invent.kde.org/system/dolphin/-/merge_requests/83) (Carson Black, Dolphin 21.04)

Discover [теперь предлагает вам установить поддержку пакетов Snap при попытке открыть URL-адрес Snap-приложения, если такая поддержка сейчас не установлена](https://bugs.kde.org/show_bug.cgi?id=406656) (Aleix Pol Gonzalez, Plasma 5.21)

Область обложки альбома виджета проигрывателя теперь [растягивается, чтобы заполнить всю горизонтальную площадь, без отступов по бокам](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/497) (Mikel Johnson и Marco Martin, Plasma 5.21):

![9](https://pointieststick.files.wordpress.com/2020/12/screenshot_20201203_100228.jpeg)

*Чёрт, это красиво*

У эффекта выделения [теперь скруглённые углы для лучшего соответствия стилю Breeze](https://invent.kde.org/plasma/breeze/-/merge_requests/42) (Carson Black, Plasma 5.21)

Панель точек входа в файловых диалогах [теперь соответствует новому поведению Dolphin по выделению элемента этой панели только когда именно выбранное расположение (не подпапка) отображается в главном представлении](https://bugs.kde.org/show_bug.cgi?id=429806) (Méven Car, Frameworks 5.77)

При попытке размонтирования подключённой общей папки NFS, содержащей какие-либо из открытых файлов, компонент интерфейса, который вы пытаетесь использовать для отсоединения, [теперь сообщает вам об этой проблеме, как и в случае физических дисков](https://bugs.kde.org/show_bug.cgi?id=411772) (Méven Car, Frameworks 5.77)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник, и совершенно не обязательно быть программистом — занятие по душе найдётся для каждого.

И наконец, если вы находите программное обеспечение KDE полезным, окажите финансовую поддержку [фонду KDE e.V.](https://ev.kde.org/)

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [boingo-00](https://t.me/boingo00)  
*Источник:* <https://pointieststick.com/2020/12/04/this-week-in-kde-big-new-accented-alternative-character-input-feature/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: get new [thing] → «Загрузить \[что-то\]» -->
<!-- 💡: headerbar → панель заголовка -->
<!-- 💡: icon → значок -->
<!-- 💡: location → расположение (в файловой системе) / местоположение (на карте) -->
<!-- 💡: playlist → список воспроизведения -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: thumbnail → миниатюра -->
