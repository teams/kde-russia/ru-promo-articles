# На этой неделе в KDE: доработка цифровой подписи в Okular

У нас есть интересные новости о Plasma 6, но я приберегу их для другой заметки через пару дней, чтобы они не отвлекали вас от остальной замечательной работы, проделанной участниками KDE на этой неделе!

## Новые возможности

Процесс добавления электронной подписи в просмотрщике документов Okular был улучшен и теперь позволяет указывать в подписи дополнительные сведения, такие как причину и адрес, а также при необходимости добавлять фоновое изображение (Sune Vuorela, Okular 23.08. [Ссылка](https://invent.kde.org/graphics/okular/-/merge_requests/745)):

![0](https://pointieststick.files.wordpress.com/2023/06/image-14.png)

## Улучшения пользовательского интерфейса

Теперь можно дважды щёлкнуть по вкладке диспетчера файлов Dolphin, чтобы дублировать её (Méven Car, Dolphin 23.08. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=470388))

Когда музыкальный проигрыватель Elisa переключает обложку альбома с одного изображения на другое, смена теперь происходит плавнее и красивее, без видимых вспышек или мерцаний (Fushan Wen, Elisa 23.08. [Ссылка](https://invent.kde.org/multimedia/elisa/-/merge_requests/466))

В Okular действие «Добавить цифровую подпись» теперь расположено на верхнем уровне меню-гамбургера для более быстрого доступа (Nate Graham, Okular 23.08. [Ссылка](https://invent.kde.org/graphics/okular/-/merge_requests/758)):

![1](https://pointieststick.files.wordpress.com/2023/06/image-11.png)

Все диалоговые окна в Системном мониторе были переведены на более новый компонент из библиотеки Kirigami, который лучше выглядит и решает несколько проблем с расположением элементов (Nate Graham, Plasma 6.0. [Ссылка 1](https://invent.kde.org/plasma/plasma-systemmonitor/-/merge_requests/213) и [ссылка 2](https://invent.kde.org/plasma/plasma-systemmonitor/-/merge_requests/215)):

![2](https://pointieststick.files.wordpress.com/2023/06/image-12.png)

Улучшена отзывчивость стандартного меню запуска приложений при наведении указателя мыши на элементы списка категорий (Derek Christ, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=462271))

Папки, добавленные на страницу «Поиск файлов» Параметров системы, теперь сопровождаются своими настоящими значками (Oliver Beard, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1568))

В диалоговом окне сохранения файла после уведомления о конфликте имён фокус клавиатуры теперь сразу возвращается в поле ввода имени, чтобы вы могли изменить его (Oliver Beard, Frameworks 5.107. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=444515))

## Другие заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

В сеансе Plasma Wayland устранена причина сбоев фоновой службы управления питанием (Powerdevil) во время сна экранов (Aleix Pol Gonzalez, Plasma 5.27.6. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=470484))

Устранена причина чрезмерной нагрузки Plasma на процессор при перемещении окон (Fushan Wen, Plasma 5.27.6. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1567))

В сеансе Wayland исправлены некоторые ошибки в логике курсоров, из-за которых они могли выглядеть неправильно в приложениях, работающих через слой совместимости XWayland (Severin von Wnuck, Plasma 5.27.6. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=459468) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=442839))

Страница «Вход в систему (SDDM)» Параметров системы снова отображает сообщения об ошибках (Nate Graham, Plasma 5.27.6. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=471040))

Проверка обновлений среды разработки PyCharm больше не приводит к аварийному завершению диспетчера окон KWin (Влад Загородний, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=470874))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 3 ошибки в Plasma с очень высоким приоритетом (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma остаётся 59 (на прошлой неделе было 55). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 99 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-06-09&chfieldto=2023-06-16&chfieldvalue=RESOLVED&list_id=2394963&query_format=advanced&resolution=FIXED)

## Как вы можете помочь

Если вы разработчик, пожалуйста, **перейдите на сеанс Plasma 6 на постоянной основе** и начните исправлять ошибки, которые вы там находите. Хватит нам уже сидеть на Plasma 5! Шестёрка вполне годится для повседневного использования энтузиастами (я в их числе) и требует много работы по устранению недочётов. Список известных проблем можно найти [тут](https://community.kde.org/Plasma/Plasma_6).

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/06/16/this-week-in-kde-nicer-digital-signing-in-okular/>  

<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Monitor → Системный монитор -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icons → значки -->
<!-- 💡: location → расположение (в файловой системе) / местоположение (на карте) -->
