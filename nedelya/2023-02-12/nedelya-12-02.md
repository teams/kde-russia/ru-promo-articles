# На этой неделе в KDE: рекордная стабильность

Plasma 5.27 LTS выйдет уже через пару дней, и пока это самая безошибочная версия на моей памяти! На момент написания [известно лишь о трёх регрессиях](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&keywords=regression%2C&keywords_type=allwords&list_id=2271708&query_format=advanced&version=5.26.90), а не дюжине с лишним, как обычно бывает. Сосредоточенная работа над стабильностью окупается!

К слову, вы, возможно, слышали о масштабной переработке поддержки нескольких мониторов в новом выпуске. Похоже, это сработало: многие пользователи, попробовавшие бета-версию, сообщают, что их давние проблемы наконец исправлены! Но я не сомневаюсь, что какие-то недочёты ещё остаются. Если вы столкнётесь с неполадками, дайте нам знать: [здесь](https://notmart.org/blog/2023/02/how-to-report-multiscreen-bugs/) подробно описано, какая информация нам потребуется в отчёте об ошибке.

Это ещё не всё! Мы добавили несколько приятных новых функций в Plasma 6 и исправили немало 15-минутных ошибок.

## Новые возможности

Диспетчер файлов Dolphin теперь может отображать на панели сведений число страниц в выбранном документе (Сергей Подтынный, Dolphin 23.04. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=374561))

Строка поиска и запуска KRunner теперь может конвертировать время между часовыми поясами (Natalie Clarius, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/264)):

![0](https://pointieststick.files.wordpress.com/2023/02/in-utc.jpg)

![1](https://pointieststick.files.wordpress.com/2023/02/in-malaysia.jpg)

## Улучшения пользовательского интерфейса

При удалении файла в Dolphin выделение теперь автоматически переходит на следующий (Сергей Подтынный, Dolphin 23.04. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=419914))

Когда вы просите музыкальный проигрыватель Elisa открыть файл списка воспроизведения, содержащий несуществующие пути, он теперь пропускает их и показывает вам сообщение об этом с возможностью открыть файл для редактирования (Никита Карпей, Elisa 23.04. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=429736)):

![2](https://pointieststick.files.wordpress.com/2023/02/failed-to-load-some-tracks-in-elisa.jpg)

Модуль Flatpak центра приложений Discover теперь должен работать значительно быстрее при наличии библиотеки AppStream версии 0.16 (Aleix Pol Gonzalez, Plasma 5.27.1. [Ссылка](https://invent.kde.org/plasma/discover/-/merge_requests/458))

На странице «Оформление окон» Параметров системы теперь используется более современный безрамочный стиль, а не представление с вкладками (Joshua Goins, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/3524)):

![3](https://pointieststick.files.wordpress.com/2023/02/window-decorations-kcm.jpg)

KRunner теперь находит файлы, которые раньше пропускал из-за несоответствия распространённым категориям (Natalie Clarius, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=457522))

Во многих программах KDE при попытке запустить консольную программу, которая не существует или не может быть найдена, теперь будет показано сообщение об этом (Thenujan Sandramohan, Frameworks 5.103. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=329094))

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Dolphin теперь должен значительно реже подвисать при просмотре смонтированных вручную сетевых ресурсов. Впрочем, ещё многое предстоит сделать, и вам всё равно стоит по возможности обращаться к сетевым расположениям по их URL-адресам (Andrew Gunnerson, Dolphin 23.04. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=465357))

В сеансе Plasma Wayland существующее окно просмотрщика документов Okular теперь, как и ожидалось, будет подниматься на передний план при открытии ещё одного документа из другого приложения (Nicolas Fella, Okular 23.04. [Ссылка](https://invent.kde.org/graphics/okular/-/merge_requests/662))

Установка обновлений системы в некоторых дистрибутивах больше не приводит к сбросу настроек сенсорной панели (David Edmundson, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=414559))

В системном лотке больше не отсутствуют иногда значки автозапускаемых приложений (David Redondo, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=425315))

При наведении курсора на плитку приложения в панели задач для показа миниатюр его окон, последующее перемещение курсора по диагонали к миниатюре над плиткой другого приложения больше не приводит к преждевременному скрытию миниатюр. Заодно устранена связанная проблема, из-за которой нельзя было достать до миниатюры окна, расположенного в нижней строке многострочной панели задач! (Bharadwaj Raju, Plasma 5.27. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=358930) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=347041)):

![4](4_diagonal.png)

В сеансе Wayland при использовании видеокарты, не поддерживающей атомарную настройку режима (modesetting), курсор больше не исчезает при касании нижнего или правого края экрана в играх, запущенных через Wine (Xaver Hugl, Plasma 5.27.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=461181))

Dolphin больше не будет зависать при попытке отобразить метаданные или миниатюры файлов Mobipocket (`.mobi`) (Méven Car, Frameworks 5.104. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=465006))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Осталось 8 ошибок в Plasma с очень высоким приоритетом (на прошлой неделе было 9). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 37 (на прошлой неделе было 42). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 109 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-02-03&chfieldto=2023-02-10&chfieldvalue=RESOLVED&list_id=2271712&query_format=advanced&resolution=FIXED)

## Автоматизация и систематизация

Добавлен новый автоматический тест для проверки целостности модели данных панели задач (Fushan Wen, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2514))

Добавлен новый тест, проверяющий поведение экрана завершения сеанса Plasma (Marco Martin, Plasma 5.27.1. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2505))

Добавлена инструкция по [разработке дополнительных модулей для текстового редактора Kate](https://develop.kde.org/docs/apps/kate/plugin/) (Waqar Ahmed, [Ссылка](https://invent.kde.org/documentation/develop-kde-org/-/merge_requests/253))

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

<!-- Временная ссылка! -->

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/02/10/this-week-in-kde-the-best-plasma-5-version-ever-2/>  

<!-- 💡: GPU → видеокарта / графический ускоритель -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: locations → расположения (в файловой системе) / местоположения (на карте) -->
<!-- 💡: playlist → список воспроизведения -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
