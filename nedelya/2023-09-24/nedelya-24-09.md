# На этой неделе в KDE: разморозка панели под Wayland с NVIDIA

Хотя общее число отслеживаемых проблем в Plasma 6 на этой неделе подросло, нам удалось исправить некоторые заметные давние проблемы из Plasma 5! Некоторые из них вам, вероятно, знакомы. Мы решили, что работа над ними более приоритетная, чем над сравнительно незначительными новыми. Мы продолжим бороться с такими недочётами, но нам нужна ваша помощь! Искать ошибки полезно, но не менее полезно их исправлять, и я прошу вас в этом поучаствовать.

## Plasma 6

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 90](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

Исправлена печально известная проблема с застыванием панелей в сеансе Plasma Wayland при использовании видеокарт, отличных от Intel, в сочетании с однопоточным циклом отрисовки QtQuick и включёнными миниатюрами окон в панели задач (David Edmundson и другие, [ссылка](https://bugs.kde.org/show_bug.cgi?id=469016))

При использовании однострочной панели задач «Только значки» последний элемент больше не пропадает иногда при определённых обстоятельствах (Marco Martin, [ссылка](https://bugs.kde.org/show_bug.cgi?id=396616))

Исправлена ошибка, из-за которой автоматически запускаемые приложения иногда не могли показать значок в системном лотке, пока пользователь не перезапускал их вручную (David Edmundson, [ссылка](https://bugs.kde.org/show_bug.cgi?id=425315))

Исправлены различные глюки и поломки курсора мыши при использовании повёрнутых экранов в сеансе Wayland с видеокартой, поддерживающей аппаратные курсоры (Xaver Hugl, [ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/4420))

Повторно отображаемые сообщения на экране блокировки теперь просто немного подпрыгивают, вместо того чтобы дублироваться (Nate Graham, [ссылка](https://bugs.kde.org/show_bug.cgi?id=470142)):

<https://i.imgur.com/DhyKaa4.mp4?_=1>

Снижено потребление ресурсов приложениями на основе QtQuick, применяющими в интерфейсе клавиши быстрого доступа — это такие маленькие подчёркивания под буквами, которые видны, когда вы удерживаете клавишу Alt (Kai Uwe Broulik, [ссылка 1](https://invent.kde.org/frameworks/kirigami/-/merge_requests/1255) и [ссылка 2](https://invent.kde.org/frameworks/kirigami/-/merge_requests/1256))

Пункт контекстного меню панели Plasma «Режим редактирования» теперь меняется на «Выйти из режима редактирования», когда вы уже находитесь в этом режиме (Nate Graham, [ссылка](https://bugs.kde.org/show_bug.cgi?id=449004))

## Другие заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Просмотрщик изображений Gwenview теперь правильнее выводит изображения при использовании дробного масштабирования экрана в сеансе Wayland (Kai Uwe Broulik, Gwenview 24.02. [Ссылка](https://invent.kde.org/graphics/gwenview/-/merge_requests/227))

В музыкальном проигрывателе Elisa исправлены ошибки, которые могли приводить к странному неправильному поведению при перестановке элементов списка воспроизведения вокруг проигрываемой в данный момент песни (Jack Hill, Elisa 24.02. [Ссылка](https://invent.kde.org/multimedia/elisa/-/merge_requests/488))

Анализатор дисков Filelight снова учитывает настройки исключения папок и границ файловой системы (Yifan Zhu, Filelight 23.08.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=473951))

В текстовых редакторах Kate и KWrite при закрытии несохранённого документа диалог подтверждения больше не появляется дважды (Kai Uwe Broulik, Kate и KWrite 23.08.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=469690))

Виджеты, использующие стандартный поставщик календаря Plasma, больше не будут иногда отображать праздники из региона по умолчанию вместо выбранного (Евгений Попов, Plasma 5.27.9. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=472483))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Осталось 4 ошибки в Plasma с очень высоким приоритетом (на прошлой неделе было 5). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma остаётся 59 (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 121 ошибка была исправлена на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-09-15&chfieldto=2023-09-22&chfieldvalue=RESOLVED&list_id=2476788&query_format=advanced&resolution=FIXED)

## Как вы можете помочь

Разработчики, пожалуйста, помогите исправить проблемы в [Qt 6, KDE Frameworks 6 и Plasma 6](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&f1=keywords&f2=product&list_id=2398791&o1=allwordssubstr&o2=notequals&query_format=advanced&v1=qt6&v2=neon)! Plasma 6 вполне годится для повседневного использования энтузиастами (я в их числе), хоть и всё ещё требует работы, чтобы стать готовой к выпуску до конца года.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/09/22/this-week-in-kde-an-unfrozen-panel-for-nvidia-wayland-users/>  

<!-- 💡: GPU → видеокарта / графический ускоритель -->
<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icons → значки -->
<!-- 💡: playlist → список воспроизведения -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
