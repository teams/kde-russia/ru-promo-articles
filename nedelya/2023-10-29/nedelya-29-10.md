# На этих неделях в KDE: управление цветом в Wayland и возвращение куба

У нас полно новостей! Приглашаю всех прочитать отчёт за последние две недели и найти для себя что-то, чего вы давно ждали. 🙂

## Plasma 6

> (Сюда входит всё ПО, которое будет выпущено 28 февраля: Plasma, приложения из состава Gear и библиотеки Frameworks)

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 109](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

В сеансе Plasma Wayland появилась поддержка поэкранного управления цветом! Теперь каждому экрану можно присвоить цветовой профиль ICC, и приложения будут работать правильно. Цвета, взятые с помощью виджета «Выбор цвета», теперь тоже соответствующим образом пересчитываются (Xaver Hugl, [ссылка 1](https://invent.kde.org/plasma/kwin/-/merge_requests/4471), [ссылка 2](https://invent.kde.org/libraries/plasma-wayland-protocols/-/merge_requests/63), [ссылка 3](https://invent.kde.org/plasma/libkscreen/-/merge_requests/164), [ссылка 4](https://invent.kde.org/plasma/kwin/-/merge_requests/4555), [ссылка 5](https://invent.kde.org/plasma/kwin/-/merge_requests/4559) и [ссылка 6](https://bugs.kde.org/show_bug.cgi?id=387757))

Эффект «Куб рабочих столов» возвращается! Теперь он входит в пакет `kdeplasma-addons` и вызывается комбинацией клавиш Meta+C (Влад Загородний, [ссылка](https://bugs.kde.org/show_bug.cgi?id=438883)):

![0](https://pointieststick.files.wordpress.com/2023/10/image-4.png)

При использовании утилиты для создания снимков экрана Spectacle в Plasma 6 теперь при желании можно отключать тени окон в режимах «Активное окно» и «Окно под курсором мыши» (Kristen McWilliam, [ссылка](https://bugs.kde.org/show_bug.cgi?id=372408)):

![1](https://pointieststick.files.wordpress.com/2023/10/image-6.png)

В центр приложений Discover внесено множество мелких улучшений интерфейса: цвет фона под карточками стал приятнее; карточки на страницах поиска и каталога теперь лучше выровнены; улучшено поведение боковой панели во время поиска; снимки экрана теперь отображаются и для продуктов, в описании которых не указаны миниатюры; приложения Flatpak с низкоуровневым доступом к PipeWire теперь помечаются как имеющие доступ к звуковой системе; а указания веса приложений Flatpak теперь короче и читаемее (Marco Martin, Arjen Hiemstra, Jonah Brüchert и Nate Graham, [ссылка 1](https://invent.kde.org/plasma/discover/-/merge_requests/666), [ссылка 2](https://invent.kde.org/plasma/discover/-/merge_requests/667), [ссылка 3](https://bugs.kde.org/show_bug.cgi?id=475842), [ссылка 4](https://invent.kde.org/plasma/discover/-/merge_requests/665), [ссылка 5](https://invent.kde.org/plasma/discover/-/merge_requests/677) и [ссылка 6](https://bugs.kde.org/show_bug.cgi?id=475849))

К слову про Discover, на страницах приложений теперь используется новый, значительно улучшенный, просмотрщик снимков экрана, исправляющий множество ошибок (Иван Ткаченко, [ссылка](https://invent.kde.org/plasma/discover/-/merge_requests/636)):

![2](https://pointieststick.files.wordpress.com/2023/10/image-5.png)

Эффект «Обзор» теперь позволяет отключить фильтрацию открытых окон по вводимому запросу и вместо этого всегда выполнять поиск через KRunner (Dashon Wells, [ссылка](https://bugs.kde.org/show_bug.cgi?id=460710))

Если у вас настроена аутентификация по отпечатку пальца или смарт-карте, теперь вы можете использовать на экране блокировки как этот метод, так и вход по паролю; ранее ввести пароль можно было лишь после нескольких неудачных попыток другого метода. Пока это касается только экрана блокировки, а не диалогового окна аутентификации Polkit, но [над ним мы тоже работаем](https://invent.kde.org/plasma/polkit-kde-agent-1/-/merge_requests/32) (Janet Blackquill, [ссылка 1](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3315) и [ссылка 2](https://invent.kde.org/plasma/kscreenlocker/-/merge_requests/163))

Заголовки страниц диалогов настройки приложений на основе QtWidgets теперь выглядят так же, как и заголовки в Параметрах системы и приложениях на основе библиотеки Kirigami (Waqar Ahmed, [ссылка](https://invent.kde.org/plasma/breeze/-/merge_requests/360)):

![3](https://pointieststick.files.wordpress.com/2023/10/image-3.png)

Программа Просмотр шрифтов теперь работает в сеансе Wayland (Kai Uwe Broulik, [ссылка](https://bugs.kde.org/show_bug.cgi?id=439470))

Страница «Комбинации клавиш» Параметров системы была немного модернизирована, чтобы разгрузить низ окна (Mike Noe, [ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1776)):

![4](https://pointieststick.files.wordpress.com/2023/10/image-2.png)

В KRunner, среди прочего, более чем в два раза увеличена скорость поиска по последним файлам (Alexander Lohnau, [ссылка](https://write.as/alexander-lohnau/reworking-recent-files-search-for-plasma-6))

Бесконечно прокручиваемый календарь в виджете «Цифровые часы» стал гораздо отзывчивее (Fushan Wen, [ссылка](https://bugs.kde.org/show_bug.cgi?id=470071))

[Инкрементальный поиск](https://en.wikipedia.org/wiki/Incremental_search) в диспетчере файлов Dolphin теперь выравнивает найденный файл по центру окна в режиме просмотра «Таблица» (Amol Godbole, [ссылка](https://bugs.kde.org/show_bug.cgi?id=423884))

## Другие заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

В сеансе Wayland исправлен случай, когда вход в систему приводил к немедленному сбою диспетчера окон KWin и возврату пользователя на экран входа (Xaver Hugl, Plasma 5.27.9. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=466174))

В набор правок Qt от KDE включено исправление самого распространённого сбоя Plasma, случавшегося при закрытии уведомлений (Marco Martin и David Edmundson, [ссылка](https://bugs.kde.org/show_bug.cgi?id=468180))

Устранён довольно частый сбой Discover при установке приложений Flatpak (David Edmundson, Plasma 5.27.9. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=463864))

Устранено раздражающее мерцание экрана при переключении виртуальных рабочих столов жестом сенсорной панели с глобально отключённой анимацией (Quinten Kock, Plasma 5.27.9. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=472901))

Устранён наиболее распространённый сбой Системного монитора при его закрытии или при переключении страниц (Arjen Hiemstra, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=464893))

Исправлен случай, когда привязки файлов могли наследоваться в неправильном порядке, что приводило к открытию определённых типов файлов в неправильных приложениях (David Redondo, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=475200))

Исправлена ​​проблема, из-за которой сложные схемы расположения экранов с мониторами, подключёнными последовательной цепочкой по DisplayPort, располагались случайным образом после пробуждения, перезагрузки или горячего подключения (Xaver Hugl, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=470718))

В сеансе Wayland использование стилуса для работы со страницей «Планшет для рисования» Параметров системы больше не приводит к утрате чувствительности к стилусу остальных Параметров системы (Aki Sakurai, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=473126))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Осталось 2 ошибки в Plasma с очень высоким приоритетом (две недели назад было 4). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma остаётся 55 (как и две недели назад). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 220 ошибок было исправлено за две недели во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-10-13&chfieldto=2023-10-27&chfieldvalue=RESOLVED&list_id=2505349&query_format=advanced&resolution=FIXED)

## Разработчикам

Теперь вы можете собрать из исходного кода весь сеанс рабочего стола Plasma, включая связанные с ним приложения вроде Параметров системы и Discover, командой `kdesrc-build workspace` (Nicolas Fella, [ссылка](https://invent.kde.org/sdk/kdesrc-build/-/merge_requests/289))

kdesrc-build теперь по умолчанию собирает всё с Qt 6 (Thiago Sueto и Mariua Pa, [ссылка 1](https://invent.kde.org/sdk/kdesrc-build/-/merge_requests/295) и [ссылка 2](https://invent.kde.org/sdk/kdesrc-build/-/merge_requests/287))

## Как вы можете помочь

Мы проводим [сбор средств, приуроченный к выпуску Plasma 6](https://kde.org/fundraisers/plasma6member/), и нам нужна ваша помощь! Если вам нравится наша работа, пожертвование — это отличный способ нас отблагодарить. 🙂

Разработчики, пожалуйста, помогите исправить проблемы в Qt 6, KDE Frameworks 6 и Plasma 6! Plasma 6 уже вполне годится для повседневного использования энтузиастами (я в их числе), хоть и всё ещё требует работы, чтобы стать готовой к выпуску в феврале.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/10/27/these-past-2-weeks-in-kde-wayland-color-management-the-desktop-cube-returns-and-optional-shadows-in-spectacle/>  

<!-- 💡: System Monitor → Системный монитор -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: get new [thing] → «Загрузить \[что-то\]» -->
<!-- 💡: playlist → список воспроизведения -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: thumbnail → миниатюра -->
