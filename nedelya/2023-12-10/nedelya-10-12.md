# На этой неделе в KDE: сроки барьеров DMA и исправления ошибок

Марафон работы над ошибками вовсю продолжается, стараясь поспевать за лавиной новых сообщений о найденных недочётах! Похоже, многие действительно тестируют бета-версию Plasma 6, и это здорово! К счастью, большинство ошибок, о которых сообщается, не критичны. Так держать!

## Мегавыпуск KDE 6

> (Сюда входит всё ПО, которое будет выпущено 28 февраля: Plasma 6, библиотеки Frameworks 6 и приложения из состава Gear 24.02)

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 205](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

### Новые возможности

При использовании автоматической отправки отчётов о сбоях уведомление об отправке теперь позволяет вам добавить пояснение для разработчиков — например, описать, что вы делали во время возникновения ошибки (Harald Sitter, [ссылка](https://invent.kde.org/plasma/drkonqi/-/merge_requests/196))

### Исправления ошибок

**Важное примечание:** я не упоминаю здесь исправления ошибок, не успевших дойти до пользователей; посреди большого цикла разработки Plasma их слишком много, чтобы про них писать (а вам — читать), да и вообще мало кто их видел. Тем не менее огромное спасибо всем, кто плотно занимается их исправлением!

Исправлено возможное аварийное завершение работы диспетчера файлов Dolphin при отмене массового переименования (Akseli Lahtinen, [ссылка](https://bugs.kde.org/show_bug.cgi?id=469586))

Диспетчер разделов больше не позволяет установить выравнивание секторов раздела в ноль (Andrius Štikonas, [ссылка](https://bugs.kde.org/show_bug.cgi?id=478224))

Страницы «Последние файлы» и «Энергосбережение» Параметров системы теперь поддерживают подсветку изменённых пунктов (Méven Car и Jakob Petsovits, [ссылка 1](https://bugs.kde.org/show_bug.cgi?id=477847), [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=477848)):

![custom-0](image.png)

Перемещение ползунков прокруткой колёсика мыши над ними (или соответствующим жестом сенсорной панели) в Plasma и приложениях на Qt Quick теперь учитывает настройку инвертированной («естественной») прокрутки (Ismael Asensio, [ссылка](https://bugs.kde.org/show_bug.cgi?id=459624))

При создании снимка экрана утилитой Spectacle, настроенной на автоматическое копирование изображения в буфер обмена, уведомление о завершении съёмки теперь позволяет открыть снимок в просмотрщике изображений по умолчанию (Noah Davis, [ссылка](https://bugs.kde.org/show_bug.cgi?id=477903))

Диалог «Добавление часовых поясов» виджета «Цифровые часы» больше не бывает слишком узким, из-за чего названия часовых поясов не умещались (Nate Graham, [ссылка](https://bugs.kde.org/show_bug.cgi?id=477936))

Исправлено несколько проблем с размещением окон при использовании политики размещения «С минимальным перекрытием» с настроенным дробным масштабированием экрана в сеансе Plasma Wayland (Yifan Zhu, [ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/4740))

Другие сведения об ошибках:

* Ошибок в Plasma с очень высоким приоритетом остаётся 5 (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma остаётся 41 (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 176 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-11-24&chfieldto=2023-12-1&chfieldvalue=RESOLVED&list_id=2536448&query_format=advanced&resolution=FIXED)

### Производительность и технические улучшения

В диспетчер окон KWin добавлена ​​поддержка «сроков барьеров DMA» (DMA fence deadlines), что должно повысить производительность и отзывчивость интерфейса на устройствах со встроенными видеокартами Intel в сеансе Wayland (Xaver Hugl, [ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/4358))

Проведена большая работа над производительностью эффекта «Обзор»; теперь он *намного* плавнее, и мы работаем над скоростью его открытия (Влад Загородний и Marco Martin, [ссылка 1](https://bugs.kde.org/show_bug.cgi?id=475250), [ссылка 2](https://invent.kde.org/plasma/kwin/-/merge_requests/4768), [3](https://invent.kde.org/plasma/kwin/-/merge_requests/4763), [4](https://invent.kde.org/plasma/kwin/-/merge_requests/4762), [5](https://invent.kde.org/plasma/kwin/-/merge_requests/4752), [6](https://invent.kde.org/plasma/kwin/-/merge_requests/4764) и [7](https://invent.kde.org/plasma/kwin/-/commit/1729dee3efda2320aad22cdb1beacd0e8dbe0555))

У пользователей некоторых видеокарт AMD исправлены проблемы с производительностью во время включения ночной цветовой схемы (Xaver Hugl, [ссылка](https://bugs.kde.org/show_bug.cgi?id=453701))

## Как вы можете помочь

Мы проводим [сбор средств, приуроченный к выпуску Plasma 6](https://kde.org/fundraisers/plasma6member/), и нам нужна ваша помощь! Благодаря вам мы пересекли отметку в 88% от цели, но пока её не достигли. Если вам нравится наша работа, пожертвование — это отличный способ нас отблагодарить. 🙂

Разработчики, пожалуйста, помогите исправить проблемы в Qt 6, KDE Frameworks 6 и Plasma 6! Plasma 6 уже вполне годится для повседневного использования энтузиастами, хоть и всё ещё требует работы, чтобы стать готовой к выпуску в феврале.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/12/08/this-week-in-kde-dma-fence-deadlines-and-lots-of-bug-fixing/>  

<!-- 💡: Partition Manager → Диспетчер разделов -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: screenshot → снимок экрана -->
