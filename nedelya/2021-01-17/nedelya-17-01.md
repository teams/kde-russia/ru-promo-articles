# На этой неделе в KDE: перекомпоновка текста в Konsole!

На этой неделе в эмуляторе терминала Konsole появилась крупная новая функция: теперь текст [перераспределяется](https://bugs.kde.org/show_bug.cgi?id=196998) при изменении размера окна! Эту функцию можно отключить, если она вам не нравится, но по умолчанию она включена. И работает очень хорошо. Большое спасибо Carlos Alves и Tomaz Canabrava за их труд! Нововведение будет доступно в Konsole 21.04.

> 10–17 января, основное — прим. переводчика

<https://invent.kde.org/utilities/konsole/uploads/7ff3097c36af86b62821d172275ad360/konsole_reflow_test.mp4>

## Другие новые возможности

Диспетчер файлов Dolphin теперь позволяет [выбрать](https://invent.kde.org/system/dolphin/-/merge_requests/140), где будут появляться вновь открытые вкладки: в конце панели вкладок или после текущей вкладки (Anthony Fieroni, Dolphin 21.04):

![2](https://pointieststick.files.wordpress.com/2021/01/screenshot_20210114_125810.jpeg)

Архиватор Ark теперь [поддерживает](https://bugs.kde.org/show_bug.cgi?id=104404) архивы формата ARJ (Natsumi Higa, Ark 21.04)

## Исправления ошибок

Spectacle теперь позволяет [изменить](https://bugs.kde.org/show_bug.cgi?id=431557) формат файла снимка экрана по умолчанию при использовании языков, отличных от английского (Nicolas Fella, Spectacle 20.12.2)

При использовании вертикальной панели Plasma дата, отображаемая под часами, больше [не становится](https://bugs.kde.org/show_bug.cgi?id=417852) огромной в некоторых случаях (Marco Martin, Plasma 5.21)

Plasma больше [не зависает](https://bugs.kde.org/show_bug.cgi?id=423594), когда приложение быстро отправляет много уведомлений (Kai Uwe Broulik, Plasma 5.21)

Исправлены различные проблемы с границами контекстных меню в стиле Breeze, которые иногда были [невидимыми](https://bugs.kde.org/show_bug.cgi?id=428710) или [чёрными](https://bugs.kde.org/show_bug.cgi?id=428095) (David Redondo, Plasma 5.21)

Виджет глобального меню теперь [правильно обновляется](https://bugs.kde.org/show_bug.cgi?id=422786) при переключении фокуса на приложение GTK или с него (David Edmundson, Plasma 5.21)

При вызове виртуальной клавиатуры на экране входа в систему или на экране блокировки, фокус на поле ввода пароля теперь [не теряется](https://bugs.kde.org/show_bug.cgi?id=430209), поэтому вводимые символы больше не пропадают без предварительной ручной фокусировки (Nate Graham, Plasma 5.21)

Теперь после включения модуля календаря в цифровых часах панель календаря [появляется мгновенно](https://bugs.kde.org/show_bug.cgi?id=431433), не требуя перезапуска Plasma (Nicolas Fella, Plasma 5.21)

[Исправлена](https://bugs.kde.org/show_bug.cgi?id=408060) ошибка, из-за которой вновь созданная панель могла быть размещена не на том экране в многомониторной конфигурации (Xaver Hugl, Plasma 5.21)

Значки языковых кодов на странице «Клавиатура» Параметров системы теперь [видны](https://bugs.kde.org/show_bug.cgi?id=431361) при использовании светлой цветовой схемы приложений с тёмной темой Plasma или наоборот (Nate Graham, Plasma 5.21)

Отмена изменений на странице «Пользователи» Параметров системы путём отмены диалога аутентификации больше [не приводит](https://bugs.kde.org/show_bug.cgi?id=425036) к применению изменений в любом случае (Nicolas Fella, Plasma 5.21)

Служба синхронизации KDE Connect больше [не завершается иногда аварийно](https://bugs.kde.org/show_bug.cgi?id=423757), когда её заваливает кучей уведомлений (Nicolas Fella, Frameworks 5.79)

## Улучшения производительности

Значки в приложениях на основе библиотеки Kirigami теперь [потребляют немного меньше памяти](https://invent.kde.org/frameworks/kirigami/-/merge_requests/209), что значительно сокращает её использование для приложений, где значков много (David Edmundson, Frameworks 5.79)

## Улучшения пользовательского интерфейса

Dolphin теперь [позволяет](https://bugs.kde.org/show_bug.cgi?id=430086) распаковывать несколько архивов одновременно с помощью соответствующего пункта контекстного меню (Elvis Angelaccio, Ark 20.12.2)

Страница «Вход в систему (SDDM)» Параметров системы была [переписана](https://invent.kde.org/plasma/sddm-kcm/-/merge_requests/3), что исправляет кучу ошибок и делает её приятнее и последовательнее (David Redondo, Plasma 5.21):

![3](https://pointieststick.files.wordpress.com/2021/01/new-sddm-kcm.jpeg)

Приложения на основе QML теперь [можно перетаскивать](https://bugs.kde.org/show_bug.cgi?id=403838) за пустые области их заголовков и фона, как приложения на основе Qt Widgets (Marco Martin, Frameworks 5.79 и Plasma 5.21)

Индикатор Plasma «Микрофон» теперь [отображает](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/38) название используемого в данный момент микрофона (Kai Uwe Broulik, Plasma 5.21)

Страница «Приложения по умолчанию» Параметров системы теперь [поддерживает](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/285) функцию «Выделить изменённые параметры» (Cyril Rossi, Plasma 5.21)

Виджет глобального меню теперь соблюдает закон Фиттса, [позволяя](https://bugs.kde.org/show_bug.cgi?id=404267) перемещать курсор из одного раздела меню в другой по ряду пикселей, которые касаются края экрана (Jan Blackquill, Plasma 5.21)

Страница «Заставка» Параметров системы теперь [находится](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/295) в разделе «Внешний вид» (Nate Graham, Plasma 5.21):

![4](https://pointieststick.files.wordpress.com/2021/01/splash-screen-in-appearance-category.jpeg)

Окно «Загрузить новые виджеты Plasma» [теперь выглядит гораздо приятнее и современнее](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/536) (Alexander Lohnau, Plasma 5.21):

![5](https://pointieststick.files.wordpress.com/2021/01/screenshot_20210115_101411.jpeg)

Приложения KDE больше [не показывают](https://bugs.kde.org/show_bug.cgi?id=422385) тома Docker на панели «Точки входа» (Méven Car, Frameworks 5.79)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник, и совершенно не обязательно быть программистом — занятие по душе найдётся для каждого.

И наконец, если вы находите программное обеспечение KDE полезным, окажите финансовую поддержку [фонду KDE e.V.](https://ev.kde.org/)

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Андреев](https://vk.com/ilyancod)  
*Источник:* <https://pointieststick.com/2021/01/15/this-week-in-kde-text-reflow-in-konsole>  

<!-- 💡: Places panel → панель «Точки входа» -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icons → значки -->
<!-- 💡: screenshot → снимок экрана -->
