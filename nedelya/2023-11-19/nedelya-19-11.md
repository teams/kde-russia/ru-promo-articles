# На этой неделе в KDE: умное скрытие панели и «время показа» Wayland

Приятно видеть, что многие уже используют [альфа-версию Plasma 6](https://kde.org/announcements/kdes-6th-megarelease-alpha/) — это ожидаемо привело к всплеску сообщений об ошибках. Спасибо, продолжайте в том же духе! Работа над нововведениями более-менее позади, и внимание смещается к работе над ошибками. До сих пор я упоминал исправления только в стабильных ветках, но, учитывая, сколько времени отведено на полировку Plasma 6, мне, возможно, стоит упоминать исправления и в ней.

Так или иначе, на этой неделе нам есть о чём рассказать!

## Plasma 6

> (Сюда входит всё ПО, которое будет выпущено 28 февраля: Plasma 6, библиотеки Frameworks 6 и приложения из состава Gear 24.02)

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 144](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

Для панелей Plasma доступен новый режим видимости «Уклоняться от окон», скрывающий панели только при касании их окнами (Bharadwaj Raju и Niccolò Venerandi, [ссылка](https://bugs.kde.org/show_bug.cgi?id=349785))

Диспетчер окон KWin получил поддержку протокола Wayland [Presentation time](https://wayland.app/protocols/presentation-time) («время показа»), что увеличит частоту кадров в браузерах на основе Chromium и приложениях на Electron, а также обеспечит более плавное воспроизведение видео (Xaver Hugl, [ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/4566))

Carl Schwan на этой неделе вовсю улучшал внешний вид приложений KDE на основе QtWidgets, и теперь они выглядят просто прекрасно! (Carl Schwan, [ссылка 1](https://invent.kde.org/plasma/breeze/-/merge_requests/367), [ссылка 2](https://invent.kde.org/plasma/breeze/-/merge_requests/370), [3](https://invent.kde.org/system/dolphin/-/merge_requests/655), [4](https://invent.kde.org/utilities/konsole/-/merge_requests/919), [5](https://invent.kde.org/plasma/breeze/-/merge_requests/368), [6](https://invent.kde.org/frameworks/kcmutils/-/merge_requests/183), [7](https://invent.kde.org/graphics/gwenview/-/merge_requests/237), [8](https://invent.kde.org/utilities/kwalletmanager/-/merge_requests/37) и [9](https://invent.kde.org/system/partitionmanager/-/merge_requests/40)):

![0](https://pointieststick.files.wordpress.com/2023/11/image-10.png)

Элементы списков в программном обеспечении KDE на основе QML теперь используют более приятный закруглённый стиль выделения (Arjen Hiemstra и Carl Schwan, [ссылка](https://invent.kde.org/frameworks/qqc2-desktop-style/-/merge_requests/270)):

![1](https://pointieststick.files.wordpress.com/2023/11/image-9.png)

На создаваемых пустых панелях теперь есть кнопка «Добавить виджеты…», ведь это почти всегда ваше следующее действие (Niccolò Venerandi, [ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1820))

В набор значков Breeze добавлены монохромные варианты значков погоды, чтобы виджет «Прогноз погоды» не был единственным цветным значком в системном лотке (Alois Spitzbart, [ссылка](https://bugs.kde.org/show_bug.cgi?id=403833)):

![2](https://pointieststick.files.wordpress.com/2023/11/image-8.png)

Диалоги загрузки новых материалов из сети при прокрутке больше не перекрываются огромным блокирующим индикатором загрузки (Rishi Kumar, [ссылка](https://bugs.kde.org/show_bug.cgi?id=473954))

Все диалоги настройки виджетов Plasma переведены на использование тех же базовых компонентов, что и у страниц Параметров системы, что унифицирует код и позволяет задействовать безрамочные области прокрутки (Nicolas Fella, [ссылка](https://invent.kde.org/plasma/plasma-desktop/-/issues/99))

Довольно сложная для понимания опция панели задач «Не начинать новую строку, пока она не заполнится максимально» ​​была переделана для большей ясности (Niccolò Venerandi, [ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1780)):

![3](https://pointieststick.files.wordpress.com/2023/11/image-11.png)

Ускорен запуск приложений в сеансе Plasma Wayland (David Edmundson, [ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/4630))

Цвет насыщенного текста в стандартной цветовой схеме эмулятора терминала Konsole бал изменён на более заметный и привлекательный синий (Thiago Sueto, [ссылка](https://invent.kde.org/utilities/konsole/-/merge_requests/920)):

![4](https://pointieststick.files.wordpress.com/2023/11/image-12.png)

В диспетчере файлов Dolphin теперь можно включать и выключать показ миниатюр клавишей F12, как и в диалоговых окнах открытия и сохранения файлов (Eric Armbruster, [ссылка](https://bugs.kde.org/show_bug.cgi?id=172967))

## Другие заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Настройка яркости теперь работает в ОС FreeBSD (Глеб Попов, Plasma 5.27.10. [Ссылка](https://invent.kde.org/plasma/powerdevil/-/merge_requests/274))

Определение предпочитаемого пользователем веб-браузера стало надёжнее (Harald Sitter, Plasma 5.27.10. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3324))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 4 ошибки в Plasma с очень высоким приоритетом (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 44 (на прошлой неделе было 47). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 190 (!) ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-11-10&chfieldto=2023-11-17&chfieldvalue=RESOLVED&list_id=2522971&query_format=advanced&resolution=FIXED)

## Как вы можете помочь

Мы проводим [сбор средств, приуроченный к выпуску Plasma 6](https://kde.org/fundraisers/plasma6member/), и нам нужна ваша помощь! Благодаря вам мы пересекли отметку в 70% от цели, но пока её не достигли. Если вам нравится наша работа, пожертвование — это отличный способ нас отблагодарить. 🙂

Разработчики, пожалуйста, помогите исправить проблемы в Qt 6, KDE Frameworks 6 и Plasma 6! Plasma 6 уже вполне годится для повседневного использования энтузиастами (я в их числе), хоть и всё ещё требует работы, чтобы стать готовой к выпуску в феврале.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/11/17/this-week-in-kde-panel-intellihide-and-wayland-presentation-time/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: get new [thing] → «Загрузить \[что-то\]» -->
<!-- 💡: icon → значок -->
<!-- 💡: icon theme → набор значков -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
