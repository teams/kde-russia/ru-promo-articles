# На этой неделе в KDE: субботник в интерфейсе

> 26 июня – 3 июля, основное — прим. переводчика

На этой неделе стандартный набор исправленных ошибок разнообразили улучшения интерфейса!

## Исправленные «15-минутные ошибки»

Текущее число ошибок: **57, а не 59, как раньше.** Добавилось 0; 1 ошибка, как выяснилось, уже исправлена; решена 1 проблема:

При масштабировании экрана с включённым по умолчанию запуском через Systemd в Plasma [больше не будет применяться неверный коэффициент масштабирования, что иногда наблюдалось сразу после входа в систему](https://bugs.kde.org/show_bug.cgi?id=449212) и вызывало размытие картинки (в сеансе Wayland) или отображение элементов неправильного размера (в X11) (David Edmundson, Plasma 5.25.2)

[Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)

## Wayland

В сеансе Plasma Wayland страница «Планшет для рисования» Параметров системы [больше не падает при повторном открытии](https://bugs.kde.org/show_bug.cgi?id=451233) (Nicolas Fella, Plasma 5.24.6)

В сеансе Wayland [смена разрешения экрана на не поддерживаемое официально больше не приводит в некоторых случаях к сбою Параметров системы](https://bugs.kde.org/show_bug.cgi?id=455477) (Xaver Hugl, Plasma 5.25.3)

В сеансе Wayland снова можно выбирать окна в эффектах «Обзор», «Все окна» и «Все рабочие столы» [с помощью сенсорного экрана](https://bugs.kde.org/show_bug.cgi?id=456133) (Marco Martin, Plasma 5.25.3)

## Улучшения пользовательского интерфейса

Spectacle [теперь показывает доступные глобальные комбинации клавиш для создания снимка экрана в выпадающем списке для выбора режима](https://bugs.kde.org/show_bug.cgi?id=389778)! (Felix Ernst, Spectacle 22.08):

![0](https://pointieststick.files.wordpress.com/2022/06/capture-mode-shortcuts.png)

При комментировании снимка экрана в Spectacle [окно теперь изменяет размер, чтобы уместить весь снимок, так что вам не нужно вручную подгонять область просмотра](https://bugs.kde.org/show_bug.cgi?id=429833) (Antonio Prcela, Spectacle 22.08)

В эффекте «Все рабочие столы» теперь можно [закрывать окна щелчком средней кнопки мыши, как в эффектах «Обзор» и «Все окна»](https://bugs.kde.org/show_bug.cgi?id=456144) (Felipe Kinoshita, Plasma 5.26)

Страница «Автозапуск» Параметров системы [теперь выдаёт предупреждение, если попытаться добавить неисполняемый сценарий входа или выхода, и даже отображает большую кнопку, на которую можно нажать, чтобы исправить проблему](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/878) (Nicolas Fella, Plasma 5.26):

![2](https://pointieststick.files.wordpress.com/2022/07/needs-to-be-executable.png)

Полноэкранный вид QR-кода из виджета «Сетевые подключения» [теперь можно закрыть с помощью клавиатуры](https://invent.kde.org/plasma/plasma-nm/-/merge_requests/130), [а в его углу теперь есть видимая кнопка закрытия](https://invent.kde.org/plasma/plasma-nm/-/commit/0d9cd3c9ce7000268fb59da490b2613aea42fd05) (Fushan Wen и Nate Graham, Plasma 5.26)

У поисковой строки в классическом меню запуска приложений [больше нет маленького, но неприятного смещения](https://bugs.kde.org/show_bug.cgi?id=404722) (Nate Graham, Plasma 5.26)

Текущие позиции курсора и прокрутки в виджете «Клейкая заметка» [теперь сохраняются после перезагрузки компьютера (или перезапуска Plasma)](https://bugs.kde.org/show_bug.cgi?id=449150) (Иван Ткаченко, Plasma 5.26)

Теперь [можно настроить виджет панели задач так, чтобы он не занимал всё свободное место](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/470), и разместить его непосредственно слева от другого элемента, например, от виджета «Меню приложения» (Ярослав Болюкин, Plasma 5.26)

Отображение подписей в виджете «Список окон» [на горизонтальной панели теперь опциональное (но по-прежнему включено по умолчанию)](https://bugs.kde.org/show_bug.cgi?id=455560), что позволяет вернуться к старому стилю из Plasma 5.24 и ранних версий, если он вам больше нравится (Nate Graham, Plasma 5.26)

## Исправления ошибок и улучшения производительности

Исправлена [одна из причин, по которой Plasma могла аварийно завершить работу сразу после входа в систему на ноутбуке с внешним монитором, подключённым по HDMI](https://bugs.kde.org/show_bug.cgi?id=454159) (David Edmundson, Plasma 5.25.2)

Кнопка «Показать QR-код сети» больше [не будет появляться в виджете «Сетевые подключения» для сетей, не поддерживающих обнаружение по QR-коду, например, проводных сетей и VPN](https://invent.kde.org/plasma/plasma-nm/-/merge_requests/131) (Nicolas Fella, Plasma 5.25.2)

Эффекты диспетчера окон KWin в сеансе Plasma [больше не будут воспроизводиться с неправильной скоростью, если вы когда-либо настраивали скорость анимации на странице «Обеспечение эффектов» Параметров системы вне Plasma](https://bugs.kde.org/show_bug.cgi?id=431259) (David Edmundson, Plasma 5.23.3)

Исправлен [ряд](https://bugs.kde.org/show_bug.cgi?id=441241) [визуальных ошибок](https://bugs.kde.org/show_bug.cgi?id=422447) в [некоторых](https://bugs.kde.org/show_bug.cgi?id=451997) нестандартных визуализациях переключения окон (Ismael Asensio, Plasma 5.25.3)

Список последних документов в файловом диалоге GTK [больше не будет самопроизвольно очищаться после работы в некоторых приложениях KDE](https://bugs.kde.org/show_bug.cgi?id=456046) (Méven Car, Frameworks 5.96)

## Изменения вне KDE, затрагивающие KDE

При подключении или отключении экрана в многомониторной конфигурации [на рабочих столах теперь будут отображаться нужные обои](https://bugs.kde.org/show_bug.cgi?id=450443) (Fushan Wen, Qt 6.3.2, включено в набор правок Qt от KDE)

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Иван Ткаченко (ratijas)](https://t.me/ratijas)  
*Источник:* <https://pointieststick.com/2022/07/01/this-week-in-kde-and-now-time-for-for-some-ui-polishing/>

<!-- 💡: Kicker → классическое меню запуска приложений -->
<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: application launcher → меню запуска приложений -->
<!-- 💡: icon → значок -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: task manager → панель задач -->
