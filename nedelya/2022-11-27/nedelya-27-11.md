# На этой неделе в KDE: крупные улучшения интерфейса

На этой неделе мы подготовили много крупных и важных улучшений пользовательского интерфейса для нескольких приложений и рабочей среды Plasma, не говоря уже об успехах в устранении заметных ошибок!

## Новые возможности

Dragon Player — минималистичный медиапроигрыватель — претерпел серьёзную переработку интерфейса, в том числе перешёл на меню-гамбургер, получил новый экран приветствия, оптимизированный более понятный набор кнопок на панели инструментов и менее глючное поведение при открытии видео в сеансе Wayland (Harald Sitter, Dragon Player 23.04, [ссылка 1](https://bugs.kde.org/show_bug.cgi?id=461387), [ссылка 2](https://invent.kde.org/multimedia/dragon/-/merge_requests/12), [ссылка 3](https://bugs.kde.org/show_bug.cgi?id=461378), [ссылка 4](https://bugs.kde.org/show_bug.cgi?id=461375), [ссылка 5](https://bugs.kde.org/show_bug.cgi?id=461383) и [ссылка 6](https://bugs.kde.org/show_bug.cgi?id=461840)):

![0](https://pointieststick.files.wordpress.com/2022/11/image-10.png)

Анализатор использования дисков Filelight теперь отображает в левой части окна список с размерами элементов. Заодно решены проблемы с всплывающими подсказками и размытостью диаграммы! (Harald Sitter, Filelight 23.04. [Ссылка](https://invent.kde.org/utilities/filelight/-/merge_requests/61)):

![1](https://pointieststick.files.wordpress.com/2022/11/image-9.png)

В Параметрах системы появилась новая страница «Сенсорный экран», которая позволяет отключать сенсорные экраны и выбирать, с какими физическими экранами сопоставляется их ввод (Nicolas Fella при поддержке [Технического университета Дрездена](https://tu-dresden.de/?set_language=en), Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1227))

В сеансе Plasma Wayland экранам теперь по умолчанию присваивается коэффициент масштабирования, более точно соответствующий плотности их пикселей и форм-фактору устройства (Nate Graham, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/kscreen/-/merge_requests/152))

Теперь можно добавить в автозапуск несколько экземпляров одного приложения; кроме того, страница настройки автозапуска теперь показывает пути автоматически запускаемых сценариев (Thenujan Sandramohan, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1782))

В виджете «Просмотр папки» теперь можно включить показ скрытых файлов (Willyanto, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=365792))

Страница «Планшет для рисования» Параметров системы теперь позволяет назначать на физические кнопки стилуса вызов комбинаций клавиш (Aleix Pol Gonzalez, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1229))

## Улучшения пользовательского интерфейса

При разблокировке экрана по отпечатку пальца больше не придётся нажимать после этого кнопку «Разблокировать» (Janet Blackquill, Plasma 5.26.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=454706))

Выбор местоположения в виджете погоды стал проще и удобнее (Ismael Asensio, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/266)):

![2](https://pointieststick.files.wordpress.com/2022/11/image-14.png)

На странице управления пользователями в Параметрах системы выбор пальцев для аутентификации по отпечатку пальца стал намного понятнее. Кроме того, теперь можно отменить регистрацию отдельных пальцев, а при смене пароля сообщение «Пароли должны совпадать» больше не показывается до нажатия кнопки «Задать пароль» или паузы в несколько секунд после окончания ввода (Janet Blackquill и Devin Lin, Plasma 5.27. [Ссылка 1](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2347), [ссылка 2](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2367) и [ссылка 3](https://bugs.kde.org/show_bug.cgi?id=462043)):

![4](https://pointieststick.files.wordpress.com/2022/11/image-12.png)

Страница настройки экранов в Параметрах системы теперь требует, чтобы экраны соприкасались и не перекрывались, что предотвращает возникновение различных странных ошибок (David Redondo, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=453926))

Всплывающая подсказка виджета «Громкость» больше не сообщает без надобности, на каком аудиовыходе воспроизводится звук, если он всего один; вместо этого упоминается возможность прокрутки над значком для изменения громкости (Nate Graham, Plasma 5.27. [Ссылка 1](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/150) и [ссылка 2](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/149))

У всплывающих окон Plasma в теме Breeze теперь больший радиус закругления, соответствующий радиусу закругления окон (Niccolò Venerandi, Frameworks 5.101. [Ссылка](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/644)):

![6](https://pointieststick.files.wordpress.com/2022/11/consistent-roundnesses.jpg)

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

В сеансе Plasma Wayland касание сенсорного экрана после отключения внешнего экрана больше не приводит к сбою диспетчера окон KWin (Xaver Hugl, Plasma 5.26.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=461901))

Верхние углы уведомлений Plasma больше не такие острые (Niccolò Venerandi, Plasma 5.26.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=452347))

В сеансе X11 отключение режима с графическими эффектами больше не создаёт просветов вокруг панелей Plasma (Niccolò Venerandi, Plasma 5.26.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460896))

Использование строки поиска и запуска KRunner в эффекте «Обзор» больше не приводит иногда к сбою KWin (Alexander Lohnau, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=448825))

Улучшено решение проблемы пустых границ справа от распахнутых окон приложений, запущенных через слой совместимости XWayland (Aleix Pol Gonzalez, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=459373))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Осталось 6 ошибок в Plasma с очень высоким приоритетом (на прошлой неделе было 10). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 47 (на прошлой неделе было 50). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 152 ошибки были исправлены на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2022-11-17&chfieldto=2022-11-25&chfieldvalue=RESOLVED&list_id=2217570&query_format=advanced&resolution=FIXED)

## Изменения вне KDE, затрагивающие KDE

В приложениях на основе QtQuick прокручиваемые представления, содержимое которых вмещается по горизонтали, больше не отображают бессмысленную горизонтальную полосу прокрутки. Раньше мы обходили эту ошибку в большинстве приложений KDE, а теперь этого не требуется! (David Redondo, Qt 6.4.2, но исправление включено в наш набор правок для Qt 5. [Ссылка](https://bugreports.qt.io/browse/QTBUG-83890))

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2022/11/25/this-week-in-kde-humongous-ui-improvements/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: icon theme → набор значков -->
<!-- 💡: location → расположение (в файловой системе) / местоположение (на карте) -->
<!-- 💡: upstream → [компонент-]зависимость | исходный репозиторий -->
<!-- 💡: venerable → древний -->
