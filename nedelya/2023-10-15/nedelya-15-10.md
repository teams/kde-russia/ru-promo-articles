# На этой неделе в KDE: фильтры коррекции дальтонизма

На этой неделе у нас много новостей, в частности, о специальных возможностях! О них и о других новых функциях и улучшениях интерфейса читайте в сегодняшней сводке:

## Plasma 6

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 103](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

В диспетчер окон KWin добавлен эффект, позволяющий пользователям с различными видами цветовой слепоты адаптировать цвета на экране под свои потребности (Fushan Wen, [ссылка](https://bugs.kde.org/show_bug.cgi?id=474470)):

![0](https://pointieststick.files.wordpress.com/2023/10/image-1.png)

В большинстве приложений KDE клавиша F10 теперь открывает главное меню приложения или меню-гамбургер. В связи с этим сочетание клавиш для создания новой папки пришлось изменить на Ctrl+Shift+N, что, впрочем, более совместимо с другими средами (Felix Ernst, [ссылка](https://wordsmith.social/felix-ernst/f10-for-accessibility-in-kf6))

Текстовые редакторы Kate и KWrite и другие приложения на основе компонента редактирования KTextEditor получили поддержку зачитывания текста из документов! (Christoph Cullmann, [ссылка](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/606))

Во всём программном обеспечении KDE на основе QtQuick прокрутка колесом мыши теперь плавно анимируется! Заметьте, что это не то же самое, что инерционная прокрутка для сенсорных панелей — она ещё не реализована. Подробнее [здесь](https://community.kde.org/Get_Involved/Design/Frequently_Discussed_Topics#Names_of_different_scrolling_effects). (HeCheng Yu и Fushan Wen, [ссылка](https://invent.kde.org/frameworks/kirigami/-/merge_requests/1318))

Если на панели Plasma есть виджет «Панель задач», функция добавления виджетов запуска приложений на эту панель теперь скрыта в пользу закрепления приложений в панели задач, чтобы избежать путаницы. Вы по-прежнему можете добавлять виджеты запуска вручную, но это не так афишируется. Вместе с тем, классический вариант панели задач был изменён, чтобы по умолчанию не скрывать плитки закреплённых приложений после их использования для запуска; теперь они остаются на своих местах и позволяют запускать дополнительные экземпляры приложений, что больше соответствует поведению виджетов запуска. Конечно, это можно настроить, если старый подход вам нравился больше! (Niccolò Venerandi, [ссылка 1](https://bugs.kde.org/show_bug.cgi?id=390817) и [ссылка 2](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1744))

Документы LibreOffice теперь отображаются в списках последних открытых файлов в Plasma (Méven Car, [ссылка](https://invent.kde.org/plasma/kactivitymanagerd/-/merge_requests/57))

Предварительный просмотр набора курсоров больше не слишком маленький при использовании масштабирования экрана в сеансе Wayland и больше не пикселизирован, если масштаб дробный (Fushan Wen, [ссылка 1](https://bugs.kde.org/show_bug.cgi?id=392155) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=413647))

Discover теперь позволяет обновлять и удалять программы, установленные из локально загруженных файлов пакетов (Alessandro Astone, [ссылка](https://bugs.kde.org/show_bug.cgi?id=467743))

Discover стал точнее показывать ход выполнения автономных обновлений (Alessandro Astone, [ссылка](https://invent.kde.org/plasma/discover/-/merge_requests/654))

В сеансе Wayland теперь можно зеркально отразить экран (Xaver Hugl, [ссылка](https://bugs.kde.org/show_bug.cgi?id=447792))

## Другие улучшения пользовательского интерфейса

В утилите Spectacle были изменены расположения по умолчанию для сохранения снимков и записей экрана: теперь это `~/Изображения/Снимки экрана` и `~/Видео/Записи экрана` соответственно. Конечно же, это настраивается! (Noah Davis, Spectacle 24.02. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=474594))

## Другие заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Исправлено быстрое переключение пользователей из учётной записи без пароля (Harald Sitter, Plasma 5.27.9. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=458951))

В диспетчере файлов Dolphin пункт контекстного меню «Создать» теперь правильно включается и отключается в соответствии с наличием прав доступа на изменение при переключении вкладок (Amol Godbole, Dolphin 24.02. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=440366))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 4 ошибки в Plasma с очень высоким приоритетом (на прошлой неделе было 3). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 55 (на прошлой неделе было 56). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 89 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-10-06&chfieldto=2023-10-13&chfieldvalue=RESOLVED&list_id=2493631&query_format=advanced&resolution=FIXED)

## Как вы можете помочь

Мы проводим [сбор средств, приуроченный к выпуску Plasma 6](https://kde.org/fundraisers/plasma6member/), и нам нужна ваша помощь! Если вам нравится наша работа, пожертвование — это отличный способ нас отблагодарить. 🙂

Разработчики, пожалуйста, помогите исправить проблемы в [Qt 6, KDE Frameworks 6 и Plasma 6](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&f1=keywords&f2=product&list_id=2398791&o1=allwordssubstr&o2=notequals&query_format=advanced&v1=qt6&v2=neon)! Plasma 6 уже вполне годится для повседневного использования энтузиастами (я в их числе), хоть и всё ещё требует работы, чтобы стать готовой к выпуску в феврале.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/10/13/this-week-in-kde-colorblindness-correction-filters/>  

<!-- 💡: applet → виджет -->
<!-- 💡: location → расположение (в файловой системе) / местоположение (на карте) -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: task manager → панель задач -->
