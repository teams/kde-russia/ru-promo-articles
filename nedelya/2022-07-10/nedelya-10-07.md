# На этой неделе в KDE: большие события

> 3–10 июля, основное — прим. переводчика

На этой неделе произошло несколько важных событий, так что давайте сразу к ним:

Илья Поминов добавил в просмотрщик изображений Gwenview 22.08 функцию, которая [позволяет вам размечать и комментировать изображения с помощью того же интерфейса, что и после создания снимка экрана в Spectacle](https://bugs.kde.org/show_bug.cgi?id=408551)!

![0](https://pointieststick.files.wordpress.com/2022/07/annotation.png)

К Plasma 5.26 Han Young [объединил страницы «Язык» и «Форматы» Параметров системы](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1147), что проясняет связь между общесистемным языком и его форматами по умолчанию и исправляет большинство ошибок, затрагивавших обе старые страницы!

![1](https://pointieststick.files.wordpress.com/2022/07/merged-region-language-kcm.png)

К выпуску Frameworks 5.97 Слава Асеев реализовал в Бумажнике KDE (KWallet) [поддержку стандарта `org.freedesktop.secrets`](https://invent.kde.org/frameworks/kwallet/-/merge_requests/11), что позволит приложениям KDE быть более совместимыми со сторонними методами хранения учётных данных. На практике это означает, что, например, лаунчер Minecraft больше не будет требовать ввести логин и пароль при каждом запуске!

И последнее в нашем списке больших изменений: для Plasma 5.26 Harald Sitter добавил в Обработчик ошибок KDE поддержку [отправки информации о сбоях в Sentry](https://invent.kde.org/plasma/drkonqi/-/merge_requests/65), серверную службу учёта сбоев, которая в конечном итоге сможет автоматически вводить отладочные символы. Это может показаться слишком техническим и скучным, но со временем должно привести к тому, что отчёты о сбоях станут полезнее и будут тратить меньше времени.

Помимо перечисленного, у нас имеется неплохая подборка других заметных изменений!

## Исправленные «15-минутные ошибки»

Текущее количество ошибок: **53, вместо бывших 57**. 0 новых, 2 уже исправлены ранее, а 2 исправлены в объединённой странице «Регион и язык»! Спасибо, замечательные люди.

[Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)

## Другие улучшения пользовательского интерфейса

Когда вы отменяете загрузку медленной папки в Dolphin, [сообщение-заполнитель в середине окна теперь говорит «Загрузка отменена», а не «В этой папке ничего нет»](https://invent.kde.org/system/dolphin/-/merge_requests/416) (Kai Uwe Broulik, Dolphin 22.08)

Перетаскивание файла на уведомление [теперь активирует и поднимает окно отправившего уведомление приложения, чтобы вы могли перетащить в него файл](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1896) (Kasper Laudrup, Martin Tobias Holmedahl Sandsmark и Luis Javier Merino, Plasma 5.26)

У типичного диалогового окна «Загрузить \[что-то\]» [внизу больше нет лишней кнопки закрытия](https://invent.kde.org/frameworks/knewstuff/-/merge_requests/192), а [при временных ошибках сети и сервера оно теперь предлагает вам повторить попытку позже](https://invent.kde.org/frameworks/knewstuff/-/merge_requests/193) (Felipe Kinoshita и Nate Graham, Frameworks 5.96)

## Исправления ошибок и улучшения производительности

Исправлена [ошибка, из-за которой Okular мог повредить файл при сохранении](https://bugs.kde.org/show_bug.cgi?id=453421) (Albert Astals Cid, Okular 22.04.3)

Новая функция «Определять цвет выделения на основе обоев» [теперь, как и ожидалось, обновляет цвет заголовка, когда обои сменяются автоматически](https://bugs.kde.org/show_bug.cgi?id=454047) (например, при использовании для обоев слайд-шоу), а также [правильно применяет выбранный вручную цвет выделения к заголовкам окон при использовании цветовой схемы](https://bugs.kde.org/show_bug.cgi?id=455395), в [которой не задействуются цвета заголовков](https://bugs.kde.org/show_bug.cgi?id=455395), например, «Breeze, классический вариант» (Евгений Попов, Plasma 5.25.3)

Эффект «Скольжение» [больше не мерцает в многомониторных конфигурациях](https://invent.kde.org/plasma/kwin/-/merge_requests/2606) (David Edmundson, Plasma 5.25.3)

Эффекты «Карусель» и «Перелистывание» [теперь плавнее и не теряют столько кадров при использовании параметра по умолчанию «Показывать выбранное окно» на странице настройки переключения окон Параметров системы](https://bugs.kde.org/show_bug.cgi?id=449180) (Ismael Asensio, Plasma 5.25.3)

В сеансе Plasma Wayland использование глобальной комбинации клавиш для запуска приложения с отключённым откликом запуска [больше не воспроизводит анимацию запуска, как и ожидалось](https://bugs.kde.org/show_bug.cgi?id=454937) (Aleix Pol Gonzalez, Plasma 5.25.3)

Удаление крайнего левого экрана в многомониторной конфигурации [больше не приводит иногда к тому, что окна на оставшихся экранах нельзя переместить](https://invent.kde.org/plasma/kwin/-/merge_requests/2613) (Влад Загородний, Plasma 5.26)

Использование глобальной комбинации клавиш для запуска приложения с включённой опцией «Запускать в терминале» [теперь действительно запускает его в окне терминала](https://bugs.kde.org/show_bug.cgi?id=455117) (Jin Liu, Frameworks 5.96)

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2022/07/09/this-week-in-kde-big-things/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: get new [thing] → «Загрузить \[что-то\]» -->
