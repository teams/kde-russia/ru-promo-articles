# На этой неделе в KDE: щелчок по касанию

На этой неделе мы продолжили улучшать предустановки устройств ввода: в Plasma 6 [эмуляция щелчка мыши по касанию сенсорной панели теперь включена по умолчанию](https://invent.kde.org/plasma/plasma-desktop/-/issues/97)! Если вам интересно узнать о причинах, перейдите по ссылке.

Кроме того, Plasma 6 начинает собираться воедино. Вы могли заметить, что число отслеживаемых проблем в ней на этой неделе уменьшилось — это хороший знак!

## Plasma 6

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 85](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

В сеансе Plasma Wayland текст, скопированный из приложения, запущенного через слой совместимости XWayland, теперь остаётся в буфере обмена после закрытия этого приложения (David Edmundson, [ссылка](https://bugs.kde.org/show_bug.cgi?id=449909))

В сеансе Wayland диалоги, родительские окна которых отображаются на нескольких виртуальных рабочих столах, теперь всегда видны на тех же рабочих столах (Влад Загородний, [ссылка](https://bugs.kde.org/show_bug.cgi?id=398628))

Минимальный размер окна Параметров системы стал меньше, чтобы оно умещалось на экранах с низким разрешением 1366×768 и толстыми панелями (Nate Graham, [ссылка](https://bugs.kde.org/show_bug.cgi?id=395476))

Страница «Принтеры» Параметров системы была переписана на язык QML для более современного и согласованного внешнего вида и упрощения поддержки (Mike Noe, Диспетчер печати 23.12 в Plasma 6. [Ссылка](https://invent.kde.org/utilities/print-manager/-/merge_requests/55)):

![0](https://pointieststick.files.wordpress.com/2023/08/printers-page-1.jpeg)

![1](https://pointieststick.files.wordpress.com/2023/08/printers-page-2.jpeg)

Значки в главной области просмотра диспетчера файлов Dolphin теперь выглядят приятнее и сглаженнее при использовании дробного масштабирования (Kai Uwe Broulik, Dolphin 23.12 в Plasma 6. [Ссылка](https://invent.kde.org/system/dolphin/-/merge_requests/603))

## Другие улучшения пользовательского интерфейса

В Dolphin щелчок по файлу средней кнопкой мыши (колёсиком) теперь открывает его во втором по списку приоритетов приложении, то есть в первом из подменю «Открыть с помощью», а не в приложении по умолчанию для этого типа файла (Méven Car, Dolphin 23.12. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=390631))

Для файлов 3MF (3D Manufacturing Format) теперь отображаются миниатюры содержащейся в них 3D-модели (Bernhard Sulzer, Dolphin 23.12. [Ссылка](https://invent.kde.org/network/kio-extras/-/merge_requests/272))

## Другие заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Системный монитор теперь показывает правильные сведения о видеокартах на устройствах с несколькими видеокартами NVIDIA (Oliver Beard, Plasma 5.27.8. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=473424))

Настроенная задержка потухания экрана теперь соблюдается как положено (Константин Харламов, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=304696))

Запуск сеанса Wayland в виртуальной машине VirtualBox стал надёжнее (Xaver Hugl, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/4357))

В приложениях KDE, использующих библиотеку KIO, повышена надёжность копирования файлов в общие папки Samba (Kevin Ottens, Frameworks 5.110. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=454693))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Осталось 4 ошибки в Plasma с очень высоким приоритетом (на прошлой неделе было 5). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 57 (на прошлой неделе было 59). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 90 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-08-18&chfieldto=2023-08-25&chfieldvalue=RESOLVED&list_id=2453128&query_format=advanced&resolution=FIXED)

## Как вы можете помочь

Разработчики, пожалуйста, помогите исправить проблемы в [Qt 6, KDE Frameworks 6 и Plasma 6](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&f1=keywords&f2=product&list_id=2398791&o1=allwordssubstr&o2=notequals&query_format=advanced&v1=qt6&v2=neon)! Plasma 6 вполне годится для повседневного использования энтузиастами (я в их числе), хоть и всё ещё требует работы, чтобы стать готовой к выпуску до конца года.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/08/25/this-week-in-kde-tap-to-click-by-default/>  

<!-- 💡: GPU → видеокарта / графический ускоритель -->
<!-- 💡: System Monitor → Системный монитор -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icons → значки -->
<!-- 💡: thumbnail → миниатюра -->
