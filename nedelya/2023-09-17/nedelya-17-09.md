# На этой неделе в KDE: работа над Plasma 6

Эта неделя была почти целиком посвящена Plasma 6. До выпуска осталось четыре с половиной месяца, так что мы вовсю работаем, чтобы уложиться в срок!

## Plasma 6

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 87](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

Отзывчивость курсора мыши в сеансе Plasma Wayland стала ещё лучше! Теперь он ну прямо вообще отзывчивый 🙂 (Xaver Hugl, [ссылка](https://bugs.kde.org/show_bug.cgi?id=472663))

Эта работа в целом существенно снизила задержки, особенно в играх (Xaver Hugl, [ссылка](https://bugs.kde.org/show_bug.cgi?id=431707))

Комбинация клавиш по умолчанию для переключения между комнатами Plasma была изменена на Meta+A, чтобы высвободить Meta+Tab под кое-что другое в ближайшем будущем… (Niccolò Venerandi, [ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1703))

Если служба управления питанием настроена на автоматический переход в спящий режим спустя какое-то время нахождения в ждущем, вызов ждущего режима из строки поиска и запуска KRunner теперь учитывает это предпочтение (Natalie Clarius, [ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3270))

Ускорен запуск Plasma и приложений на основе Qt Quick за счёт загрузки мобильной панели редактирования текста лишь при необходимости, а не всегда (Fushan Wen, [ссылка 1](https://invent.kde.org/frameworks/qqc2-desktop-style/-/merge_requests/279) и [ссылка 2](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/865))

Диски NFS, смонтированные через fstab, больше не дублируются на панели «Точки входа» различных приложений KDE и диалогов открытия и сохранения файлов (Méven Car, [ссылка](https://bugs.kde.org/show_bug.cgi?id=474242))

Центр приложений Discover теперь красивее и понятнее отображает информацию об обновлениях системы SteamOS (Jeremy Whiting, [ссылка](https://invent.kde.org/plasma/discover/-/merge_requests/632))

Страница «О программе» Discover теперь использует новый, более привлекательный, стиль (Carl Schwan, [ссылка](https://invent.kde.org/plasma/discover/-/merge_requests/634)):

![0](https://pointieststick.files.wordpress.com/2023/09/image-5.png)

Различные диалоговые окна на основе QtWidgets со строками меню и панелями инструментов теперь используют тот же стиль объединённой области заголовка, что и обычные окна приложений KDE (Carl Schwan, [ссылка](https://invent.kde.org/plasma/breeze/-/merge_requests/349)):

![1](https://pointieststick.files.wordpress.com/2023/09/kleopatra_tools_area.png)

В библиотеку Kirigami добавлен новый компонент для заголовков областей просмотра списком и сеткой (`InlineViewHeader`), и многие места уже переведены на его использование (Nate Graham, [ссылка](https://invent.kde.org/frameworks/kirigami/-/issues/63)):

![2](https://pointieststick.files.wordpress.com/2023/09/after_slideshow_wallpaper.jpeg)

## Другие заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Утилита Spectacle теперь правильно делает снимки прямоугольных областей экрана при использовании коэффициента масштабирования менее 100% (Noah Davis, Spectacle 24.02.0. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=462860) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=473888))

Исправлен случай, когда фоновая служба сенсорной панели могла в произвольный момент завершить работу в сеансе X11 (Gabriel Souza Franco и Fushan Wen, Plasma 5.27.8. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=426937))

Устранено редкое аварийное завершение диспетчера окон KWin в сеансе Wayland при выходе из сна (Xaver Hugl, Plasma 5.27.9. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=471562))

Переключатель окон (Alt+Tab) теперь полностью доступен через стандартное средство чтения с экрана Orca в сеансе Wayland (Fushan Wen, Plasma 5.27.9. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=472643))

При обновлении большого числа изолированных приложений Flatpak Discover больше не может превысить лимит на число одновременно открытых файлов, что приводило к ошибке (Harald Sitter, Discover 5.27.9. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=474231))

Настройка автоматического отключения подсветки клавиатуры ноутбука при переходе на питание от батареи теперь работает (Nicolas Fella, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=437715))

Исправлена ​​ошибка, которая иногда могла приводить к сбою приложений, использующих библиотеку KIO, при попытке перезаписи файла (Kevin Ottens, Frameworks 5.111. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=474451))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 5 ошибок в Plasma с очень высоким приоритетом (на прошлой неделе было 4). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma остаётся 59 (на прошлой неделе было 58). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 119 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-09-08&chfieldto=2023-09-15&chfieldvalue=RESOLVED&list_id=2470592&query_format=advanced&resolution=FIXED)

## Как вы можете помочь

Разработчики, пожалуйста, помогите исправить проблемы в [Qt 6, KDE Frameworks 6 и Plasma 6](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&f1=keywords&f2=product&list_id=2398791&o1=allwordssubstr&o2=notequals&query_format=advanced&v1=qt6&v2=neon)! Plasma 6 вполне годится для повседневного использования энтузиастами (я в их числе), хоть и всё ещё требует работы, чтобы стать готовой к выпуску до конца года.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/09/15/this-week-in-kde-more-plasma-6-dev/>  

<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: Places panel → панель «Точки входа» -->
<!-- 💡: applet → виджет -->
<!-- 💡: daemon → служба -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: task manager → панель задач -->
