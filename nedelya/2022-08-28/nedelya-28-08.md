# На этой неделе в KDE: переназначение кнопок мыши

> 21–28 августа, основное — прим. переводчика

На этой неделе мы добавили поддержку [переназначения кнопок на многокнопочных мышах](https://invent.kde.org/plasma/kwin/-/merge_requests/2312)! Кнопкам можно сопоставлять нажатия отдельных клавиш или комбинаций на клавиатуре. Спасибо David Redondo, реализовавшему это для Plasma 5.26!

Но это ещё не всё: у нас полно нового, в том числе масса работы над центром приложений Discover!

## Другие новые возможности

В текстовом редакторе Kate теперь есть функция клавиатурных макросов! (Pablo Rauzy, Kate 22.12. [Ссылка](https://kate-editor.org/post/2022/2022-08-24-kate-new-features-august-2022/))

<https://kate-editor.org/post/2022/2022-08-24-kate-new-features-august-2022/videos/kate-keyboard-macros-select-commands_h264.mp4>

Discover теперь позволяет настроить, как часто он уведомляет об обновлениях. И в пределах этой частоты он стал куда менее навязчивым: больше не будет такого, что вы установили обновления, выполнили перезагрузку, а потом сразу же получаете уведомление об очередном обновлении! Наконец, этот интерфейс настройки частоты также управляет частотой автоматических обновлений, если они у вас включены. (Aleix Pol Gonzalez, Plasma 5.26. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=416193), [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=436927), [ссылка 3](https://invent.kde.org/plasma/discover/-/merge_requests/354)):

![1](https://pointieststick.files.wordpress.com/2022/08/discover-update-page.jpeg)

## Улучшения пользовательского интерфейса

Диспетчер файлов Dolphin больше не сообщает вам непонятно зачем «Пользователь отменил действие», если вы отменили операцию до её подтверждения или завершения (Kai Uwe Broulik, Dolphin 22.08.1. [Ссылка](https://invent.kde.org/system/dolphin/-/merge_requests/438))

При использовании Discover в мобильном (компактном) режиме щелчок в шторке по категории, не являющейся родительской, теперь автоматически закрывает шторку (Nate Graham, Plasma 5.25.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=457539))

Элементы списка на странице «Звуковые устройства» Параметров системы стали компактнее, так что небольшое число устройств больше не заполняет собой всю видимую область (Oliver Beard, Plasma 5.26. [Ссылка](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/125)):

![2](https://pointieststick.files.wordpress.com/2022/08/audio-kcm.jpeg)

В диалоговом окне «Открыть с помощью…» для изолированных приложений (или приложений, использующих портал) значительно улучшена навигация с помощью клавиатуры (Nate Graham, Plasma 5.26. [Ссылка](https://invent.kde.org/plasma/xdg-desktop-portal-kde/-/merge_requests/125)):

<https://i.imgur.com/4KtED4L.mp4?_=1>

На странице «Обновления» в Discover для обновлений прошивки теперь выводится правильная установленная версия, а для приложений и сред выполнения Flatpak больше не отображаются одновременно номер версии и имя ветки, что сбивало с толку в случае, когда они выглядели похоже, так что казалось, что у приложения два номера версии (Nate Graham, Plasma 5.26. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=411186), [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=458273)):

![4](https://pointieststick.files.wordpress.com/2022/08/after.jpeg)

Положение курсора мыши теперь запоминается при подключении и отключении экранов (Xaver Hugl, Plasma 5.26. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/2853))

Адресная строка в различных приложениях KDE, таких как Dolphin и просмотрщик изображений Gwenview, теперь принимает относительные пути (Ahmad Samir, Frameworks 5.98. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=353883))

Kate и другие приложения на основе компонента редактирования KTextEditor теперь поддерживают различные способы добавления дополнительных курсоров с помощью мыши для функции множественного курсора (Waqar Ahmad, KDE Frameworks 5.98. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=456733))

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Анализатор дисков Filelight снова использует правильные цвета текста с тёмной цветовой схемой и выглядит нормально при использовании дробного коэффициента масштабирования (Harald Sitter, Filelight 22.08.1. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=458274), [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=458181))

Discover теперь запрещает автоматический переход устройства в спящий режим во время установки приложений или обновлений (Aleix Pol Gonzalez, Plasma 5.24.7. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=401304))

Discover больше не зависает при запуске без подключения к сети (Aleix Pol Gonzalez, Plasma 5.25.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=454442))

На странице «Обновления» в Discover приложения и среды выполнения Flatpak больше не показывают иногда неправильный номер версии (Aleix Pol Gonzalez, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=448521))

Все приложения KDE, использующие библиотеку KIO для передачи файлов, получили приятное улучшение в виде выросшей скорости копирования, особенно в случае NFS, где копирование теперь может работать в 3-4 раза быстрее! (Méven Car, Frameworks 5.98. [Ссылка](https://invent.kde.org/frameworks/kio/-/merge_requests/957))

Другие сведения об ошибках, которые могут вас заинтересовать:

* 46 15-минутных ошибок Plasma (на прошлой неделе было 51). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 21 ошибка Plasma с очень высоким приоритетом (на прошлой неделе было 22). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 118 ошибок исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2022-08-18&chfieldto=2022-08-26&chfieldvalue=RESOLVED&list_id=2139915&query_format=advanced&resolution=FIXED)

## Изменения вне KDE, затрагивающие KDE

Стандартный диалог выбора цвета Qt теперь может брать цвета с экрана в сеансе Plasma Wayland (Harald Sitter, Qt 6.5. [Ссылка](https://bugreports.qt.io/browse/QTBUG-81538))

Снижение яркости до минимальной при использовании OLED-экрана с видеокартой Intel больше не отключает экран до перезагрузки компьютера (Jouni Högander, следующий выпуск графических драйверов Intel. [Ссылка](https://gitlab.freedesktop.org/drm/intel/-/issues/3657))

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2022/08/26/this-week-in-kde-re-bindable-mouse-buttons/>  

<!-- 💡: GPU → видеокарта / графический ускоритель -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: playlist → список воспроизведения -->
<!-- 💡: screenshot → снимок экрана -->
