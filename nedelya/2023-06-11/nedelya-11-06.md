# На этой неделе в KDE: важная связующая работа в Plasma 6

На этой неделе в Plasma 6 были [серьёзно переработаны](https://invent.kde.org/frameworks/plasma-framework/-/issues/15) основные программные интерфейсы виджетов, что призвано сделать их современнее и снизить риск внесения ошибок при разработке. Поскольку оболочка Plasma практически полностью состоит из виджетов, потребовалось множество изменений и тестов. Спустя месяц работы всё готово! Видимых для пользователей отличий нет, а вот разработчикам будет полезно знать, что нужно учесть при адаптации их виджетов к новой версии. Большинству виджетов в любом случае потребовались бы изменения для совместимости с Qt 6, так что, надеюсь, эти дополнительные правки не сильно усложнят задачу. [Руководство по переносу виджетов](https://develop.kde.org/docs/plasma/widget/porting_kf6/) уже написано. Всем этим занимался Marco Martin, а я помог ему с тестированием.

К слову, на этой неделе мы стали более организованно подходить к работе над Plasma 6. Теперь мы централизованно отслеживаем её состояние на [новой вики-странице](https://community.kde.org/Plasma/Plasma_6), перечисляющей нерешённые проблемы и заметные изменения. Кажется, я начинаю видеть свет в конце тоннеля! И хотя в Plasma 6 мне пока приходилось использовать X11, так как сеанс Wayland там ещё слишком нестабилен для моей продуктивности, сеанс X11 в Plasma 6, кажется, уже лишь немного более глючный, чем в Plasma 5. Это уже какой-никакой результат.

Чем больше людей будут тестировать и дорабатывать Plasma 6, тем лучше будет итоговый выпуск. [Нестабильное издание](https://vk.com/wall-33239_13892) [KDE Neon](https://neon.kde.org/download) теперь поставляется с Plasma 6, что делает его хорошей тестовой платформой для смельчаков. Особенно это касается тех, у кого настройки системы сильно отличаются от значений по умолчанию, а также тех, кто использует экзотическое оборудование: попробуйте Plasma 6 и отправьте нам отчёты об ошибках! Обязательно добавьте к ним пометку «qt6».

## Улучшения пользовательского интерфейса

В приложении для сканирования Skanpage теперь можно переупорядочивать страницы перетаскиванием; кроме того, были улучшены обработка комбинаций клавиш и показ ошибок (Skanpage 23.08. [Ссылка](https://invent.kde.org/utilities/skanpage/-/merge_requests/48))

Просмотрщик документов Okular больше не докучает вам при сохранении файла, удалённого с диска; теперь он просто повторно сохраняет файл, как вы и просили (Nate Graham, Okular 23.08. [Ссылка](https://invent.kde.org/graphics/okular/-/merge_requests/752))

В контекстном меню виджета «Словарь» теперь более полезные действия, и у них есть значки (Laurent Montel, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/399)):

![0](https://pointieststick.files.wordpress.com/2023/06/image-3.png)

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Строка поиска и запуска KRunner больше не даёт сбой при попытке вычислить некоторые математические выражения, да и просто при вводе чисел (Максим Романовский, Plasma 5.27.6. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=470219))

Использование дробного коэффициента масштабирования экрана в сеансе Plasma Wayland больше не вызывает повсюду артефакты в виде линий (Matthias Dahl, Plasma 5.27.6. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=465158))

В сеансе Wayland при добавлении второй раскладки клавиатуры переключатель раскладок в системном лотке теперь появляется сразу же (Marco Martin, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=449531))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 3 ошибки в Plasma с очень высоким приоритетом (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma остаётся 55 (на прошлой неделе было 52). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 70 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-06-02&chfieldto=2023-06-09&chfieldvalue=RESOLVED&list_id=2389036&query_format=advanced&resolution=FIXED)

## Автоматизация и систематизация

[Документация по оформлениям рабочего стола Plasma](https://develop.kde.org/docs/plasma/theme/) была обновлена и уточнена (Thiago Sueto, [ссылка](https://invent.kde.org/documentation/develop-kde-org/-/merge_requests/288))

## Сетевое присутствие

Растущий ассортимент страниц «KDE для вас» пополнился новой шикарной [вводной страницей](https://kde.org/ru/for/), доступной из главного меню [KDE.org](https://kde.org/ru/) (Carl Schwan):

![1](https://pointieststick.files.wordpress.com/2023/06/image-2.png)

Кроме того, появилась новая страница [«KDE для активистов»](https://kde.org/ru/for/activists/), рассказывающая, как упор KDE на конфиденциальность может помочь вам организоваться вокруг того, во что вы верите (Carl Schwan):

![2](https://pointieststick.files.wordpress.com/2023/06/image-1.png)

Обратите внимание, что она ценностно-нейтральна: программное обеспечение KDE доступно вам независимо от того, что вы считаете правильным и где видите себя на политическом спектре.

## Как вы можете помочь

Если вы разработчик, пожалуйста, **перейдите на сеанс Plasma 6 на постоянной основе** и начните исправлять ошибки, которые вы там находите. Plasma 6 годится для повседневного использования энтузиастами (я в их числе), хоть и всё ещё сырая и требует много работы, прежде чем сможет быть выпущена.

Пользователи, ищущие приключений, тоже могут попробовать Plasma 6 в [Neon Unstable](https://vk.com/wall-33239_13892). Если вы займётесь этим, не забудьте завести отчёты об ошибках с пометкой «qt6».

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/06/09/this-week-in-kde-major-plumbing-work-in-plasma-6/>  

<!-- 💡: System Monitor → Системный монитор -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: icons → значки -->
<!-- 💡: system tray → системный лоток -->
