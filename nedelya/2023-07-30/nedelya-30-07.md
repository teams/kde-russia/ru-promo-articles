# На этой неделе в KDE: звуки Plasma 6

На этой неделе мы круто преуспели в добавлении в Plasma 6 [полной поддержки звуковых тем](https://invent.kde.org/plasma/plasma-workspace/-/issues/47), а также проделали важную работу по повышению производительности диспетчера окон KWin!

## Plasma 6

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 70](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

Значительно уменьшена задержка курсора мыши при большой нагрузке в сеансе Plasma Wayland! (Xaver Hugl, [ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/3828))

В Параметрах системы появилась новая страница для выбора звуковой темы (Ismael Asensio, [ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3096)):

![0](https://pointieststick.files.wordpress.com/2023/07/image-2.png)

На странице настройки уведомлений от конкретных приложений содержимое диалога конфигурации отдельных событий было перенесено на саму страницу, чтобы улучшить связность интерфейса (Ismael Asensio, [ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3055)):

![1](https://pointieststick.files.wordpress.com/2023/07/image-3.png)

Страница «Часовые пояса» диалога настройки виджета «Цифровые часы» теперь задействует современный безрамочный стиль, что заодно решает проблему вложенной области прокрутки (Nate Graham, [ссылка 1](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/821), [ссылка 2](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1602) и [ссылка 3](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1627)):

![2](https://pointieststick.files.wordpress.com/2023/07/frameless-timezones.jpeg)

## Другие новые возможности

В текстовом редакторе Kate появилась функция синхронизации прокрутки между створками (Kate 23.12. [Ссылка](https://invent.kde.org/utilities/kate/-/merge_requests/1203))

## Другие заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Утилита Spectacle больше не закрывается сразу после её вызова для создания снимка экрана, если включены и автоматическое сохранение, и выход из программы после ручного сохранения (Noah Davis, Spectacle 23.08. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=469646))

Исправлены две причины аварийного завершения работы Plasma при откреплении или быстрой перестановке приложений, закреплённых на панели задач (Will Horne, Plasma 5.27.7. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=472378) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=472524))

Ошибочные или намеренно сформированные особым образом файлы `.desktop` больше не приводят к замене значков приложений на основе GTK в панели задач (Fushan Wen, Plasma 5.27.7. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=428559) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=472576))

Исправлено ​​недавнее ухудшение, которое могло приводить к показу неправильных миниатюр окон во всплывающих подсказках панели задач (Fushan Wen, Plasma 5.27.7. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=467426))

Страница «Вход в систему (SDDM)» Параметров системы теперь правильно вспоминает, какое изображение вы в последний раз выбрали в качестве обоев для экрана входа (Fabian Vogt, Plasma 5.27.7. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=436272))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 5 ошибок в Plasma с очень высоким приоритетом (на одну больше, чем на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma стало 62 (на прошлой неделе было 60). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 94 ошибки были исправлены на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-07-21&chfieldto=2023-07-28&chfieldvalue=RESOLVED&list_id=2430024&query_format=advanced&resolution=FIXED)

## Как вы можете помочь

Разработчики, пожалуйста, помогите исправить проблемы в [Qt 6, KDE Frameworks 6 и Plasma 6](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&f1=keywords&f2=product&list_id=2398791&o1=allwordssubstr&o2=notequals&query_format=advanced&v1=qt6&v2=neon)! Plasma 6 вполне годится для повседневного использования энтузиастами (я в их числе), хоть и всё ещё требует работы, чтобы стать готовой к выпуску до конца года.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/07/28/this-week-in-kde-sounds-like-plasma-6/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icons → значки -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: thumbnail → миниатюра -->
