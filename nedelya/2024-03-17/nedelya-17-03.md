# На этой неделе в KDE: прокачиваем Dolphin

Помимо непрекращающегося труда по стабилизации Plasma 6 и начавшейся работы над новшествами для Plasma 6.1, на этой неделе много улучшений получил диспетчер файлов Dolphin. Смотрите сами!

## Новые возможности

Виджет «Веб-браузер» теперь позволяет выбрать, загружать всегда определённую страницу или запоминать последнюю посещённую (Shubham Arora, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/548)):

![0](https://pointieststick.files.wordpress.com/2024/03/image-2.png)

В диалоговое окно выбора значков добавлен фильтр, позволяющий показывать только пиктограммы или, наоборот, скрывать все пиктограммы (Kai Uwe Broulik, Frameworks 6.1. [Ссылка](https://invent.kde.org/frameworks/kiconthemes/-/merge_requests/125)):

![1](https://pointieststick.files.wordpress.com/2024/03/image-3.png)

## Улучшения пользовательского интерфейса

Значок Dolphin снова меняется в зависимости от настроенного цвета выделения (Kai Uwe Broulik, Dolphin 24.02.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482581))

Большинство панелей в окне Dolphin теперь анимируются при появлении и скрытии (Felix Ernst, Dolphin 24.05. [Ссылка](https://invent.kde.org/system/dolphin/-/merge_requests/725)):

<https://i.imgur.com/S9FcNzq.mp4?_=1>

К некоторым папкам в Dolphin по умолчанию применяются специальные настройки просмотра, например, к «Корзине» и «Последним файлам/расположениям». Теперь эти настройки просмотра будут использоваться даже с опцией «Использовать общий режим просмотра для всех папок» (Jin Liu, Dolphin 24.05. [Ссылка](https://invent.kde.org/system/dolphin/-/merge_requests/731))

В окно настроек Dolphin добавлена новая вкладка с настройками панелей; раньше они прятались в контекстном меню. Пока там представлена ​​только панель «Сведения», но позже могут быть добавлены и другие! (Benedikt Thiemer, Dolphin 24.05. [Ссылка](https://invent.kde.org/system/dolphin/-/merge_requests/723#note_896090)):

![3](https://pointieststick.files.wordpress.com/2024/03/image-1.png)

В эмуляторе терминала Konsole улучшена работа сенсорной прокрутки (Willian Wang, Konsole 24.05. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=450440))

Доработано масштабирование текстового курсора в Konsole, особенно с дробным коэффициентом (Luis Javier Merino Morán, Konsole 24.05. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=483197))

Виджет «Управление питанием и батареей» теперь скрывает опцию «Показывать на значке процент заряда при неполной зарядке» на устройствах без батарей (Natalie Clarius, Plasma 6.0.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482089))

Для обоев типа «Слайд-шоу», показываемых не в случайном порядке, Plasma теперь запоминает последнее просмотренное изображение и начинает с него же при следующем входе в систему (Fushan Wen, Plasma 6.0.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482543))

Улучшена навигация с помощью клавиатуры по боковым панелям приложений на основе библиотеки Kirigami (Carl Schwan, Frameworks 6.1. [Ссылка](https://invent.kde.org/frameworks/kirigami/-/merge_requests/1481))

## Исправления ошибок

Устранена одна из причин чёрного экрана блокировки под X11. Их может быть больше, мы разбираемся с этим (Jakob Petsovits, Plasma 6.0.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=481308))

Исправлена ​​ошибка, из-за которой виджет «Управление питанием и батареей» мог привести к сбою Plasma (Natalie Clarius, Plasma 6.0.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=483216))

Исправлена ​​ошибка, из-за которой Plasma могла аварийно завершить работу при щелчке колёсиком по элементам панели задач или при быстрых щелчках левой кнопкой мыши по случайным задачам, воспроизводящим звук (Fushan Wen, Plasma 6.0.2, [ссылка 1](https://bugs.kde.org/show_bug.cgi?id=483027) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=482756))

В сеансе X11 панели Plasma в верхней части экрана больше не съедают щелчки мышью по распахнутым окнам (Yifan Zhu, Plasma 6.0.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482827))

В сеансе X11 снова работает подавление блокировки экрана и перехода в спящий режим (Jakub Gocoł, Plasma 6.0.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482141))

Исправлено раскрашивание большинства значков системного лотка при смешении тёмных и светлых тем. Остаётся ещё один недочёт, над ним работают (Nicolas Fella, Plasma 6.0.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482645))

Музыку из Spotify снова можно перематывать из виджета «Проигрыватель» (Fushan Wen, Plasma 6.0.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482603))

Виджеты Plasma после вызова больше не передают фокус ввода родительской панели, что решило несколько проблем (Niccolò Venerandi, Plasma 6.0.2. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2110))

Перетаскивание виджетов на панели или с них больше не приводит иногда к сбою Plasma или зависанию виджета в виде призрака на рабочем столе (Marco Martin, Plasma 6.0.3. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=483287) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=482371))

В Wayland при добавлении второй раскладки клавиатуры виджет переключателя раскладок в системном лотке теперь появляется сразу же, а не только после перезапуска Plasma (Harald Sitter, Plasma 6.0.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=449531))

Исправлена возможная ​​ошибка при сопряжении с Bluetooth-устройством (Айрат Махмутов, Plasma 6.0.3, [Ссылка](https://invent.kde.org/plasma/bluedevil/-/merge_requests/159))

В сеансе X11 диалог быстрой настройки экранов снова работает (Fushan Wen, Plasma 6.0.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482642))

Темой GTK по умолчанию снова назначается Breeze (Fabian Vogt, Plasma 6.0.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482763))

В который раз исправлено воспроизведение звука входа в систему (Harald Sitter, Plasma 6.0.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482716))

Отправка событий указателя под Wayland снова происходит старым, но более надёжным способом, что устраняет многочисленные проблемы вроде неожиданной телепортации окон и курсора при перетаскивании для распахивания или восстановления окон (Влад Загородний, Plasma 6.1. [Ссылка 1](https://invent.kde.org/plasma/kwin/-/merge_requests/5393), [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=449105) и [ссылка 3](https://bugs.kde.org/show_bug.cgi?id=459218))

Решено множество странных проблем с курсором при использовании видеокарт, неправильно реализующих поддержку переменной частоты обновления экрана (Xaver Hugl, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/5396))

Устранён источник сбоев службы портала XDG (`xdg-desktop-portal`) при входе в сеанс (David Redondo, Frameworks 6.1 [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482730))

Решены две проблемы с диалоговыми окнами загрузки материалов из сети, из-за которых они неправильно отображали ход установки и зависали после удаления (Akseli Lahtinen, Frameworks 6.1. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=483108) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=476152))

Диаграммы и графики Системного монитора теперь отображаются правильно у пользователей старых (10+ лет) встроенных видеокарт Intel (Arjen Hiemstra, Frameworks 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482988))

Больше элементов интерфейса в программном обеспечении KDE на основе QtQuick теперь перестают анимироваться, когда анимация отключена глобально, что также устраняет проблему, из-за которой подсветка кнопок Plasma исчезала при глобальном отключении анимации (Nate Graham, Frameworks 6.1. [Ссылка 1](https://invent.kde.org/plasma/libplasma/-/merge_requests/1077) и [ссылка 2](https://invent.kde.org/frameworks/qqc2-desktop-style/-/merge_requests/377#note_891690))

Другие сведения об ошибках:

* Осталось 3 ошибки в Plasma с очень высоким приоритетом (на прошлой неделе было 2). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma остаётся 36 (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 207 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-3-8&chfieldto=2024-3-15&chfieldvalue=RESOLVED&list_id=2648559&query_format=advanced&resolution=FIXED)

## Производительность и технические улучшения

Устранён источник 25-секундной задержки запуска Plasma на устройствах с отключённым или отсутствующим Bluetooth по вине KDE Connect (Simon Redman, следующий выпуск KDE Connect, но большинство дистрибутивов уже включили это исправление. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=481870))

Устранена ещё одна причина медленного запуска Plasma, связанная с использованием «Изображения дня» Bing (Fushan Wen, Plasma 6.0.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482267))

Диспетчер окон KWin теперь выполняет прямой аппаратный вывод изображения и на повёрнутые экраны (Xaver Hugl, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/5374))

## Автоматизация и систематизация

Подготовлено руководство по настройке системы непрерывной интеграции приложений KDE для публикации в магазине Windows (Ingo Klöcker, [ссылка](https://invent.kde.org/documentation/develop-kde-org/-/merge_requests/362))

Добавлено несколько автоматических тестов поведения окон, специфичных для X11 (Влад Загородний, [ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/5410))

## Как вы можете помочь

Помогите, пожалуйста, с [сортировкой отчётов об ошибках](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging)! В Bugzilla сейчас просто завал, и мы не справляемся. Подробнее [здесь](https://pointieststick.com/2024/03/09/how-you-help-with-quality/).

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

И напоследок: KDE на 99,9% обеспечивается трудом, не оплаченным KDE e.V. Если вы хотите помочь изменить это, [рассмотрите возможность пожертвования](https://kde.org/community/donations/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2024/03/15/this-week-in-kde-dolphin-levels-up/>  

<!-- 💡: Info Center → Информация о системе -->
<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: OSD → экранное уведомление -->
<!-- 💡: System Monitor → Системный монитор -->
<!-- 💡: applet → виджет -->
<!-- 💡: get new [thing] → «Загрузить \[что-то\]» -->
<!-- 💡: icon → значок -->
<!-- 💡: locations → расположения (в файловой системе) / местоположения (на карте) -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
