# На этой неделе в KDE: всё дело в приложениях

На этой неделе ваши любимые приложения KDE — диспетчер файлов Dolphin, текстовый редактор Kate, просмотрщик документов Okular, музыкальный проигрыватель Elisa, анализатор дисков Filelight и Диспетчер разделов — получили много приятных улучшений!

Параллельно продолжается работа по переносу Plasma на Qt 6. На моём ПК теперь есть работающий сеанс Plasma 6, который я использую для участия в разработке. Он пока очень шероховатый, но всё же им кое-как можно пользоваться. Теперь я понимаю, почему более опытные участники предсказывали необходимость пропустить выпуск и дать начальной версии Plasma 6 как минимум 8-месячный цикл разработки, а не 4-месячный, как обычно. Но не переживайте, всё будет!

## Новые возможности

В двухпанельном режиме Dolphin теперь есть пункты контекстного меню и сочетания клавиш для быстрого перемещения и копирования объектов в другую створку (Méven Car, Dolphin 23.08. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=356436))

На ссылки в файлах, открытых в Kate, теперь можно нажимать! Обратите внимание, что на данный момент для этого требуется вручную включить плагин «Открытие ссылок», который входит в стандартный набор, но по умолчанию отключён (Waqar Ahmed, Kate 23.08. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=93087))

## Улучшения пользовательского интерфейса

У Диспетчера разделов наконец-то появился свой собственный значок вместо заимствования значка у Filelight (Gerson Alvarado, Диспетчер разделов 23.08 и Frameworks 5.106. [Ссылка](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/258)):

![0](https://pointieststick.files.wordpress.com/2023/04/new-partition-manager-icon.jpg)

Исходный размер окна Filelight больше не слишком велик, чтобы помещаться на экране 1366×768 (Nate Graham, Filelight 23.04. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=468118))

Dolphin снова пытается предложить вам, что делать вместо запуска его через `sudo` (Nate Graham, Dolphin 23.04. [Ссылка](https://invent.kde.org/system/dolphin/-/merge_requests/527)):

![1](https://pointieststick.files.wordpress.com/2023/04/dolphin-root-message.jpg)

*«Запуск Dolphin с помощью `sudo` не поддерживается, так как это может привести к неполадкам и уязвимостям. Вместо этого установите пакет `kio-admin` из репозиториев вашего дистрибутива и используйте его для управления расположениями, принадлежащими root, через пункт „Открыть от имени администратора“ в их контекстном меню».*

Прокрутка в виджетах панели задач и переключателя рабочих столов теперь работает надёжнее, если вы иногда листаете с помощью сенсорной панели, а иногда — с помощью колёсика мыши (Prajna Sariputra, Plasma 5.27.5 [Ссылка 1](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1458) и [ссылка 2](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1457))

Контекстное меню значков системного лотка теперь можно открыть на сенсорном экране, коснувшись и удерживая их (Fushan Wen, Plasma 5.27.5. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2576))

Прокрутка над виджетами «Батарея и яркость», «Громкость» и «Проигрыватель» теперь всегда изменяет яркость и громкость в соответствии с направлением прокрутки, без учёта настройки естественного или инвертированного направления прокрутки (Влад Загородний и Nate Graham, Plasma 6.0. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=442379) и [ссылка 2](https://invent.kde.org/plasma/plasma-workspace/commit/32b8679ff268b35de98c71657776eeb0fed29a68))

Оптимизирован внешний вид страниц с описаниями классных функций Plasma в Помощнике первого запуска (Oliver Beard, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/plasma-welcome/-/merge_requests/70)):

![2](https://pointieststick.files.wordpress.com/2023/04/welcome-center-plasma-feature-page.jpg)

При отображении экрана выхода из системы по нажатию кнопки питания или Ctrl+Alt+Delete теперь по умолчанию предварительно выбирается действие «Выключить», а не «Завершить сеанс» (Nate Graham, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=467532))

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Исправлена ошибка, из-за которой Okular мог аварийно завершить работу при попытке сохранить изменения после заполнения форм в документе (Albert Astals Cid, Okular 23.04. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=467603))

При использовании Elisa на языке, отличном от английского, кнопки «Воспроизвести» и «Добавить в список воспроизведения» теперь работают сразу после запуска приложения (Matthieu Gallien, Elisa 23.04. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=467348))

Исправлено недавнее ухудшение в размере и чёткости кнопок сворачивания, распахивания и закрытия окон GTK с декорациями на стороне клиента, когда не используется масштабирование (Fushan Wen, Plasma 5.27.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=468203))

На странице «Сетевые интерфейсы» приложения Информация о системе кнопка «Обновить» теперь действительно работает (Harald Sitter, Plasma 5.27.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=468026))

Перетаскивание окон центра приложений Discover и многих других приложений на основе библиотеки Kirigami за пустые области панели инструментов теперь работает всегда, а не только на части страниц и представлений (Marco Martin, Kirigami 5.106. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=467857))

И последнее, но не менее важное: исправлена главная причина печально известной ошибки Dolphin, из-за которой папки не обновлялись сразу после изменения их содержимого другим приложением (Méven Car, Frameworks 5.106. [Ссылка](https://invent.kde.org/frameworks/kio/-/merge_requests/1239))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 13 ошибок в Plasma с очень высоким приоритетом (на прошлой неделе было 12). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma остаётся 51 (на прошлой неделе было 48). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 99 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-04-01&chfieldto=2023-04-07&chfieldvalue=RESOLVED&list_id=2329258&query_format=advanced&resolution=FIXED)

## Автоматизация и систематизация

Добавлен автоматический тест для проверки различных способов открытия объектов в Dolphin (Felix Ernst, [ссылка](https://invent.kde.org/system/dolphin/-/merge_requests/525))

При попытке изменить текст перевода в одном из git-репозиториев KDE вас теперь останавливает сценарий git hook, так как переводы обрабатываются в другом месте и вливаются в git-репозитории автоматически (Ben Cooksley, [ссылка](https://invent.kde.org/sysadmin/repo-management/-/commit/83eb2de79f03870cca46f430196d2babc49ea1bd))

## Изменения вне KDE, затрагивающие KDE

Завершение сеанса Wayland (не выключение или перезагрузка устройства, а просто возврат к экрану входа в систему) теперь работает должным образом (Fabian Vogt, SDDM 0.20. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=467547))

## Как вы можете помочь

Если вы пользователь, обновитесь до [Plasma 5.27](https://www.kde.org/announcements/plasma/5/5.27.0/)! Если обновление в вашем дистрибутиве не доступно и не предвидится, подумайте о переходе на другой дистрибутив, поставляющий программное обеспечение ближе к графикам разработчиков.

Разработчики, помогите нам исправить [известные проблемы в Plasma 5.27](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&keywords=regression%2C%20&keywords_type=allwords&list_id=2278517&query_format=advanced&version=5.26.90&version=5.27.0&version=5.27.1&version=5.27.2&version=5.27.3&version=5.27.4&version=5.27.5). Также посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этих списков — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

<!-- Временная ссылка! -->

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/04/08/this-week-in-kde-all-about-the-apps-2/>  

<!-- 💡: CSD → декорации окон на стороне клиента -->
<!-- 💡: Info Center → Информация о системе -->
<!-- 💡: Partition Manager → Диспетчер разделов -->
<!-- 💡: System Monitor → Системный монитор -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: locations → расположения (в файловой системе) / местоположения (на карте) -->
<!-- 💡: playlist → список воспроизведения -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
