# На этой неделе в KDE: двойной щелчок по умолчанию

Начнём с главного: да, это правда, по умолчанию в Plasma 6 для выбора файлов и папок будет использоваться одиночный щелчок мыши, а для открытия — двойной. Соответствующее [изменение](https://invent.kde.org/plasma/plasma-desktop/-/issues/72), предложенное мной, уже принято. Разумеется, как и раньше, это поведение настраивается.

Но это далеко не всё!

## Больше крутости в Plasma 6

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 87](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

Эффект размытия диспетчера окон KWin был полностью переписан для большей надёжности. Среди прочего, это исправляет печально известные артефакты, которые появлялись при использовании дробных коэффициентов масштабирования, а также следы от курсора у пользователей NVIDIA и некоторых видеокарт AMD (Влад Загородний, [ссылка](https://bugs.kde.org/show_bug.cgi?id=455526))

Мастер отправки отчёта об ошибке (DrKonqi), который открывается при сбое приложения KDE по нажатию на кнопку в уведомлении, был значительно упрощён и научился автоматически сообщать об аварийном завершении приложений в нашу новую систему отслеживания сбоев на основе Sentry, не требуя учётной записи Bugzilla! (Harald Sitter, [ссылка](https://invent.kde.org/plasma/drkonqi/-/merge_requests/137#note_741503)):

![0](https://pointieststick.files.wordpress.com/2023/08/image-6.png)

Страница «Автозапуск» Параметров системы теперь позволяет просматривать технические сведения о последовательностях запуска элементов, что поможет в отладке причин неожиданного поведения (Thenujan Sandramohan, [ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3066)):

![1](https://pointieststick.files.wordpress.com/2023/08/image-7.png)

При смене уровня яркости подсветки клавиатуры комбинацией клавиш (на многих ноутбуках это Fn+Пробел) теперь отображается экранное уведомление об этом (Natalie Clarius, [ссылка](https://bugs.kde.org/show_bug.cgi?id=406790))

При выключении и последующем включении подсветки клавиатуры теперь вспоминается ранее выбранный уровень яркости (Natalie Clarius, [ссылка](https://bugs.kde.org/show_bug.cgi?id=464755))

Минимальная яркость экрана теперь всегда равна 1, а минимальная яркость подсветки клавиатуры — 0, благодаря чему подсветка экрана больше никогда не будет выключаться полностью при убавлении яркости, в то время как подсветку клавиатуры всегда можно будет отключить (Nate Graham и Natalie Clarius, [ссылка 1](https://invent.kde.org/plasma/powerdevil/-/merge_requests/184) и [ссылка 2](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3166))

Всплывающее окно «Варианты виджета» было обновлено и теперь использует обычный эффект подсветки при наведении (Lukas Spies, [ссылка](https://bugs.kde.org/show_bug.cgi?id=411155))

Обои рабочего стола теперь можно поменять сразу после изменения типа комнаты (например, с «Просмотра папки» на «Рабочий стол» или обратно) (Fushan Wen, [ссылка](https://bugs.kde.org/show_bug.cgi?id=407619))

В диалоге настройки системного лотка значки, обозначающие виджеты, теперь совпадают со значками, в действительности отображаемыми лотком (Nate Graham, [ссылка](https://bugs.kde.org/show_bug.cgi?id=473278)):

![2](https://pointieststick.files.wordpress.com/2023/08/image-9.png)

## Другие улучшения пользовательского интерфейса

Структура настроек диспетчера файлов Dolphin стала логичнее (Dimosthenis Krallis, Dolphin 23.12. [Ссылка](https://invent.kde.org/system/dolphin/-/merge_requests/583))

В области заголовка диалогов настроек приложений на основе Qt Widgets теперь есть строка поиска по опциям! (Waqar Ahmed, Frameworks 6.0. [Ссылка](https://invent.kde.org/frameworks/kwidgetsaddons/-/merge_requests/191)):

![3](https://pointieststick.files.wordpress.com/2023/08/image-8.png)

В Breeze улучшен значок для файлов резервных копий (например, `.bak`) (Alexander Wilms, Frameworks 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=473014))

## Другие заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Исправлена ​​​​ошибка, иногда приводившая к падению Plasma при запуске из панели задач приложения, для которого в тот момент отображалась всплывающая подсказка (Fushan Wen, Plasma 5.27.8. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=473432))

Реализован обход странной проблемы, которая могла приводить к замедлению и аварийному завершению работы Plasma на устройствах, где часто меняется расположение экранов. В Plasma 6 этот недочёт будет полноценно устранён (Harald Sitter, Plasma 5.27.8. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=469445))

Исправлена ​​ошибка, из-за которой утилита Spectacle могла зависнуть при создании снимка экрана в сеансе Plasma X11 (Noah Davis, Plasma 5.27.8. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/4334))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Осталось 5 ошибок в Plasma с очень высоким приоритетом (на прошлой неделе было 6). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 59 (на прошлой неделе было 60). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 91 ошибка была исправлена на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-08-11&chfieldto=2023-08-18&chfieldvalue=RESOLVED&list_id=2448138&query_format=advanced&resolution=FIXED)

## Автоматизация и систематизация

Долгое время завершавшийся неудачей тест из репозитория `plasma-workspace` наконец был пройден, для чего потребовалось исправить различные штуки, которые и правда были сломаны — кто бы мог подумать! (Marco Martin, [ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3184))

Добавлены обширные автоматические тесты для генератора профилей службы управления питанием PowerDevil (Jacob Petsovits, [ссылка](https://invent.kde.org/plasma/powerdevil/-/merge_requests/204))

Теперь для принятия любого изменения в репозитории `plasma-framework`, `plasma-workspace`, `plasma-desktop` и `print-manager` требуется прохождение тестов (Harald Sitter и Nate Graham, [ссылка 1](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/851), [ссылка 2](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1666), [ссылка 3](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3133) и [ссылка 4](https://invent.kde.org/utilities/print-manager/-/merge_requests/52))

## Изменения вне KDE, затрагивающие KDE

Браузер Firefox теперь поддерживает протокол Wayland для дробного масштабирования, так что он будет плавнее работать и лучше выглядеть при использовании дробных коэффициентов масштаба (Emilio Cobos Álvarez и Robert Mader, Firefox 118. [Ссылка](https://bugzilla.mozilla.org/show_bug.cgi?id=1767142))

Устранён распространённый сбой приложений на основе Qt при отключении экрана (Axel Spörl, Qt 5.15.12 или последний срез набора правок Qt от KDE. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=461723))

Устранён серьёзный графический дефект, затрагивающий пользователей некоторых видеокарт AMD (Melissa Wen, Linux kernel 6.5. [Ссылка](https://gitlab.freedesktop.org/drm/amd/-/issues/1513))

На интегрированных графических чипах Intel ускорена операция `glCopyTexSubImage2D`, что поможет различным эффектам KWin (Святослав Пелешко, Mesa 23.3, [ссылка](https://gitlab.freedesktop.org/mesa/mesa/-/issues/9429))

## Как вы можете помочь

Разработчики, пожалуйста, помогите исправить проблемы в [Qt 6, KDE Frameworks 6 и Plasma 6](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&f1=keywords&f2=product&list_id=2398791&o1=allwordssubstr&o2=notequals&query_format=advanced&v1=qt6&v2=neon)! Plasma 6 вполне годится для повседневного использования энтузиастами (я в их числе), хоть и всё ещё требует работы, чтобы стать готовой к выпуску до конца года.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/08/18/this-week-in-kde-double-click-by-default/>  

<!-- 💡: OSD → экранное уведомление -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: icon theme → набор значков -->
<!-- 💡: location → расположение (в файловой системе) / местоположение (на карте) -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
