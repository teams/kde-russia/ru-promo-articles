# На этой неделе в KDE: разработка Plasma 6 продолжается

## Plasma 6

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 84](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

Больше наших виджетов и индикаторов системного лотка теперь всегда используют монохромный значок независимо от толщины содержащей панели, даже если у них есть цветные версии бо́льших размеров. Сюда входят виджеты «Корзина», «Просмотр папки» и «Свернуть все окна», а также индикатор таймера приготовления чая KTeaTime (Nate Graham, [ссылка 1](https://bugs.kde.org/show_bug.cgi?id=416045), [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=416041), [ссылка 3](https://bugs.kde.org/show_bug.cgi?id=473205) и [ссылка 4](https://bugs.kde.org/show_bug.cgi?id=419347))

Обновлены структура и внешний вид страницы Параметров системы с настройками уведомлений от конкретных приложений (Nate Graham, [ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3155)):

![0](https://pointieststick.files.wordpress.com/2023/08/after__per-event__top.png)

При откате к программной отрисовке у подписи виджета «Значок» теперь есть фон в виде скруглённого чёрного прямоугольника, как и у обычных значков рабочего стола (Mike Noe, [ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3162))

Интернет-соединения через Bluetooth теперь помечаются соответствующим значком (Manuel Jesús de la Fuente и Ismael Asensio, [ссылка 1](https://bugs.kde.org/show_bug.cgi?id=472429) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=409417)):

![1](https://pointieststick.files.wordpress.com/2023/08/nm-applet-bt-after.png)

У кнопки «Открыть» на панели инструментов во многих приложениях KDE на основе QtWidgets появилась небольшая стрелка сбоку, которая обеспечивает быстрый доступ к последним документам (Kai Uwe Broulik, [ссылка](https://invent.kde.org/frameworks/kconfigwidgets/-/merge_requests/209)):

![2](https://pointieststick.files.wordpress.com/2023/08/image-2.png)

Во всём программном обеспечении KDE на основе QtQuick маленькие значки, отображаемые с помощью компонента `Kirigami.Icon`, больше не будут иногда без хорошей на то причины автоматически перекрашиваться в чёрный; теперь это делается исключительно по запросу, что исправляет массу ошибок (Nate Graham, [ссылка](https://invent.kde.org/frameworks/kirigami/-/merge_requests/1192))

## Другие новые возможности

Все приложения KDE теперь поддерживают [изображения формата QOI](https://qoiformat.org/) (Ernest Gupik, Frameworks 5.110. [Ссылка](https://invent.kde.org/frameworks/kimageformats/-/merge_requests/164))

## Другие улучшения пользовательского интерфейса

Жест масштабирования щипком в просмотрщике документов Okular теперь применяется к средней точке между пальцами, как и ожидалось (Nicolas Fella, Okular 23.12. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=390707))

Простой графический редактор KolourPaint теперь позволяет увеличивать масштаб гораздо сильнее (Moritz Zwerger, KolourPaint 23.12. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=442862))

## Другие заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Утилита для создания снимков экрана Spectacle больше не вылетает при изменении настроек экранов во время её работы (Albert Astals Cid, Spectacle 23.08. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=468694))

Исправлена ​​странная ошибка, которая могла приводить к сбою Plasma при обновлении содержимого некоторыми виджетами с открытыми всплывающими окнами (Fushan Wen, Plasma 5.27.8. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=473054))

Двойное нажатие клавиши Meta для открытия и закрытия меню запуска приложений больше не крадёт фокус ввода у ранее выбранного окна (Fushan Wen, Plasma 5.27.7.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=472909))

Утилита KDESU снова работает в дистрибутиве Kubuntu (Fabian Vogt, Frameworks 5.10. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=452532))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 6 ошибок в Plasma с очень высоким приоритетом (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma остаётся 60 (на прошлой неделе было 58). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 98 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-08-04&chfieldto=2023-08-11&chfieldvalue=RESOLVED&list_id=2442188&query_format=advanced&resolution=FIXED)

## Изменения вне KDE, затрагивающие KDE

В `xdg-desktop-portal` добавлен портал `InputCapture`, что позволит приложениям вроде Barrier и Input Leap удалённо управлять вводом с клавиатуры и мыши в сеансе Wayland! В реализации от KDE поддержки нового портала ещё нет, но она скоро появится (Peter Hutterer, xdg-desktop-portal 1.18. [Ссылка](https://github.com/flatpak/xdg-desktop-portal/pull/714))

## Как вы можете помочь

Разработчики, пожалуйста, помогите исправить проблемы в [Qt 6, KDE Frameworks 6 и Plasma 6](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&f1=keywords&f2=product&list_id=2398791&o1=allwordssubstr&o2=notequals&query_format=advanced&v1=qt6&v2=neon)! Plasma 6 вполне годится для повседневного использования энтузиастами (я в их числе), хоть и всё ещё требует работы, чтобы стать готовой к выпуску до конца года.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/08/11/this-week-in-kde-plasma-6-development-continues-2/>  

<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: application launcher → меню запуска приложений -->
<!-- 💡: icon → значок -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: upstream → [компонент-]зависимость | исходный репозиторий -->
