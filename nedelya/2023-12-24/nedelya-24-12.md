# На этой неделе в KDE: праздничные исправления

Как и на прошлой неделе, наше основное внимание было сосредоточено на подготовке к февральскому мегавыпуску. Попутно у многих начались заслуженные выходные, и темп работы, понятное дело, снизился. Соответственно, это последнее еженедельное обновление в этом году. Всем хороших праздников! Отдохните и перезарядитесь, чтобы с новыми силами взяться за дело в 2024-м. 🙂

## Мегавыпуск KDE 6

> (Сюда входит всё ПО, которое будет выпущено 28 февраля: Plasma 6, библиотеки Frameworks 6 и приложения из состава Gear 24.02)

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 216](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

### Улучшения пользовательского интерфейса

Значки смартфонов в наборе Breeze были обновлены, чтобы соответствовать тому, как выглядят современные телефоны (Áron Kovács, [ссылка](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/296)):

![0](https://pointieststick.files.wordpress.com/2023/12/image-4.png)

Страница «Управление шрифтами» Параметров системы была обновлена для лучшего соответствия новому безрамочному стилю Plasma 6 (Carl Schwan, [ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3641)):

![1](https://pointieststick.files.wordpress.com/2023/12/image-7.png)

Окна, не отображаемые в панели задач и в переключателе окон (Alt+Tab), больше не видны и в эффекте «Обзор» (Akseli Lahtinen, [ссылка](https://bugs.kde.org/show_bug.cgi?id=463542))

Не изменяемые пользователем безрамочные вкладки в стиле Breeze (например, вкладки панели инструментов или страницы настроек) теперь расширяются на всё доступное по горизонтали место, поскольку нет причин этого не делать (Carl Schwan, [ссылка](https://invent.kde.org/plasma/breeze/-/merge_requests/387)):

![custom-0](tabs.png)

Улучшен контраст текста с некоторыми цветами выделения (Akseli Lahtinen, [ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3710))

Если после вставки файла в окно диспетчера файлов Dolphin файл окажется вне видимой области, область теперь будет прокручена до него (Méven Car, [ссылка](https://bugs.kde.org/show_bug.cgi?id=476670))

В просмотрщике документов Okular кнопка «Показать панель подписей» теперь также открывает боковую панель, содержащую панель подписей, если она была закрыта (Albert Astals Cid, [ссылка](https://bugs.kde.org/show_bug.cgi?id=478542))

Музыкальный проигрыватель Elisa теперь поддерживает изображения обложек в формате WebP (Jack Hill, [ссылка](https://invent.kde.org/multimedia/elisa/-/merge_requests/530))

### Исправления ошибок

> **Важное примечание:** я не упоминаю здесь исправления ошибок, не успевших дойти до пользователей; посреди большого цикла разработки Plasma их слишком много, чтобы про них писать (а вам — читать), да и вообще мало кто их видел. Тем не менее огромное спасибо всем, кто плотно занимается их исправлением!

В блокировщик экрана теперь встроена резервная тема, которая используется, если настроенная тема экрана блокировки сломана. А если вдруг по какой-то причине сама резервная тема не работает, процесс блокировки экрана теперь показывает печально известное сообщение «Работа блокировщика экрана нарушена», а не вовсе не блокирует экран, что ещё хуже (Joshua Goins, [ссылка](https://invent.kde.org/plasma/kscreenlocker/-/merge_requests/193))

В диалогах выбора файлов, открытых некоторыми сторонними приложениями на Qt, теперь будут правильно установлены фильтры имён (Nicolas Fella, [ссылка](https://bugs.kde.org/show_bug.cgi?id=470893))

При использовании дробного коэффициента масштабирования обводка окон в оформлении окон Breeze теперь рисуется без визуальных дефектов (Влад Загородний, [ссылка](https://bugs.kde.org/show_bug.cgi?id=462005))

Исправлена ​​проблема, из-за которой курсор мыши мог оставлять за собой следы при использовании дробного коэффициента масштабирования с некоторыми видеокартами, не поддерживающими аппаратные курсоры (Влад Загородний, [ссылка](https://bugs.kde.org/show_bug.cgi?id=477455))

Виджеты, на активацию которых были назначены комбинации клавиш, теперь более надёжно на них откликаются (Akseli Lahtinen, [ссылка](https://bugs.kde.org/show_bug.cgi?id=478192))

Использование памяти видеокартами NVIDIA теперь отображается в правильных единицах измерения в Системном мониторе и его виджетах (Arjen Hiemstra, [ссылка](https://bugs.kde.org/show_bug.cgi?id=477687))

Регулировка громкости Bluetooth-гарнитуры встроенными в неё кнопками теперь всегда приводит к показу экранного уведомления об изменении (Bharadwaj Raju, [ссылка](https://bugs.kde.org/show_bug.cgi?id=456164))

Полосы прокрутки меню в приложениях на основе QtQuick больше не перекрывают пункты меню (Tomislav Pap, [ссылка](https://invent.kde.org/frameworks/qqc2-desktop-style/-/merge_requests/343))

Другие сведения об ошибках:

* Осталось 3 ошибки в Plasma с очень высоким приоритетом (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma остаётся 35 (на прошлой неделе было 34). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 155 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-12-15&chfieldto=2023-12-22&chfieldvalue=RESOLVED&list_id=2557937&query_format=advanced&resolution=FIXED)

### Производительность и технические улучшения

Okular был переведён на Qt 6 (Nicolas Fella, Sune Vuorela и Carl Schwan, [ссылка](https://invent.kde.org/graphics/okular/-/merge_requests/826))

Виджет «Планшет Wacom» теперь тоже использует Qt 6 (Nicolas Fella, [ссылка](https://invent.kde.org/plasma/wacomtablet/-/merge_requests/33))

Исправлена одна из причин зависания Dolphin при просмотре медленных общих папок Samba — она была связана с медленным созданием миниатюр (Harald Sitter, [ссылка](https://bugs.kde.org/show_bug.cgi?id=478457))

Снижено использование памяти записью экрана с помощью KPipeWire. Это пока не решит полностью проблему чрезмерного использования памяти, но существенно улучшит ситуацию и предотвратит полное истощение ресурсов (Arjen Hiemstra, [ссылка 1](https://bugs.kde.org/show_bug.cgi?id=469005) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=475894))

Мастер отправки отчётов о сбоях (DrKonqi) теперь может сообщать о сбоях подсистемы управления питанием PowerDevil (Harald Sitter, [ссылка](https://invent.kde.org/plasma/powerdevil/-/merge_requests/296))

## Как вы можете помочь

Мы проводим [сбор средств, приуроченный к выпуску Plasma 6](https://kde.org/fundraisers/plasma6member/), и нам нужна ваша помощь! Благодаря вам мы пересекли отметку в 98% от цели, но пока её не достигли. Если вам нравится наша работа, пожертвование — это отличный способ нас отблагодарить. 🙂

Разработчики, пожалуйста, помогите исправить проблемы в Qt 6, KDE Frameworks 6 и Plasma 6! Plasma 6 уже вполне годится для повседневного использования энтузиастами, хоть и всё ещё требует работы, чтобы стать готовой к выпуску в феврале.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/12/22/this-week-in-kde-holiday-bug-fixes/>  

<!-- 💡: OSD → экранное уведомление -->
<!-- 💡: System Monitor → Системный монитор -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: icon theme → набор значков -->
<!-- 💡: location → расположение (в файловой системе) / местоположение (на карте) -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: thumbnail → миниатюра -->
