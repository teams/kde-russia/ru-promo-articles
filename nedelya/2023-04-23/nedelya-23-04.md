# На этой неделе в KDE: устраняем все ошибки

На этой неделе случился настоящий фестиваль исправлений недочётов: список ошибок в Plasma с очень высоким приоритетом сократился почти вдвое! Многие другие приложения тоже не остались в стороне. Да и без новых функций и улучшений интерфейса не обошлось!

## Новые возможности

Приложение для сканирования Skanpage теперь предлагает функцию «предварительного просмотра», где вы можете выбрать конкретные области изображения для сканирования или автоматически разбить изображение на две страницы, что пригодится при сканировании книг (Skanpage 23.08. [Ссылка](https://invent.kde.org/utilities/skanpage/-/merge_requests/43)):

![0](https://pointieststick.files.wordpress.com/2023/04/skanpage-with-multi-scan-areas.jpg)

Музыкальный проигрыватель Elisa теперь позволяет управлять настройками автоповтора и перемешивания по протоколу MPRIS, а значит, и через стандартный виджет Plasma «Проигрыватель» (Melissa Autumn, Elisa 23.08. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=448215))

## Улучшения пользовательского интерфейса

Несколько наименее важных инструментов редактирования убраны с боковой панели просмотрщика изображений Gwenview (их можно вызвать через меню), чтобы окно снова умещалось на экранах с разрешением 1366×768 (Nate Graham, Gwenview 23.08. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=458987))

Улучшен выбор масштаба в Gwenview: теперь можно щёлкнуть в любой точке ползунка масштаба, чтобы выбрать этот уровень масштабирования, а выпадающий список со значениями масштаба стало удобнее использовать с клавиатуры (Евгений Попов, Gwenview 23.08. [Ссылка 1](https://invent.kde.org/graphics/gwenview/-/merge_requests/192) и [ссылка 2](https://invent.kde.org/graphics/gwenview/-/merge_requests/194))

Над миниатюрами окон в панели задач Plasma теперь отображается правильная подпись для окон, в строке заголовка которых не указано имя соответствующего приложения (Nate Graham и Fushan Wen, Plasma 5.27.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=462760))

Уточнён расчёт предполагаемого времени до разрядки батареи (Stefan Brüns, Plasma 5.27.5. [Ссылка](https://invent.kde.org/plasma/powerdevil/-/merge_requests/163))

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Анализатор дисков Filelight больше не перестаёт иногда запускаться при полностью забитом диске, чего не хотелось бы от инструмента, призванного помочь расчистить дисковое пространство (Harald Sitter, Filelight 23.04.1. [Ссылка](https://invent.kde.org/utilities/filelight/-/merge_requests/81))

В сеансе Plasma X11 функция «Открыть содержащую папку» в различных приложениях теперь учитывает текущие виртуальный рабочий стол и комнату, так что существующий экземпляр диспетчера файлов Dolphin с другого виртуального рабочего стола или из другой комнаты больше не будет неожиданно переиспользоваться. Поддержка Wayland появится позже, так как требует более глубоких изменений (Méven Car, Dolphin 23.04.1. [Ссылка](https://invent.kde.org/system/dolphin/-/merge_requests/529))

В Skanpage кнопки поворота изображения теперь работают в ожидаемых направлениях, а список языков для распознавания текста при необходимости прокручивается (Nate Graham, Skanpage 23.04.1. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=464270) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=468522))

Быстрый повторный вызов режима выделения в Dolphin больше не приводит к его сбою (Felix Ernst, Dolphin 23.08. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=468548))

Месяцы на одноимённой вкладке виджета календаря Plasma больше не теряют иногда загадочным случайным образом свои названия (Harald Sitter, Plasma 5.27.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=463080))

Исправлены несколько причин аварийного завершения работы Plasma в определённых многомониторных конфигурациях (Harald Sitter, Plasma 5.27.5. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=468430) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=466960))

Исправлен коварный недочёт в интерфейсе импорта конфигураций VPN-соединений, из-за которого добавленные соединения не сохранялись на диск, если заодно не менялась какая-либо другая настройка (Nicolas Fella, Plasma 5.27.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=468666))

Режимы ускорения мыши теперь работают правильно при использовании библиотеки Libinput версии 1.3 или новее (Илья Кац, Plasma 5.27.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=468217))

Исправлено множество существенных ошибок на странице «Права доступа пакетов Flatpak» Параметров системы: она больше не генерирует иногда неверные переопределения; поддержка пользовательских переменных среды теперь работает достаточно хорошо, так что мы снова её включили; добавление новых расположений файловой системы больше не влияет иногда на состояние других элементов списка; опция «Чтение и запись» для «Всех файлов пользователя» больше не исчезает в некоторых случаях (Иван Ткаченко, Plasma 5.27.5. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=465502), [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=464876), [ссылка 3](https://bugs.kde.org/show_bug.cgi?id=466877) и [ссылка 4](https://bugs.kde.org/show_bug.cgi?id=465343))

Исправлена ошибка, иногда приводившая к несоблюдению настроенных вами приоритетов экранов (Harald Sitter, Plasma 5.27.5. [Ссылка](https://invent.kde.org/plasma/libkscreen/-/merge_requests/123))

Решена проблема, из-за которой приложения и Plasma могли зависать при одновременном перемещении большого числа файлов (Harald Sitter, Frameworks 5.106. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=358231))

Plasma больше не падает при воспроизведении некоторых видео на сайте YouTube при наличии настроенной службы интеграции браузера с Plasma, иногда также при использовании прокси-сервера (Fushan Wen, Frameworks 5.106. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=465454))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Осталось 6 ошибок в Plasma с очень высоким приоритетом (на прошлой неделе было 11). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 47 (на прошлой неделе было 50). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 136 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-04-14&chfieldto=2023-04-21&chfieldvalue=RESOLVED&list_id=2342570&query_format=advanced&resolution=FIXED)

## Как вы можете помочь

Если вы пользователь, обновитесь до [Plasma 5.27](https://www.kde.org/announcements/plasma/5/5.27.0/)! Если обновление в вашем дистрибутиве не доступно и не предвидится, подумайте о переходе на другой дистрибутив, поставляющий программное обеспечение ближе к графикам разработчиков.

Разработчики, помогите нам исправить [известные проблемы в Plasma 5.27](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&keywords=regression%2C%20&keywords_type=allwords&list_id=2278517&query_format=advanced&version=5.26.90&version=5.27.0&version=5.27.1&version=5.27.2&version=5.27.3&version=5.27.4&version=5.27.5). Также посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этих списков — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

<!-- Временная ссылка! -->

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/04/21/this-week-in-kde-stomping-all-the-bugs/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: task manager → панель задач -->
