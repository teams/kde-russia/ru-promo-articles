# На этой неделе в KDE: сходимся к выпуску

Невероятно, но факт: до Мегавыпуска осталось уже меньше месяца! Все силы брошены на исправление ошибок и доработку. Впрочем, для грядущих выпусков Plasma и приложений KDE тоже постепенно накапливаются приятные улучшения.

## Мегавыпуск KDE 6

> (Сюда входит всё ПО, которое будет выпущено 28 февраля: Plasma 6, библиотеки Frameworks 6 и приложения из состава Gear 24.02)

### Улучшения пользовательского интерфейса

В сеансе Plasma Wayland окна, лежащие на пути перетаскиваемого файла, теперь поднимаются на передний план только при задержке курсора над ними на секунду — раньше это происходило уже через четверть секунды (Xaver Hugl, [ссылка](https://bugs.kde.org/show_bug.cgi?id=480511))

Скрытые панели в режимах видимости «Автосокрытие» и «Скрывать под окнами» больше не появляются самопроизвольно при выходе из ждущего режима или изменении параметров экрана (Влад Загородний, [ссылка 1](https://bugs.kde.org/show_bug.cgi?id=474699) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=448420))

Ещё немного улучшено ранжирование поисковой выдачи KRunner (Alexander Lohnau, [ссылка](https://bugs.kde.org/show_bug.cgi?id=478087))

Серая канавка, появлявшаяся при наведении курсора на полосы прокрутки, была удалена, поскольку из-за и так всегда видимой рамки не несла никакого дополнительного смысла. В будущем, если мы избавимся от этой рамки, канавка может вернуться (Akseli Lahtinen, [ссылка 1](https://invent.kde.org/plasma/breeze/-/merge_requests/400) и [ссылка 2](https://invent.kde.org/plasma/libplasma/-/merge_requests/1043)):

![0](https://pointieststick.files.wordpress.com/2024/02/image-4.png)

При использовании оформления рабочего стола или цветовой схемы «Breeze, тёмный вариант» всплывающие окна виджетов Plasma теперь появляются без белой вспышки (Niccolò Venerandi, [ссылка](https://bugs.kde.org/show_bug.cgi?id=455173))

Виджеты системного лотка «Буфер обмена» и «Состояние фиксации режимов клавиш» теперь полностью исчезают, когда им нечего вам показать, вместо того чтобы прятаться в расширенной части лотка и показывать сообщение в духе «Тут ничего нет, кыш» при нажатии (Jin Liu, [ссылка 1](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3850) и [ссылка 2](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/533))

Цвет и толщина линий-разделителей и контуров стали единообразными во всём программном обеспечении KDE с оформлением Breeze (Akseli Lahtinen и Marco Martin, [ссылка 1](https://invent.kde.org/plasma/breeze/-/merge_requests/395), [ссылка 2](https://invent.kde.org/plasma/breeze/-/merge_requests/396), [ссылка 3](https://invent.kde.org/frameworks/kirigami/-/merge_requests/1453), [ссылка 4)](https://invent.kde.org/frameworks/qqc2-desktop-style/-/merge_requests/361), [ссылка 5](https://bugs.kde.org/show_bug.cgi?id=438175))

### Исправления ошибок

> **Важное примечание:** я не упоминаю здесь исправления ошибок, не успевших дойти до пользователей; посреди большого цикла разработки Plasma их слишком много, чтобы про них писать (а вам — читать), да и вообще мало кто их видел. Тем не менее огромное спасибо всем, кто плотно занимается их исправлением!

Устранён сбой Plasma при присвоении виртуальному рабочему столу несуразно длинного имени (Влад Загородний, [ссылка](https://bugs.kde.org/show_bug.cgi?id=480614))

Комбинацию клавиш Alt+Print Screen теперь можно настроить на вызов глобального действия (Yifan Zhu, [ссылка](https://bugs.kde.org/show_bug.cgi?id=386253))

Починили импорт страниц в Системном мониторе (Arjen Hiemstra, [ссылка](https://bugs.kde.org/show_bug.cgi?id=480160))

В оформлениях рабочего стола Plasma внешний вид всплывающих подсказок теперь определяется SVG-файлом всплывающих подсказок, а не диалогов, что было довольно странно (David Edmundson, [ссылка](https://bugs.kde.org/show_bug.cgi?id=415601))

Внесено множество исправлений во внешний вид окон при использовании дробных коэффициентов масштабирования (Akseli Lahtinen и Kai Uwe Broulik, [ссылка 1](https://bugs.kde.org/show_bug.cgi?id=480533), [ссылка 2](https://invent.kde.org/plasma/breeze/-/merge_requests/418), [ссылка 3](https://invent.kde.org/frameworks/kwidgetsaddons/-/merge_requests/236) и [ссылка 4](https://bugs.kde.org/show_bug.cgi?id=478080))

Исправлен внешний вид контекстных меню, в которых есть пункты с многострочным текстом, в том числе меню индикаторов системного лотка (Илья Бизяев, [ссылка](https://bugs.kde.org/show_bug.cgi?id=477299))

Программа командной строки `kinfo` теперь правильно определяет используемую графическую платформу (X11 или Wayland) (Harald Sitter, [ссылка](https://bugs.kde.org/show_bug.cgi?id=476012))

Другие сведения об ошибках:

* Остаётся 4 ошибки в Plasma с очень высоким приоритетом (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 32 (на прошлой неделе было 34). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 149 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-1-26&chfieldto=2024-2-2&chfieldvalue=RESOLVED&list_id=2599979&query_format=advanced&resolution=FIXED)

### Производительность и технические улучшения

Исправлена ​​ошибка, из-за которой диспетчер окон KWin мог полностью загружать одно ядро процессора всякий раз, когда что-либо использовало PipeWire для захвата видео с экрана — а это и миниатюры окон в панели задач, и эффект «Обзор» (Xaver Hugl, [ссылка](https://bugs.kde.org/show_bug.cgi?id=469777))

Меню запуска приложений теперь значительно быстрее переключается между категориями приложений при быстром перемещении курсора между ними (David Edmundson и Nicolas Fella, [ссылка 1](https://bugs.kde.org/show_bug.cgi?id=435600) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=480366))

Ускорен просмотр смонтированных сетевых файловых систем (Сергей Катунин, [ссылка 1](https://invent.kde.org/frameworks/kio/-/merge_requests/1532) и [ссылка 2](https://invent.kde.org/frameworks/kcoreaddons/-/merge_requests/421))

Свойство окна «Закрываемое» в правилах KWin теперь работает в сеансе Wayland (Влад Загородний, [ссылка](https://bugs.kde.org/show_bug.cgi?id=443129))

## После Мегавыпуска

Инструмент «Текст» в утилите для создания снимков экрана Spectacle теперь позволяет вставлять переносы строк (Noah Davis, Spectacle 23.05. [Ссылка](https://invent.kde.org/graphics/spectacle/-/merge_requests/325))

Калькулятор KCalc перешёл на современный безрамочный стиль (Carl Schwan, KCalc 24.05. [Ссылка](https://invent.kde.org/utilities/kcalc/-/merge_requests/75)):

![1](https://pointieststick.files.wordpress.com/2024/02/image-2.png)

Виджет «Погода» теперь отображает вероятность осадков для всех поставщиков метеоданных, кроме Би-би-си, не предоставляющего этих сведений (думаю, в Великобритании можно просто считать, что вероятность осадков равна 100% в любой точке страны) (Ismael Asensio, Plasma 6.1. [Ссылка 1](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3833) и [ссылка 2](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/532)):

![2](https://pointieststick.files.wordpress.com/2024/02/image.png)

Виджет системного лотка «Громкость» теперь использует более простые названия для устройств ввода и вывода звука (Harald Sitter, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/239)):

![3](https://pointieststick.files.wordpress.com/2024/02/image-3.png)

Кнопки уведомлений о ходе выполнения операций над файлами стали понятнее (Oliver Beard, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3765)):

![4](https://pointieststick.files.wordpress.com/2024/02/clearer-button-arrangement.jpeg)

Экран завершения работы, вызванный определённым действием (например, «Выключить»), теперь будет показывать вам только это действие и кнопку отмены (Nate Graham, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=478022)):

![5](https://pointieststick.files.wordpress.com/2024/02/screenshot_20240202_100452.jpeg)

Меню, открываемое щелчком правой кнопкой мыши по панели, теперь содержит пункт «Настройка панели», более понятный, чем «Режим редактирования» (Marco Martin, [ссылка](https://bugs.kde.org/show_bug.cgi?id=480044))

## Как вы можете помочь

Спасибо всем, кто поучаствовал в [сборе средств, приуроченном к выпуску Plasma 6](https://kde.org/fundraisers/plasma6member/) — он крайне успешен! Я думал, что 500 новых спонсоров — это слишком оптимистичная цель для фонда KDE e.V., но вы показали мне, что я, к счастью, ошибался. Сейчас их уже **738**, и это число продолжает расти. Благодарю всех, кто поверил в нас, — мы постараемся не подвести! Если вам нравится наша работа, пожертвование — это отличный способ нас отблагодарить. 🙂

Разработчики, пожалуйста, помогите исправить проблемы в Qt 6, KDE Frameworks 6 и Plasma 6! Plasma 6 уже вполне годится для повседневного использования, хоть и всё ещё требует работы, чтобы стать готовой к выпуску в феврале.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2024/02/02/this-week-in-kde-converging-on-a-release/>  

<!-- 💡: GPU → видеокарта / графический ускоритель -->
<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Monitor → Системный монитор -->
<!-- 💡: applet → виджет -->
<!-- 💡: application launcher → меню запуска приложений -->
<!-- 💡: location → расположение (в файловой системе) / местоположение (на карте) -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: thumbnail → миниатюра -->
