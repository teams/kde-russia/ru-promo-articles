# На этой неделе в KDE: явная синхронизация

На этой неделе была добавлена важная функция: [поддержка явной синхронизации](https://invent.kde.org/plasma/kwin/-/merge_requests/4693) под Wayland!

Для чего она нужна? Если коротко, то благодаря ей приложения могут сообщать компоновщику, в какой момент отображать кадры на экране, что позволяет снизить задержку и уменьшить число графических артефактов. Особенно работа этой функции должна быть заметна на видеокартах NVIDIA, которые поддерживают только такой способ отрисовки. Отсутствие его поддержки в Wayland было самой распространённой причиной артефактов и подтормаживаний.

Этим занимался Xaver Hugl, результаты его усилий доступны в версии Plasma 6.1. Подробнее можно почитать в [его блоге](https://zamundaaa.github.io/wayland/2024/04/05/explicit-sync.html).

Это серьёзное, но в первую очередь техническое достижение. А ведь на этой неделе было ещё множество улучшений интерфейса и исправлений ошибок.

> 7–14 апреля, основное — прим. переводчика

## Улучшения пользовательского интерфейса

Повышено визуальное качество снимков экрана, которые утилита Spectacle делает при использовании нескольких экранов с разными коэффициентами масштабирования (Noah Davis, Spectacle 24.05. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=478426))

Улучшено качество размытия, пикселизации и теней при редактировании снимков экрана в Spectacle (Noah Davis, Spectacle 24.05. [Ссылка](https://invent.kde.org/graphics/spectacle/-/merge_requests/339))

Текстовый редактор KWrite теперь по умолчанию показывает меню-гамбургер вместо традиционной строки меню. Для быстрого доступа к популярным командам в верхней части нового меню находится тщательно подобранный набор действий (Nathan Garside и Christoph Cullmann, KWrite 24.05. [Ссылка 1](https://invent.kde.org/utilities/kate/-/merge_requests/1422), [ссылка 2](https://invent.kde.org/utilities/kate/-/merge_requests/1445) и [ссылка 3](https://invent.kde.org/utilities/kate/-/merge_requests/1446)):

![0](https://pointieststick.files.wordpress.com/2024/04/image-3.png)

*Меню изменилось только в KWrite! В текстовом редакторе Kate всё по-прежнему.*

В диалогах загрузки материалов из сети изменилось предупреждение, показываемое вверху окна. Теперь, если доступное содержимое может запускать исполняемый код в системе, это будет более очевидно (David Edmundson и Nate Graham, Plasma 6.1 и приложения Gear 24.05. [Ссылка 1](https://invent.kde.org/frameworks/knewstuff/-/merge_requests/309) и [ссылка 2](https://invent.kde.org/frameworks/knewstuff/-/merge_requests/310)):

![1](https://pointieststick.files.wordpress.com/2024/04/image-4.png)

В традиционном виджете Plasma «Панель задач» порог отображения текста снижен, поэтому теперь текст будет отображаться и на довольно узких плитках задач. Предполагается, что если вы используете этот виджет, то вы хотите видеть текст! (Nate Graham, Plasma 6.0.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=483070))

В сеансе Wayland эффект «Затемнение основного окна» (когда открыто дочернее) теперь временно отключается, если диалоговое окно представляет собой инструмент выбора цвета с экрана (Иван Ткаченко, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=172921))

На странице настройки виртуальных рабочих столов теперь используется стандартный подход «элементы управления — в строке заголовка», что позволяет разместить больше содержимого (Jakob Petsovits, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/5572)):

![2](https://pointieststick.files.wordpress.com/2024/04/screenshot_20240412_052048.jpeg)

В виджете Plasma «Управление питанием и батарея» элементы управления и интерфейс для функции запрета блокировки экрана и перехода в спящий режим переместились из заголовка в основную область, что соответствует расположению других интерактивных элементов управления и позволяет не перегружать заголовок (когда запрет наложен несколькими приложениями) (Natalie Clarius, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4162)):

![3](https://pointieststick.files.wordpress.com/2024/04/image-6.png)

Системный монитор при наведении курсора на сокращённый фрагмент в таблице теперь отображает подсказку с полным текстом (Joshua Goins, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482785))

Размер всплывающих окон с папками, вызываемых из панели Plasma, теперь можно изменить, если нужно больше места для просмотра их содержимого (Иван Ткаченко, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=483692))

Если в традиционной панели задач Plasma вам больше нравился старый стиль запуска приложений (когда значок закреплённого приложения при запуске исчезает), теперь его можно вернуть (Niccolò Venerandi, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482707))

Радиус закругления углов во всех элементах интерфейса в теме Breeze теперь одинаковый и составляет 5 пикселей, и это шаг вперёд: до этого радиус мог принимать значения от 2 до 5 пикселей, а иногда даже 8 и 16 пикселей! Это [тривиальный вопрос](https://ru.wikipedia.org/wiki/Закон_тривиальности), поэтому воздержитесь от дебатов — для начала посмотрите все снимки экрана в этой статье, на которых это изменение отражено, и признайтесь: они вам казались вполне себе красивыми, пока я не сказал про закругление углов. Правда же? 😁 (Akseli Lahtinen, Plasma 6.1 и Frameworks 6.2. [Ссылка](https://invent.kde.org/teams/vdg/issues/-/issues/45))

## Исправления ошибок

В режиме просмотра «Таблица» диспетчера файлов Dolphin при открытии подкаталога текущий режим сортировки больше не сбрасывается (Felix Ernst, Dolphin 24.02.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=480246))

Gwenview больше не запрещает блокировку экрана и переход в спящий режим при обычном просмотре изображений (Daniel Strnad, Gwenview 24.02.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=481481))

Исправлена неприятная ошибка, из-за которой слой затенения, отображаемый утилитой Spectacle при захвате прямоугольной области, оставался на экране, если перед этим выполнить запись экрана и не закрыть утилиту с последующим перезапуском (Noah Davis, Spectacle 24.05. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=481471))

Plasma больше не завершает работу аварийно при закрытии уведомления, которое даёт возможность отменить удаление виджета «Значок» (Nicolas Fella, Plasma 6.0.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=474385))

Центр приложений Discover на странице «Установленные» снова показывает всё, что было установлено, а не лишь подмножество пакетов, зависящее от используемого дистрибутива (Harald Sitter, Plasma 6.0.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=477773))

Если в меню запуска приложений прокрутить вниз одну категорию приложений, а затем переключиться на другую, то на новой странице больше не появляются случайные элементы из предыдущей категории (David Redondo, Plasma 6.0.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=445512))

В сеансе Wayland диалоговое окно выбора цвета больше не выдаёт неправильные цвета, если к экрану применён профиль ICC (Xaver Hugl, Plasma 6.0.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=484497))

Исправлена странная проблема в Plasma, из-за которой элементы во всплывающих окнах просмотра папок (в режиме списка) на панели можно было выделить только при перемещении курсора вниз, но не вверх (Akseli Lahtinen, Plasma 6.0.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=483581))

Исправлена ошибка, из-за которой при определённых обстоятельствах можно было перетащить окно полностью за пределы экрана (Yifan Zhu, Plasma 6.0.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=485337))

В сеансе Wayland меню буфера обмена, которое открывается при нажатии Meta+V, больше не перекрывается окнами с настройкой «Поддерживать поверх других» (Tino Lorenz, Plasma 6.0.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=462181))

В эффекте «Обзор» диспетчера окон KWin миниатюры неактивных виртуальных рабочих столов теперь не статичные, а динамичные (David Redondo, Plasma 6.0.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=456280))

Исправлена проблема с созданием новых VPN WireGuard (Stephen Robinson, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=479179))

Исправлена ошибка, из-за которой на странице «Мышь» Параметров системы клавиатуры иногда отображались как мыши (Nate Graham, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=465957))

Быстрое включение и выключение параметра «Плавающая» для панели Plasma больше не приводит к застреванию панели в положении, в котором она не «плавает», но при этом отделена от края экрана (Влад Загородний, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=481800))

Исправлена проблема, из-за которой приложениям KDE могло не хватить памяти, если они пытались открыть некорректные файлы изображений определённых типов (Mirco Miranda, Frameworks 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=479612))

Устранён самый распространённый сбой в службе индексирования файлов Baloo: на момент написания статьи было 113 дубликатов сообщения об ошибке (Christoph Cullmann, Frameworks 6.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=389848))

Исправлена редкая ошибка, из-за которой подсистема ввода-вывода KIO могла исчерпать всю память в результате неудачной попытки обработать HTTP-запрос при определённых (не самых обычных) обстоятельствах (Harald Sitter, Frameworks 6.2. [Ссылка](https://invent.kde.org/frameworks/kio/-/merge_requests/1603))

Для файлов, просматриваемых по протоколу WebDAV через Dolphin и другие приложения KDE, время изменения снова отображается правильно (Fabian Vogt, Frameworks 6.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=484671))

Другие сведения об ошибках:

* Остаётся 3 ошибки в Plasma с очень высоким приоритетом (на прошлой неделе было 2). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma — 36 (на прошлой неделе было 34). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 145 ошибок было исправлено на этой неделе во всём программном обеспечении KDE! [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-4-05&chfieldto=2024-4-12&chfieldvalue=RESOLVED&list_id=2680830&query_format=advanced&resolution=FIXED)

## Производительность и технические улучшения

Изменения в Qt привели к проблеме при смене цветовых схем, однако теперь это действие снова происходит почти мгновенно, что даёт эффекту KWin «Плавные переходы» достаточно времени, чтобы обеспечить аккуратную смену цветов (Kai Uwe Broulik, Frameworks 6.2. [Ссылка](https://invent.kde.org/frameworks/qqc2-desktop-style/-/merge_requests/398))

Файлы и папки, копируемые или перетаскиваемые с рабочего стола, теперь доступны через систему порталов, поэтому размещение их в изолированных приложениях работает должным образом (Karol Kosek, Plasma 6.0.4. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2053))

## Как вы можете помочь

Сообщество KDE стало важным явлением в мире — благодаря вашему труду и вложенному времени! Однако по мере роста проекта важно обеспечить стабильность работы над ним, а это значит, что труд должен *оплачиваться*. Сегодня KDE по большей части использует работу людей, которым фонд KDE e.V. не платит, и это проблема. Мы предприняли шаги, которые позволят изменить ситуацию, и наняли [платных технических подрядчиков](https://ev.kde.org/corporate/staffcontractors/), однако из-за ограниченности финансовых ресурсов этого всё ещё мало. Если вы хотите помочь, [сделайте пожертвование](https://kde.org/ru/community/donations/)!

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* коммерческий  
*Источник:* <https://pointieststick.com/2024/04/12/this-week-in-kde-explicit-sync/>  
