# На этой неделе в KDE: предвкушаем Plasma 6.1

На этой неделе я хотел бы напомнить о классной функции, доступной в нашем проверенном временем обработчике ошибок DrKonqi начиная с Plasma 6: автоматической отправке отчётов о сбоях. Автоматическая отправка отчётов о сбоях — дело добровольное, но уже многие пользователи включили эту функцию и поделились с нами информацией, дающей куда более полную и правдивую картину сбоев, чем когда-либо могла Bugzilla! Благодаря этой системе за прошедшую неделю нам удалось исправить как минимум три серьёзных ошибки: с двумя справился Fushan Wen ([ссылка 1](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4156#note_906400) и [ссылка 2](https://invent.kde.org/plasma/plasma-nm/-/merge_requests/340)), а с третьей — Влад Загородний ([ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/5493)). Возможно, исправлений было ещё больше, а я упустил их из виду.

Эти отчёты не уходят в пустоту, они имеют большое значение. Если вы сталкиваетесь с аварийным завершением программы, пожалуйста, используйте функцию автоматической отправки отчётов о сбоях в DrKonqi!

Кроме того, было проделано немало технической работы, в том числе по повышению производительности, особенно в центре приложений Discover и индексаторе файлов Baloo. Наконец, мы приступили к внедрению новшеств и доработке пользовательского интерфейса к Plasma 6.1.

> 24–31 марта, основное — прим. переводчика

## Новые возможности

Виджет «Управление питанием и батарея» теперь реагирует на среднюю кнопку и колёсико мыши: щелчком средней кнопки можно включить или отключить автоматическую блокировку экрана и переход в спящий режим, а с помощью колёсика — сменить активный профиль энергопотребления (Natalie Clarius, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3557))

## Улучшения пользовательского интерфейса

При отключении блокировки экрана и перехода в спящий режим вручную в виджете «Управление питанием и батарея» теперь отображается соответствующий значок (Natalie Clarius, Plasma 6.1. [Ссылка 1](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4159) и [ссылка 2](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/343)):

![0](https://pointieststick.files.wordpress.com/2024/03/image-8.png)

Скорость анимации при открытии и закрытии раскрывающихся элементов списков в виджетах системного лотка Plasma теперь соответствует общесистемной скорости анимации; заодно анимации стали немного быстрее и ощущаются отзывчивее (Nate Graham, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=477092))

## Исправления ошибок

Исправлена ошибка, часто приводившая к аварийному завершению работы Discover при обновлении содержимого KNewStuff (Harald Sitter, Plasma 6.0.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=473472))

Исправлено несколько взаимосвязанных ошибок, из-за которых при выходе из спящего режима или входе в систему на компьютерах с видеокартой AMD и несколькими мониторами панели переключались на другой экран (David Redondo, Plasma 6.0.3. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=483102), [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=470434) и [ссылка 3](https://bugs.kde.org/show_bug.cgi?id=483348))

В виджете «Настройка экранов» снова можно отключить режим презентации тем же способом, которым он был включён (Natalie Clarius, Plasma 6.0.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=477355))

При синхронизации параметров с экраном входа в систему SDDM на компьютерах, где SDDM запускается в диспетчере окон KWin как сервере Wayland, а не в Xorg как сервере X, теперь также синхронизируются коэффициент масштабирования и расположение экранов (Xaver Hugl, Plasma 6.0.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=467039))

Исправлено несколько мелких глюков с отображением миниатюр окон в панели задач: в некоторых случаях миниатюры обрезались, смещались или не отображались вовсе (Влад Загородний, Plasma 6.0.3. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2142))

Исправлена ошибка, которая при определённых обстоятельствах приводила к аварийному завершению работы Discover при запуске (Harald Sitter, Plasma 6.0.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=481993))

В сеансе X11 страницу «Все рабочие столы» эффекта «Обзор» теперь можно закрыть той же комбинацией клавиш, с помощью которой она была открыта (по умолчанию Meta+G) (Niccolò Venerandi, Plasma 6.0.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482931))

Исправлена ошибка, из-за которой Plasma могла аварийно завершить работу при изменении расположения панели в определённых конфигурациях (Fushan Wen, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=483762))

Исправлена ошибка, из-за которой индексатор файлов Baloo мог аварийно завершить работу при создании либо переименовании файлов или папок (Méven Car, Frameworks 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=478854))

Другие сведения об ошибках:

* Остаётся 4 ошибки в Plasma с очень высоким приоритетом (на прошлой неделе было 3). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma — 35 (на прошлой неделе было 34). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 209 ошибок было исправлено на этой неделе во всём программном обеспечении KDE! [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-3-22&chfieldto=2024-3-29&chfieldvalue=RESOLVED&list_id=2665693&query_format=advanced&resolution=FIXED)

## Производительность и технические улучшения

Discover теперь намного быстрее показывает отзывы о приложениях, особенно сразу после его запуска (Harald Sitter, Plasma 6.0.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=477978))

Discover также стал быстрее выводить сведения о крупных автономных обновлениях (Harald Sitter, Plasma 6.0.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=464467))

Индексатор файлов Baloo больше не пытается индексировать содержимое временно подключённых файловых систем, например, сетевых папок и накладных файловых систем (overlayfs) (Adam Fontenot, Frameworks 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460509))

Список последних файлов, сохраняемый на диск диалогами открытия и сохранения файлов и другими потребителями `KFileWidget`, теперь записывается в файл конфигурации изменяющихся данных, а не в файл конфигурации с пользовательскими настройками (Nicolas Fella, Frameworks 6.1. [Ссылка](https://invent.kde.org/frameworks/kio/-/merge_requests/1590))

## Как вы можете помочь

Помогите, пожалуйста, с [сортировкой отчётов об ошибках](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging)! В Bugzilla всё ещё завал, и мы будем благодарны за помощь.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

И напоследок: KDE на 99,9% обеспечивается трудом, не оплаченным KDE e.V. Если вы хотите помочь изменить это, [рассмотрите возможность пожертвования](https://kde.org/community/donations/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2024/03/29/this-week-in-kde-looking-forward-towards-plasma-6-1/>  

<!-- 💡: GPU → видеокарта / графический ускоритель -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: icons → значки -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: thumbnail → миниатюра -->
<!-- 💡: venerable → древний | почтенный -->
