# На этой неделе в KDE: дальше исправляем Wayland

Не опять, а снова — на этой неделе мы продолжили устранять недочёты в работе сеанса Wayland. Но не только это: есть приятные доработки интерфейса в некоторых приложениях KDE, а на фоне вовсю идёт переход на Qt 6. Смотрите сами!

## Улучшения пользовательского интерфейса

Стартовый экран архиватора Ark теперь предлагает больше функций, опираясь на аналогичный экран в текстовом редакторе Kate (Евгений Попов, Ark 23.04. [Ссылка](https://invent.kde.org/utilities/ark/-/merge_requests/147)):

![0](https://pointieststick.files.wordpress.com/2023/03/new-ark-welcome-view.png)

Музыкальный проигрыватель Elisa теперь отображает пункт меню «Выход» в меню-гамбургере при использовании значка в системном лотке, правильно возвращается к предыдущему состоянию окна при выходе из полноэкранного режима и сбрасывает бегунок позиции воспроизведения при очистке пользователем списка воспроизведения (Никита Карпей и Nate Graham, Elisa 23.04. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=466835), [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=467356) и [ссылка 3](https://bugs.kde.org/show_bug.cgi?id=467352))

Стандартные настройки панели инструментов просмотрщика документов Okular были немного изменены, и теперь она по умолчанию содержит меню «Режим просмотра», а также показывает кнопки масштабирования и просмотра с левой стороны, а инструменты — с правой стороны (Nate Graham, Okular 23.04. [Ссылка 1](https://invent.kde.org/graphics/okular/-/merge_requests/690) и [ссылка 2](https://invent.kde.org/graphics/okular/-/merge_requests/698)):

![1](https://pointieststick.files.wordpress.com/2023/03/okular-toolbar.png)

Когда автоматическое решение проблем в мастере публикации общих папок Samba терпит неудачу, теперь отображается соответствующее сообщение об ошибке, объясняющее, что пошло не так (Nate Graham, kdenetwork-filesharing 23.08. [Ссылка](https://invent.kde.org/network/kdenetwork-filesharing/-/merge_requests/41))

Plasma теперь предоставляет глобальные действия для перезагрузки и выключения устройства, на которые можно назначить комбинации клавиш. Ранее у нас были только варианты этих действий «без подтверждения», тогда как новые сначала будут запрашивать подтверждение (Nate Graham, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=467017))

При импорте конфигураций VPN-соединений любые ошибки теперь отображаются в пользовательском интерфейсе, чтобы вы могли выяснить, что пошло не так, и, возможно, самостоятельно это исправить (Nicolas Fella, Plasma 5.27.3. [Ссылка](https://invent.kde.org/plasma/plasma-nm/-/merge_requests/224))

Во время загрузки новых изолированных приложений Flatpak центр приложений Discover теперь правильно отражает это в статусе задачи (Aleix Pol Gonzalez, Plasma 5.27.4. [Ссылка](https://invent.kde.org/plasma/discover/-/merge_requests/503))

Если на вашей клавиатуре есть клавиша «Эмодзи», нажатие на неё теперь открывает панель выбора эмодзи (Konrad Borowski, Plasma 5.27.4. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1434))

Боковая панель приложения Информация о системе теперь использует заголовки разделов вместо дополнительного уровня вложенности. Это должно упростить и ускорить доступ к нужным страницам (Oliver Beard, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/kinfocenter/-/merge_requests/137)):

![2](https://pointieststick.files.wordpress.com/2023/03/flatter-navigated-info-center.png)

Когда вы применяете параметры Plasma к экрану входа в систему (SDDM), теперь также синхронизируется размер курсора мыши (Nate Graham, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=467326))

Для стороннего приложения GParted при выбранном наборе значков Breeze больше не используется значок анализатора дисков Filelight (Nate Graham, Frameworks 5.105. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=467319))

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

В Параметрах системы исправлен источник сбоев при импорте файлов конфигурации VPN-соединений (Nicolas Fella, Plasma 5.27.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=465484))

В Plasma исправлена ещё одна причина сбоев, связанных с буфером обмена (Fushan Wen, Plasma 5.27.4. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2730))

Значительно повышена надёжность настройки взаимного расположения экранов в многомониторных конфигурациях, где у нескольких мониторов одинаковый идентификатор EDID (Xaver Hugl, Plasma 5.27.3. [Ссылка](https://invent.kde.org/plasma/kscreen/-/merge_requests/187))

Сопоставление контейнеров Plasma с экранами в многомониторных конфигурациях стало надёжнее (David Edmundson, Plasma 5.27.3. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2735))

Исправлено масштабирование приложений GTK в сеансе Plasma Wayland при использовании нескольких экранов с разной плотностью пикселей (Luca Bacci, Plasma 5.27.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=466463))

В сеансе Wayland процесс оболочки Plasma больше не завершает работу, если приложение отправляет ему невероятно длинный заголовок окна (David Edmundson, Plasma 5.27.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=465775))

В сеансе Wayland запись экрана и миниатюры окон в панели задач теперь работают правильно у пользователей видеокарт NVIDIA с проприетарными драйверами (Jan Grulich, Plasma 5.27.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=448839))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 13 ошибок в Plasma с очень высоким приоритетом (на прошлой неделе было 12). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma остаётся 51 (на прошлой неделе было 50). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 100 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-03-10&chfieldto=2023-03-17&chfieldvalue=RESOLVED&list_id=2308088&query_format=advanced&resolution=FIXED)

## Автоматизация и систематизация

Добавлен тест пользовательского интерфейса для Discover, проверяющий, что установка и удаление приложений через модуль PackageKit работают правильно (Harald Sitter. [Ссылка](https://invent.kde.org/plasma/discover/-/merge_requests/463))

## Изменения вне KDE, затрагивающие KDE

У браузера Firefox при работе в сеансе Wayland в «родном» режиме (без прослойки XWayland) больше нет невидимой анимации, которая заставляла диспетчер окон KWin постоянно перерисовывать экран, впустую нагружая процессор (Emilio Cobos Álvarez, Firefox 113, [ссылка](https://bugzilla.mozilla.org/show_bug.cgi?id=1754810))

## Как вы можете помочь

Если вы пользователь, обновитесь до [Plasma 5.27](https://www.kde.org/announcements/plasma/5/5.27.0/)! Если обновление в вашем дистрибутиве не доступно и не предвидится, подумайте о переходе на другой дистрибутив, поставляющий программное обеспечение ближе к графикам разработчиков.

Разработчики, помогите нам исправить [известные проблемы в Plasma 5.27](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&keywords=regression%2C%20&keywords_type=allwords&list_id=2278517&query_format=advanced&version=5.26.90&version=5.27.0&version=5.27.1&version=5.27.2&version=5.27.3&version=5.27.4&version=5.27.5). Также посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этих списков — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

<!-- Временная ссылка! -->

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/03/17/this-week-in-kde-more-wayland-fixes/>  

<!-- 💡: Info Center → Информация о системе -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: emoji → эмодзи -->
<!-- 💡: icon → значок -->
<!-- 💡: icon theme → набор значков -->
<!-- 💡: playlist → список воспроизведения -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: thumbnail → миниатюра -->
