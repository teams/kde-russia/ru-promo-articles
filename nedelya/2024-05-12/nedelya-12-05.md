# На этой неделе в KDE: крутости через край

На этой неделе в кодовую базу были включены результаты нескольких недель работы — посмотрим, что же там интересного раздают бесплатно! Разве это не прекрасно? Новинки каждый день, неделя за неделей — и бесплатно. Никаких покупок, рекламы, шпионажа, активации, подписок и всякой чепухи — только качественная работа на благо общественности. И потрудились здесь не только участники сообщества KDE: есть же ещё и программный стек, на который мы полагаемся, и дистрибутивы, распространяющие наше ПО, и так далее и тому подобное! В какое же удивительное время мы живём.

> 5–12 мая, основное — прим. переводчика

## Новые возможности

Теперь, если дважды щёлкнуть по фону представления папки, диспетчер файлов Dolphin по умолчанию выбирает все элементы в ней. При этом на двойной щелчок можно настроить и другие действия, в том числе запуск пользовательских консольных команд! (George Florea Bănuș, Dolphin 24.08. [Ссылка](https://invent.kde.org/system/dolphin/-/merge_requests/764)):

![0](https://pointieststick.files.wordpress.com/2024/05/image-5.png)

В Параметрах системы появилась страница, на которой можно включить и настроить удалённый вход по протоколу RDP (Akseli Lahtinen и Nate Graham, Plasma 6.1. [Ссылка 1](https://invent.kde.org/plasma/krdp/-/merge_requests/16) и [ссылка 2](https://invent.kde.org/plasma/krdp/-/merge_requests/25)):

![1](https://pointieststick.files.wordpress.com/2024/05/image-4.png)

В сеансе Wayland диспетчер окон KWin теперь можно настроить на получение информации о цветовом профиле из метаданных EDID монитора (если они есть). Информация о цветовом профиле в метаданных EDID часто неверна, поэтому будьте внимательны при использовании этой настройки. К функции добавлен текст встроенной справки с соответствующим предупреждением (Xaver Hugl, Plasma 6.2. [Ссылка 1](https://invent.kde.org/plasma/kscreen/-/merge_requests/289), [ссылка 2](https://invent.kde.org/plasma/libkscreen/-/merge_requests/184), [ссылка 3](https://invent.kde.org/plasma/kwin/-/merge_requests/5308) и [ссылка 4](https://invent.kde.org/libraries/plasma-wayland-protocols/-/merge_requests/72))

## Улучшения пользовательского интерфейса

Теперь стало понятнее, как завершить запись экрана в утилите Spectacle, — благодаря анимации значка «идёт запись» в системном лотке. Кроме того, Spectacle отправляет соответствующее системное уведомление (Noah Davis, Spectacle 24.08. [Ссылка](https://invent.kde.org/graphics/spectacle/-/merge_requests/360))

Теперь будет очевидно, как закрыть красивый новый диалог настроек панели в Plasma 6: в углу появилась кнопка «Готово»! (Taro Tanaka и Nate Graham, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2240)):

![2](https://pointieststick.files.wordpress.com/2024/05/done-button.jpeg)

Если отключиться от сети, когда открыт график скорости, это окно теперь автоматически переключается на показ сведений (Евгений Попов, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/plasma-nm/-/merge_requests/346))

Плавная прокрутка в приложениях KDE на основе QML теперь необязательна (но, как и ранее, по умолчанию включена). Возможно, и сторонние приложения начнут учитывать этот параметр — как, оказывается, Firefox теперь учитывает глобальный параметр скорости анимации (Nathan Misner, Plasma 6.2. [Ссылка 1](https://invent.kde.org/frameworks/kirigami/-/merge_requests/1492) и [ссылка 2](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2138))

Визуально переработаны небольшие диалоговые окна в ПО на основе QtQuick: удалено всё ненужное, чтобы можно было сфокусироваться на тексте и кнопках (Carl Schwan, Frameworks 6.3. [Ссылка 1](https://invent.kde.org/frameworks/kirigami/-/merge_requests/1519) и [ссылка 2](https://invent.kde.org/frameworks/qqc2-desktop-style/-/merge_requests/402)):

![3](https://pointieststick.files.wordpress.com/2024/05/image-2.png)

*Это не окончательный вид, но общее направление дизайна на данный момент*

Внешний вид панелей команд в приложениях на основе QtWidgets также был переработан ближе к такому минималистичному стилю (Евгений Попов, Frameworks 6.3. [Ссылка](https://invent.kde.org/frameworks/kconfigwidgets/-/merge_requests/237)):

![4](https://pointieststick.files.wordpress.com/2024/05/image-3.png)

## Исправления ошибок

Музыкальный проигрыватель Elisa больше не зависает, если включить режим вечеринки во время воспроизведения, предварительно изменив размер области заголовка определённым образом (Pedro Nishiyama, Elisa 24.05. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=483613))

Решены две давние проблемы, которые могли приводить к сбою Plasma, если при пробуждении или загрузке системы не удавалось найти все ожидаемые экраны (Marco Martin, Plasma 6.0.4 и 6.0.5. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=484687) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=456947))

Центр приложений Discover больше не показывает некорректные заявления о том, что приложения, для которых не указаны лицензии, являются проприетарными (Harald Sitter, Plasma 6.0.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=486037))

Устранено ухудшение в Plasma 6, из-за которого Discover при просмотре страниц материалов из каталога store.kde.org отображал докучливые сообщения о незначительных ошибках (Harald Sitter, Plasma 6.0.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=486330))

Строка поиска в виджете «Принтеры» Plasma наконец-то работает (Mike Noe, Plasma 6.0.5. [Ссылка](https://invent.kde.org/plasma/print-manager/-/merge_requests/160))

Устранено ухудшение в Plasma 6, из-за которого виджеты панели перекрывались, если на горизонтальной панели был виджет «Переключение комнат» (Edo Friedman, Plasma 6.0.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482098))

KWin теперь более надёжно обрабатывает выключение экранов при наличии некоторых особенностей оборудования и драйверов, из-за которых раньше этот процесс на некоторых устройствах был менее предсказуемым (Arsen Arsenović, Plasma 6.0.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=480026))

Исправлена ошибка, из-за которой при использовании смешанного светлого и тёмного оформления рабочей среды (например, Breeze Twilight) в экранном меню профилей энергопотребления, а также в окнах настройки виджетов системного лотка и Системного монитора Plasma у некоторых значков и элементов управления интерфейса были неправильные цвета (Akseli Lahtinen, Евгений Харченко и Nicolas Fella, Plasma 6.0.5. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=483689), [ссылка 2](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4297) и [ссылка 3](https://bugs.kde.org/show_bug.cgi?id=480254))

Код виджета панели задач Plasma был реорганизован, что позволило упростить его, а также исправить две серьёзных ошибки в макете, в том числе — ухудшение в Plasma 6, когда задачи в многострочном режиме просмотра перекрывались соседними виджетами (Marco Martin, Plasma 6.1. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=485223) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=424055))

Другие сведения об ошибках:

* Остаётся 3 ошибки в Plasma с очень высоким приоритетом (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma — 38 (на прошлой неделе было 35). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 133 ошибки были исправлены на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-5-03&chfieldto=2024-5-10&chfieldvalue=RESOLVED&list_id=2709748&query_format=advanced&resolution=FIXED)

## Автоматизация и систематизация

В git-репозитории `plasma-workspace` внедрён шаблон запроса на слияние (MR), который поможет писать более полезные описания изменений, лучше тестировать их и включать снимки экрана «до» и «после». Если новинка приживётся, мы внедрим её где-нибудь ещё (Nate Graham, [ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4247))

## Как вы можете помочь

Сообщество KDE стало важным явлением в мире — благодаря вашему труду и вложенному времени! Однако по мере роста проекта важно обеспечить стабильность работы над ним, а это значит, что труд должен *оплачиваться*. Сегодня KDE по большей части использует работу людей, которым фонд KDE e.V. не платит, и это проблема. Мы предприняли шаги, которые позволят изменить ситуацию, и наняли [платных технических подрядчиков](https://ev.kde.org/corporate/staffcontractors/), однако из-за ограниченности финансовых ресурсов этого всё ещё мало. Если вы хотите помочь, [сделайте пожертвование](https://kde.org/ru/community/donations/)!

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* коммерческий  
*Источник:* <https://pointieststick.com/2024/05/10/this-week-in-kde-our-cup-overfloweth-with-cool-stuff-for-you/>  
