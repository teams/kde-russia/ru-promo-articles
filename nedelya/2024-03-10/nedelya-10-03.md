# На этой неделе в KDE: лавина новых функций

Шлюзы полностью открылись, и разработчики начали внедрять интересные новшества в Plasma 6.1!

Но это не всё… мы просили вас присылать отчёты об ошибках, и вы не заставили себя ждать! Обычно мы получаем по 30-50 в день, а сейчас по 150-200!

Заметьте, это не означает, что программное обеспечение действительно настолько глючное. Это говорит лишь о том, что им начали *пользоваться*! Большинство поступающих отчётов об ошибках на самом деле вообще не о проблемах KDE: там есть и ошибки в графических драйверах, и недочёты в темах, и проблемы в сторонних приложениях. Встречается много дубликатов про уже известные недочёты, а также описаний экзотических проблем, воспроизводимых лишь в специфических комбинациях нестандартных настроек.

Конечно, попадаются и более серьёзные проблемы, но, насколько мне известно, большинство из них мы уже решили. Для ещё нескольких — таких как медленный вход в систему и чёрный экран блокировки у некоторых пользователей — решения уже почти готовы и скоро будут приняты.

## Новые возможности

Встроенные представления терминала в текстовом редакторе Kate теперь можно разделять по горизонтали или по вертикали (Akseli Lahtinen, Kate 24.05. [Ссылка](https://invent.kde.org/utilities/kate/-/merge_requests/1417))

Теперь можно настроить, будет ли лупа в режиме съёмки прямоугольной области экрана Spectacle всегда видна, всегда скрыта или видима только при удержании клавиши Shift (Noah Davis, Spectacle 24.05. [Ссылка](https://invent.kde.org/graphics/spectacle/-/merge_requests/338))

Для пользователей многомониторных конфигураций добавлены функции [«краевой барьер»](https://bugs.kde.org/show_bug.cgi?id=416570) и [«угловой барьер»](https://bugs.kde.org/show_bug.cgi?id=451744). Они добавляют виртуальное расстояние между экранами, чтобы вам было легче взаимодействовать с пикселями на смежных краях экранов. Это полезно, например, если вы хотите [разместить между экранами автоматически скрываемую панель](https://bugs.kde.org/show_bug.cgi?id=351175) или упростить нажатие на кнопку закрытия распахнутого окна рядом с другим экраном. Обратите внимание, что эти функции доступны только в Wayland. И да, их можно настроить или вовсе отключить (Yifan Zhu, Plasma 6.1):

![0](https://pointieststick.files.wordpress.com/2024/03/image.png)

В виджете «Веб-браузер» теперь можно скрыть панель навигации, если вы всегда используете его для одной и той же страницы (Shubham Arora, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/550))

## Улучшения пользовательского интерфейса

Если Spectacle настроен так, что не делает снимок экрана при запуске, то его окно теперь сразу позволяет вам запустить запись экрана (Noah Davis, 24.05. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=468778))

В результатах поиска Plasma точные совпадения с названиями страниц Параметров системы теперь имеют более высокий приоритет (Alexander Lohnau, Plasma 6.0.1. [Ссылка](https://invent.kde.org/plasma/systemsettings/-/merge_requests/297))

Строку поиска «классического» меню запуска приложений теперь можно использовать для вычислений, преобразования единиц измерения и вызова действий управления питанием и сеансом (Nate Graham, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2090))

Новый эффект «Увеличение курсора» для поиска курсора встряхиванием мыши теперь включён по умолчанию (Влад Загородний, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/5365))

Всплывающие окна виджетов на панели Plasma теперь закрываются при щелчке по пустой области виджета «Панель задач» (David Edmundson, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=367815))

Приложениям, работающим через слой совместимости XWayland, теперь по умолчанию разрешено подслушивать нажатия не буквенно-цифровых клавиш и комбинации клавиш, содержащие клавиши-модификаторы. Это позволяет любым глобально вызываемым действиям таких приложений работать без вмешательства пользователя, но при этом не даёт прослушивать произвольные нажатия буквенно-цифровых клавиш, что могло бы быть использовано злонамеренно (Nate Graham, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/4601))

Ошибки подключения Bluetooth теперь дополнительно упоминаются во всплывающем окне самого виджета, рядом с соответствующим устройством (Patrik Fábián, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=449517)):

![1](https://invent.kde.org/plasma/bluedevil/uploads/b54d142598756a50326b2c769cc1aa14/test.png)

## Исправления ошибок

Сворачивание окна диспетчера файлов Dolphin больше не приводит к скрытию всех его панелей (Nicolas Fella, Dolphin 24.02.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=481952))

Устранён глюк многострочного выделения текста в просмотрщике документов Okular (Okular 24.02.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482249))

Файлы, лежащие на пути перетаскиваемого объекта в Dolphin, больше не открываются, если на некоторое время задержать курсор над ними (Akseli Lahtinen, Dolphin 24.05. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=479960))

Plasma больше не завершает работу аварийно при открытии меню запуска приложений во время удаления из системы приложения, добавленного в список избранного (Marco Martin, Plasma 6.0.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=481855))

В «классическом» меню запуска приложений исправлен выбор элементов клавишей Enter (Marco Martin, Plasma 6.0.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482005))

Результаты вида «Установить \[приложение\]» от поиска Plasma снова работают (Nicolas Fella, Plasma 6.0.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=481932))

Перетаскиваемые из Dolphin на рабочий стол файлы больше не складываются в стопку до перезапуска Plasma (Marco Martin, Plasma 6.0.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482302))

Центр приложений Discover больше не падает на некоторых довольно распространённых поисковых запросах, включая «libreoffice» (Aleix Pol Gonzalez, Plasma 6.0.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482095))

В сеансе X11 исправлена работа пункта контекстного меню заголовков окон «На рабочих столах > На всех рабочих столах» (Nicolas Fella, Plasma 6.0.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482670))

Устранено возможное завершение работы Plasma из-за ошибки протокола Wayland после выключения и повторного включения экранов (Влад Загородний, Plasma 6.0.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482399))

Устранено возможное аварийное завершение работы диспетчера окон KWin при открытии окна на внешнем мониторе, подключённом к вторичной видеокарте (Xaver Hugl, Plasma 6.0.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482859))

Нашего предыдущего исправления невозможности открыть проигрыватели VLC и MPV на весь экран оказалось недостаточно, поэтому мы его прокачали, и теперь оно должно работать всегда (Łukasz Patron, Plasma 6.0.2. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=477086) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=481456))

Исправлена ​​ошибка, из-за которой ночная цветовая схема не работала с определёнными видеокартами (Xaver Hugl, Plasma 6.0.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482143))

Первый результат поиска в «классическом» меню запуска приложений больше не перекрывается иногда строкой поиска (Marco Martin, Plasma 6.0.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482736))

Когда вы перетаскиваете окно за левую часть экрана в сеансе X11, фактическая позиция курсора больше не смещается относительно визуальной (Yifan Zhu, Plasma 6.0.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482687))

Исправлен случай, когда Discover мог аварийно завершить работу при запуске с включённым модулем поддержки Flatpak (David Redondo, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/discover/-/merge_requests/771))

Окно настройки панели на верхнем краю экрана больше не перекрывает глобальную панель инструментов режима редактирования; вместо этого панель инструментов перемещается в нижнюю часть экрана (Niccolò Venerandi, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460714))

В диалогах загрузки новых материалов из сети исправлено скачивание элементов, для которых доступен только один файл (Akseli Lahtinen, Frameworks 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482349))

Различные действия в приложениях KDE, открывающие приложение терминала по умолчанию, — например, пункт меню Dolphin «Открыть терминал в этой папке» — снова работают (Nicolas Fella, Frameworks 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482107))

Диаграммы вида «Горизонтальные столбцы» в различных виджетах Системного монитора теперь используют правильные цвета (Arjen Hiemstra, Frameworks 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482671))

Пункты контекстных меню текстовых полей в приложениях на основе QtQuick теперь переводимы (Евгений Чесноков, Frameworks 6.1. [Ссылка](https://invent.kde.org/frameworks/qqc2-desktop-style/-/merge_requests/376))

Некоторые значки точек входа из набора значков Breeze стали учитывать настроенный цвет выделения, как и их сородичи (Frameworks 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=478016))

Другие сведения об ошибках:

* Осталось 2 ошибки в Plasma с очень высоким приоритетом (на прошлой неделе было 3). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma — 36 (на 3 больше, чем на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 173 ошибки были исправлены на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-3-1&chfieldto=2024-3-8&chfieldvalue=RESOLVED&list_id=2638359&query_format=advanced&resolution=FIXED)

## Производительность и технические улучшения

Устранён источник задержек и пропусков кадров на устройствах с некоторыми видеокартами (Xaver Hugl, Plasma 6.0.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482064))

## Автоматизация и систематизация

Подготовлено руководство по настройке [автоматической публикации вашего приложения KDE](https://develop.kde.org/docs/packaging/android/packaging_applications/) в [репозитории KDE для F-Droid](https://search.f-droid.org/?q=kde&lang=en) (Ingo Klöcker, [ссылка](https://invent.kde.org/documentation/develop-kde-org/-/merge_requests/353#note_886741))

Обновлено руководство по написанию страниц Параметры системы (KCM) (Akseli Lahtinen, [ссылка](https://invent.kde.org/documentation/develop-kde-org/-/merge_requests/355))

## Как вы можете помочь

Помогите, пожалуйста, с [сортировкой отчётов об ошибках](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging)! В Bugzilla сейчас просто завал, и мы не справляемся. Подробнее [здесь](https://pointieststick.com/2024/03/09/how-you-help-with-quality/).

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

И напоследок: KDE на 99,9% обеспечивается трудом, не оплаченным KDE e.V. Если вы хотите помочь изменить это, [рассмотрите возможность пожертвования](https://kde.org/community/donations/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2024/03/08/this-week-in-kde-a-deluge-of-new-features/>  

<!-- 💡: GPU → видеокарта / графический ускоритель -->
<!-- 💡: Kicker → классическое меню запуска приложений -->
<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Monitor → Системный монитор -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: get new [thing] → «Загрузить \[что-то\]» -->
<!-- 💡: icon → значок -->
<!-- 💡: icon theme → набор значков -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
