# На этой неделе в KDE: время новшеств

На этой неделе отовсюду хлынули результаты длительной работы: приятные новшества для Plasma 6, долгожданные исправления ошибок, интересные автоматические тесты и многое другое!

## Plasma 6

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 94](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

Эффекты «Обзор» и «Все рабочие столы» были объединены в один, поддерживающий переходы между состояниями с помощью плавных и естественных жестов сенсорной панели. Это действительно потрясающая работа, закрывшая целую пачку отчётов об ошибках! (Niccolò Venerandi, [ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/4336)):

![0](https://pointieststick.files.wordpress.com/2023/09/image-8.png)

![1](https://pointieststick.files.wordpress.com/2023/09/image-11.png)

В сеансе Plasma Wayland в системном лотке появился индикатор использования камеры, аналогичный индикаторам захвата экрана и использования микрофона (Fushan Wen, [ссылка 1](https://invent.kde.org/plasma/kpipewire/-/merge_requests/86) и [ссылка 2](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3310)):

![2](https://pointieststick.files.wordpress.com/2023/09/image-7.png)

У плавающих панелей Plasma теперь есть красивые тени, а когда они приземляются, у них больше нет уродливых массивных полей! Кроме того, любые окна виджетов с плавающей панели теперь тоже плавают, а все их углы скруглены. Завершение этой необходимой работы позволило перейти на использование плавающих панелей по умолчанию! (Niccolò Venerandi, [ссылка 1](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1290), [ссылка 2](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3249), [ссылка 3](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/880) и [ссылка 4](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3175)):

<https://i.imgur.com/fsqIIEX.mp4?_=1>

Добавлена глобальная комбинация клавиш (по умолчанию Meta+Alt+L) для переключения между текущей и ранее использованной раскладками клавиатуры, что может пригодиться пользователям, регулярно переключающимся между более чем двумя раскладками (Mihail Milev, [ссылка 1](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1722) и [ссылка 2](https://invent.kde.org/plasma/kwin/-/merge_requests/4433))

Значки, отображаемые через соответствующий компонент библиотеки Kirigami — а в Plasma 6 это почти все значки в программах KDE на основе QML — теперь выглядят лучше и чётче при использовании дробного масштабирования экрана (Marco Martin, [ссылка](https://bugs.kde.org/show_bug.cgi?id=474376))

В ​​Параметрах системы исправлены многочисленные проблемы с фокусировкой: боковую панель снова можно выбрать после выбора текущей страницы, а нажатие клавиши со стрелкой вниз в строке поиска теперь переводит фокус на список результатов (Fushan Wen, [ссылка 1](https://bugs.kde.org/show_bug.cgi?id=404065) и [ссылка 2](https://invent.kde.org/plasma/systemsettings/-/merge_requests/256))

Мы начали улучшать внешний вид списка прав доступа приложений Flatpak в центре приложений Discover: теперь там используются более подходящие значки и более понятные формулировки, а разрешение на воспроизведение звука больше не игнорируется (Nate Graham, [ссылка 1](https://invent.kde.org/plasma/discover/-/merge_requests/639), [ссылка 2](https://invent.kde.org/plasma/discover/-/merge_requests/641), и [ссылка 3](https://invent.kde.org/plasma/discover/-/merge_requests/642))

Перефразирована настройка «отложенных» («автономных») обновлений, там больше не используется эта запутанная терминология (Nate Graham, [ссылка](https://invent.kde.org/plasma/discover/-/merge_requests/637)):

![4](https://pointieststick.files.wordpress.com/2023/09/image-9.png)

*«Применять обновления системы: При перезагрузке (рекомендуется) / Сразу»*

В механизм управления мониторами внесены некоторые исправления, которые должны снизить вероятность самопроизвольного пробуждения монитора сразу после его перехода в режим энергосбережения (Xaver Hugl, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/2940))

При использовании интеграции Plasma с systemd (а она включена по умолчанию, если у вас есть systemd), Plasma теперь правильнее завершает процессы при выходе из системы, что должно предотвратить сбои программ при завершении сеанса и зависание процессов, препятствующее повторному входу в систему. Есть шанс, что это исправление войдёт в корректирующий выпуск для Plasma 5.27! (David Edmundson, [ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3302))

При использовании панели Plasma на нижнем краю экрана, всплывающие подсказки панели задач с элементами управления воспроизведением теперь всегда располагаются в ожидаемом месте (David Edmundson, [ссылка](https://bugs.kde.org/show_bug.cgi?id=463272))

Приложения на основе GTK 2 теперь выглядят как надо при использовании темы оформления «Breeze, тёмный вариант» ([ссылка](https://bugs.kde.org/show_bug.cgi?id=443104))

Из списка избранного в меню запуска приложений теперь можно удалить элементы, соответствующие ранее удалённым приложениям и файлам (Méven Car, [ссылка](https://bugs.kde.org/show_bug.cgi?id=474120))

Заголовки страниц в программном обеспечении на основе Kirigami больше не будут иногда сокращаться, когда они прекрасно умещаются целиком (Иван Ткаченко, [ссылка](https://bugs.kde.org/show_bug.cgi?id=474881))

## Другие заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Работа блокировщика экрана больше не может нарушаться из-за исчерпания свободного места на диске, приводившего к повреждению кэша QML. Бывают и другие причины, но над их исправлением мы тоже работаем! (Harald Sitter, Plasma 5.27.9. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=471952))

Устранён корень проблемы безграничного роста файла конфигурации `ScreenMapping`, приводившего к аварийному завершению Plasma или невозможности запускать приложения. Заодно это решает проблему сброса позиций значков на рабочем столе (Marco Martin, Plasma 5.27.9. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1738))

Редактор меню KDE снова правильно создаёт файлы `.desktop`, указывающие на исполняемые файлы, в пути к которым есть пробелы или другие специальные символы (Méven Car, Plasma 5.27.9. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=474758))

Поиск недавно открытых файлов через строки поиска, основанные на KRunner, снова полностью регистронезависим, как и ожидалось (Alexander Lohnau, Plasma 5.27.9. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=474782))

Исправлена ​​довольно сложная проблема, из-за которой приложения GNOME в формате Flatpak отображали текст без сглаживания при запуске в Plasma (Timothée Ravier, Plasma 5.27.9. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=474746))

Если была настроена калибровочная матрица сенсорного экрана, диспетчер окон KWin теперь её учитывает (Plasma 5.27.9. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=474110))

Теперь можно использовать клавиатуру для передачи фокуса кнопкам на панелях инструментов в приложениях KDE, построенных с использованием библиотеки KXMLGui (Felix Ernst, Frameworks 5.111. [Ссылка](https://invent.kde.org/frameworks/kxmlgui/-/merge_requests/188))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 4 ошибки в Plasma с очень высоким приоритетом (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 58 (на прошлой неделе было 59). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 127 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-09-20&chfieldto=2023-09-29&chfieldvalue=RESOLVED&list_id=2481505&query_format=advanced&resolution=FIXED)

## Автоматизация и систематизация

Добавлены базовые автоматические тесты пользовательского интерфейса для виджетов Plasma и страниц Параметров системы, живущих в репозитории `plasma-workspace`! (Fushan Wen, [ссылка 1](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3342) и [ссылка 2](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3344))

Исправлены различные проблемные тесты в библиотеках Kirigami и KSvg, и их прохождение теперь обязательно для одобрения любых предлагаемых изменений (Marco Martin, [ссылка 1](https://invent.kde.org/frameworks/kirigami/-/merge_requests/1285) и [ссылка 2](https://invent.kde.org/frameworks/ksvg/-/merge_requests/15#note_771397))

## Как вы можете помочь

Разработчики, пожалуйста, помогите исправить проблемы в [Qt 6, KDE Frameworks 6 и Plasma 6](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&f1=keywords&f2=product&list_id=2398791&o1=allwordssubstr&o2=notequals&query_format=advanced&v1=qt6&v2=neon)! Plasma 6 вполне годится для повседневного использования энтузиастами (я в их числе), хоть и всё ещё требует работы, чтобы стать готовой к выпуску до конца года.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/09/29/this-week-in-kde-time-for-the-new-features/>  

<!-- 💡: KMenuEdit → Редактор меню KDE -->
<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Monitor → Системный монитор -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
