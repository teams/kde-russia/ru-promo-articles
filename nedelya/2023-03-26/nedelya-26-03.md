# На этой неделе в KDE: обновление Fedora через Discover

## Новые возможности

Новая экспериментальная утилита XWaylandVideoBridge позволяет приложениям, запущенным через слой совместимости XWayland, захватывать содержимое «родных» окон Wayland для демонстрации или записи — это полезно, например, для Discord (Aleix Pol Gonzalez и David Edmundson, [ссылка](http://blog.davidedmundson.co.uk/blog/xwaylandvideobridge/))

В диспетчере файлов Dolphin появилась опция для обновления данных и миниатюры в панели сведений только при явном выборе файлов, а не просто при наведении курсора на них (Oliver Beard, Dolphin 23.08. [Ссылка](https://invent.kde.org/system/dolphin/-/merge_requests/501)):

![0](https://pointieststick.files.wordpress.com/2023/03/show-item-on-hover-in-dolphin.jpg)

Центр приложений Discover научился выполнять обновление дистрибутива Fedora KDE до следующего крупного выпуска! (Alessandro Astone, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=446924))

## Улучшения пользовательского интерфейса

При использовании нескольких мониторов с одинаковым именем и серийным номером для их отличия в интерфейсе теперь отображаются имена разъёмов (David Redondo, Plasma 5.27.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=466046))

В набор значков Breeze добавлены новые красивые значки для функции «Ночная цветовая схема» (Philip Murray, Frameworks 5.105. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=462215)):

![1](https://pointieststick.files.wordpress.com/2023/03/night-color-off-icon.jpg)

![2](https://pointieststick.files.wordpress.com/2023/03/night-color-on-icon.jpg)

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Наша прошлая попытка исправить визуальные искажения оформлений окон Aurorae не учитывала все обстоятельства, но теперь мы, надеюсь, всё-таки решили эту проблему для всех (David Edmundson, Plasma 5.27.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=465790))

Параметры системы больше не вылетают при отказе от внесённых изменений на стартовой странице (David Redondo, Plasma 5.27.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=465510))

В списке доступных разрешений экрана больше не отображаются те, выбор которых из-за особенностей используемых графических драйверов вызвал бы артефакты или сбои (Xaver Hugl, Plasma 5.27.4. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/3878))

Минимальная толщина неплавающих панелей больше не чрезмерно большая при использовании тем Plasma с большим радиусом закругления углов (Niccolò Venerandi, Plasma 5.27.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=466098))

Смена темы экрана загрузки (Plymouth) теперь правильно работает в дистрибутивах, использующих `mkinitcpio` вместо `update-initramfs` (Antonio Rojas, Plasma 5.27.4. [Ссылка](https://invent.kde.org/plasma/plymouth-kcm/-/merge_requests/28))

Исправлена ошибка, из-за которой экран входа в систему SDDM мог на время застыть при использовании темы Breeze (Иван Ткаченко, Plasma 5.27.4. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2767))

Служба индексирования файлов Baloo больше не добавляет в индекс непечатаемые символы, что иногда приводило к сбоям (Игорь Побойко, Frameworks 5.105. [Ссылка](https://invent.kde.org/frameworks/baloo/-/merge_requests/87))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 14 ошибок в Plasma с очень высоким приоритетом (на прошлой неделе было 13). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 49 (на прошлой неделе была 51). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 84 ошибки было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-03-17&chfieldto=2023-03-24&chfieldvalue=RESOLVED&list_id=2315999&query_format=advanced&resolution=FIXED)

## Изменения вне KDE, затрагивающие KDE

При использовании экрана с высокой частотой обновления в сеансе X11 анимации QML больше не воспроизводятся слишком быстро (Qt 6.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=419421))

В сеансе Plasma Wayland теперь работает быстрое переключение пользователей (Fabian Vogt, SDDM 0.20. [Ссылка](https://github.com/sddm/sddm/pull/1690))

При использовании Wayland для запуска самого SDDM, тот больше не пытается вызвать `xdg-desktop-portal-kde`, что приводило к циклическому сбою (Aleix Pol Gonzalez, SDDM 0.20. [Ссылка](https://github.com/sddm/sddm/pull/1687))

## Как вы можете помочь

Если вы пользователь, обновитесь до [Plasma 5.27](https://www.kde.org/announcements/plasma/5/5.27.0/)! Если обновление в вашем дистрибутиве не доступно и не предвидится, подумайте о переходе на другой дистрибутив, поставляющий программное обеспечение ближе к графикам разработчиков.

Разработчики, помогите нам исправить [известные проблемы в Plasma 5.27](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&keywords=regression%2C%20&keywords_type=allwords&list_id=2278517&query_format=advanced&version=5.26.90&version=5.27.0&version=5.27.1&version=5.27.2&version=5.27.3&version=5.27.4&version=5.27.5). Также посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этих списков — простой способ сделать значительный вклад. Вы могли заметить, что число ошибок последнее время держалось на одном уровне. Это вызвано тем, что основные разработчики Plasma заняты адаптацией кода к Qt 6, так что у них остаётся мало времени на исправление ошибок. Но волонтёры могут помочь восполнить этот пробел! 🙂

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

<!-- Временная ссылка! -->

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/03/25/this-week-in-kde-distro-upgrades-for-fedora-kde-in-discover/>  

<!-- 💡: Kicker → классическое меню запуска приложений -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: icon theme → набор значков -->
