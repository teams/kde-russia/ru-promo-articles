# На этой неделе в KDE: настраиваемый тайлинг

> 27 ноября – 4 декабря, основное — прим. переводчика

На этой неделе в диспетчере окон KWin появилась классная новая функция: встроенный [механизм тайлинга](https://invent.kde.org/plasma/kwin/-/merge_requests/2560) с возможностью создавать пользовательскую разметку областей, заодно позволяющий [одновременно изменять размер нескольких смежных окон](https://bugs.kde.org/show_bug.cgi?id=438788) перетаскиванием границы между ними!

![0](https://pointieststick.files.wordpress.com/2022/12/tiling-1.jpg)

*Пользовательское разбиение*

![1](https://pointieststick.files.wordpress.com/2022/12/tiling-2.jpg)

*Полноэкранный редактор*

![2](https://pointieststick.files.wordpress.com/2022/12/tiling-3.jpg)

*Заготовки разметок*

Эта функция пока находится в зачаточном состоянии и не предназначена для полного воспроизведения рабочего процесса тайловых диспетчеров окон. Но мы ожидаем, что со временем она будет развиваться, а разработчики сторонних сценариев для KWin будут использовать её для реализации более продвинутых функций. Большое спасибо Marco Martin за его работу, она войдёт в Plasma 5.27!

Но есть и много чего ещё!

## Другие новые возможности

Теперь вы можете просматривать содержимое устройств Apple с iOS в диспетчере файлов Dolphin, файловых диалогах и других инструментах управления файлами (Kai Uwe Broulik, kio-extras 23.04. [Ссылка](https://blog.broulik.de/2022/11/introducing-kio-afc/)):

![3](https://pointieststick.files.wordpress.com/2022/12/kio-afc.png)

Эмулятор терминала Konsole теперь использует меню-гамбургер (Nate Graham, Felix Ernst и Андрей Бутырский, Konsole 23.04. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=439895)):

![4](https://pointieststick.files.wordpress.com/2022/11/image-16.png)

*Как обычно, если вы терпеть не можете меню-гамбургер, вы можете использовать традиционную строку меню, видимость которой управляется сочетанием клавиш Ctrl+Shift+M*

Панель вкладок в Konsole теперь по умолчанию расположена вверху окна, как в большинстве других приложений, а не внизу (Nate Graham, Konsole 23.04. [Ссылка](https://invent.kde.org/utilities/konsole/-/merge_requests/774))

На виджет «Выбор цвета» теперь можно перетащить изображение, и он вычислит усреднённый цвет этого изображения (Fushan Wen, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/249)):

![5](https://pointieststick.files.wordpress.com/2022/12/image-1.png)

Когда строка поиска и запуска KRunner ничего не находит, теперь она будет предлагать выполнить поиск в Интернете по вашему запросу (Alexander Lohnau, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2311))

Добавлена поддержка портала глобальных комбинаций клавиш, что позволит Flatpak и другим изолированным приложениям, использующим порталы, вызывать стандартизированный интерфейс для настройки и редактирования глобальных комбинаций клавиш (Aleix Pol Gonzalez, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/xdg-desktop-portal-kde/-/merge_requests/80))

## Улучшения пользовательского интерфейса

Когда вы удаляете папку, просматриваемую в данный момент в Dolphin, область просмотра теперь автоматически переходит к родительской папке (Вова Кулик и Méven Car, Dolphin 23.04. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460305))

Когда вы запускаете центр приложений Discover из пункта контекстного меню «Удалить или настроить дополнения…» в меню запуска приложений, и выбранное приложение доступно из нескольких источников, Discover теперь всегда открывает страницу приложения на том источнике, из которого оно фактически установлено, чтобы вы могли сразу нажать кнопку «Удалить», если это было вашей целью (Aleix Pol Gonzalez, Plasma 5.26.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=461664))

К слову об этом контекстном меню: при первом его вызове щелчком правой кнопкой мыши по приложению в меню запуска приложений список действий теперь появляется сразу, а не с запозданием на несколько секунд (David Redondo, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=429052))

Режим размещения новых окон «Каскадом» был удалён из KWin, потому что все остальные режимы размещения окон, где это имеет смысл, теперь сами используют это поведение! (Natalie Clarius, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=58063)):

![6](https://pointieststick.files.wordpress.com/2022/12/image-2.png)

В диалоговом окне демонстрации экрана, которое вы видите при работе с приложениями Flatpak и Snap, использующими механизм порталов XDG, теперь есть миниатюры для всех доступных экранов и окон (Aleix Pol Gonzalez, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460735)):

![7](https://pointieststick.files.wordpress.com/2022/12/image-3.png)

Виджет «Батарея и яркость» теперь считает батарею полностью заряженной, если заряд достиг настроенного пользователем предела (Nate Graham, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2315))

Раздел «Поиск», польза от которого была сомнительна, по умолчанию удалён с панели точек входа, чтобы снизить визуальное захламление. Функциональность никуда не делась, и вы, разумеется, можете вновь добавить эти элементы, если хотите (Nate Graham, Frameworks 5.101. [Ссылка](https://invent.kde.org/frameworks/kio/-/merge_requests/1057)):

![9](https://pointieststick.files.wordpress.com/2022/12/default-places-panel-now.jpg)

*Так теперь по умолчанию выглядит панель «Точки входа»*

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Прокрутка в диалоге выбора языка на странице «Региональные и языковые параметры» Параметров системы больше не такая дёрганая (Nate Graham, Plasma 5.26.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=462057))

Если установленная пользователем сторонняя тема экрана блокировки сломана, но фоновый процесс `kscreenlocker_greet` не завершил работу аварийно, снова будет показываться резервная тема, а не ужасный экран «Работа блокировщика экрана нарушена» (David Redondo, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/kscreenlocker/-/merge_requests/110))

Виджет погоды больше не выходит за пределы своей области в системном лотке и не перекрывает другие значки при различных размерах значков и панелей (Ismael Asensio, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=425305))

Ночная цветовая схема больше не перестаёт работать до повторного включения при перезапуске сеанса или KWin (Влад Загородний, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=428854))

Уведомления теперь можно читать с помощью средства чтения с экрана (Fushan Wen, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2295))

Ускорена отрисовка некоторых элементов интерфейса в Plasma и приложениях на основе Qt Quick (Arjen Hiemstra, Frameworks 5.101. [Ссылка 1](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/645) и [ссылка 2](https://invent.kde.org/frameworks/qqc2-desktop-style/-/merge_requests/202))

В сеансе Plasma Wayland при перетаскивании окна, содержащего элементы интерфейса Qt Quick, на экран с другим коэффициентом масштабирования это окно теперь адаптируется для правильного отображения в соответствии с заданным масштабом того экрана, без размытия или пикселизации. Это работает и тогда, когда окно находится частично на одном экране и частично на другом! (David Edmundson, Frameworks 5.101. [Ссылка 1](https://invent.kde.org/frameworks/qqc2-desktop-style/-/merge_requests/207) и [ссылка 2](https://invent.kde.org/frameworks/qqc2-desktop-style/-/merge_requests/203))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 7 ошибок в Plasma с очень высоким приоритетом (на прошлой неделе было 6). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 47 (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 166 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2022-11-25&chfieldto=2022-12-02&chfieldvalue=RESOLVED&list_id=2222278&query_format=advanced&resolution=FIXED)

## Автоматизация и систематизация

До этого момента приложения для Plasma Mobile выпускались отдельно в соответствии с собственным графиком релизов под названием «Plasma Mobile Gear». Отныне эти приложения будут включены в обычный график набора KDE Gear, а выпуск «Plasma Mobile Gear» будет прекращён для упрощения подготовки пакетов ([ссылка](https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/))

## Изменения вне KDE, затрагивающие KDE

Принят новый протокол Wayland для дробного масштабирования, что открывает двери для его поддержки в Qt и KWin. Это улучшит внешний вид и производительность приложений на Qt, в том числе и разрабатываемых KDE! Работа на стороне Qt и KDE продолжается, но пока не завершена. Как только это состоится, я вам обязательно сообщу! (Kenny Levinsen, wayland-protocols 1.31. [Ссылка](https://gitlab.freedesktop.org/wayland/wayland-protocols/-/merge_requests/143)).

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

<!-- Временная ссылка! -->

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2022/12/02/this-week-in-kde-custom-tiling/>  

<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: Places panel → панель «Точки входа» -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: thumbnail → миниатюра -->
<!-- 💡: window manager → диспетчер окон -->
