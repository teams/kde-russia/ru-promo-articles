# На этой неделе в KDE: доработка интерфейса

## Новые возможности

В простом графическом редакторе KolourPaint теперь можно выбирать уровень качества изображений при сохранении в форматах AVIF, HEIF и HEIC (Nate Graham, KolourPaint 23.04. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=463212))

По виджету проигрывателя теперь можно проводить пальцем вверх и вниз для изменения громкости, а также влево и вправо для перемотки (Fushan Wen, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2438))

## Улучшения пользовательского интерфейса

На странице «Комбинации клавиш» Параметров системы стало куда проще добавить пользовательскую команду для запуска (Bharadwaj Raju, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1253)):

![0](https://pointieststick.files.wordpress.com/2023/01/image-2.png)

Страницы «Запуск приложений» в Параметрах системы больше нет, её содержимое было перемещено во всплывающее окно на странице настройки курсора мыши (Fushan Wen и Janet Blackquill, Plasma 5.27. [Ссылка 1](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2466), [ссылка 2](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/531) и [ссылка 3](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2471#note_588716)):

![1](https://pointieststick.files.wordpress.com/2023/01/image.png)

Кнопка «Выделить изменённые пункты» в Параметрах системы была перемещена из нижнего колонтитула боковой панели в меню-гамбургер (Alexander Wilms, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/systemsettings/-/merge_requests/177))

При вставке ссылок в виджет заметки они теперь по умолчанию вставляются отформатированными, то есть их можно открывать щелчком. А для очистки форматирования добавлен соответствующий пункт контекстного меню (Martin Frueh, Plasma 5.27. [Ссылка 1](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/303) и [ссылка 2](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/310))

В контекстное меню заголовков окон добавлен пункт для перемещения окна в другую комнату Plasma (Xaver Hugl, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=440036))

Клавиши навигации Home, End, Page Up и Page Down теперь работают должным образом в списке виджета «Буфер обмена» (Tom Warnke, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=461351))

Стандартное меню запуска приложений теперь показывает разделители, добавленные через Редактор меню KDE (Сергей Катунин, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1324)):

![2](https://pointieststick.files.wordpress.com/2023/01/image-1.png)

На очень маленьких экранах стандартное меню запуска приложений теперь использует более компактное представление, чтобы сэкономить пространство (Fushan Wen, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460544)):

![3](3_kickoff.png)

При вставке полного пути файла в адресную строку диалогового окна выбора файла, указанный файл будет выбран, как вы, видимо, и хотели (Fushan Wen, Frameworks 5.102. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=459900))

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Включение ночной цветовой схемы теперь можно вручную настроить на время после 19:00 (Martin Frueh, Plasma 5.26.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=415028))

Окна, активированные другими приложениями, расположенными на другом виртуальном рабочем столе, больше не перемещаются между рабочими столами, если это не настроено специально в Параметрах системы (Nicolas Fella, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=462996))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 6 ошибок в Plasma с очень высоким приоритетом (на одну больше, чем на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma стало 49 (на 2 больше, чем на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 133 ошибки были исправлены на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2022-12-30&chfieldto=2023-01-06&chfieldvalue=RESOLVED&list_id=2245254&query_format=advanced&resolution=FIXED)

## Автоматизация и систематизация

На [портале разработчика](https://develop.kde.org) появилась новая страница, посвящённая [распространению приложений KDE в виде пакетов Flatpak](https://develop.kde.org/docs/packaging/flatpak/integration/) (Thiago Sueto, [ссылка](https://invent.kde.org/documentation/develop-kde-org/-/merge_requests/222))

Также существенно обновлён [учебник по KXmlGui](https://develop.kde.org/docs/use/kxmlgui/) (Thiago Sueto, [ссылка](https://invent.kde.org/documentation/develop-kde-org/-/merge_requests/186))

Все ссылки на [портале разработчика](https://develop.kde.org) теперь автоматически проверяются на работоспособность, так что сломанные ссылки будут своевременно обнаруживаться и исправляться (Alessio Mattiazi, [ссылка](https://invent.kde.org/documentation/develop-kde-org/-/merge_requests/227))

## Другое

Matrix-клиент NeoChat теперь доступен в магазине приложений Microsoft! В этой версии сквозное шифрование включено по умолчанию, если вам интересно 🙂 (Ingo Klöcker и разработчики NeoChat. [Ссылка](https://apps.microsoft.com/store/detail/neochat/9PNXWVNRC29H?hl=ru-ru))

![4](4_neochat_win.png)

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

<!-- Временная ссылка! -->

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/01/06/this-week-in-kde-big-ui-improvements/>  

<!-- 💡: KMenuEdit → Редактор меню KDE -->
<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: directory → каталог -->
