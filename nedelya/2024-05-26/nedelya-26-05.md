# На этой неделе в KDE: тройная буферизация и другие вкусности

Мы только что выделили в отдельную ветку разработку Plasma 6.1 и [выпустили её бета-версию](https://kde.org/announcements/plasma/6/6.0.90/), поэтому новые функции добавляться больше не будут. Но мы успели сделать много всего интересного! Plasma 6.1 обещает быть большим и впечатляющим выпуском.

И гвоздём программы можно по праву считать поддержку тройной буферизации в Wayland! Благодаря этой технологии отрисовка экрана и анимации станут в целом более плавными — в идеале до уровня сеанса X11, который уже поддерживает тройную буферизацию. Этим долго занимался Xaver Hugl, результаты его усилий доступны в версии Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/4833)

Но это ещё не всё… далеко не всё:

> 19–26 мая, основное — прим. переводчика

## Новые возможности

В диспетчере файлов Dolphin появилась функция перемещения сразу нескольких выбранных элементов в новую папку (Ahmet Hakan Çelik, Dolphin 24.08. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=484555)):

![0](https://pointieststick.com/wp-content/uploads/2024/05/image-10.png)

Реализация портала XDG от KDE теперь включает в себя поддержку [портала захвата устройств ввода](https://flatpak.github.io/xdg-desktop-portal/docs/doc-org.freedesktop.portal.InputCapture.html) (David Redondo, Plasma 6.1. [Ссылка 1](https://invent.kde.org/plasma/xdg-desktop-portal-kde/-/merge_requests/283#note_949796), [ссылка 2](https://invent.kde.org/plasma/kwin/-/merge_requests/5742) и [ссылка 3](https://invent.kde.org/plasma/xdg-desktop-portal-kde/-/merge_requests/284))

Plasma теперь поддерживает включение и отключение функции некоторых ноутбуков Lenovo IdeaPad и Legion, позволяющей ограничить зарядку определённым уровнем (60 или 80 %, в зависимости от устройства) для продления срока службы батареи (Fabian Arndt, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=441057))

Экран блокировки теперь можно настроить на разблокировку без пароля, что позволит использовать его в качестве классической заставки (скринсейвера), включив красивый модуль обоев и отключив часы (Kristen McWilliam, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=442092))

## Улучшения пользовательского интерфейса

Plasma теперь перехватывает попытки активировать системный сигнал (обычно он настолько неприятный, что сразу же хочется кому-то сделать больно) и заменяет их приятным звуком из активной темы (Nicolas Fella, Plasma 6.1. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=381887), [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=451546) и [ссылка 3](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2249))

Строка поиска и запуска KRunner по умолчанию уже отдаёт приоритет приложениям, а теперь будет отдавать предпочтение ещё и страницам Параметров системы (Alexander Lohnau, Plasma 6.1, [ссылка 1](https://invent.kde.org/plasma/milou/-/merge_requests/80) и [ссылка 2](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2238))

На странице «Управление питанием» Параметров системы несколько элементов интерфейса, в которых использовались поля счётчиков, были заменены красивыми выпадающими списками. Это позволило исправить несколько ошибок и в большинстве случаев ускорить настройку — за счёт ограниченного выбора стандартных значений. При этом пользователь может указать в специальном диалоговом окне и конкретное значение (Jakob Petsovits, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/powerdevil/-/merge_requests/358)):

<https://i.imgur.com/2YISTOz.mp4?_=2>

Страница «Принтеры» Параметров системы теперь поможет установить пакет `system-config-printer`, улучшающий обнаружение принтеров, если он не был установлен в дистрибутиве сразу (Mike Noe, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/print-manager/-/merge_requests/142))

Получение информации от метеосервисов иногда бывает нестабильным, поэтому виджет Plasma «Прогноз погоды» теперь будет сообщать о проблеме и предлагать попробовать чуть позже (Nate Graham, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/588)):

![3](https://pointieststick.com/wp-content/uploads/2024/05/image-14.png)

Представление KRunner в Помощнике первого запуска существенно изменилось: теперь он показывает красивую анимацию использования этого инструмента! Кроме того, последняя страница была упрощена и теперь менее настойчиво требует от пользователя времени и денег (Oliver Beard, Plasma 6.1. [Ссылка 1](https://invent.kde.org/plasma/plasma-welcome/-/merge_requests/144) и [ссылка 2](https://invent.kde.org/plasma/plasma-welcome/-/merge_requests/157)):

* ![4](https://pointieststick.com/wp-content/uploads/2024/05/screenshot_20240524_110941.jpeg)
* ![5](https://pointieststick.com/wp-content/uploads/2024/05/screenshot_20240524_111010.jpeg)

Виджет Plasma «Прогноз погоды» больше не отображает страницу «Внешний вид» в окне настройки при использовании на рабочем столе, поскольку эта страница неприменима к такому режиму (Ismael Asensio, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=487341))

Страница «Управление службами» по умолчанию больше не отображается в Параметрах системы: все элементы управления на ней — детали реализации, и вмешательство в настройки здесь — прямой способ сломать систему. Если вы опытный пользователь, вы найдёте эту страницу через KRunner — но в Параметрах системы её больше не будет (Nicolas Fella, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2196))

Улучшен способ отрисовки SVG-изображений на экране при использовании дробного масштаба: картинка получается менее размытой (Marco Martin, Frameworks 6.3. [Ссылка](https://invent.kde.org/frameworks/ksvg/-/merge_requests/37))

## Исправления ошибок

Анализатор дисков Filelight больше не считает, что файлы, хранящиеся в облаке OneDrive, являются локальными и занимают место на диске (Harald Sitter, Filelight 24.05.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=486549))

В утилите для выбора цвета KColorChooser кнопка «Взять цвет с экрана» отсутствовала, если использовался сеанс Wayland, — эта ошибка устранена (Thomas Weißschuh, KColorChooser 24.05.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=479406))

Повысилась устойчивость Plasma к сбоям в случаях, если у виджетов неверные значения размера — что бывает при определённых обстоятельствах (Marco Martin, Plasma 6.0.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=487230))

Когда диспетчер окон KWin возвращался к использованию программного курсора после того, как графический драйвер отклонил использование аппаратного, при определённых обстоятельствах мог блокироваться весь экран — например, если приложения на компьютерах Mac с чипами Apple использовали слой совместимости XWayland. Теперь эта ошибка устранена (Xaver Hugl, Plasma 6.0.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=487037))

Утилита Spectacle больше не делает размытые снимки экрана в конфигурациях с несколькими экранами с разным масштабом (Владимир Золотопупов, Plasma 6.0.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=483876))

В NixOS и других дистрибутивах, многократно автоматически пересоздающих кеш `sycoca`, глобальные комбинации клавиш теперь работают более надёжно и стабильно (Влад Загородний, Plasma 6.0.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=484682))

Исправлено несколько давних проблем в приложении Параметры системы, из-за которых переключение страниц, очистка строки поиска и открытие страницы извне приложения приводили к тому, что столбец подкатегории отображал не то, что нужно (Nicolas Fella, Plasma 6.0.5. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=440487), [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=446288) и [ссылка 3](https://bugs.kde.org/show_bug.cgi?id=434345))

Исправлена ошибка, из-за которой выключение внешнего монитора, подключённого к ноутбуку с закрытой крышкой, могло привести к сбою KWin (Xaver Hugl, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=483008))

Plasma в сеансе Wayland больше не закрывается, если открыть огромное количество окон (Xaver Hugl, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=479492))

Категория «Жесты активации» на странице «Специальные возможности» в Параметрах системы вернулась — после того как была случайно удалена при переходе на QML (Nicolas Fella, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=457838))

Если в сеансе Wayland запустить приложение со значками в системном лотке, то в верхнем левом углу экрана мог появиться маленький невидимый квадрат, который «съедал» ввод текста, а при определённых вариантах расположения экранов повышалась загрузка процессора — эти ошибки исправлены (David Edmundson, Plasma 6.1. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=433079) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=425271))

KWin теперь правильнее определяет физические размеры экранов (Jakub Piecuch, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=486585))

При использовании панели Plasma с шириной «По содержимому», когда на ней виджет «Панель задач (только значки)» и больше ничего, в правой части панели при входе в систему больше нет ненужного пустого места (Akseli Lahtinen, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=487310))

В диалоговом окне выбора окон и экранов для демонстрации нажатие на *флажки* для выбора элементов теперь работает. Раньше приходилось нажимать на сами элементы (флажки не срабатывали) — теперь работают оба варианта (Nate Graham, Frameworks 6.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=481461))

Исправлено несколько проблем, из-за которых некоторые значки Breeze не могли правильно скорректировать свой цвет для тёмной цветовой схемы, а также устранены проблемы с созданием статических значков, совместимых с тёмной темой (Corbin Schwimmbeck, Frameworks 6.3. [Ссылка 1](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/375) и [ссылка 2](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/372))

При активации в неактивном окне контекстные меню теперь с меньшей вероятностью будут отображаться в виде странных отдельных окон с заголовками (Влад Загородний, Qt 6.7.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=481933))

Другие сведения об ошибках:

* Остаётся 3 ошибки в Plasma с очень высоким приоритетом (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 36 (на прошлой неделе было 39). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 105 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-5-17&chfieldto=2024-5-24&chfieldvalue=RESOLVED&list_id=2722400&query_format=advanced&resolution=FIXED)

## Производительность и технические улучшения

Снижен пропуск кадров на различном оборудовании (Xaver Hugl, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/5759))

Ускорены запуск центра приложений Discover и его реакция при прокрутке длинных списков приложений, когда активен модуль поддержки Flatpak (Aleix Pol Gonzalez, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/discover/-/merge_requests/821))

## Как вы можете помочь

Сообщество KDE стало важным явлением в мире — благодаря вашему труду и вложенному времени! Однако по мере роста проекта важно обеспечить стабильность работы над ним, а это значит, что труд должен *оплачиваться*. Сегодня KDE по большей части использует работу людей, которым фонд KDE e.V. не платит, и это проблема. Мы предприняли шаги, которые позволят изменить ситуацию, и наняли [платных технических подрядчиков](https://ev.kde.org/corporate/staffcontractors/), однако из-за ограниченности финансовых ресурсов этого всё ещё мало. Если вы хотите помочь, [сделайте пожертвование](https://kde.org/ru/community/donations/)!

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* коммерческий  
*Источник:* <https://pointieststick.com/2024/05/25/this-week-in-kde-triple-buffering-and-other-sources-of-amazingness/>  
