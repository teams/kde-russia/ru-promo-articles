# На этой неделе в KDE: сенсор, GTK, значки

> 11–17 ноября — прим. переводчика

У нас есть несколько классных штук для отчёта, и я думаю, они вам понравятся! В особенности, я думаю, народ заинтересуют улучшения в интеграции приложений GNOME/GTK, а также два набора исправлений, относящихся к сенсорному вводу и прокрутке в Okular и в меню запуска приложений; подробности ниже:

## Новые возможности

Приложения GNOME и GTK [теперь наследуют настройки шрифтов, значков, курсоров и панелей инструментов от приложений KDE, вместо того чтобы заставлять вас настраивать их отдельно где-то ещё](https://phabricator.kde.org/D24743) (Михаил Золотухин, Plasma 5.18.0)

Флажки и радиокнопки в приложениях на основе GTK 3 [снова следуют цветам цветовой схемы](https://phabricator.kde.org/D25336). [Изменение в библиотеке librsvg исправляет проблемы, которые у нас были с этой функцией ранее](https://gitlab.gnome.org/GNOME/librsvg/issues/525), так что мы можем вернуть её обратно в следующем выпуске Plasma! (Carson Black, Plasma 5.18.0)

При прокрутке в просмотрщике документов Okular при помощи мыши, сенсорного экрана или клавиатуры [перемещение области просмотра теперь анимировано и имеет инерцию](https://bugs.kde.org/show_bug.cgi?id=413989). Я считаю это огромным улучшением для чтения документов на ноутбуке-трансформере! (Kezi Olio, Okular 1.10.0)

В меню запуска приложений по умолчанию [сильно улучшена поддержка сенсорного ввода, что включает сенсорную прокрутку, перетаскивание элементов и долгое нажатие для открытия контекстного меню](https://phabricator.kde.org/D21829) (Steffen Hartlieb, Plasma 5.18.0)

## Исправления ошибок и улучшения производительности

Интеграция Git в диспетчер файлов Dolphin [теперь надёжнее отображает статус и доступные действия для больших репозиториев](https://bugs.kde.org/show_bug.cgi?id=413870) (Maciej Dems, Dolphin 19.12.0)

При использовании панели сведений в Dolphin столбец «Дата съёмки» [больше не пустой для файлов JPEG, содержащих корректную информацию EXIF о дате и времени](https://bugs.kde.org/show_bug.cgi?id=411173) (Méven Car, Dolphin 19.12.0)

Исправлена ошибка, которая [могла приводить к зависанию экрана блокировки при проигрывании медиа с квадратной обложкой альбома](https://bugs.kde.org/show_bug.cgi?id=413087) (David Edmundson, Plasma 5.17.3)

Повёрнутые экраны [теперь помнят своё положение относительно других дисплеев после перезагрузки системы](https://bugs.kde.org/show_bug.cgi?id=413627) (Roman Gilg, Plasma 5.17.3)

Ползунки полос прокрутки [снова правильного цвета в приложениях на основе GTK и в браузере Firefox](https://bugs.kde.org/show_bug.cgi?id=413498) (Carson Black, Plasma 5.17.3)

[Устранено распространённое падение приложения Параметры системы](https://bugs.kde.org/show_bug.cgi?id=414003), которое могло быть вызвано посещением одной и той же категории дважды (David Edmundson, Frameworks 5.65)

Образы дисков, которые были примонтированы и размонтированы, [теперь исчезают из виджета «Подключаемые устройства», как и ожидалось](https://bugs.kde.org/show_bug.cgi?id=394348) (Rok Mandeljc, Frameworks 5.65)

Удаление файлов [теперь многопоточное, так что, например, удаление большого файла больше не «подвешивает» Dolphin](https://bugs.kde.org/show_bug.cgi?id=390748) (Méven Car, Frameworks 5.65)

## Улучшения пользовательского интерфейса

При наведении курсора на файл в Dolphin [информация о файле, показываемая в строке состояния, больше не исчезает через секунду](https://bugs.kde.org/show_bug.cgi?id=399267) (Méven Car, Dolphin 19.12.0)

Метаданные файлов [теперь доступны, когда индексатор файлов Baloo выполняет первоначальное индексирование](https://bugs.kde.org/show_bug.cgi?id=410114) (Stefan Brüns, Dolphin 20.04.0)

Строка поиска в Редакторе меню KDE [теперь в фокусе по умолчанию, как это работает и в других приложениях с всегда видимыми строками поиска](https://bugs.kde.org/show_bug.cgi?id=413563) (Nate Graham, Plasma 5.18.0)

Значок выбора цвета [теперь использует узнаваемую пиктограмму пипетки](https://bugs.kde.org/show_bug.cgi?id=403924) (Nate Graham, Frameworks 5.65):

![0](https://i.imgur.com/U020Hv2.png)

Добавлены [новые значки для поиска](https://phabricator.kde.org/D24959) и [индексатора файлов Baloo](https://phabricator.kde.org/D24957) (Alexander Stippich, Frameworks 5.65):

![1](https://i.imgur.com/3qJt6eh.png)

Утилита для создания снимков экрана Spectacle [теперь предлагает OBS Studio как другую опцию для записи экрана](https://bugs.kde.org/show_bug.cgi?id=412320) (Méven Car, Frameworks 5.65)

## Как вы можете помочь

Вам нравятся значки? Конечно, нравятся! Но что более важно, заинтересованы ли в том, чтобы помочь их делать? Я уверен, что да! И правильно, [потому что у нас много работы](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&component=Icons&list_id=1682703&product=Breeze&query_format=advanced)! К счастью, это на самом деле очень легко, и у нас есть [страница в Руководстве по разработке интерфейсов (HIG), описывающая правила](https://develop.kde.org/hig/style/icons/index.html), которые помогут вам двигаться в правильном направлении. Значки в теме Breeze — это векторные файлы в формате SVG, и мы используем Inkscape, чтобы создавать и править их. Я начал участвовать в работе над значками недавно без какого-либо опыта в иконографии или Inkscape. Моя работа была примитивной, но участники VDG *очень вежливо и терпеливо* помогли мне продвинуться, и вы тоже можете это сделать! Вот как: <https://community.kde.org/Guidelines_and_HOWTOs/Submit_an_icon>

Ну и, как обычно, вы можете прочитать [эту статью](https://kde.ru/join) и выяснить, как стать частью того, что действительно имеет значение. Каждый участник KDE играет огромную роль; к вам не относятся как к очередному сотруднику или винтику системы. Программистом вам тоже быть не требуется. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [пожертвования](https://kde.org/ru/community/donations/) средств в фонд [KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [boingo-00](https://t.me/boingo00)  
*Источник:* <https://pointieststick.com/2019/11/16/this-week-in-kde-touchy-and-scrolly-and-gtk-ey-and-iconey/>  

<!-- 💡: KMenuEdit → Редактор меню KDE -->
<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: application launcher → меню запуска приложений -->
<!-- 💡: icon → значок -->
<!-- 💡: icon theme → набор значков -->
<!-- 💡: upstream → [компонент-]зависимость | исходный репозиторий -->
