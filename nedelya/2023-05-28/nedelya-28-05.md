# На этой неделе в KDE: ночная цветовая схема в Wayland с NVIDIA

Главная новость этой недели, пожалуй, в том, что в Plasma 6 функция ночной цветовой схемы [будет работать как положено в сеансе Wayland у пользователей видеокарт NVIDIA](https://bugs.kde.org/show_bug.cgi?id=450327)! Поскольку драйверы NVIDIA, в отличие от Intel и AMD, не поддерживают таблицу поиска гаммы, необходимую для оптимальной реализации этой функции, нам пришлось использовать менее эффективный подход — но, надеюсь, это всё же лучше, чем если бы она вообще не работала. Если для вас критично повышенное использование ресурсов, вы можете не использовать эту возможность. Спасибо Xaver Hugl за работу над этим!

## Улучшения пользовательского интерфейса

Строка поиска и запуска KRunner теперь должна возвращать более полезную выдачу для коротких поисковых запросов из 2-3 символов (Alexander Lohnau, Plasma 5.27.6. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=469769))

Улучшена навигация с помощью клавиатуры в боковой панели Параметров системы: теперь можно использовать клавиши со стрелками вместо табуляции, если это удобнее или если у вашего устройства есть крестовина, но нет очевидной клавиши табуляции (Иван Ткаченко, Plasma 5.27.6. [Ссылка](https://invent.kde.org/plasma/systemsettings/-/merge_requests/230))

В сеансе Plasma Wayland при вызове эффекта диспетчера окон жестом сенсорной панели его теперь можно закрыть обратным по направлению жестом (Aleix Pol Gonzalez, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/4070))

Страница «Поиск файлов» Параметров системы была визуально обновлена и теперь выглядит лучше (Nate Graham и Helden Hoierman, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1524)):

![0](https://pointieststick.files.wordpress.com/2023/05/baloo-kcm-overhaul.jpeg)

Всплывающие подсказки панели задач с миниатюрами окон больше не исчезают сами по себе, пока на их задачу наведён курсор мыши, что соответствует поведению всех остальных всплывающих подсказок (Nate Graham, Plasma 6. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=470043))

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

При использовании горизонтальной панели Plasma внизу экрана, всплывающие подсказки панели задач больше не появляются иногда в неправильном месте (Bharadwaj Raju, Plasma 5.27.6. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=463272))

В сеансе Plasma X11 перетаскивание файлов на плитки панели задач с попутным щелчком правой кнопкой мыши больше не приводит иногда к странному поведению (Fushan Wen, Plasma 5.27.6. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=466675))

В сеансе Plasma Wayland некоторые прозрачные и размытые контекстные меню с темой Breeze больше не становятся иногда визуально сломанными (Mouse Zhang, Plasma 5.27.6. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=437062))

Когда приложения KDE запускаются с тёмной цветовой схемой в рабочих средах, отличных от Plasma, без пакета `plasma-integration`, значки Breeze теперь используют светлые цвета, а не остаются тёмными и нечитаемыми (Jan Grulich, Frameworks 5.107. [Ссылка](https://invent.kde.org/frameworks/kiconthemes/-/merge_requests/95))

Если ваша система настроена на шифрование домашнего каталога, миниатюры предварительного просмотра файлов и папок с других зашифрованных носителей теперь будут кешироваться в своём обычном расположении в вашем домашнем каталоге, чтобы их не приходилось повторно создавать при каждом доступе (Payton Quinn, Frameworks 5.107. [Ссылка](https://invent.kde.org/frameworks/kio/-/merge_requests/1287))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 4 ошибки в Plasma с очень высоким приоритетом (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma остаётся 52 (на прошлой неделе было 50). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 76 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-05-19&chfieldto=2023-05-26&chfieldvalue=RESOLVED&list_id=2377039&query_format=advanced&resolution=FIXED)

## Как вы можете помочь

Если вы пользователь, обновитесь до [Plasma 5.27](https://www.kde.org/announcements/plasma/5/5.27.0/)! Если обновление в вашем дистрибутиве не доступно и не предвидится, подумайте о переходе на другой дистрибутив, поставляющий программное обеспечение ближе к графикам разработчиков.

Разработчики, пожалуйста, переходите на Plasma 6 и начинайте исправлять ошибки в ней! Она годится для повседневного использования энтузиастами (я в их числе), хоть и всё ещё очень сырая и требует много работы, чтобы стать готовой к выпуску до конца года.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

<!-- Временная ссылка! -->

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/05/26/this-week-in-kde-night-color-on-wayland-with-nvidia/>  

<!-- 💡: GPU → видеокарта / графический ускоритель -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: directory → каталог -->
<!-- 💡: icons → значки -->
<!-- 💡: location → расположение (в файловой системе) / местоположение (на карте) -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: thumbnail → миниатюра -->
