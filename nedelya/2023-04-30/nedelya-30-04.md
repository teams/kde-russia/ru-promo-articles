# На этой неделе в KDE: резня с ошибками продолжается

Взятый на прошлой неделе курс на устранение недочётов сохранился и на этой неделе: число ошибок в Plasma с очень высоким приоритетом сократилось до трёх! Кроме того, теперь, когда Plasma 6 немного устаканилась, стали появляться улучшения пользовательского интерфейса для неё. Между прочим, эту заметку я пишу из самого настоящего сеанса Plasma 6!

## Улучшения пользовательского интерфейса

Во время комментирования снимка экрана в утилите Spectacle при наведении мыши на фигуры вокруг них теперь показывается обводка, так что понятно, как их выбирать (Noah Davis, Spectacle 23.08. [Ссылка](https://invent.kde.org/graphics/spectacle/-/merge_requests/233))

В виджете Plasma «Диски и устройства» для устройств, подключённых по протоколу MTP, больше не отображается бесполезное действие «Подключить» (Nate Graham, Plasma 5.27.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=446278))

Настройка панелей стала намного нагляднее, и у опций появились пояснения (Tanbir Jishan и Niccolò Venerandi, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1483)):

![0](https://pointieststick.files.wordpress.com/2023/04/new-edit-settings.jpg)

Поиск в центре приложений Discover стал умнее и теперь выше ценит полные совпадения с названиями и вхождение запроса в названия, а не в описания (Aleix Pol Gonzalez, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=432845))

Результаты поиска в меню запуска приложений теперь упорядочиваются так же, как в строке поиска и запуска KRunner (Alexander Lohnau, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=431204))

Математические функции вроде `sqrt()` теперь можно вычислять в KRunner без необходимости ставить перед выражением знак равенства (Alexander Lohnau, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=467418))

Когда изображение, выбранное в качестве обоев, изменяется на диске, обои теперь сразу же обновляются (Олег Соловьёв, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2840))

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Режим выделения в диспетчере файлов Dolphin больше не переключается клавишей пробела, если она не назначена на это действие или в интерфейсе выделен какой-либо интерактивный элемент управления (Евгений Попов, Dolphin 23.08. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=465489))

Устранён недавно появившийся источник сбоев диспетчера окон KWin в сеансе Plasma Wayland при наведении курсора на значки в панели задач или закрытии окон (Aleix Pol Gonzalez, Plasma 5.27.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=469055))

Приложение Параметры системы больше не вылетает при запуске, когда повреждена база данных комнат Plasma (Иван Ткаченко, Plasma 5.27.5. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1490))

Исправлена ещё одна причина однопиксельного перекрытия экранов в многомониторных конфигурациях, что вызывало другое странное поведение (Harald Sitter, Plasma 5.27.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=455394))

Кнопка разблокировки на экране блокировки теперь всегда срабатывает с первого нажатия в многомониторных конфигурациях (Harald Sitter, Plasma 5.27.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=456210))

Длинные заголовки уведомлений в истории больше не выталкивают иногда частично кнопку закрытия уведомления за границу видимой области (Евгений Попов, Plasma 5.27.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=468176))

При подключении устройства Bluetooth оно больше не перекрывается на короткое время разделителем между подключёнными и отключёнными устройствами (Harald Sitter, Plasma 5.27.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=438610))

Индикаторы выполнения в стиле Breeze, находящиеся вне видимой области, больше не анимируются и не потребляют ресурсы процессора во всём программном обеспечении на основе Qt (Иван Ткаченко, Plasma 5.27.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=468903))

Ввод путей с пробелами теперь работает правильно в диалоговом окне «Свойства», а также на страницах «Комбинации клавиш» и «Автозапуск» Параметров системы (Bharadwaj Raju, Plasma 5.27.5 и Frameworks 5.106, [ссылка 1](https://bugs.kde.org/show_bug.cgi?id=467369), [ссылка 2](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1489) и [ссылка 3](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2857))

Исправлена основная причина сбоев при копировании файлов в Dolphin и других приложениях (Fushan Wen, Frameworks 5.106. [Ссылка](https://invent.kde.org/frameworks/kio/-/merge_requests/1260))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Осталось 3 ошибки в Plasma с очень высоким приоритетом (на прошлой неделе было 6). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma остаётся 47 (как на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 208 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-04-21&chfieldto=2023-04-28&chfieldvalue=RESOLVED&list_id=2350123&query_format=advanced&resolution=FIXED)

## Как вы можете помочь

Разработчики, помогите нам исправить [известные проблемы в Plasma 5.27](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&keywords=regression%2C%20&keywords_type=allwords&list_id=2278517&query_format=advanced&version=5.26.90&version=5.27.0&version=5.27.1&version=5.27.2&version=5.27.3&version=5.27.4&version=5.27.5). Также посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этих списков — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

<!-- Временная ссылка! -->

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/04/28/this-week-in-kde-the-bug-slaughterfest-continues/>  

<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icons → значки -->
<!-- 💡: playlist → список воспроизведения -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: task manager → панель задач -->
