# На этой неделе в KDE: настройка порядка результатов в KRunner

Это была важная неделя для строки поиска и запуска KRunner! Кроме того, число отслеживаемых проблем в Plasma 6 продолжает снижаться. Спасибо всем, кто этому содействует!

## Plasma 6

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 81](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

В настройках KRunner теперь можно отметить определённые типы результатов поиска как избранные, и они всегда будут появляться вверху выдачи! (Alexander Lohnau, [ссылка](https://bugs.kde.org/show_bug.cgi?id=340283)):

![0](https://pointieststick.files.wordpress.com/2023/09/image-2.png)

Ещё KRunner был существенно оптимизирован (Alexander Lohnau, [ссылка](https://write.as/alexander-lohnau/profiling-and-optimizing-krunner))

Также удалось повысить производительность диспетчера окон KWin, в том числе снизив объём впустую выполняемой им работы по перерисовке не изменившихся слоёв изображения (Xaver Hugl, [ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/4207))

Производительность Plasma тоже подросла: различные части Plasma и Параметров системы теперь запускаются вплоть до сотен миллисекунд быстрее (Fushan Wen, [ссылка 1](https://bugs.kde.org/show_bug.cgi?id=473798), [ссылка 2](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3234), [ссылка 3](https://invent.kde.org/frameworks/kdeclarative/-/merge_requests/214) и [ссылка 4](https://invent.kde.org/frameworks/knewstuff/-/merge_requests/273))

Значок «обновления» из набора Breeze и все другие значки в виде «круга со стрелками» были обновлены и теперь используют более приятный стиль стрелок (Philip Murray, [ссылка](https://bugs.kde.org/show_bug.cgi?id=462635)):

![1](https://pointieststick.files.wordpress.com/2023/09/image-3.png)

Из строк поиска, основанных на KRunner, в случае поддержки устройством теперь можно вручную вызвать гибридный спящий режим, когда система сначала переходит в ждущий режим, а спустя несколько часов — в спящий (Natalie Clarius, [ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3227)):

![2](https://pointieststick.files.wordpress.com/2023/09/image-1.png)

Виджет «Настройка экранов» стал менее навязчив и теперь появляется в видимой части системного лотка только при включённом режиме презентации (Fushan Wen, [ссылка](https://invent.kde.org/plasma/kscreen/-/merge_requests/228))

## Улучшения пользовательского интерфейса

Миниатюры HDR-изображений, просматриваемые в приложениях, не использующих режим HDR, теперь преобразуются в цветовое пространство sRGB, чтобы их действительно можно было просмотреть (Mirco Miranda, kio-extras 23.12. [Ссылка](https://invent.kde.org/network/kio-extras/-/merge_requests/278))

Многопроцессная архитектура эмулятора терминала Konsole научилась помещать каждый процесс в собственную контрольную группу диспетчера служб Systemd, благодаря чему они теперь корректно отображаются как дочерние процессы Konsole в Системном мониторе (Theodore Wang, Konsole 23.12. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=439805))

## Другие заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Исправлена ещё одна причина сбоев Plasma при смене оформления рабочей среды (Harald Sitter, Plasma 5.27.8. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=454946))

Фильтрация по категории в списке виджетов Plasma снова работает, когда язык системы отличен от английского — извините за недавнюю поломку! (David Redondo, Plasma 5.27.8. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=473035))

Исправлен один из самых распространённых случайных и необъяснимых для пользователя сбоев в Plasma (David Edmundson, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=445893))

В сеансе Plasma Wayland режим размещения окон «Распахнуто» больше не применяется по ошибке к экранным уведомлениям и меню (David Edmundson, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=428147))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Осталось 3 ошибки в Plasma с очень высоким приоритетом (на прошлой неделе было 4). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma остаётся 57 (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 103 ошибки были исправлены на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-08-25&chfieldto=2023-09-01&chfieldvalue=RESOLVED&list_id=2458620&query_format=advanced&resolution=FIXED)

## Автоматизация и систематизация

Добавлены автоматические тесты для компонента текста-заполнителя `PlaceholderMessage` из библиотеки Kirigami (Иван Ткаченко, [ссылка](https://invent.kde.org/frameworks/kirigami/-/merge_requests/1227))

Добавлены автоматические тесты для виджета «Проигрыватель» (Fushan Wen, [ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3171))

## Как вы можете помочь

Разработчики, пожалуйста, помогите исправить проблемы в [Qt 6, KDE Frameworks 6 и Plasma 6](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&f1=keywords&f2=product&list_id=2398791&o1=allwordssubstr&o2=notequals&query_format=advanced&v1=qt6&v2=neon)! Plasma 6 вполне годится для повседневного использования энтузиастами (я в их числе), хоть и всё ещё требует работы, чтобы стать готовой к выпуску до конца года.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/09/01/this-week-in-kde-custom-ordering-for-krunner-search-results/>  

<!-- 💡: OSDs → экранные уведомления -->
<!-- 💡: System Monitor → Системный монитор -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: icon theme → набор значков -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: thumbnail → миниатюра -->
