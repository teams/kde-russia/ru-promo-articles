# На этих неделях в KDE: диалоги аутентификации наигрались в прятки

Помимо изменений, внесённых и объявленных в ходе недавней [встречи разработчиков Plasma](https://vk.com/@kde_ru-plasma6-better-defaults) в офисе Tuxedo Computers в Аугсбурге, за прошедшие две недели у нас накопились и другие наработки!

## Новые возможности

Текстовый редактор Kate теперь поддерживает работу с языковым сервером (LSP) игрового движка Godot (Michael Alexsander, Kate 23.08. [Ссылка](https://invent.kde.org/utilities/kate/-/merge_requests/1220))

В просмотрщике документов Okular теперь можно копировать текст комментария, используя его контекстное меню в списке комментариев на боковой панели (Kai Uwe Broulik, Okular 23.04. [Ссылка](https://invent.kde.org/graphics/okular/-/merge_requests/735))

Добавлена команда `kinfo`, которую можно запустить в командной строке для вывода номеров версий различных программных компонентов, что может пригодиться для отладки и при составлении отчётов об ошибках. Эта команда отображает ту же информацию, что и на странице «О системе» в Параметрах системы и в приложении Информация о системе (Harald Sitter, Plasma 5.27.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=403077)):

![0](https://pointieststick.files.wordpress.com/2023/05/kinfo.jpg)

Для экономии энергии заблокированные экраны теперь выключаются быстрее, чем разблокированные. Задержка настраивается и по умолчанию равна минуте (Méven Car, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=348529))

По цветному кружку в виджете выбора цвета теперь можно щёлкнуть средней кнопкой мыши, чтобы скопировать код цвета в буфер обмена (Fushan Wen, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/337))

При регулировке громкости с помощью горячих клавиш или прокрутки над виджетом «Громкость» теперь можно удерживать клавишу Shift, чтобы менять значение с шагом в 1% (Fushan Wen, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/175))

## Улучшения пользовательского интерфейса

В новом интерфейсе комментирования утилиты для создания снимков экрана Spectacle стало так же легко, как и в версии 22.12, рисовать фигуры без заливки — например, чтобы добавить идеальную окружность вокруг чего-то для привлечения внимания (Noah Davis, Spectacle 23.04.1. [Ссылка](https://invent.kde.org/graphics/spectacle/-/merge_requests/236))

Значительно повышена производительность прокрутки в списке воспроизведения музыкального проигрывателя Elisa, когда в нём много дорожек (Nicolas Fella, Elisa 23.04.1. [Ссылка](https://invent.kde.org/multimedia/elisa/-/merge_requests/454))

При перетаскивании ползунка громкости в Elisa теперь появляется всплывающая подсказка с текущим уровнем (Jack Hill, Elisa 23.08. [Ссылка](https://invent.kde.org/multimedia/elisa/-/merge_requests/451)):

![1](https://pointieststick.files.wordpress.com/2023/05/image-1.png)

Центр приложений Discover теперь ещё усерднее пытается отправить вас за помощью к вашему дистрибутиву, когда возникает проблема с обновлением (Nate Graham. Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/discover/-/merge_requests/547)):

![2](https://pointieststick.files.wordpress.com/2023/05/discover-crushed.jpg)

*Смотрите-ка, номера версий Discord снова не в том порядке! Но в этот раз виновником оказался не Discover, а Flathub.*

При поиске через KRunner, когда поисковый запрос совпадает как с названием приложения, так и с именем файла без расширения, приложение теперь в приоритете (Alexander Lohnau, Plasma 5.27.5. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2909))

При смене виртуального рабочего стола через верхнюю полосу в эффекте «Обзор» переключение теперь происходит в самом эффекте вместо его закрытия (Влад Загородний, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=448668))

QR-код подключённой сети теперь отображается в самом виджете сетевых подключений, а не огромным полноэкранным слоем (Kai Uwe Broulik, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/plasma-nm/-/merge_requests/248))

В наборе значков Breeze маленькие монохромные версии значков исполняемых файлов MS DOS теперь учитывают выбранную цветовую схему, как и ожидалось (Michael Alexander, Frameworks 5.106. [Ссылка](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/259))

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

В Okular теперь можно «Сохранить как…» документ, изменённый или удалённый извне, чтобы не потерять ваши несохранённые изменения (Nate Graham, Okular 23.04.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=402017))

Диалог аутентификации больше не будет иногда появляться *за* окном, которое его запросило, как это часто бывало при запуске Диспетчера разделов (Harald Sitter, Plasma 5.27.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=312325))

В сеансе Wayland исправлена ошибка, из-за которой Plasma могла неоднократно падать при входе в систему с определёнными комбинациями настроек (David Redondo, Plasma 5.27.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=468408))

В сеансе Wayland текст, скопированный из какого-либо приложения при работающем диспетчере буфера обмена (Klipper) с нормальными настройками, теперь по-прежнему можно вставить в другое место после закрытия этого приложения (Tobias Fella, Plasma 5.27.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=468422))

В сеансе Wayland заработали основные функции залипания клавиш! (Nicolas Fella, Plasma 6.0. [Ссылка 1](https://invent.kde.org/plasma/kwin/-/merge_requests/3365), [ссылка 2](https://invent.kde.org/plasma/kwin/-/merge_requests/4073) и [ссылка 3](https://invent.kde.org/plasma/kwin/-/merge_requests/4075))

В сеансе Wayland заработала кинетическая прокрутка в приложениях GTK (Влад Загородний, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=454428))

В сеансе Wayland теперь работает поиск в KRunner по содержимому буфера обмена с помощью сочетания клавиш `Alt+Shift+F2` (Ismael Asensio, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=451747))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 3 ошибки в Plasma с очень высоким приоритетом (как и две недели назад). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 46 (две недели назад было 47). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 199 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-04-29&chfieldto=2023-05-12&chfieldvalue=RESOLVED&list_id=2362645&query_format=advanced&resolution=FIXED)

## Изменения вне KDE, затрагивающие KDE

При запуске Plasma с включённой поддержкой Systemd (как это по умолчанию уже год или два) приложения, добавленные в автозапуск, в `.desktop`-файле которых есть ключ `X-GNOME-Autostart-Phase`, теперь будут автоматически запускаться, как и ожидалось (David Edmundson, Systemd 254. [Ссылка](https://github.com/systemd/systemd/issues/18791))

PDF-файлы, подписанные электронной подписью через Okular, больше не блокируют подписывание кем-либо другим через Adobe Acrobat (Nicolas Fella при поддержке Технического университета Дрездена, Poppler 23.05.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=461371))

## Как вы можете помочь

Если вы пользователь, обновитесь до [Plasma 5.27](https://www.kde.org/announcements/plasma/5/5.27.0/)! Если обновление в вашем дистрибутиве не доступно и не предвидится, подумайте о переходе на другой дистрибутив, поставляющий программное обеспечение ближе к графикам разработчиков.

Разработчики, помогите нам исправить [известные проблемы в Plasma 5.27](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&keywords=regression%2C%20&keywords_type=allwords&list_id=2278517&query_format=advanced&version=5.26.90&version=5.27.0&version=5.27.1&version=5.27.2&version=5.27.3&version=5.27.4&version=5.27.5), или же соберите из исходного кода Plasma 6 и начните работать над неразрешёнными проблемами и первоочередными задачами переноса на Qt 6. Также посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Работа в любой из этих областей позволяет быстро внести значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

<!-- Временная ссылка! -->

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/05/11/these-weeks-in-kde-no-more-hidey-hidey-authentication-dialog-games/>  

<!-- 💡: Info Center → Информация о системе -->
<!-- 💡: Klipper → диспетчер буфера обмена (Klipper) -->
<!-- 💡: OSD → экранное уведомление -->
<!-- 💡: Partition Manager → Диспетчер разделов -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icons → значки -->
<!-- 💡: playlist → список воспроизведения -->
