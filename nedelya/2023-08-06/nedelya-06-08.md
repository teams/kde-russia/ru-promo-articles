# На этой неделе в KDE: портирование, Dolphin и KWin

В последнее время мы старались поднажать с работой, связанной с портированием Plasma, так что исправлений и улучшений интерфейса на этой неделе не так много — но кое-какие всё же есть, особенно в диспетчере окон KWin и в диспетчере файлов Dolphin!

## Plasma 6

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 82](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

По мере портирования страницы Параметров системы избавляются от двойных подвалов, перемещая некоторые действия в область заголовка. Выглядит отлично:

![custom-0](image.webp)

При попытке скопировать на рабочий стол файлы, которые не поместятся там из-за нехватки места на диске, теперь отображается уведомление об этом (Thenujan Sandramohan, [ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1646))

В сеансе Plasma Wayland курсоры мыши теперь выглядят лучше при использовании дробных коэффициентов масштабирования (Влад Загородний, [ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/4280))

## Улучшения пользовательского интерфейса

Когда в просмотрщике изображений Gwenview отключена анимация, переключение между двумя изображениями теперь происходит бесшовно, так как в процессе не мелькает пустой холст; для изображений, которые загружаются дольше — например, из-за большого размера или медленного сетевого ресурса — по-прежнему показывается индикатор загрузки (Felix Ernst, Gwenview 23.08. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=472351))

Адресную строку Dolphin теперь также можно выбрать комбинацией клавиш Alt+D (Amol Godbole, Dolphin 23.12. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=468445))

Смена масштаба в Dolphin прокруткой колеса мыши с зажатым Ctrl теперь всегда работает в ожидаемую сторону в сеансе Wayland (Amol Godbole, Dolphin 23.12. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=469354))

Режим выделения в Dolphin теперь можно вызвать также долгим касанием на сенсорном экране уже выбранных файлов или папок (Steffen Hartlieb, Dolphin 23.12. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=462778))

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

При поиске в стандартном меню запуска приложений исходно выбранным элементом теперь всегда служит первый, а не тот, который случайно оказался под курсором мыши (Fushan Wen, Plasma 5.27.7. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=466192))

В сеансе Plasma Wayland переключение между окнами по Alt+Tab больше не происходит в обратном порядке, когда включён Caps Lock (Xaver Hugl, Plasma 5.27.8. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=453918))

Исправлена ​​ошибка, из-за которой ночная цветовая схема не отключалась после выхода компьютера из ждущего режима в более позднее время, чем то, на которое настроено автоматическое переключение (Xaver Hugl, Plasma 5.27.8. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=461657))

В экранных уведомлениях о смене громкости и яркости подпись «100 %» больше не разбивается иногда на две строки в некоторых языках, в том числе в русском (Kai Uwe Broulik, Plasma 5.27.8. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=469576))

Исправлена ​​ошибка, из-за которой некоторые файлы могли пропускаться при копировании очень большого числа объектов (Harald Sitter, Frameworks 5.109. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=472716))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 6 ошибок в Plasma с очень высоким приоритетом (на прошлой неделе было 5). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 58 (на прошлой неделе было 62). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 113 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-07-28&chfieldto=2023-08-04&chfieldvalue=RESOLVED&list_id=2435975&query_format=advanced&resolution=FIXED)

## Автоматизация и систематизация

Монохромные значки для набора Breeze Dark теперь автоматически создаются во время сборки из светлого Breeze, что устраняет потребность в куче ручной работы! (Noah Davis, Frameworks 6.0. [Ссылка 1](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/274) и [ссылка 2](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/276))

## Как вы можете помочь

Разработчики, пожалуйста, помогите исправить проблемы в [Qt 6, KDE Frameworks 6 и Plasma 6](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&f1=keywords&f2=product&list_id=2398791&o1=allwordssubstr&o2=notequals&query_format=advanced&v1=qt6&v2=neon)! Plasma 6 вполне годится для повседневного использования энтузиастами (я в их числе), хоть и всё ещё требует работы, чтобы стать готовой к выпуску до конца года.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/08/04/this-week-in-kde-porting-dolphin-and-kwin/>  

<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: OSDs → экранные уведомления -->
<!-- 💡: applet → виджет -->
<!-- 💡: application launcher → меню запуска приложений -->
<!-- 💡: icon → значок -->
<!-- 💡: icon theme → набор значков -->
<!-- 💡: location → расположение (в файловой системе) / местоположение (на карте) -->
