# На этой неделе в KDE: спокойный выпуск

В прошлую среду состоялся мегавыпуск KDE, и я рад сообщить, что он прошёл хорошо. Первые впечатления, кажется, почти у всех положительные! Я следил за отчётами об ошибках и соцсетями в поисках серьёзных проблем, но пока всё чисто. Считаю, что 3 месяца предварительного тестирования окупились! Так что **поздравляю всех, мы поработали на славу!** Надеюсь, это поможет изгнать болезненные воспоминания о KDE 4, которым уже 16 лет. 🙂 Это новое KDE — быстрее, выше, сильнее!

К сожалению, обновление Neon прошло хуже. Сейчас большинство проблем с пакетами уже решены — если вы столкнулись с ними, установите последние исправления. Мы разбираемся, как это произошло, чтобы предотвратить подобное в будущем. Пользователи Neon, спасибо за ваше терпение!

Разумеется, мы боролись с недочётами не только в Neon. Было ещё несколько ухудшений, и многие из них мы уже умудрились исправить. Я просто впечатлён участниками KDE на этой неделе! ❤️

## Новые возможности

В диспетчере окон KWin появился новый эффект «Скрывать курсор» (пока отключён по умолчанию, но попробуйте!), который автоматически прячет указатель мыши после периода бездействия (Jin Liu, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=465119))

На странице «Поддержка устаревших приложений X11» Параметров системы теперь есть возможность разрешить приложениям, работающим через слой совместимости XWayland, также подслушивать щелчки мыши (Oliver Beard, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=466448))

## Улучшения пользовательского интерфейса

Сообщение, отображаемое вместо виджетов, несовместимых с Plasma 6, стало понятнее (Niccolò Venerandi, Plasma 6.1, хотя, возможно, будет включено в 6.0.1 или 6.0.2. [Ссылка 1](https://invent.kde.org/plasma/libplasma/-/merge_requests/1066) и [ссылка 2](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2065)):

![0](https://pointieststick.files.wordpress.com/2024/03/better-message.jpeg)

При попытке вызова эффекта «Куб» с менее чем тремя виртуальными рабочими столами он теперь объяснит вам, почему не может работать, и предложит добавить недостающие столы (Влад Загородний, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/540)):

![1](https://pointieststick.files.wordpress.com/2024/03/screenshot_20240302_053053.jpeg)

Верхний левый угол экрана, по умолчанию при надавливании мышью открывающий «Обзор», снова стал закрывать эффект при повторной его активации (Влад Загородний, Plasma 6.0.1)

Ряд страниц Параметров системы были модернизированы: их кнопки были перемещены на панель инструментов, а сообщения-заполнители стали единообразнее (Shubham Arora, Nate Graham, Fushan Wen и Jakob Petsovits, Plasma 6.1. [Ссылка 1](https://invent.kde.org/plasma/plasma-firewall/-/merge_requests/84), [ссылка 2](https://invent.kde.org/plasma/kinfocenter/-/merge_requests/192), [3](https://invent.kde.org/plasma/kscreen/-/merge_requests/287), [4](https://invent.kde.org/plasma/kwin/-/merge_requests/5228), [5](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3931), [6](https://invent.kde.org/plasma/bluedevil/-/merge_requests/157), [7](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2052), [8](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2051), [9](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2050), [10](https://invent.kde.org/plasma/print-manager/-/merge_requests/114), [11](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2049) и [12](https://invent.kde.org/plasma/plasma-thunderbolt/-/merge_requests/36)):

* ![2](https://pointieststick.files.wordpress.com/2024/03/window-rules.jpeg)
* ![3](https://pointieststick.files.wordpress.com/2024/03/firewall-off.jpeg)
* ![4](https://pointieststick.files.wordpress.com/2024/03/info.jpeg)
* ![5](https://pointieststick.files.wordpress.com/2024/03/game-controller.jpeg)

В приложениях на основе Kirigami анимация перехода с одной страницы на другую стала намного плавнее и приятнее (Devin Lin, Frameworks 6.1. [Ссылка](https://invent.kde.org/frameworks/kirigami/-/merge_requests/840))

Помните тот неловкий отрезок на панелях инструментов Kirigami-приложений, отделявший боковую панель от основного содержимого? Теперь он выглядит как обычный разделитель (Carl Schwan, Frameworks 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=435481)):

![6](https://pointieststick.files.wordpress.com/2024/03/screenshot_20240302_053707.jpeg)

## Исправления ошибок

Теперь в Spectacle можно сделать снимок экрана сразу после окончания его записи (Noah Davis, Spectacle 24.02.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=481471))

Полноэкранный режим проигрывателя VLC снова работает (David Edmundson, Plasma 6.0.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=481456))

Устранён источник кратковременных подвисаний экрана в сеансе X11 (Влад Загородний, Plasma 6.0.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=481721))

Исправлен случайно проявлявшийся сбой в Plasma (Fushan Wen, Plasma 6.0.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=481992))

Перетаскивание файлов или папок с рабочего стола на другой экран больше не приводит к их временному исчезновению, причём исправление этой проблемы также устраняет сбой в Plasma, который мог быть вызван перетаскиванием файлов или папок с рабочего стола в папку, открытую в диспетчере файлов Dolphin (Marco Martin, Plasma 6.0.1. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=481254) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=466337))

Нажатие кнопки «По умолчанию» на странице «Переключение окон» Параметров системы больше не нарушает работу переключателя задач, пока вы не выберете его снова вручную (Marco Martin, Plasma 6.0.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=481640))

Щелчок по элементу панели снова приводит к его выбору, даже если открыто всплывающее окно другого виджета (David Edmundson, Plasma 6.0.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=481671))

Вокруг активного виртуального рабочего стола (а теперь и рабочего стола под курсором выши) в представлении «Все рабочие столы» эффекта «Обзор» снова есть синяя обводка (Akseli Lahtinen, Plasma 6.0.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=481812))

Когда вы щёлкаете правой кнопкой мыши по автоматически скрываемой панели и выбираете «Добавить виджеты…», панель больше не скрывается сразу после открытия проводника виджетов, что не позволяло добавить виджет на панель этим способом и жутко бесило (Niccolò Venerandi, Plasma 6.0.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=448393))

Редактор областей мозаичного режима больше не ломает макет при перетаскивании створок друг над другом (Akseli Lahtinen, Plasma 6.0.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=465721))

Эффект «Обзор» больше не закрывается щелчком по строке поиска (Patrik Fábián, Plasma 6.0.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=481497))

Сохранение изменений, внесённых в команды, назначенные на глобальные комбинации клавиш, теперь работает (Nate Graham, Plasma 6.0.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=481825))

В уведомлении по завершении отправки файла на устройство Bluetooth больше не отображается неработающая ссылка (Kai Uwe Broulik, Plasma 6.0.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=481759))

Окна проигрывателя MPV с включённой опцией «Сохранять пропорции» теперь можно делать полноэкранными (Łukasz Patron, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/5299))

Исправлен внешний вид диалогового окна перезаписи файла в окнах загрузки новых материалов из сети (Akseli Lahtinen, Frameworks 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=480338))

Исправлена ​​проблема, из-за которой на графиках и диаграммах Системного монитора могли появляться артефакты в виде горизонтальных линий при использовании дробного коэффициента масштабирования с некоторыми встроенными видеокартами Intel (Arjen Hiemstra, Frameworks 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=434462))

Другие сведения об ошибках:

* Остаётся 3 ошибки в Plasma с очень высоким приоритетом (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma — 33 (на 3 больше, чем на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 123 ошибки были исправлены на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-2-23&chfieldto=2024-3-1&chfieldvalue=RESOLVED&list_id=2630045&query_format=advanced&resolution=FIXED)

## Автоматизация и систематизация

Задокументировано [написание тестов интерфейса на основе Appium для программного обеспечения KDE](https://develop.kde.org/docs/apps/tests/appium/) (Fushan Wen и Thiago Sueto, [ссылка](https://invent.kde.org/documentation/develop-kde-org/-/merge_requests/348))

Добавлена [документация о предоставлении моделей на C++ графическому интерфейсу на QML в программном обеспечении KDE](https://develop.kde.org/docs/getting-started/kirigami/advanced-connect_models/) (Thiago Sueto, [ссылка](https://invent.kde.org/documentation/develop-kde-org/-/merge_requests/347))

## Как вы можете помочь

Благодаря вам наш [сбор средств на Plasma 6](https://kde.org/fundraisers/plasma6member/) имел сумасшедший успех! В итоге его поддержали невероятные **885** человек. Не тысяча, но, учитывая, что исходная цель была 500 (и даже её я считал крайне оптимистичной), я просто ошеломлён уровнем поддержки. Благодарю всех, кто поверил в нас, — мы постараемся не подвести! Если же вы только хотите сделать пожертвование, [ещё не поздно](https://kde.org/community/donations/)!

Разработчики, давайте ещё недели две продолжим концентрироваться на отчётах об ошибках в нашем ПО, чтобы никакие замеченные пользователями проблемы не остались без исправления.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2024/03/02/this-week-in-kde-a-smooth-release/>  

<!-- 💡: System Monitor → Системный монитор -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: get new [thing] → «Загрузить \[что-то\]» -->
<!-- 💡: screenshot → снимок экрана -->
