# На этой неделе в KDE: мозговитый KRunner

> 30 октября – 6 ноября, основное — прим. переводчика

На этой неделе строка поиска и запуска KRunner стала сообразительнее при подборе поисковой выдачи. Годами люди жаловались на то, что ожидаемые результаты поиска находятся не вверху списка, и вот Natalie Clarius и Alexander Lohnau решили внести некоторые существенные улучшения:

* Страницы Параметров системы, названия которых точно соответствуют поисковому запросу, теперь имеют гораздо больший вес и должны отображаться первыми ([Ссылка](https://invent.kde.org/plasma/systemsettings/-/merge_requests/165)).
* Различные другие объекты, имя которых точно совпадает с запросом, тоже получили гораздо больший вес, поэтому они должны предлагаться первыми, а релевантность последних открытых файлов теперь уменьшается в соответствии с их возрастом ([Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2177)).
* Запросы вроде «время MSK» или «время Tomsk» теперь возвращают вам не только время, но и разницу с ним в часах относительно вашего часового пояса ([Ссылка](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/260)).
* Строка `Exec=` в ярлыках веб-приложений Chrome и приложений Flatpak больше не учитывается при поиске ([Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=460796) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=461241)).

Большое им спасибо!

## Новые возможности

Просмотрщик изображений Gwenview теперь позволяет менять яркость, контрастность и гамму изображений (Илья Поминов, Gwenview 22.12. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=82979)):

![0](https://pointieststick.files.wordpress.com/2022/11/image-2.png)

## Улучшения пользовательского интерфейса

При использовании отложенных обновлений теперь можно просматривать списки изменений для отдельных пакетов в центре приложений Discover, как и при обычных обновлениях (Aleix Pol Gonzalez, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=457534))

Обои из представлений их выбора теперь можно перетаскивать — например, в графические редакторы для изменения или в диспетчер файлов для копирования (Fushan Wen, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2284))

На экране блокировки теперь можно нажать клавишу Esc, чтобы выключить экран и сэкономить немного энергии (Aleix Pol Gonzalez, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=338796))

Когда ваш компьютер подключён к сети и использует профиль энергопотребления «Производительный», или какое-либо приложение запросило использование режима энергосбережения, теперь вы можете увидеть это прямо в системном лотке (Nate Graham, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2280)):

*![2](https://pointieststick.files.wordpress.com/2022/11/perf_mode_auto.jpg)
*![3](https://pointieststick.files.wordpress.com/2022/11/perf_mode_manual.jpg)

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Графический редактор Krita теперь поддерживает глобальное меню (Antonio Rojas, Krita 5.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=408015))

Обнаружение VPN Libreswan теперь работает с версиями 4.9 и новее (Douglas Kosovic, Plasma 5.24.8. [Ссылка](https://invent.kde.org/plasma/plasma-nm/-/merge_requests/167))

В сеансе Plasma Wayland при использовании настройки по умолчанию «Устаревшие приложения (X11): Масштабирование средствами приложений» Steam и некоторые другие приложения, работающие через слой совместимости XWayland, теперь масштабируются до правильного размера (Nate Graham, Plasma 5.26.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=443215))

Вы снова можете отключить эффект «Все окна» внутри эффекта «Все рабочие столы», чтобы окна при просмотре виртуальных рабочих столов оставались на своих местах (Marco Martin, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=455350))

Приложения KDE больше не зависают при запуске, когда файлы из их списков последних открытых по какой-либо причине недоступны (Christoph Cullmann, Frameworks 5.100. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460868))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Известно об 11 ошибках в Plasma с очень высоким приоритетом (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 15-минутных ошибок в Plasma осталось 50 (на прошлой неделе было 56). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 133 ошибки были исправлены на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2022-10-28&chfieldto=2022-11-04&chfieldvalue=RESOLVED&list_id=2198714&query_format=advanced&resolution=FIXED)

## Автоматизация и систематизация

При сбое сборки модуля с помощью утилиты `kdesrc-build` теперь предлагается способ установки отсутствующих зависимостей для сборки (Marius Pa. [Cсылка](https://invent.kde.org/sdk/kdesrc-build/-/merge_requests/160))

Добавлены автоматические тесты, проверяющие работоспособность генерации миниатюр файлов (Nicolas Fella, kio-extras 22.12. [Ссылка](https://invent.kde.org/network/kio-extras/-/merge_requests/192))

В Plasma добавлен автоматический тест, проверяющий, что окна правильно сопоставляются со своими значками на панели задач (Fushan Wen, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2282))

## Сетевое присутствие

На сайте KDE появилась страница «[KDE для творчества](https://kde.org/ru/for/creators/)» (Carl Schwan, Nicolas Fella, Áron Kovács и другие)! Она дополняет существующую страницу «[KDE для детей](https://kde.org/ru/for/kids/)», и скоро мы добавим ещё!

![4](https://pointieststick.files.wordpress.com/2022/11/forcreatorsinstragram1.jpg)

Компьютеры от [Tuxedo](https://www.tuxedocomputers.com/) получили более заметное описание на [странице устройств с ПО KDE](https://kde.org/hardware/), так как они теперь предустанавливают Plasma по умолчанию! (Paul Brown, Carl Schwan и Nate Graham, [ссылка](https://bugs.kde.org/show_bug.cgi?id=459718)):

![5](https://pointieststick.files.wordpress.com/2022/11/image-1.png)

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2022/11/04/this-week-in-kde-big-brain-krunner/>  

<!-- 💡: Escape → Esc -->
<!-- 💡: Kicker → классическое меню запуска приложений -->
<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: file manager → диспетчер файлов -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: thumbnail → миниатюра -->
