# На этой неделе в KDE: Plasma 6 начинается

Как мы уже ранее сообщали, на этой неделе основная ветка репозиториев, входящих в состав Plasma, была переведена на Qt 6. Работа продолжается, но непосредственно переключение происходит очень быстро, и некоторые смельчаки уже даже запускают будущую Plasma 6 в приемлемом виде! Это возможно благодаря многолетней фоновой работе по переносу старого кода с устаревших API и библиотек, чем занимались Nicolas Fella, Friedrich Kossebau, Volker Krause и многие другие. Неказистая и неблагодарная, эта работа, тем не менее, крайне важна и служит фундаментом для быстрого перехода в настоящий момент. Так что я оптимистично оцениваю наши шансы выпустить надёжную и качественную Plasma 6 в этом году!

…И, собственно, потому на этой неделе не так много других новостей. Но не бойтесь, Plasma 5.27 продолжает поддерживаться и исправляться!

## Новые возможности

Теперь можно настроить интенсивность обводки окон в оформлении Breeze, в том числе полностью отключить её. Пока это применимо только к Plasma 6.0, но мы обдумываем включение необходимых изменений в 5.27 (Akseli Lahtinen, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=465948)):

![0](https://pointieststick.files.wordpress.com/2023/03/no-outline-setting.jpg)

## Улучшения пользовательского интерфейса

Новое диалоговое окно «Открыть с помощью», предоставляемое порталом XDG, больше не используется неизолированными приложениями: они снова получают старый диалог. Мы всё ещё планируем вернуться к упомянутому подходу в будущем, но сначала необходимо добавить в новый диалог недостающие функции, чтобы ничего не было потеряно при переходе (Nate Graham, Plasma 5.27.3. [Ссылка](https://invent.kde.org/plasma/plasma-integration/-/merge_requests/73))

Улучшен внешний вид связанных кнопок в приложениях GTK, использующих тему Breeze, — например, в Rhythmbox (Иван Ткаченко, Plasma 5.27.3. [Ссылка](https://invent.kde.org/plasma/breeze-gtk/-/merge_requests/69)):

![1](https://pointieststick.files.wordpress.com/2023/03/rhythmbox-linked-buttons-in-header.jpg)

Уведомления во всплывающем окне истории теперь отображаются в хронологическом порядке, а не сортируются по сложному для понимания критерию на основе типа и срочности (Joshua Goins, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=454070))

Сохранение приложениями KDE геометрии окон стало работать существенно надёжнее в многомониторных конфигурациях (Nate Graham, Frameworks 5.104. [Ссылка](https://invent.kde.org/frameworks/kconfig/-/merge_requests/184))

Файлы в корзине теперь можно безвозвратно удалять нажатием клавиши Delete (Méven Car, Frameworks 5.104. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=459545))

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

При использовании видеокарты NVIDIA после перезагрузки или выхода системы из ждущего режима внешние экраны больше не отключаются сами по себе, а различные значки и подписи в Plasma больше не исчезают (Иван Ткаченко, Plasma 5.27.2. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=460341) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=462725))

Исправлена ошибка, из-за которой диспетчер окон KWin мог аварийно завершить работу при смене темы оформления окон (Влад Загородний, Plasma 5.27.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=466313))

Значки рабочего стола в активной комнате Plasma больше не будут самовольно перестраиваться при изменении набора подключённых экранов. Впрочем, пока мы решали эту проблему, мы выяснили, что код для хранения расположений элементов на рабочем столе по своей сути проблематичен и нуждается в полной переработке, как это произошло с поддержкой нескольких мониторов в Plasma 5.27. Мы займёмся этим к Plasma 6.0, и, надеюсь, плохая память Plasma на положения значков останется в прошлом (Marco Martin, Plasma 5.27.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=464873))

Просмотрщик изображений Gwenview теперь регистрирует свой интерфейс MPRIS только тогда, когда это имеет смысл — например, при показе слайд-шоу — так что он больше не будет просто так захватывать глобальные горячие клавиши для управления воспроизведением (Joshua Goins, Gwenview 23.04. [Ссылка](https://invent.kde.org/graphics/gwenview/-/merge_requests/180))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Осталось 14 ошибок в Plasma с очень высоким приоритетом (на прошлой неделе было 16). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma стало 50 (на прошлой неделе было 44). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 115 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-02-24&chfieldto=2023-03-03&chfieldvalue=RESOLVED&list_id=2293903&query_format=advanced&resolution=FIXED)

## Автоматизация и систематизация

Теперь у нас есть [инструкция по созданию наборов курсоров](https://develop.kde.org/docs/features/cursor/) (Magno Lomardo, [ссылка](https://invent.kde.org/documentation/develop-kde-org/-/merge_requests/271))

Подготовлено руководство по [публикации приложений KDE в Microsoft Store](https://develop.kde.org/docs/packaging/microsoftstore/) (Thiago Sueto, [ссылка](https://invent.kde.org/documentation/develop-kde-org/-/merge_requests/266))

## Как вы можете помочь

Если вы пользователь, обновитесь до [Plasma 5.27](https://www.kde.org/announcements/plasma/5/5.27.0/)! Если обновление в вашем дистрибутиве не доступно и не предвидится, подумайте о переходе на другой дистрибутив, поставляющий программное обеспечение ближе к графикам разработчиков.

Разработчики, помогите нам исправить [известные проблемы в Plasma 5.27](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&keywords=regression%2C%20&keywords_type=allwords&list_id=2278517&query_format=advanced&version=5.26.90&version=5.27.0&version=5.27.1&version=5.27.2&version=5.27.3&version=5.27.4&version=5.27.5). Также посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этих списков — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

<!-- Временная ссылка! -->

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/03/03/this-week-in-kde-plasma-6-begins/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: icons → значки -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
