# На этой неделе в KDE: много работы над Discover

На этой неделе Aleix Pol Gonzalez много работал над центром приложений Discover! Ещё это была насыщенная неделя для Plasma: разработчики добавили несколько полезных новых функций, доработали пользовательский интерфейс и исправили многие высокоприоритетные проблемы.

> 24–31 июля, основное — прим. переводчика

## Исправленные «15-минутные ошибки»

Я собираюсь начать упоминать здесь также «очень высокоприоритетные» ошибки в Plasma, так как думаю, что есть смысл и их считать 15-минутными. Пока я помещу их в группу «добавлены и исправлены», но вместо этого я мог бы добавить их к общему числу, если вы считаете, что так будет правильнее. В таком случае общее число достигло бы 79. Или я могу упоминать их отдельно. Дайте знать, что вы думаете.

Текущее число ошибок: **51, вместо бывших 52**. 5 добавлено и 6 исправлено:

Мыши [больше не теряют свои настройки](https://bugs.kde.org/show_bug.cgi?id=435113) при подключении на лету или при выходе устройства из ждущего режима (Иван Ткаченко, Plasma 5.25.4)

Discover [больше не помечает по ошибке различные приложения и дополнения как проприетарные](https://bugs.kde.org/show_bug.cgi?id=454480) (Aleix Pol Gonzalez, Plasma 5.25.4)

Опция для отключения запоминания недавно открытых файлов [больше не располагается непонятно почему на странице «Комнаты» Параметров системы, а переехала на собственную страницу в разделе «Поведение рабочей среды»](https://bugs.kde.org/show_bug.cgi?id=416271) (Méven Car, Plasma 5.26):

![0](https://pointieststick.files.wordpress.com/2022/07/now-it-lives-elsewhere.jpeg)

Исправлен один из сценариев аварийного завершения Plasma с потенциальной потерей панелей и рабочих столов [при подключении или отключении экранов, а также при настройке их масштаба](https://bugs.kde.org/show_bug.cgi?id=454064) (David Edmundson, Plasma 5.26)

Когда Plasma перезапускается вручную или автоматически (например, после сбоя) с вызванным эффектом «Показать рабочий стол», [ваши окна больше не становятся невидимыми (хоть и интерактивными) на полминуты](https://bugs.kde.org/show_bug.cgi?id=449445) (Arjen Hiemstra, Plasma 5.26)

[Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)

## Новые возможности

Ввод текста в эффекте «Обзор» [теперь фильтрует окна, когда есть соответствия тексту поиска](https://invent.kde.org/plasma/kwin/-/merge_requests/2604), в дополнение к поиску через KRunner, когда ни одно открытое окно не соответствует запросу (Niklas Stephanblome, Plasma 5.26):

![1](https://pointieststick.files.wordpress.com/2022/07/filter-in-overview.jpeg)

*Это по сути почти полностью покрывает возможности эффекта «Все окна», и я выступаю за их слияние*

Виджет «Цифровые часы» [теперь позволяет настраивать размер шрифта](https://bugs.kde.org/show_bug.cgi?id=413394), гарнитуру и начертание. К счастью, изменения, необходимые для этого, также устраняют [ошибку, затрагивавшую старый пользовательский интерфейс выбора шрифта](https://bugs.kde.org/show_bug.cgi?id=395468), и заставляют виджет [больше не менять размер, когда отображаются секунды](https://bugs.kde.org/show_bug.cgi?id=421548) (Jin Liu, Plasma 5.26):

![2](https://pointieststick.files.wordpress.com/2022/07/manual-style.jpeg)

В сеансе Plasma Wayland [теперь можно настроить, как область ввода графического планшета сопоставляется с координатами вашего экрана](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1025) (Aleix Pol Gonzalez, Plasma 5.26):

![3](https://pointieststick.files.wordpress.com/2022/07/tablet-fitting.jpeg)

## Улучшения пользовательского интерфейса

Нажатие клавиши Esc в утилите для создания снимков экрана Spectacle с открытым режимом комментирования [теперь приводит к выходу только из этого режима, а не из всего приложения](https://bugs.kde.org/show_bug.cgi?id=456823) (Antonio Prcela, Spectacle 22.08)

Перетаскивание окна над другими окнами в эффектах «Обзор» и «Все окна» [больше не выделяет нижние окна](https://invent.kde.org/plasma/kwin/-/merge_requests/2736) и [больше не приводит к тому, что перетаскиваемое окно вдруг появляется *под* ними](https://invent.kde.org/plasma/kwin/-/merge_requests/2736) (Иван Ткаченко, Plasma 5.25.4)

Теперь вы можете [перетаскивать приложения из результатов поиска в меню запуска приложений на пустую область панели задач, чтобы закрепить их там](https://bugs.kde.org/show_bug.cgi?id=456984) (Nicolas Fella, Plasma 5.25.4)

Всплывающее окно виджета «Цифровые часы» [теперь полностью управляемо с клавиатуры](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1416) (Fushan Wen, Plasma 5.26)

Изменение аватара вашей учётной записи пользователя [больше не требует прав администратора](https://bugs.kde.org/show_bug.cgi?id=437286) (Jan Blackquill, Plasma 5.26)

Discover [теперь предупреждает вас, когда вы просматриваете приложение с бета-канала обновлений, и предупреждает ещё сильнее, когда версия, доступная на бета-канале, старше, чем версия на стабильном канале](https://invent.kde.org/plasma/discover/-/merge_requests/339) (Aleix Pol Gonzalez, Plasma 5.26):

![5](https://pointieststick.files.wordpress.com/2022/07/firefox-beta.jpeg)

*«Доступна более стабильная версия приложения Firefox»*

![6](https://pointieststick.files.wordpress.com/2022/07/bender-old-beta.jpeg)

*«Разрабатываемая версия приложения Blender устарела. Настоятельно рекомендуется использовать стабильную версию»*

Discover [теперь лучше показывает вам, когда репозиторий Flatpak добавлен для конкретного пользователя](https://invent.kde.org/plasma/discover/-/merge_requests/342), чтобы отличить его от того же репозитория, добавленного на системном уровне (Aleix Pol Gonzalez, Plasma 5.26)

Когда вы закрываете главное окно Discover во время обновления, [его теперь можно открыть снова, повторно запустив Discover](https://bugs.kde.org/show_bug.cgi?id=457204), а если вы закроете его во второй раз, оно [не создаст второе уведомление, а будет использовать существующее](https://bugs.kde.org/show_bug.cgi?id=457208) (Aleix Pol Gonzalez, Plasma 5.26)

При отмене операции массового переименования в диспетчере файлов Dolphin [больше не отправляется уведомление, озаглавленное «Перемещение»](https://bugs.kde.org/show_bug.cgi?id=437510) (Ahmad Samir, Frameworks 5.97)

## Другие исправления и улучшения производительности

Discover [больше не вылетает при просмотре определённых дополнений, у которых нет отзывов](https://bugs.kde.org/show_bug.cgi?id=457145), или [когда обновление прошивки завершается ошибкой](https://bugs.kde.org/show_bug.cgi?id=455132) (Aleix Pol Gonzalez, Plasma 5.24.7)

Приоритет, который вы присвоили репозиториям Flatpak в Discover, [теперь всегда соблюдается должным образом](https://bugs.kde.org/show_bug.cgi?id=451667) (Aleix Pol Gonzalez, Plasma 5.24.7)

Исправлена ошибка, из-за которой Discover мог [аварийно завершить работу при закрытии после успешной установки обновлений](https://bugs.kde.org/show_bug.cgi?id=457278) (Aleix Pol Gonzalez, Plasma 5.25.4)

В сеансе Plasma Wayland переключение виртуальных рабочих столов жестом смахивания на сенсорной панели [больше не может вызвать сбой приложений и игр, работающих через Wine или Steam Proton](https://bugs.kde.org/show_bug.cgi?id=455952) (Xaver Hugl, Plasma 5.25.4)

Когда вы закрываете главное окно Discover в разгаре установки обновлений, [уведомление, которое появляется на его месте, теперь показывает точное число элементов, оставшихся для обновления](https://bugs.kde.org/show_bug.cgi?id=457203) (Aleix Pol Gonzalez, Plasma 5.25.4)

В сеансе Wayland [теперь можно использовать касания для взаимодействия с некоторыми всплывающими окнами в GTK-приложениях, с которыми раньше это не работало](https://bugs.kde.org/show_bug.cgi?id=452967) (Aleix Pol Gonzalez, Plasma 5.26)

Discover [теперь быстрее запускает модуль поддержки Flatpak](https://invent.kde.org/plasma/discover/-/merge_requests/338) (Aleix Pol Gonzalez, Plasma 5.26)

[Исправлено несколько сбоев анимации](https://invent.kde.org/plasma/kwin/-/merge_requests/2742) в эффектах «Обзор» и «Все окна», а ещё они [больше не запинаются при открытии, если у вас несколько экранов](https://bugs.kde.org/show_bug.cgi?id=451590) (Иван Ткаченко и David Edmundson, Plasma 5.26)

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2022/07/29/this-week-in-kde-lots-of-work-on-discover/>  

<!-- 💡: Escape → Esc -->
<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: thumbnail → миниатюра -->
