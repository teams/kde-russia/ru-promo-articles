# На этой неделе в KDE: реорганизация настроек

## Plasma 6

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 91](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

Боковая панель Параметров системы подверглась столь необходимой реорганизации! Новая структура разделов готова примерно на 90% и ещё может измениться. Сейчас она выглядит так (Nate Graham, [ссылка](https://invent.kde.org/plasma/systemsettings/-/issues/15)):

![0](0_syse.png)

*(коллаж, читать сверху вниз и слева направо)*

Изменён компонент, управляющий расположениями экранов в сеансе Plasma Wayland: до сих пор это был KScreen, а в Plasma 6 эту роль возьмёт на себя диспетчер окон KWin. Это значительно облегчит обеспечение хорошего пользовательского опыта, поскольку состояние будет храниться в одном месте, а не поддерживаться согласованным между двумя компонентами. В Plasma 5 старый подход показал свою ненадёжность. В ходе работы уже [исправлены](https://bugs.kde.org/show_bug.cgi?id=457430) [три](https://bugs.kde.org/show_bug.cgi?id=466208) [ошибки](https://bugs.kde.org/show_bug.cgi?id=455082), и это ещё не все. Обратите внимание, что функционал KScreen теперь заморожен, то есть никаких дальнейших изменений в работе с несколькими экранами в сеансе X11 ожидать не следует (Xaver Hugl, [ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/4431))

Индикатор хода проверки обновлений в центре приложений Discover теперь гораздо больше соответствует действительности (Alessandro Astone, [ссылка 1](https://invent.kde.org/plasma/discover/-/merge_requests/646) и [ссылка 2](https://invent.kde.org/plasma/discover/-/merge_requests/653))

Discover теперь позволяет дистрибутивам включать автоматическое удаление зависимостей при удалении приложений (Alessandro Astone, [ссылка](https://invent.kde.org/plasma/discover/-/merge_requests/645))

При изменении разрешения или масштаба экрана — что может произойти в том числе при подключении дополнительных экранов — обои рабочего стола теперь мгновенно подстраивают размер, а не медленно анимируют переход выцветанием, что в этом контексте выглядело странно и сломанно (Marco Martin, [ссылка 1](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3361) и [ссылка 2](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/472))

Улучшен значок ночной цветовой схемы в наборе Breeze (Philip Murray, [ссылка](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/289)):

![1](https://pointieststick.files.wordpress.com/2023/10/image.png)

## Другие новые возможности

Функция записи видео в утилите для создания снимков экрана Spectacle получила поддержку аппаратного ускорения с помощью кодека VP9. Заодно расположение по умолчанию для сохранения видеозаписей стало настраиваемым (Noah Davis, [ссылка 1](https://invent.kde.org/graphics/spectacle/-/merge_requests/269), [ссылка 2](https://invent.kde.org/plasma/kpipewire/-/merge_requests/90) и [ссылка 3](https://bugs.kde.org/show_bug.cgi?id=474594))

## Другие заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Исправлена ​​ошибка в Discover, которая могла приводить к его сбою сразу после его запуска или после выполнения поиска (Harald Sitter, [ссылка 1](https://bugs.kde.org/show_bug.cgi?id=465711) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=467888))

В сеансе X11 у различных диалоговых окон на основе QtQuick больше не будет отсутствовать кнопка закрытия (Влад Загородний, Plasma 5.27.9. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=464193))

Приложения и Plasma больше не будут аварийно завершать работу, когда им предлагается использовать D-Bus для запуска приложения с неверным именем файла `.desktop`, не соответствующим спецификации (David Redondo, Frameworks 5.111. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=475266))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Осталось 3 ошибки в Plasma с очень высоким приоритетом (на прошлой неделе было 4). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 56 (на прошлой неделе было 58). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 91 ошибка была исправлена на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-09-29&chfieldto=2023-10-06&chfieldvalue=RESOLVED&list_id=2487489&query_format=advanced&resolution=FIXED)

## Автоматизация и систематизация

Добавлены дополнительные автоматические тесты для виджета «Батарея и яркость», страницы с QR-кодом в виджете «Буфер обмена» и диалога изменения аватара пользователя в Параметрах системы (Fushan Wen, [ссылка 1](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3323), [ссылка 2](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3366) и [ссылка 3](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3352))

## Как вы можете помочь

Мы проводим [сбор средств, приуроченный к выпуску Plasma 6](https://kde.org/fundraisers/plasma6member/), и нам нужна ваша помощь! Если вам нравится наша работа, пожертвование — это отличный способ нас отблагодарить. 🙂

Разработчики, пожалуйста, помогите исправить проблемы в [Qt 6, KDE Frameworks 6 и Plasma 6](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&f1=keywords&f2=product&list_id=2398791&o1=allwordssubstr&o2=notequals&query_format=advanced&v1=qt6&v2=neon)! Plasma 6 уже вполне годится для повседневного использования энтузиастами (я в их числе), хоть и всё ещё требует работы, чтобы стать готовой к выпуску в феврале.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/10/06/this-week-in-kde-re-organized-system-settings/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: icons → значки -->
<!-- 💡: location → расположение (в файловой системе) / местоположение (на карте) -->
<!-- 💡: screenshot → снимок экрана -->
