# На этой неделе в KDE: автосохранение в Dolphin и улучшенное масштабирование

Мы на финишной прямой!

Разработка начальных версий шестого поколения Plasma и приложений KDE была выделена в отдельную ветку — а значит, все изменения, принимаемые только в основную ветку, попадут уже в последующие версии. Для Plasma это 6.1, а для приложений из состава Gear — 24.05. Там уже немало новых функций и улучшений интерфейса! Вот некоторые из них:

Диспетчер файлов Dolphin теперь периодически сохраняет открытые окна и вкладки, чтобы вы не потеряли их в случае сбоя приложения или непредвиденного перезапуска системы (Amol Godbole, Dolphin 24.05. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=425627))

В Dolphin теперь можно настроить, показывать ли файлы резервных копий, когда показ скрытых файлов отключён (Méven Car, Dolphin 24.05. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=475805))

В двухпанельном режиме Dolphin одну из панелей теперь можно выделить в новое окно (Loren Burkholder, Dolphin 24.05. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=270604))

Исправлена ​​проблема в Dolphin, из-за которой он мог зависать при многократном дублировании одного и того же файла (Евгений Попов, Dolphin 24.05. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=475410))

Просмотрщик документов Okular теперь поддерживает отображение всплывающих меню в некоторых видах PDF-документов, которые их содержат (Alexis Murzeau, Okular 24.05. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=479777))

В виджете «Сети» теперь указывается не только частота сети, но и её канал (Kai Uwe Broulik, [ссылка](https://invent.kde.org/plasma/plasma-nm/-/merge_requests/315))

## Мегавыпуск KDE 6

> (Сюда входит всё ПО, которое будет выпущено 28 февраля: Plasma 6, библиотеки Frameworks 6 и приложения из состава Gear 24.02)

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 237](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

### Улучшения пользовательского интерфейса

На глобальной панели инструментов режима редактирования Plasma появилась кнопка «Добавить панель», что позволило убрать пункты «Добавить виджеты…» и «Добавить панель» из контекстного меню рабочего стола, снизив его загруженность. Конечно, при желании эти пункты можно вернуть, просто добавив их обратно 🙂 (Akseli Lahtinen и Nate Graham, [ссылка 1](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3774), [ссылка 2](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1980) и [ссылка 3](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3781)):

![0](https://pointieststick.files.wordpress.com/2024/01/image.png)

В диалоговом окне портала «Демонстрация экрана» элементы теперь выбираются одиночным щелчком мыши, за исключением режима множественного выбора, где одиночный щелчок только выделяет элемент. Кроме того, в режиме множественного выбора в углу элементов в качестве подсказки появились небольшие флажки (Yifan Zhu и Nate Graham, [ссылка 1](https://bugs.kde.org/show_bug.cgi?id=470211) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=479925)):

![1](https://pointieststick.files.wordpress.com/2024/01/multi-select.png)

*Над интерфейсом этого диалога ещё предстоит поработать*

### Исправления ошибок

> **Важное примечание:** я не упоминаю здесь исправления ошибок, не успевших дойти до пользователей; посреди большого цикла разработки Plasma их слишком много, чтобы про них писать (а вам — читать), да и вообще мало кто их видел. Тем не менее огромное спасибо всем, кто плотно занимается их исправлением!

Запуск службы управления питанием PowerDevil при входе в систему больше не завершается сбоем при использовании библиотеки `ddcutil-2.0.0` и некоторых DDC-совместимых мониторов (David Edmundson, [ссылка](https://bugs.kde.org/show_bug.cgi?id=476375)). Обратите внимание, что у нас есть ещё сообщения о новых проблемах, связанных с `ddcutil-2.1.0`, но они отличаются и требуют отдельного расследования.

Устранены разрывы при отрисовке значков и кнопок в приложениях на основе QtQuick при использовании дробного коэффициента масштабирования. Ещё предстоит поработать над текстом, а также контурами и тенями окон (Arjen Hiemstra и Marco Martin, [ссылка](https://bugs.kde.org/show_bug.cgi?id=464069))

Диспетчер окон KWin стал надёжнее восстанавливать настройки в многомониторных конфигурациях, где у одного из экранов отсутствует EDID (Stefan Hoffmeister, [ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/4913))

При использовании источника данных о погоде, дающего прогнозы более чем на 7 дней вперёд (например, EnvCan), правый край прогноза больше не обрезается виджетом «Погода» в системном лотке (Ismael Asensio, [ссылка](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/526))

Панели Plasma больше не будут странно мерцать в некоторых играх, зачем-то многократно переключающих свои окна между полноэкранным и распахнутым состояниями (Xaver Hugl, [ссылка](https://bugs.kde.org/show_bug.cgi?id=474488))

Не работавшее с Wayland свойство «Тип окна» в правилах KWin было заменено на новое свойство «Слой окна», лучше подходящее для тех же целей (Влад Загородний, [ссылка](https://bugs.kde.org/show_bug.cgi?id=466016))

Другие сведения об ошибках:

* Осталось 3 ошибки в Plasma с очень высоким приоритетом (на прошлой неделе было 4). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 35 (на прошлой неделе было 37). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 127 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-1-12&chfieldto=2024-1-19&chfieldvalue=RESOLVED&list_id=2585071&query_format=advanced&resolution=FIXED)

### Производительность и технические улучшения

Чтение различных параметров из файлов конфигурации, часто выполняемое программным обеспечением KDE, было ускорено на 13–16% (Friedrich Kossebau, [ссылка](https://invent.kde.org/frameworks/kconfig/-/merge_requests/270))

## Исправления для Frameworks 5

На этой неделе у нас есть пара хороших исправлений и для библиотек из Frameworks 5!

Исправлена ​​проблема, из-за которой при перемещении или копировании большого числа файлов некоторые из них могли быть пропущены (и потенциально потеряны) после пропуска дублирующихся папок (Евгений Попов, Frameworks 5.115. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=479082))

Исправлена ​​проблема, из-за которой каталоги внутри сетевых папок нельзя было раскрыть в режиме просмотра «Таблица» (Alessandro Astone, Frameworks 5.115. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=479531))

## Как вы можете помочь

Спасибо всем, кто поучаствовал в [сборе средств, приуроченном к выпуску Plasma 6](https://kde.org/fundraisers/plasma6member/) — он крайне успешен! Я думал, что 500 новых спонсоров — это слишком оптимистичная цель для фонда KDE e.V., но вы показали мне, что я, к счастью, ошибался. Сейчас их уже целых **665**, и это число продолжает расти. Благодарю всех, кто поверил в нас, — мы постараемся не подвести! Если вам нравится наша работа, пожертвование — это отличный способ нас отблагодарить. 🙂

Разработчики, пожалуйста, помогите исправить проблемы в Qt 6, KDE Frameworks 6 и Plasma 6! Plasma 6 уже вполне годится для повседневного использования, хоть и всё ещё требует работы, чтобы стать готовой к выпуску в феврале.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2024/01/19/this-week-in-kde-auto-save-in-dolphin-and-better-fractional-scaling/>  

<!-- 💡: applet → виджет -->
<!-- 💡: icons → значки -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: system tray → системный лоток -->
