# На этой неделе в KDE: правильная поддержка нескольких видеокарт

На этой неделе было устранено множество проблем, в том числе была существенно переработана поддержка нескольких видеокарт (Intel и AMD) диспетчером окон KWin, так что в Plasma 6 она [«больше не отстойная»](https://invent.kde.org/plasma/kwin/-/merge_requests/3859)! Большое спасибо Xaver Hugl за его впечатляющую работу.

А вот выборка из других заметных наработок:

## Новые возможности

Появилась опция, позволяющая отключить автоматический переход к окну на другом виртуальном рабочем столе при выполнении действий, которые в противном случае сделали бы это. Она может быть полезна для некоторых задач, например, для открытия большого числа ссылок из почтового клиента в веб-браузере, расположенном на другом рабочем столе, чтобы вам не приходилось после каждой открытой ссылки переключаться обратно на почтовый клиент (Nicolas Fella, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/3448))

## Улучшения пользовательского интерфейса

Функция «Выделить изменённые пункты» Параметров системы теперь работает на странице «Права доступа пакетов Flatpak» (Иван Ткаченко, Plasma 5.27.5. [Ссылка](https://invent.kde.org/plasma/flatpak-kcm/-/merge_requests/97))

Панель выбора эмодзи (Meta+.) стала открываться значительно быстрее (Fushan Wen, Plasma 5.27.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=468328))

Интерфейс диалоговых окон аутентификации был оптимизирован, чтобы подчёркивать важное (Devin Lin, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/polkit-kde-agent-1/-/merge_requests/21)):

![0](https://pointieststick.files.wordpress.com/2023/04/new-polkit-dialog.png)

Когда виджет «Просмотр папки» использует режим просмотра списком, объекты в нём теперь всегда открываются одиночным щелчком мыши, поскольку он соответствует парадигме меню, а пункты меню всегда активируются одним щелчком (Nate Graham, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=420261))

На странице «Особые параметры окон» Параметров системы настройки различных свойств теперь объясняются намного понятнее (Nate Graham и Ismael Asensio, Plasma 6.0. [Ссылка 1](https://invent.kde.org/plasma/kwin/-/merge_requests/3962), [ссылка 2](https://invent.kde.org/plasma/kwin/-/merge_requests/3964) и [ссылка 3](https://bugs.kde.org/show_bug.cgi?id=431265))

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

В сеансе Plasma Wayland утилита для создания снимков экрана Spectacle теперь делает снимки быстрее и никогда не показывает на них собственное окно (Noah Davis, Spectacle 23.04 с Plasma 5.27.4.1 или новее. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/3961))

Пользователи видеокарт AMD снова могут установить частоту обновления экрана выше 60 Гц, так как мы временно обошли [ошибку](https://gitlab.freedesktop.org/drm/amd/-/issues/2501) в открытом драйвере AMD (Xaver Hugl, Plasma 5.27.4.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=468235))

Исправлена серьёзная утечка памяти, которая при определённых обстоятельствах могла быстро скушать всю память при подключении внешнего монитора (Harald Sitter, Plasma 5.27.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=466362))

В случае сбоя автономного обновления системы вы больше не будете получать уведомления об этом при каждом входе в систему, даже после того, как нажмёте на кнопку «Восстановить систему» в уведомлении от центра приложений Discover (Aleix Pol Gonzalez и Nate Graham. [Ссылка 1](https://invent.kde.org/plasma/discover/-/merge_requests/518) и [ссылка 2](https://invent.kde.org/plasma/discover/-/merge_requests/527))

Discover больше не путает иногда версии пакетов Flatpak до и после предлагаемого обновления, а также не заявляет по ошибке, что обновление не поменяет номер версии, когда на самом деле установится следующая версия — хотя это не всегда ошибка, иногда так действительно бывает (Ismael Asensio, Plasma 5.27.5. [Ссылка](https://invent.kde.org/plasma/discover/-/merge_requests/529))

Пара разделителей без фиксированного размера теперь правильно выравнивает виджеты между ними по центру не только горизонтальных, но и вертикальных панелей Plasma (Niccolò Venerandi, Plasma 5.27.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=467416))

Discover стал успешно применять некоторые разновидности обновлений прошивки, которые раньше сбивали его с толку (Aleix Pol Gonzalez, Plasma 5.27.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=468379))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Осталось 11 ошибок в Plasma с очень высоким приоритетом (на прошлой неделе было 13). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 50 (на прошлой неделе была 51). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 148 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-04-07&chfieldto=2023-04-14&chfieldvalue=RESOLVED&list_id=2335655&query_format=advanced&resolution=FIXED)

## Как вы можете помочь

Если вы пользователь, обновитесь до [Plasma 5.27](https://www.kde.org/announcements/plasma/5/5.27.0/)! Если обновление в вашем дистрибутиве не доступно и не предвидится, подумайте о переходе на другой дистрибутив, поставляющий программное обеспечение ближе к графикам разработчиков.

Разработчики, помогите нам исправить [известные проблемы в Plasma 5.27](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&keywords=regression%2C%20&keywords_type=allwords&list_id=2278517&query_format=advanced&version=5.26.90&version=5.27.0&version=5.27.1&version=5.27.2&version=5.27.3&version=5.27.4&version=5.27.5). Также посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этих списков — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

<!-- Временная ссылка! -->

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/04/14/this-week-in-kde-make-multi-gpu-not-suck/>  

<!-- 💡: GPU → видеокарта / графический ускоритель -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: emoji → эмодзи -->
<!-- 💡: icon → значок -->
<!-- 💡: icon theme → набор значков -->
<!-- 💡: icons → значки -->
<!-- 💡: screenshot → снимок экрана -->
