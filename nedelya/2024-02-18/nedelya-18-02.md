# На этой неделе в KDE: решаем давние проблемы

На этой неделе основное внимание уделялось давним надоедливым проблемам, и многие из них теперь в прошлом! Если вы сколько-нибудь долго использовали программное обеспечение KDE, уверен, что вы замечали хотя бы одну из проблем, упомянутых ниже, и вместе со мной порадуетесь их устранению! Заодно число 15-минутных ошибок в Plasma снизилось до самого низкого уровня за всё время — их осталось всего 30. Мегавыпуск уже меньше чем через две недели, и мы хотим, чтобы он был великолепным!

## До Мегавыпуска

…Но сначала — несколько исправлений для ветки Plasma 5.27!

Исправлена ​​ошибка, из-за которой диспетчер окон KWin мог аварийно завершить работу в сеансе Plasma Wayland (Xaver Hugl, Plasma 5.27.11. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=481239))

Файлы, скопированные в буфер обмена, теперь доступны изолированным приложениям через механизм порталов, так что вы можете вставлять их в такие приложения (Karol Kosek, Plasma 5.27.11. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=476600))

После открытия крышки ноутбука яркость подсветки клавиатуры теперь правильно восстанавливается до значения, использовавшегося до закрытия крышки (Werner Sembach, Plasma 5.27.11. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=469819))

## Мегавыпуск KDE 6

> (Сюда входит всё ПО, которое будет выпущено 28 февраля: Plasma 6, библиотеки Frameworks 6 и приложения из состава Gear 24.02)

### Улучшения пользовательского интерфейса

Всплывающие окна виджетов на панели теперь закрываются при щелчке по пустому месту панели (David Edmundson, [ссылка](https://bugs.kde.org/show_bug.cgi?id=367815))

После добавления и последующего удаления глобального меню, все строки меню в ваших приложениях, скрывшиеся из-за наличия глобального меню, теперь сами сразу же возвращаются, а не ждут, пока вы вручную их включите (David Edmundson, [ссылка](https://bugs.kde.org/show_bug.cgi?id=439266))

Страница «Экран и монитор» Параметров системы при открытии больше не выбирает для показа экраны, помеченные как выключенные (David Edmundson, [ссылка](https://bugs.kde.org/show_bug.cgi?id=461561))

Добавлены значки Breeze для файлов конфигурации OpenVPN и Cisco VPN (Kai Uwe Broulik, [ссылка](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/322)):

![0](https://pointieststick.files.wordpress.com/2024/02/image-5.png)

### Исправления ошибок

> **Важное примечание:** я не упоминаю здесь исправления ошибок, не успевших дойти до пользователей; посреди большого цикла разработки Plasma их слишком много, чтобы про них писать (а вам — читать), да и вообще мало кто их видел. Тем не менее огромное спасибо всем, кто плотно занимается их исправлением!

Устранено несколько распространённых случайных сбоев в реализации комнат Plasma (Harald Sitter, [ссылка](https://invent.kde.org/plasma/plasma-activities-stats/-/merge_requests/58))

Исправлен неприятный недочёт, из-за которого в большинстве дистрибутивов некоторые типы файлов по умолчанию открывались не в тех приложениях (например, изображения — в просмотрщике документов Okular) до тех пор, пока пользователь в первый раз не менял какую-либо из привязок файлов (Harald Sitter, [ссылка](https://bugs.kde.org/show_bug.cgi?id=305136))

Исправлен визуальный дефект в Gwenview, из-за которого миниатюры изображений могли перекрывать друг друга при использовании дробного коэффициента масштабирования (David Edmundson, [ссылка](https://bugs.kde.org/show_bug.cgi?id=480659))

В утилите для создания снимков экрана Spectacle добавление пометок с помощью сенсорного экрана или стилуса больше не приводит к появлению где попало лишних прямых линий (Marco Martin, [ссылка](https://invent.kde.org/graphics/spectacle/-/merge_requests/334))

Исправлен случай, когда курсор мыши не исчезал должным образом в некоторых играх, запущенных через слой совместимости Wine (Влад Загородний, [ссылка](https://bugs.kde.org/show_bug.cgi?id=480582))

Исправлена ​​утечка памяти в Spectacle при записи экрана (Aleix Pol Gonzalez, [ссылка](https://invent.kde.org/graphics/spectacle/-/merge_requests/328))

Другие сведения об ошибках:

* Остаётся 3 ошибки в Plasma с очень высоким приоритетом (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 30 (на прошлой неделе было 32). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 135 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-2-9&chfieldto=2024-2-16&chfieldvalue=RESOLVED&list_id=2614190&query_format=advanced&resolution=FIXED)

## После Мегавыпуска

В Gwenview появился минималистичный режим просмотра, в котором видны только изображение и заголовок окна (Равиль Сайфуллин, Gwenview 24.05. [Ссылка](https://invent.kde.org/graphics/gwenview/-/merge_requests/222)):

![1](https://pointieststick.files.wordpress.com/2024/02/screenshot_20240216_055404.jpeg)

Предупреждение на странице «Прокси-сервер» Параметров системы больше не утверждает, что её настройки не распространяются на браузеры на основе Chromium, так как это больше не соответствует действительности (Кто-то под псевдонимом «Chaotic Abide», kio-extras 25.04. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=480847))

Устранён наиболее распространённый сбой в центре приложений Discover для Plasma 6.1, и мы думаем, как избавиться от него и в 6.0 (Иван Ткаченко, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=463864))

Интерфейс выбора времени на странице «Блокировка экрана» Параметров системы стал понятнее (Kristen McWilliam, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/kscreenlocker/-/merge_requests/175#note_867727)):

![2](https://pointieststick.files.wordpress.com/2024/02/standard-durations.jpeg)

*Стандартная задержка*

![3](https://pointieststick.files.wordpress.com/2024/02/custom-durations.jpeg)

*Пользовательская задержка с возможностью выбора единиц*

В «Особые параметры окон» добавлено правило KWin, которое можно использовать для управления состоянием поддержки переменной частоты обновления экрана (VRR, Adaptive Sync) для отдельных окон (Равиль Сайфуллин, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/5167))

## Производительность и технические улучшения

KWin теперь поддерживает прямой аппаратный вывод на экран при запуске во вложенном режиме внутри другого компоновщика (Xaver Hugl, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/5187))

В Gwenview значительно ускорены создание миниатюр и загрузка списка файлов в целом (Arjen Hiemstra, Мегавыпуск 6. [Ссылка 1](https://invent.kde.org/graphics/gwenview/-/merge_requests/253) и [ссылка 2](https://invent.kde.org/frameworks/kio/-/merge_requests/1550))

Значительно улучшена отзывчивость выделения прямоугольной области в Spectacle (Noah Davis, Spectacle 24.05. [Ссылка](https://invent.kde.org/graphics/spectacle/-/merge_requests/333))

Отступы, используемые в Plasma и в приложениях на основе библиотеки Kirigami, больше не зависят от метрик шрифтов; теперь они просто заданы статически, как в приложениях на основе QtWidgets. Это должно немного приблизить нас к общей визуальной согласованности, позволив создавать пользовательские интерфейсы с более предсказуемыми отступами. Это скорее долгосрочная задача, но начало положено! (Noah Davis, Kirigami 6.0. [Ссылка](https://invent.kde.org/frameworks/kirigami/-/merge_requests/479))

## Как вы можете помочь

Спасибо всем, кто поучаствовал в [сборе средств, приуроченном к выпуску Plasma 6](https://kde.org/fundraisers/plasma6member/) — он крайне успешен! Я думал, что 500 новых спонсоров — это слишком оптимистичная цель для фонда KDE e.V., но вы показали мне, что я, к счастью, ошибался. Сейчас их уже **825**, и это число продолжает расти. Благодарю всех, кто поверил в нас, — мы постараемся не подвести! Если вам нравится наша работа, пожертвование — это отличный способ нас отблагодарить. 🙂

Разработчики, помогите исправить проблемы в Qt 6, KDE Frameworks 6 и Plasma 6! Plasma 6 близка к выпуску и в хорошем состоянии, но ещё немного доработки напоследок не помешает.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2024/02/16/this-week-in-kde-longstanding-issues-crushed/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icons → значки -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: thumbnail → миниатюра -->
