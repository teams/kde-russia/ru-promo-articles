# На этой неделе в KDE: улучшений через край!

> 13–19 апреля — прим. переводчика

Сегодня слово предоставляется трём основных темам: Dolphin и другим инструментам управления файлами, доработке Plasma и Wayland. Мы неплохо продвигаемся во всяких штуках для Wayland, и в дальнейшем там можно ожидать больше исправлений! Во всех трёх областях мы сконцентрировались на исправлении давних проблем. Но и ими, конечно, не обошлось!

## Новые возможности

Миниатюры [теперь доступны для файлов и папок в зашифрованных файловых системах, таких как зашифрованные папки Plasma](https://bugs.kde.org/show_bug.cgi?id=411919); это стало возможным благодаря хранению заготовленных миниатюр в самой ФС, или, при необходимости, генерации вовсе без сохранения где-либо (Marcin Gurtowski, Frameworks 5.70 и Dolphin 20.08.0)

Смена цветовой схемы в Параметрах системы [теперь меняет цветовую схему запущенных приложений на основе GTK 3 моментально, без необходимости перезапускать их](https://phabricator.kde.org/D28072) (Carson Black и Михаил Золотухин, Plasma 5.19.0):

<https://i.imgur.com/xCp7mA2.mp4?_=1>

Теперь можно выставить [дробные размеры шрифтов на странице настройки шрифтов в Параметрах системы (например, 10,5 пт Noto Sans)](https://bugs.kde.org/show_bug.cgi?id=352758) (Ahmad Samir, Plasma 5.19.0)

Различные приложения KDE получили базовую поддержку [просмотра изображений в формате XCF, используемом графическим редактором GIMP](https://invent.kde.org/frameworks/kimageformats/-/commit/c60e77c048d32ccf743cec695743b77b2b25dc87) (Martin T. H. Sandsmark, Frameworks 5.70)

## Исправления ошибок и улучшения производительности

Устранено [распространённое падение диспетчера файлов Dolphin при использовании Qt 5.14.2](https://bugs.kde.org/show_bug.cgi?id=419585) (Martin T. H. Sandsmark, Dolphin 20.04.0)

При настройке утилиты для создания снимков экрана Spectacle на автосохранение изображений в подпапке, [теперь можно перетаскивать их из главного окна](https://bugs.kde.org/show_bug.cgi?id=417722) (Franz Baumgärtner, Spectacle 20.04.0)

Исправлен [глюк отрисовки в определённых разновидностях файлов DJVU при больших значениях масштаба в Okular](https://bugs.kde.org/show_bug.cgi?id=419962) (Albert Astals Cid, Okular 20.04.0)

Попытка установить действия Dolphin, распространяемые в виде пакетов DEB/RPM, [теперь передаёт установку другому инструменту, способному это выполнить (обычно Discover), и поэтому работает](https://phabricator.kde.org/D28795) (Alex Lohnau, Dolphin 20.04.0)

Dolphin [теперь показывает осмысленный текст в заголовке окна при поиске файлов с включённой опцией «Полный путь в заголовке окна»](https://bugs.kde.org/show_bug.cgi?id=406624) (Antonio Prcela, Dolphin 20.04.0)

При запуске эмулятора терминала Konsole в фоновом режиме [он теперь получает фокус ввода с клавиатуры автоматически, без необходимости щёлкать по нему мышкой](https://bugs.kde.org/show_bug.cgi?id=411543) (Антон Карманов, Konsole 20.04.0)

Проигрывание или добавление в очередь папки с музыкой из вашей файловой системы [теперь проигрывает или добавляет в очередь рекурсивно всё содержимое папки вместе с подпапками, а не только верхний уровень](https://invent.kde.org/kde/elisa/-/merge_requests/117) (Graham Littlewood, Elisa 20.08.0)

Исправлена [утечка памяти в Dolphin при наведении на пункт «Комнаты» в контекстном меню](https://phabricator.kde.org/D28739) (Александр Кандауров, Dolphin 20.08.0)

Исправлена ошибка, [делающая звук системных уведомлений по умолчанию отключённым и не управляемым через Параметры системы без первичной регулировки громкости через стороннее приложение Pavucontrol](https://bugs.kde.org/show_bug.cgi?id=407397) (Nicolas Fella, Plasma 5.18.5)

Завершение сеанса Wayland [больше не приводит к падению KWin, выкидывающему вас в чёрный экран](https://bugs.kde.org/show_bug.cgi?id=420077) (Méven Car, Plasma 5.18.5)

Зашифрованные папки Plasma [больше не падают, когда вы отменяете диалог подключения после того, как служба не смогла смонтировать хранилище, поскольку точка монтирования не пуста](https://bugs.kde.org/show_bug.cgi?id=418262) (Ivan Čukić, Plasma 5.18.5)

Исправлены [различные падения и проблемы интерфейса мастера отчёта об ошибках (DrKonqi) в Wayland](https://phabricator.kde.org/D28832) (David Edmundson, Plasma 5.18.5)

Приложения на основе GTK 2, такие как GIMP и Inkscape, [больше не раскрашиваются в странные неподходящие цвета при использовании нестандартной цветовой схемы; вместо этого они даже не пытаются соблюдать цветовую схему, поскольку это невозможно, учитывая то, как работает GTK 2](https://bugs.kde.org/show_bug.cgi?id=412331) (Carson Black, Plasma 5.19.0)

[Удалена настройка размера курсора «В зависимости от разрешения», которая никогда нормально не работала](https://bugs.kde.org/show_bug.cgi?id=385920). Это должно решить некоторые (но не все) из [недавних проблем с размером курсора](https://bugs.kde.org/show_bug.cgi?id=413783). Другие исправления все ещё ожидаются (Benjamin Port, Plasma 5.19.0)

В сеансе Wayland окно строки поиска и запуска KRunner [больше не перекрывается окнами, поддерживаемыми поверх других](https://bugs.kde.org/show_bug.cgi?id=389964) (Aleix Pol Gonzalez, Plasma 5.19.0)

Функция «Определение выходов» [теперь работает и в Wayland](https://bugs.kde.org/show_bug.cgi?id=385672) (Benjamin Port, Plasma 5.19.0)

Выпадающие списки, содержащие значки, [теперь отображают их правильно при использовании глобального масштабирования](https://phabricator.kde.org/D28725) (Kai Uwe Broulik, Plasma 5.19.0):

![1](https://pointieststick.files.wordpress.com/2020/04/screenshot_20200410_180405.png)

Служба индексации файлов Baloo [стала использовать меньше ресурсов системы — в частности файлового ввода/вывода — когда пользователь активно взаимодействует с системой](https://phabricator.kde.org/D28864) (Stefan Brüns, Frameworks 5.70)

Исправлено недавнее ухудшение в компоненте `OverlaySheet` библиотеки Kirigami: [щелчок внутри диалога больше не приводит к его самовольному закрытию](https://bugs.kde.org/show_bug.cgi?id=419691) (Marco Martin, Plasma 5.70)

## Улучшения пользовательского интерфейса

Список действий Dolphin [теперь отсортирован по алфавиту](https://bugs.kde.org/show_bug.cgi?id=419938) (Nate Graham и Kai Uwe Broulik, Dolphin 20.04.0):

![2](https://pointieststick.files.wordpress.com/2020/04/screenshot_20200414_095156.png)

Панель сведений Dolphin [теперь показывает полезную информацию для Корзины](https://bugs.kde.org/show_bug.cgi?id=413091) (Méven Car, Frameworks 5.70 и Dolphin 20.8.0):

![3](https://pointieststick.files.wordpress.com/2020/04/screenshot_20200414_083209.png)

Теперь сложнее случайно удалить ваши панели и виджеты, потому что [кнопка «Удалить панель» снова спрятана в меню «Дополнительно», и все кнопки удаления расположены в своих меню далеко от указателя мыши](https://phabricator.kde.org/D28710) (Nate Graham, Plasma 5.19.0)

Виджет проигрывателя [получил визуальное обновление](https://phabricator.kde.org/D27160) (Carson Black и Ismael Asensio, Plasma 5.19.0):

![4](https://pointieststick.files.wordpress.com/2020/04/screenshot_20200412_160739.png)

У различных виджетов с панелью инструментов в системном лотке — [начиная с Bluetooth](https://phabricator.kde.org/D28467) — строка заголовка и панель инструментов теперь совмещены (Niccolò Venerandi, Plasma 5.19.0):

![5](https://pointieststick.files.wordpress.com/2020/04/bluetooth-applet-with-visually-merged-header.png)

Различные виджеты системного лотка, представляющие собой списки, элементы которых разворачиваются при щелчке, чтобы показать больше опций, [теперь используют одинаковый код](https://phabricator.kde.org/T12812), делающий их [удобными при сенсорном вводе](https://phabricator.kde.org/D28808) и приводящий к более стабильному и согласованному поведению (Nate Graham, Plasma 5.19.0):

<https://i.imgur.com/RTcXbEU.mp4?_=2>

Выпадающие списки в ПО на основе QML [теперь могут быть закрыты щелчком по пустой области окна, как и списки из Qt Widgets](https://phabricator.kde.org/D27701) (Kai Uwe Broulik, Frameworks 5.70)

## Как вы можете помочь

Версия 20.04 приложений, выпускаемых Службой релизов ПО KDE, — в том числе Dolphin, Gwenview, Okular, Konsole, Spectacle и Elisa — [скоро будет опубликована](https://community.kde.org/Schedules/release_service/20.04_Release_Schedule)! Как только они станут доступны, пожалуйста, обновитесь и отправьте отчёты об ошибках, чтобы мы могли исправить найденные вами проблемы как можно скорее — надеемся, к 20.04.1. Если у вашего дистрибутива есть нестабильные или предварительные версии, не стесняйтесь пробовать новые версии заранее. Это сильно помогает!

Также можно присоединиться к работе над другими задачами. Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник, и совершенно не обязательно быть программистом: занятие по душе найдётся для каждого.

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [пожертвования](https://kde.org/ru/community/donations/) средств в фонд [KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [boingo-00](https://t.me/boingo00)  
*Источник:* <https://pointieststick.com/2020/04/17/this-week-in-kde-our-cup-overfloweth-with-improvements/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: location → расположение (в файловой системе) / местоположение (на карте) -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: thumbnail → миниатюра -->
