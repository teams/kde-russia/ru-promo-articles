# На этой неделе в KDE: комментарии в Spectacle

На этой неделе мы добавили функцию, которую вы так долго ждали: [комментарии в Spectacle](https://bugs.kde.org/show_bug.cgi?id=372464)! Её реализовал Nicolas Fella, она войдёт в Spectacle 20.12. Осталось несколько ошибок и недоработок, но мы должны исправить их к выпуску через несколько месяцев. Спасибо, Nicolas!

> 31 августа – 6 сентября — прим. переводчика

![0](https://pointieststick.files.wordpress.com/2020/09/screenshot_20200902_115748.jpeg)

## Другие новые возможности

Страницы настроек Bluetooth в Параметрах системы [были объединены в одну модную новую страницу на основе QML с гораздо лучшим пользовательским интерфейсом](https://invent.kde.org/plasma/bluedevil/-/merge_requests/8) (Nicolas Fella, Plasma 5.20):

![1](https://pointieststick.files.wordpress.com/2020/09/screenshot_20200903_092146-1.jpeg)

Строка поиска и запуска KRunner [теперь сохраняет ранее введённый текст при закрытии и повторном открытии, поэтому вы можете очень легко вернуться к предыдущему поиску, если он ещё актуален](https://bugs.kde.org/show_bug.cgi?id=397092). Если это поведение вас раздражает, вы, конечно же, можете отключить его 😀 (Alexander Lohnau, Plasma 5.20)

## Исправления ошибок и улучшения производительности

Повышены [скорость и производительность создания миниатюр для файлов и папок](https://invent.kde.org/network/kio-extras/-/merge_requests/16) (Stefan Brüns, Dolphin 20.12)

Эмулятор терминала Konsole [стал запускаться чуть быстрее](https://invent.kde.org/utilities/konsole/-/merge_requests/204) (Martin Tobias Holmedahl Sandsmark, Konsole 20.12)

KRunner [теперь надёжнее выполняет математические вычисления с длинными числами при использовании региональных настроек, задающих точку в качестве разделителя тысяч](https://bugs.kde.org/show_bug.cgi?id=406388) (Alexander Lohnau, Plasma 5.20)

Страница «Сценарии KWin» Параметров системы [теперь обновляется после удаления сценариев](https://bugs.kde.org/show_bug.cgi?id=382136) (Alexander Lohnau, Plasma 5.20)

Отмена установки новой темы экрана входа в систему (SDDM) [больше не приводит к загадочному появлению пустого диалогового окна](https://bugs.kde.org/show_bug.cgi?id=407686) (Alexander Lohnau, Plasma 5.20)

Исправлена ошибка, из-за которой [виджеты, установленные с помощью диалога «Загрузить виджеты Plasma», не могли быть обновлены центром программ Discover](https://bugs.kde.org/show_bug.cgi?id=424429) (Alexander Lohnau, Plasma 5.20)

При вводе путей в адресные строки в программном обеспечении KDE [к концу имени текущей папки снова автоматически добавляется косая черта](https://bugs.kde.org/show_bug.cgi?id=425387) (Noah Davis, Frameworks 5.74)

Снова можно [назначать глобальные комбинации клавиш, использующие символы, для доступа к которым необходимо удерживать Shift](https://bugs.kde.org/show_bug.cgi?id=423305) (например, Meta+! или Meta+&) (Jan Paul Batrina, Frameworks 5.74)

Открытие области меток (`tags:/`) в диспетчере файлов Dolphin [больше не съедает столько системных ресурсов для создания миниатюр](https://bugs.kde.org/show_bug.cgi?id=419429) (Stefan Brüns, Frameworks 5.74)

Недавно установленные или удалённые элементы в диалогах «Загрузка материалов из интернета» [теперь появляются или исчезают, как ожидалось, при применении фильтра](https://bugs.kde.org/show_bug.cgi?id=425135) (Alexander Lohnau, Frameworks 5.74)

## Улучшения пользовательского интерфейса

На исполнителя и название альбома воспроизводимой в данный момент дорожки в музыкальном проигрывателе Elisa [теперь можно нажать, и вы попадёте на страницу этого исполнителя или альбома](https://bugs.kde.org/show_bug.cgi?id=417858) (Stef Lep, Elisa 20.12)

Счётчик очереди воспроизведения в Elisa [больше не меняет структуру области заголовка, когда появляется или исчезает, и делает это с плавной анимацией](https://invent.kde.org/multimedia/elisa/-/merge_requests/151) (Shantanu Tuschar, Elisa 20.12)

Функция «Открыть терминал» в Dolphin [теперь работает и в ОС Windows](https://invent.kde.org/system/dolphin/-/merge_requests/51) (Alexander Lohnau, Dolphin 20.12)

Размер главного окна Konsole по умолчанию [был немного увеличен](https://invent.kde.org/utilities/konsole/-/merge_requests/125) (Claudius Ellsel, Konsole 20.12)

Пункт контекстного меню рабочего стола «Настроить рабочий стол» [был перемещён в верхнюю часть меню](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/237) и [переименован в «Настроить рабочий стол и обои…», чтобы смену обоев было проще найти](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/141) (Nate Graham, Plasma 5.20):

![2](https://pointieststick.files.wordpress.com/2020/09/desktop-menu.jpeg)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник, и совершенно не обязательно быть программистом — занятие по душе найдётся для каждого.

И наконец, если вы находите программное обеспечение KDE полезным, окажите финансовую поддержку [фонду KDE e.V.](https://ev.kde.org/)

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Максим Маршев](https://t.me/msmarshev)  
*Источник:* <https://pointieststick.com/2020/09/04/this-week-in-kde-annotations-in-spectacle/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: get new [thing] → «Загрузить \[что-то\]» -->
<!-- 💡: location → расположение (в файловой системе) / местоположение (на карте) -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: thumbnail → миниатюра -->
