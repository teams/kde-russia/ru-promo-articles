# На этой неделе в KDE: будет клааааааааассно!

> 15–22 декабря, основное — прим. переводчика

Работа над Plasma и приложениями KDE в самом разгаре! Но слышали ли вы о конкурсе обоев для Plasma 5.18? Мы проводили такой для Plasma 5.16, и это привело к появлению [фантастических обоев «Лёд»](https://community.kde.org/KDE_Visual_Design_Group/Plasma_5.16_Wallpaper_Competition#We_have_a_Winner.21) от Santiago Cézar. Теперь у вас есть шанс представить обои для Plasma 5.18 — выпуска с долгосрочной поддержкой, так что вашу работу будут видеть годами миллионы людей! У нас есть потрясающие призы от спонсора [Tuxedo Computers](https://www.tuxedocomputers.com/). Ознакомьтесь с правилами по ссылке: <https://community.kde.org/KDE_Visual_Design_Group/Plasma_5.18_Wallpaper_Competition>

## Новые возможности

Диспетчер файлов Dolphin [теперь позволяет вам включать метки файлов в критерий поиска](https://bugs.kde.org/show_bug.cgi?id=412564) (Ismael Asensio, Dolphin 20.04.0):

<https://i.imgur.com/4Dp0sKO.mp4?_=1>

При поиске в Параметрах системы [список результатов теперь включает сами подходящие страницы, а не только категории верхнего уровня](https://bugs.kde.org/show_bug.cgi?id=400303) (Marco Martin, Plasma 5.18.0):

<http://s1.webmshare.com/Ry0L6.webm?_=2>

Виджет «Сетевой монитор» [теперь может быть настроен на отображение скорости передачи в байтах](https://bugs.kde.org/show_bug.cgi?id=383019) (George Vogiatzis, Plasma 5.18.0):

![2](https://i.imgur.com/cXkRtxr.png)

Теперь можно [включать и выключать режим «Не беспокоить» сочетанием клавиш](https://phabricator.kde.org/D26018) (Kai Uwe Broulik, Plasma 5.18.0):

![3](https://i.imgur.com/tWxHvzS.png)

## Исправления и улучшения производительности

Диалог фиксации изменений SVN в Dolphin [снова работает](https://phabricator.kde.org/D26115) (Николай Крашенинников, Dolphin 19.12.1)

Колёсико мыши в [Dolphin](https://bugs.kde.org/show_bug.cgi?id=386379) и [файловых диалогах](https://bugs.kde.org/show_bug.cgi?id=223937) больше не прокручивает слишком быстро при использовании режима просмотра с большими значками (Nate Graham и Arjen Hiemstra, Dolphin 19.12.1 и Frameworks 5.66)

[Исправлено ухудшение в размере текста температуры в виджете погоды при использовании очень коротких панелей](https://bugs.kde.org/show_bug.cgi?id=415187), которое я вызвал несколько недель назад, пытаясь сделать размер текста таким же, как в виджете часов. Теперь проблема решена, и размеры текста действительно всегда одинаковы (Nate Graham, Plasma 5.17.5)

При очистке содержимого буфера обмена последний элемент [теперь действительно, на самом деле, удаляется](https://bugs.kde.org/show_bug.cgi?id=409366) (Piotr Dabrowski, Plasma 5.18.0)

При изменении размера шрифта [элементы интерфейса, динамически изменяющие свой размер в зависимости от настроек шрифта, теперь делают это немедленно в приложениях на основе QML](https://phabricator.kde.org/D25983) (Kai Uwe Broulik, Frameworks 5.66)

Смонтированные вручную сетевые хранилища NFS [больше не появляются дважды на панели «Точки входа» в Dolphin, в файловых диалогах и других приложениях](https://bugs.kde.org/show_bug.cgi?id=406242) (Méven Car, Frameworks 5.66)

Даты во всех приложениях KDE [больше не используют неоднозначные относительные понятия, такие как «Прошедший понедельник»](https://phabricator.kde.org/D25878) (это было в понедельник на этой неделе или на прошлой?); вместо этого они вернулись к использованию абсолютных дат начиная с трёх дней в прошлое или будущее (Nate Graham, Frameworks 5.66)

Использование центра программ Discover в openSUSE для установки приложений при доступных обновлениях [больше не устанавливает молча эти обновления](https://bugzilla.opensuse.org/show_bug.cgi?id=1154973) (Jonathan Kang, последний пакет PackageKit в openSUSE)

## Улучшения пользовательского интерфейса

[Теперь очевидно, какие из тем Plasma следуют вашей системной цветовой схеме](https://bugs.kde.org/show_bug.cgi?id=364953) (Filip Fila и Nate Graham, Plasma 5.18):

![4](https://i.imgur.com/FivWSnF.png)

Индикатор оставшегося времени на всплывающих уведомлениях [теперь круговой и окружает кнопку закрытия](https://phabricator.kde.org/D25993) (Arjen Hiemstra, Plasma 5.18.0):

<https://i.imgur.com/DHtc8qB.mp4?_=3>

[В уведомлении «Загрузка завершена» появился перетаскиваемый значок-посредник](https://phabricator.kde.org/D25782), позволяющий вам перетаскивать его в различные места и делать с ним всякие крутые штуки (Kai Uwe Broulik, Plasma 5.18.0):

![6](https://i.imgur.com/cOU9s2y.png)

Plasma [теперь отображает уведомление, предупреждающее вас о скорой разрядке батареи подключённого Bluetooth-устройства](https://phabricator.kde.org/D25950) (Kai Uwe Broulik, Plasma 5.18.0):

![7](https://i.imgur.com/U1yLUvi.png)

У всплывающих подсказок панели задач [теперь есть эффект выделения при наведении](https://phabricator.kde.org/D25959) (Nate Graham, Plasma 5.18.0):

<http://s1.webmshare.com/AWyVw.webm?_=4>

[В разных местах](https://phabricator.kde.org/D25998) [в Plasma](https://phabricator.kde.org/D26015), где возможно перетаскивание, курсор мыши теперь приобретает соответствующую форму сжатой или разжатой руки (Nate Graham, Plasma 5.18.0):

<https://i.imgur.com/EpZT7IX.mp4?_=5>

Виджет Plasma «Сетевые подключения» [больше не содержит два почти одинаковых элемента в контекстном меню; они были объединены](https://bugs.kde.org/show_bug.cgi?id=407561) (Jan Grulich, Plasma 5.18.0):

![10](https://i.imgur.com/pepXkpa.png)

Уведомление о необходимости входа в открытую сеть через страницу гостевого портала [больше не дублируется при определённых обстоятельствах и исчезает после успешного входа](https://phabricator.kde.org/D26042) (Jan Grulich, Plasma 5.18.0)

Маленькие стрелочки вниз на кнопках «Назад» и «Вперёд» в Dolphin [теперь меньше и не увеличивают ширину кнопок, что улучшило внешний вид](https://phabricator.kde.org/D26139) (Noah Davis, Plasma 5.18.0):

![11](https://i.imgur.com/UfEJ69z.png)

## Как вы можете помочь

Если у вас есть творческий талант, расчехляйте свои цифровые кисти и пробуйте свои силы в работе, которую будут видеть миллионы пользователей Plasma LTS в течение многих лет, приняв участие в нашем конкурсе обоев: <https://community.kde.org/KDE_Visual_Design_Group/Plasma_5.18_Wallpaper_Competition>!

Ну и, как обычно, вы можете прочитать [эту статью](https://kde.ru/join) и выяснить, как стать частью того, что действительно имеет значение. Каждый участник KDE играет огромную роль; к вам не относятся как к очередному сотруднику или винтику системы. Программистом вам тоже быть не требуется. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

И наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [пожертвования](https://kde.org/ru/community/donations/) средств в фонд [KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [boingo-00](https://t.me/boingo00)  
*Источник:* <https://pointieststick.com/2019/12/22/this-week-in-kde-its-gonna-be-amaaaaaaaaaaazing/>
