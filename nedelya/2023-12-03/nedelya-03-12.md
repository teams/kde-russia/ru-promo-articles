# На этой неделе в KDE: смена обоев из Параметров системы

Мы опубликовали [бета-версию Plasma 6](https://kde.org/announcements/megarelease/6/beta1/) и получили очень положительные отзывы! Прежде чем перейти к предстоящей многомесячной работе над ошибками, мы всё-таки докинули напоследок ещё пару новшеств. Пожалуйста, продолжайте тестировать бета-версию и сообщать нам обо всех замеченных проблемах — или устранять их самостоятельно, если это в ваших силах! 🙂

## Мегавыпуск KDE 6

> (Сюда входит всё ПО, которое будет выпущено 28 февраля: Plasma 6, библиотеки Frameworks 6 и приложения из состава Gear 24.02)

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 164](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

### Новые возможности

Обои для любого из подключённых экранов теперь можно установить через Параметры системы! (Méven Car, [ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3395)):

![0](https://pointieststick.files.wordpress.com/2023/12/screenshot_20231201_225336.jpeg)

Если в качестве источника приложений по умолчанию в центре приложений Discover выбраны Flatpak или Snap, то на его стартовой странице теперь отображается раздел «Новые и недавно обновлённые», показывающий жизнь экосистемы приложений для Linux! В будущем он будет включён и если основным источником служат репозитории дистрибутива, но только если ваш дистрибутив действительно регулярно обновляет отдельные приложения, а не просто выкатывает обновление всех пакетов в один день, лишая этот раздел смысла (Иван Ткаченко, [ссылка](https://invent.kde.org/plasma/discover/-/merge_requests/696)):

![1](https://pointieststick.files.wordpress.com/2023/12/image-1.png)

Страница «Ночная цветовая схема» Параметров системы теперь показывает визуализацию времени работы, в том числе переходных периодов (Ismael Asensio, [ссылка](https://bugs.kde.org/show_bug.cgi?id=414809)):

![2](https://pointieststick.files.wordpress.com/2023/12/screenshot_20231201_225738.jpeg)

В диспетчере окон KWin реализован эффект «Обнаружение курсора встряхиванием», аналогичный тому, что есть в macOS. Пока он отключён по умолчанию и включается вручную на странице «Эффекты рабочего стола» Параметров системы (Влад Загородний, [ссылка](https://bugs.kde.org/show_bug.cgi?id=432927))

Архиватор Ark теперь предлагает опцию «Распаковать в эту папку и удалить архив» в своём подменю контекстного меню Dolphin (Severin von Wnuck, [ссылка](https://invent.kde.org/utilities/ark/-/merge_requests/109)):

![3](https://pointieststick.files.wordpress.com/2023/12/image.png)

### Улучшения пользовательского интерфейса

Автоматически скрываемые панели теперь учитывают настраиваемую пользователем задержку перед реакцией для эффектов краёв экрана, то есть теперь можно сделать так, чтобы панель появлялась не сразу после касания (Bharadwaj Raju, [ссылка](https://bugs.kde.org/show_bug.cgi?id=267277))

Эффект свечения, подсказывающий, что курсор мыши приближается к краю или углу экрана, настроенному на вызов какого-либо действия, теперь учитывает настроенный цвет выделения (Иван Ткаченко, [ссылка](https://invent.kde.org/plasma/libplasma/-/merge_requests/847))

Анимация преобразования всплывающих окон теперь использует стандартную функцию плавности, которая ощущается привычнее и быстрее (Timothy Bautista, [ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/4719/diffs#note_821693))

KRunner и другие строки поиска на его основе, например, в «Обзоре», теперь правильно отображают щелчок по результату поиска (Kai Uwe Broulik, [ссылка](https://invent.kde.org/plasma/milou/-/merge_requests/76))

Вкладки боковых панелей текстового редактора Kate теперь можно переставлять перетаскиванием (Waqar Ahmed, [ссылка 1](https://invent.kde.org/utilities/kate/-/merge_requests/1366) и [ссылка 2](https://invent.kde.org/utilities/kate/-/merge_requests/1361)):

<https://invent.kde.org/utilities/kate/uploads/cac94d7ccba3436daed6dff50071cd68/Peek_2023-11-26_16-18.mp4>

Нажатие клавиши Esc в режиме создания снимка прямоугольной области Spectacle теперь возвращает вас в главное окно, а не сразу же закрывает приложение (Noah Davis, [ссылка](https://bugs.kde.org/show_bug.cgi?id=470322))

### Исправления ошибок

Исправлен наиболее распространённый сбой диспетчера файлов Dolphin, который мог произойти при взаимодействии с диалогом перезаписи файла во время копирования большого числа объектов, если окно Dolphin было закрыто (Akseli Lahtinen, [ссылка](https://bugs.kde.org/show_bug.cgi?id=448532))

В сеансе Plasma Wayland подменю «классического» меню запуска приложений больше не перекрываются нижней панелью, а окна с пометкой «Поддерживать поверх других» больше не перекрывают всплывающие окна панели (David Edmundson, [ссылка 1](https://bugs.kde.org/show_bug.cgi?id=465354) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=476080))

Исправлены различные визуальные дефекты анимации подпрыгивающего курсора при запуске приложений под Wayland при использовании масштабирования экрана (Влад Загородний, [ссылка 1](https://invent.kde.org/plasma/kwin/-/merge_requests/4724), [ссылка 2](https://invent.kde.org/plasma/kwin/-/merge_requests/4725) и [ссылка 3](https://invent.kde.org/plasma/kwin/-/merge_requests/4726))

Экранные уведомления теперь выглядят на экране блокировки так же, как и во всех остальных местах (Bharadwaj Raju, [ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3590))

Если у вас есть несколько виджетов «Питание и батарея», переключатель «Запретить блокировку экрана и переход в ждущий режим» теперь синхронизируется между всеми ними (Natalie Clarius, [ссылка](https://bugs.kde.org/show_bug.cgi?id=455802))

* Ошибок в Plasma с очень высоким приоритетом стало 5 (на прошлой неделе было 4). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma — 41 (на прошлой неделе было 40). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 180 (!) ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-11-24&chfieldto=2023-12-1&chfieldvalue=RESOLVED&list_id=2536448&query_format=advanced&resolution=FIXED)

### Технические улучшения

Реализована ​​поддержка аппаратного вывода курсора мыши для видеокарт NVIDIA (Doğukan Korkmaztürk, [ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/4695))

## Как вы можете помочь

Мы проводим [сбор средств, приуроченный к выпуску Plasma 6](https://kde.org/fundraisers/plasma6member/), и нам нужна ваша помощь! Благодаря вам мы пересекли отметку в 83% от цели, но пока её не достигли. Если вам нравится наша работа, пожертвование — это отличный способ нас отблагодарить. 🙂

Разработчики, пожалуйста, помогите исправить проблемы в Qt 6, KDE Frameworks 6 и Plasma 6! Plasma 6 уже вполне годится для повседневного использования энтузиастами, хоть и всё ещё требует работы, чтобы стать готовой к выпуску в феврале.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/12/01/this-week-in-kde-changing-the-wallpaper-from-within-system-settings/>  

<!-- 💡: Escape → Esc -->
<!-- 💡: Kicker → классическое меню запуска приложений -->
<!-- 💡: OSDs → экранные уведомления -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: location → расположение (в файловой системе) / местоположение (на карте) -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: task manager → панель задач -->
