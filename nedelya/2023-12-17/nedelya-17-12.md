# На этой неделе в KDE: непритязательная работа над надёжностью

На этой неделе все мы продолжали бороться с ошибками. В результате стало меньше как отслеживаемых проблем Plasma 6, так и старых высокоприоритетных недочётов в Plasma! У меня очень хорошее предчувствие по поводу предстоящего выпуска. Уже сейчас я получаю удовольствие от использования Plasma 6 на повседневной основе. Думаю, у нас всё получится. 🙂

## Мегавыпуск KDE 6

 > (Сюда входит всё ПО, которое будет выпущено 28 февраля: Plasma 6, библиотеки Frameworks 6 и приложения из состава Gear 24.02)

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 199](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

### Улучшения пользовательского интерфейса

Страница «Дополнительные параметры управления питанием» Параметров системы стала дочерней страницей «Энергосбережения» (Jakob Petsovits, [ссылка](https://invent.kde.org/plasma/powerdevil/-/merge_requests/214)):

![0](https://pointieststick.files.wordpress.com/2023/12/image-2.png)

Улучшено отображение батарей устройств на странице «Энергопотребление» Информации о системе: теперь правильно идентифицируется больше типов устройств и отображаются их модели, чтобы было легче различать батареи разных устройств одного типа (Shubham Arora, [ссылка 1](https://invent.kde.org/frameworks/solid/-/merge_requests/150), [ссылка 2](https://invent.kde.org/plasma/kinfocenter/-/merge_requests/175), [3](https://invent.kde.org/plasma/kinfocenter/-/merge_requests/176), [4](https://invent.kde.org/plasma/kinfocenter/-/merge_requests/174))

При попытке подключения к сети Wi-Fi по неверному паролю пользователь теперь сразу же информируется об этой причине неудачи (David Redondo, [ссылка](https://bugs.kde.org/show_bug.cgi?id=471378))

Значок Telegram в наборе Breeze был обновлён до актуальной версии логотипа мессенджера (Onur Ankut, [ссылка](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/299)):

![1](https://pointieststick.files.wordpress.com/2023/12/image-3.png)

Набор курсоров Breeze теперь содержит больше предварительно генерируемых размеров, благодаря чему курсоры выглядят лучше с различными коэффициентами масштабирования и в большем числе приложений, ещё не использующих протокол Wayland `cursor-shape-v1` (Jin Liu, [ссылка](https://invent.kde.org/plasma/breeze/-/merge_requests/380))

Встроенное в архиватор Ark окно предпросмотра файлов при открытии теперь вспоминает свой последний размер (Илья Поминов, [ссылка](https://invent.kde.org/utilities/ark/-/merge_requests/216))

При создании снимка прямоугольной области экрана утилитой Spectacle теперь можно удерживать клавишу Shift для показа экранной лупы (Noah Davis, [ссылка](https://bugs.kde.org/show_bug.cgi?id=477399)):

<https://invent.kde.org/graphics/spectacle/uploads/f771e93c939d9858891c7581a2bf743f/Screencast_20231208_191503.webm>

### Исправления ошибок

> **Важное примечание:** я не упоминаю здесь исправления ошибок, не успевших дойти до пользователей; посреди большого цикла разработки Plasma их слишком много, чтобы про них писать (а вам — читать), да и вообще мало кто их видел. Тем не менее огромное спасибо всем, кто плотно занимается их исправлением!

Диспетчер окон KWin больше не будет аварийно завершать работу, когда приложение просит его создать окно недопустимого размера (Xaver Hugl, [ссылка](https://bugs.kde.org/show_bug.cgi?id=478269))

Редактирование файла `.desktop` таким образом, что полю `Exec=` задаётся значение, содержащее знак равенства, больше не будет приводить к сбою диалогового окна свойств при следующем использовании его для редактирования того же файла (Harald Sitter, [ссылка](https://bugs.kde.org/show_bug.cgi?id=465290))

Исправлено распространённое случайное падение Plasma (David Redondo, [ссылка](https://bugs.kde.org/show_bug.cgi?id=463276))

Редактор меню KDE больше не будет завершаться аварийно, если вы создадите новую запись и сразу же удалите её, а потом нажмёте на кнопку «Сохранить» (Harald Sitter, [ссылка](https://bugs.kde.org/show_bug.cgi?id=478398))

В сеансе Plasma Wayland действия по управлению питанием и сеансами вновь работают после сбоя KWin (David Edmundson, [ссылка](https://bugs.kde.org/show_bug.cgi?id=449948))

Исправлена пара визуальных недочётов при использовании нескольких экранов, для хотя бы одного из которых задан дробный масштабный коэффициент (Yifan Zhu, [ссылка 1](https://bugs.kde.org/show_bug.cgi?id=477791) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=464842))

В диспетчере файлов Dolphin при использовании режима просмотра «Таблица» для работы с деревом папок раскрытие подпапки больше не приводит к расположению элементов в неправильном порядке, если содержимое родительской папки отсортировано по размеру (Akseli Lahtinen, [ссылка](https://bugs.kde.org/show_bug.cgi?id=473999))

Изменение размера курсора теперь отражается в Plasma сразу же, а не только после перезапуска процесса `plasmashell` (Влад Загородний и другие, [ссылка](https://bugs.kde.org/show_bug.cgi?id=382604))

Закрытие окон приложений на основе GTK 4 теперь анимируется KWin, а не приводит к мгновенному их исчезновению (Влад Загородний, [ссылка](https://bugs.kde.org/show_bug.cgi?id=478297))

Другие сведения об ошибках:

* Осталось 3 ошибки в Plasma с очень высоким приоритетом (на прошлой неделе было 5). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 34 (на прошлой неделе была 41). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 175 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-12-8&chfieldto=2023-12-15&chfieldvalue=RESOLVED&list_id=2551123&query_format=advanced&resolution=FIXED)

### Производительность и технические улучшения

Эффект «Обзор» теперь запускается намного быстрее, а его анимация куда более плавная! (Влад Загородний, Marco Martin и Arjen Hiemstra, [ссылка 1](https://bugs.kde.org/show_bug.cgi?id=455780) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=476002))

## Как вы можете помочь

Мы проводим [сбор средств, приуроченный к выпуску Plasma 6](https://kde.org/fundraisers/plasma6member/), и нам нужна ваша помощь! Благодаря вам мы пересекли отметку в 91% от цели, но пока её не достигли. Если вам нравится наша работа, пожертвование — это отличный способ нас отблагодарить. 🙂

Разработчики, пожалуйста, помогите исправить проблемы в Qt 6, KDE Frameworks 6 и Plasma 6! Plasma 6 уже вполне годится для повседневного использования энтузиастами, хоть и всё ещё требует работы, чтобы стать готовой к выпуску в феврале.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/12/15/this-week-in-kde-un-flashy-important-stability-work/>  

<!-- 💡: Info Center → Информация о системе -->
<!-- 💡: KMenuEdit → Редактор меню KDE -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: playlist → список воспроизведения -->
