# На этой неделе в KDE: приближается Akademy

Неделя выдалась тихой: люди в отпусках или готовятся к конференции [Akademy 2023](https://akademy.kde.org/2023/), которая начнётся на следующей неделе. Тем не менее работа над Plasma 6 продолжается, и мы получаем всё больше отчётов об ошибках в ней благодаря растущему числу [отважных тестировщиков](https://community.kde.org/Plasma/Plasma_6#How_to_use/test_it)! Давно известные проблемы тоже не остались без внимания.

## Plasma 6

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 63](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

Набор курсоров Breeze был обновлён для большей привлекательности и лучшего соответствия развитию Breeze за время Plasma 5! (Manuel Jesús de la Fuente, [ссылка](https://invent.kde.org/plasma/breeze/-/merge_requests/287)):

![0](https://pointieststick.files.wordpress.com/2023/07/cursors.png)

Если в настройках переключения окон не отмечена опция «Включать пункт „Показать рабочий стол“», переключатели окон по Alt+Tab больше не отображаются при вызове без окон или с единственным открытым окном (Daniel Lipovetsky, [ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/4226))

Комбинация клавиш для вызова эффекта «Рисование мышью» теперь настраивается (Андрей Акулов, [ссылка](https://bugs.kde.org/show_bug.cgi?id=337043))

## Улучшения пользовательского интерфейса

В библиотеку Kirigami добавлен новый компонент для показа баннеров в верхней части окон и представлений. Раньше мы использовали для этого компонент `Kirigami.InlineMessage`, но его было сложно вписать в это место, а результат всё равно выглядел неважно. Новый безрамочный компонент баннера выглядит намного приятнее! Ожидайте увидеть его в Kirigami-приложениях в ближайшие месяцы и годы. (Carl Schwan, [ссылка](https://invent.kde.org/libraries/kirigami-addons/-/merge_requests/116)):

![1](https://pointieststick.files.wordpress.com/2023/07/banner.png)

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Исправлена ​ошибка, снижавшая производительность просмотрщика документов Okular при увеличении масштаба (Max Müggler, Okular 23.08. [Ссылка](https://invent.kde.org/graphics/okular/-/merge_requests/693#note_710216))

Устранена ​​проблема, которая могла вызывать графические артефакты в сеансе Plasma Wayland на некоторых устройствах с несколькими видеокартами (Xaver Hugl, Plasma 5.27.7. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=471517))

Исправлена ​​ошибка, из-за которой все подключённые экраны иногда могли считаться «главными» (Иван Ткаченко, Plasma 5.27.7. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=471195))

Исправлена ​​ошибка, из-за которой диспетчер окон KWin мог иногда аварийно завершать работу в сеансе Wayland после использования буфера обмена (David Edmundson, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=471789))

Исправлено недавнее ​​серьёзное ухудшение, из-за которого при перемещении или копировании символической ссылки вместо неё перемещался или копировался её целевой объект (Harald Sitter, Frameworks 5.108. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=464225))

Редактирование файлов с привилегиями администратора с помощью kio-admin больше не меняет неожиданно их права доступа (Harald Sitter, Frameworks 5.108. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=471539))

Исправлена ​​​​ошибка, из-за которой Plasma могла аварийно завершить работу при закрытии всех запущенных приложений щелком средней кнопкой мыши по их значкам в панели задач (Harald Sitter, Frameworks 5.108. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=446531))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 3 ошибки в Plasma с очень высоким приоритетом (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma — 61 (на одну больше, чем на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 73 ошибки были исправлены на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-06-30&chfieldto=2023-07-07&chfieldvalue=RESOLVED&list_id=2412726&query_format=advanced&resolution=FIXED)

## Сетевое присутствие

Раздел «[KDE для вас](https://kde.org/ru/for/)» основного сайта пополнился ещё одной страницей, на этот раз [для тех, кто часто путешествует](https://kde.org/ru/for/travelers/) (Carl Schwan и команда KDE Promo):

![2](https://pointieststick.files.wordpress.com/2023/07/image.png)

## Автоматизация и систематизация

Добавлен тест для разбора метрик видеокарт NVIDIA, что позволит раньше отлавливать ухудшения, вызванные самим драйвером NVIDIA (David Redondo, [ссылка](https://invent.kde.org/plasma/ksystemstats/-/merge_requests/60))

## Как вы можете помочь

Разработчики, пожалуйста, помогите исправить проблемы в [Qt 6, KDE Frameworks 6 и Plasma 6](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&f1=keywords&f2=product&list_id=2398791&o1=allwordssubstr&o2=notequals&query_format=advanced&v1=qt6&v2=neon)! Plasma 6 вполне годится для повседневного использования энтузиастами (я в их числе), хоть и всё ещё требует работы, чтобы стать готовой к выпуску до конца года.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/07/07/this-week-in-kde-akademy-approaches/>  

<!-- 💡: GPU → видеокарта / графический ускоритель -->
<!-- 💡: applet → виджет -->
<!-- 💡: icons → значки -->
<!-- 💡: task manager → панель задач -->
