# На этой неделе в KDE: Plasma 5.17 приближается

> 7–13 октября — прим. переводчика

На этой неделе было проделано много замечательной работы, которая очень важна, но не очень заметна. Большинство незавершённых дел, о которых я говорил на прошлой неделе, ещё не закончены, поэтому видимые пользователям изменения в этот раз вышли скромными. Но ничего! Plasma 5.17 проходит последние этапы финальной полировки и исправления ошибок перед грядущим выпуском, и полным ходом идёт работа над новшествами для Plasma 5.18 и последующих версий!

## Исправления ошибок и улучшения производительности

Настройки сглаживания шрифтов [теперь читаются и пишутся корректно, поэтому по умолчанию вы будете видеть шрифт с лёгким хинтингом и субпиксельной отрисовкой RGB, как и было задумано](https://phabricator.kde.org/D24562) (Kai Uwe Broulik, Plasma 5.17.0)

Когда центр программ Discover загружает снимки приложений, [анимация загрузки теперь находится в правильном месте](https://bugs.kde.org/show_bug.cgi?id=409504) (Aleix Pol Gonzalez, Plasma 5.17.0)

Discover [больше не даёт вам впустую пытаться отменить операции обновления, которые уже не могут быть отменены](https://bugs.kde.org/show_bug.cgi?id=407466) (Aleix Pol Gonzalez, Plasma 5.17.0)

Discover [теперь правильно отображает лицензии приложений](https://bugs.kde.org/show_bug.cgi?id=408112) (Aleix Pol Gonzalez, Plasma 5.17.0)

Значки KeePassX и некоторых других приложений в системном лотке [теперь отображаются как надо, а не в виде белых квадратов](https://bugs.kde.org/show_bug.cgi?id=358240), в некоторых ситуациях [больше не уменьшаются](https://bugs.kde.org/show_bug.cgi?id=366047), и [в некоторых редких случаях Plasma больше не падает из-за них](https://bugs.kde.org/show_bug.cgi?id=409652) (Konrad Materka, Plasma 5.17.1)

Когда Discover используется для установки автономных обновлений системы и процесс обновления завершается неудачей, [центр приложений теперь предлагает починить систему](https://bugs.kde.org/show_bug.cgi?id=412273) (Aleix Pol Gonzalez, Plasma 5.18.0)

Полноэкранные окна [больше нельзя перемещать или изменять в размере в сеансах X11](https://phabricator.kde.org/D24515) и [Wayland](https://phabricator.kde.org/D24542) (Marco Martin, Plasma 5.18.0)

Страница настройки поиска файлов в Параметрах системы [была переписана на язык QML](https://phabricator.kde.org/D23718) и, несмотря на не изменившийся внешний вид, теперь обеспечивает хорошую базу для исправления ошибок и реализации новых функций (Tomaz Canabrava, Plasma 5.18.0)

Выбор цветовой схемы в текстовом редакторе Kate [теперь работает правильно при использовании второго дисплея с глобальным масштабированием](https://bugs.kde.org/show_bug.cgi?id=412675) (Christoph Cullmann, Frameworks 5.64)

Выбранную кнопку «Сделать новый снимок» в Spectacle [снова можно нажать клавишей Enter](https://bugs.kde.org/show_bug.cgi?id=412184) (David Redondo, Spectacle 19.08.3)

Диспетчер файлов Dolphin [больше не падает при открытии из командной строки со схемой поиска](https://phabricator.kde.org/D24432) (например, `dolphin --new-window baloosearch://`) (Ismael Asensio, Dolphin 19.08.3)

Панель сведений в Dolphin [больше не учитывает скрытые файлы при подсчёте числа файлов, если не включён показ скрытых файлов](https://bugs.kde.org/show_bug.cgi?id=412396) (Méven Car, Dolphin 19.12.0)

При добавлении нескольких альбомов в список воспроизведения музыкального проигрывателя Elisa [порядок песен больше не перемешивается](https://bugs.kde.org/show_bug.cgi?id=406466) (Matthieu Gallien, Elisa 19.12.0)

Дополнения к приложению для записи дисков K3b [снова видны в Discover](https://bugs.kde.org/show_bug.cgi?id=409502) (Aleix Pol Gonzalez, K3b 19.12.0)

## Улучшения пользовательского интерфейса

Список установленных приложений в Discover [теперь сортируется по имени](https://bugs.kde.org/show_bug.cgi?id=412653) (Aleix Pol Gonzalez, Plasma 5.17.0)

Виджет «Громкость» и соответствующая ему страница Параметров системы [теперь выглядят проще и менее избыточно, а также более похоже друг на друга](https://phabricator.kde.org/D24407) (Nate Graham и Sefa Eyeoglu, Plasma 5.18.0):

![0](https://i.imgur.com/KNufKIe.png)

Диалоговое окно эмулятора терминала Konsole с вопросом «Вы уверены, что хотите закрыть эти вкладки?» [теперь можно отключить](https://bugs.kde.org/show_bug.cgi?id=405481) (Kurt Hindenburg, Konsole 19.12.0)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join) и выясните, как стать частью того, что действительно имеет значение. Вам не требуется быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Если вы находите программное обеспечение KDE полезным, рассмотрите возможность [пожертвования](https://kde.org/ru/community/donations/) средств в фонд [KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Дима](https://vk.com/pxllime)  
*Источник:* <https://pointieststick.com/2019/10/13/this-week-in-kde-plasma-5-17-approaches/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: playlist → список воспроизведения -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: system tray → системный лоток -->
