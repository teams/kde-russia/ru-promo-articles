# На этой неделе в KDE: выпуск всё ближе

До Мегавыпуска KDE 6 осталось меньше трёх недель! Мы продолжаем усердно устранять недочёты, потому что хотим, чтобы этот выпуск прошёл максимально гладко и без лишней драмы! Если вы ещё не [протестировали его](https://community.kde.org/Plasma/Plasma_6#How_to_use/test_it), поспешите, чтобы мы могли исправить найденные вами недостатки до финального выпуска!

## Мегавыпуск KDE 6

> (Сюда входит всё ПО, которое будет выпущено 28 февраля: Plasma 6, библиотеки Frameworks 6 и приложения из состава Gear 24.02)

### Улучшения пользовательского интерфейса

Опция диспетчера окон KWin «Активный экран следует за мышью» была удалена; теперь текущим экраном всегда считается экран с активным окном. Оказывается, это намного проще для пользователей и соответствует ожиданиям большинства, так что должно уменьшить поток жалоб на открытие окон на неожиданных экранах (Влад Загородний, [ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/5102))

Для диаграмм вида «Горизонтальные столбцы» в Системном мониторе и его виджетах теперь можно вручную задать диапазон данных (Arjen Hiemstra, [ссылка](https://bugs.kde.org/show_bug.cgi?id=479573))

Опция системного лотка «Всегда показывать все элементы» теперь и правда всегда показывает все элементы, вместо того чтобы всё же скрывать часть из них в соответствии с неочевидной внутренней логикой (Jin Liu, [ссылка](https://bugs.kde.org/show_bug.cgi?id=480781))

Поиск по журналу буфера обмена теперь регистронезависим (Yifan Zhu, [ссылка](https://bugs.kde.org/show_bug.cgi?id=480802))

### Исправления ошибок

> **Важное примечание:** я не упоминаю здесь исправления ошибок, не успевших дойти до пользователей; посреди большого цикла разработки Plasma их слишком много, чтобы про них писать (а вам — читать), да и вообще мало кто их видел. Тем не менее огромное спасибо всем, кто плотно занимается их исправлением!

Исправлена ​​проблема, из-за которой экран мог быть полностью чёрным (или только с курсором мыши) после выхода из ждущего режима компьютера с видеокартой NVIDIA и проприетарным графическим драйвером (David Redondo, [ссылка](https://bugs.kde.org/show_bug.cgi?id=475605))

Датчики температуры процессора теперь работают на различных материнских платах Intel и AMD, где они раньше не работали (Arjen Hiemstra, [ссылка 1](https://bugs.kde.org/show_bug.cgi?id=474766) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=452763))

Восстановление только что открытого распахнутого окна Системного монитора теперь возвращает его к предыдущему размеру, как и ожидалось (Arjen Hiemstra, [ссылка](https://bugs.kde.org/show_bug.cgi?id=478442))

При настройке экрана на дублирование другого для них теперь выбираются более подходящие параметры (Yifan Zhu, [ссылка](https://invent.kde.org/plasma/kscreen/-/merge_requests/275))

Кнопки и полосы прокрутки в виджете «Клейкая заметка» теперь всегда различимы независимо от выбранного цвета фона заметки и системной цветовой схемы (Nate Graham, [ссылка](https://bugs.kde.org/show_bug.cgi?id=353819))

Панель задач, настроенная на показ окон только с текущего экрана, теперь продолжает работать правильно после изменения расположения экранов и до перезапуска Plasma (Кто-то классный, [ссылка](https://bugs.kde.org/show_bug.cgi?id=434176))

Чересчур длинные заголовки виджетов в системном лотке теперь сокращаются, вместо того чтобы выходить за границы маленьких всплывающих окон (Nate Graham, [ссылка](https://bugs.kde.org/show_bug.cgi?id=480880)):

![0](https://invent.kde.org/plasma/plasma-workspace/uploads/9a02550501ab26fd8dbe69dad0acd140/After.jpeg)

Другие сведения об ошибках:

* Осталось 3 ошибки в Plasma с очень высоким приоритетом (на прошлой неделе было 4). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma остаётся 32 (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 145 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-2-2&chfieldto=2024-2-9&chfieldvalue=RESOLVED&list_id=2607393&query_format=advanced&resolution=FIXED)

## После Мегавыпуска

При нажатии комбинации клавиш Alt+Tab, когда не открыто ни одного окна, теперь отображается красивое сообщение «Нет открытых окон», а не сломанный переключатель окон или вообще ничего (Влад Загородний, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/5128)):

![1](https://invent.kde.org/plasma/kwin/uploads/b545aa98ad600a7035efac8470c91b23/Screenshot_20240206_083633.png)

Возвращена давно утерянная возможность добавления разделителей в заголовки окон! (Влад Загородний, Plasma 6.1, [ссылка](https://bugs.kde.org/show_bug.cgi?id=348393)):

![2](https://invent.kde.org/plasma/kwin/uploads/83c29433b144dfaff113ea76963f6947/Screenshot_20240111_223116.png)

## Как вы можете помочь

Спасибо всем, кто поучаствовал в [сборе средств, приуроченном к выпуску Plasma 6](https://kde.org/fundraisers/plasma6member/) — он крайне успешен! Я думал, что 500 новых спонсоров — это слишком оптимистичная цель для фонда KDE e.V., но вы показали мне, что я, к счастью, ошибался. Сейчас их уже **800**, и это число продолжает расти. Благодарю всех, кто поверил в нас, — мы постараемся не подвести! Если вам нравится наша работа, пожертвование — это отличный способ нас отблагодарить. 🙂

Разработчики, помогите исправить проблемы в Qt 6, KDE Frameworks 6 и Plasma 6! Plasma 6 близка к выпуску и в хорошем состоянии, но ещё немного доработки напоследок не помешает.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2024/02/09/this-week-in-kde-inching-closer/>  

<!-- 💡: GPU → видеокарта / графический ускоритель -->
<!-- 💡: OSDs → экранные уведомления -->
<!-- 💡: System Monitor → Системный монитор -->
<!-- 💡: applet → виджет -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
