# На этой неделе в KDE: улучшения управления питанием

> 3–10 сентября, основное — прим. переводчика

На этой неделе мы славно потрудились «под капотом», особенно в области управления питанием!

## Plasma 6

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 81](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

## Новые возможности

Стартовую страницу Помощника первого запуска теперь могут настраивать дистрибутивы — или, раз уж на то пошло, сами пользователи (Nate Graham, [ссылка](https://invent.kde.org/plasma/plasma-welcome/-/merge_requests/101))

## Улучшения пользовательского интерфейса

Реализация портала XDG от KDE теперь поддерживает новый стандарт передачи настроенного цвета выделения приложениям (Fushan Wen, Plasma 5.27.8. [Ссылка](https://invent.kde.org/plasma/xdg-desktop-portal-kde/-/merge_requests/182))

Разделитель между областью заголовка (включая панель инструментов) и основным содержимым окна в приложениях KDE теперь правильной толщины на экранах с высокой плотностью пикселей (Carl Schwan, Plasma 5.27.8. [Ссылка](https://invent.kde.org/plasma/breeze/-/merge_requests/351))

Основанные на KRunner строки поиска теперь предлагают только те режимы сна, которые действительно поддерживаются системой, и их теперь можно найти поиском по слову «питание» (Natalie Clarius, Plasma 6.0. [Ссылка 1](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3263) и [ссылка 2](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3261))

Улучшены навигация с клавиатуры и поддержка специальных возможностей при работе с отзывами в центре приложений Discover (Иван Ткаченко, Plasma 6.0. [Ссылка 1](https://invent.kde.org/plasma/discover/-/merge_requests/613) и [ссылка 2](https://invent.kde.org/plasma/discover/-/merge_requests/611))

## Другие заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

В диспетчере файлов Dolphin исправлена ​​ошибка, из-за которой после очистки поискового запроса не отображались вновь файлы из текущей папки (Amol Godbole, Dolphin 24.02. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=473775))

При запуске на виртуальной машине Plasma теперь отключает автоматическую приостановку сеанса, так как она может привести к зависанию виртуальной машины (Natalie Clarius, Plasma 5.27.8. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=473835))

Гибридный спящий режим теперь действительно работает (Natalie Clarius, Plasma 5.27.8. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3243))

Страница «Специальные возможности» Параметров системы больше не просит вас попусту сохранить изменения, когда вы покидаете её, ничего не изменив (Jürgen Dev, Plasma 5.27.8. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=471154))

Служба поиска файлов Baloo больше не переиндексирует всё после каждой перезагрузки при использовании файловой системы Btrfs (Tomáš Trnka, Frameworks 6.0. [Ссылка](https://invent.kde.org/frameworks/baloo/-/merge_requests/131))

Диалоговое окно сохранения файлов снова позволяет сохранять файлы с кавычками в именах (Andreas Bontozoglou, Frameworks 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=426728))

В различных приложениях на основе QtWidgets оценки в виде звёздочек больше не отображаются неправильно при использовании наборов значков, в которых отсутствует значок `rating-unrated` (Felix Ernst, Frameworks 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=339863))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 4 ошибки в Plasma с очень высоким приоритетом (на прошлой неделе было 3). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma остаётся 58 (на прошлой неделе было 57). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 110 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-09-01&chfieldto=2023-09-08&chfieldvalue=RESOLVED&list_id=2464940&query_format=advanced&resolution=FIXED)

## Автоматизация и систематизация

Улучшены некоторые автоматические тесты в Plasma и библиотеке KPipeWire (Fushan Wen и David Edmundson, [ссылка 1](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1693) и [ссылка 2](https://invent.kde.org/plasma/kpipewire/-/merge_requests/82))

## Как вы можете помочь

Разработчики, пожалуйста, помогите исправить проблемы в [Qt 6, KDE Frameworks 6 и Plasma 6](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&f1=keywords&f2=product&list_id=2398791&o1=allwordssubstr&o2=notequals&query_format=advanced&v1=qt6&v2=neon)! Plasma 6 вполне годится для повседневного использования энтузиастами (я в их числе), хоть и всё ещё требует работы, чтобы стать готовой к выпуску до конца года.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/09/08/this-week-in-kde-power-management-galore/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: icon theme → набор значков -->
