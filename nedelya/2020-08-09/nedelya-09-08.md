# На этой неделе в KDE: миниатюры окон для Wayland

> 3–9 августа — прим. переводчика

На этой неделе была проделана уйма работы, в том числе для отображения миниатюр окон в сеансе Plasma Wayland. Реализация этой возможности ещё более приблизила функционал сеанса Wayland к X11.

## Новые возможности

В эмуляторе терминала Konsole теперь можно [настраивать степень затемнения терминала](https://invent.kde.org/utilities/konsole/-/merge_requests/145) в неактивных створках при включённой функции «Затемнять неактивные терминалы» (Tomaz Canabrava, Konsole 20.12.0):

![0](https://pointieststick.files.wordpress.com/2020/08/screenshot_20200807_230434.jpeg)

В сеансе Wayland [заработал показ миниатюр окон в панели задач](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/36/) (Aleix Pol Gonzalez, Plasma 5.20)

Центр программ Discover теперь умеет [обновлять материалы, установленные из диалога «Загрузка материалов из интернета»](https://invent.kde.org/plasma/discover/-/merge_requests/19) (Dan Leinir Turthra Jensen, Plasma 5.20)

В диалогах настройки виджетов [появилась страница «О виджете»](https://bugs.kde.org/show_bug.cgi?id=357790) (David Redondo, Plasma 5.20):

![1](https://pointieststick.files.wordpress.com/2020/08/screenshot_20200804_083450.jpeg)

В строку состояния Kate и других текстовых редакторов, основанных на библиотеке KTextEditor, [добавлено отображение значения масштаба, если он отличен от 100%](https://bugs.kde.org/show_bug.cgi?id=368296) (Jan Paul Batrina, Frameworks 5.74):

![2](https://pointieststick.files.wordpress.com/2020/08/screenshot_20200807_230610.jpeg)

## Исправления ошибок и улучшения производительности

[Исправлено](https://bugs.kde.org/show_bug.cgi?id=423007) открытие музыкальным проигрывателем Elisa звуковых файлов из диспетчера файлов Dolphin, строки поиска и запуска KRunner или любого другого приложения (Matthieu Gallien, Elisa 20.08.0)

В просмотрщике документов Okular [исправлено](https://invent.kde.org/graphics/okular/-/merge_requests/236#note_85791) переключение экранов в режиме презентации (David Hurka, Okular 20.08.0)

В диспетчере окон KWin исправлена [ошибка, которая могла приводить к его аварийному завершению при выходе из сеанса Wayland](https://bugs.kde.org/show_bug.cgi?id=420077) (Андрей Бутырский, Plasma 5.20)

Исправлена ошибка, [приводившая к завершению всего сеанса Wayland при аварийном завершении слоя совместимости XWayland](https://invent.kde.org/plasma/kwin/-/issues/8); теперь он просто перезапускается (Влад Загородний, Plasma 5.20)

Для строки поиска и запуска KRunner [реализовано](https://bugs.kde.org/show_bug.cgi?id=421426) применение изменений набора используемых модулей без перезапуска (Alexander Lohnau, Plasma 5.20)

Виджет поиска [теперь использует тот же список активных модулей, что и KRunner](https://bugs.kde.org/show_bug.cgi?id=360359) (Alexander Lohnau, Plasma 5.20)

[Исправлено](https://bugs.kde.org/show_bug.cgi?id=423871) замирание курсора мыши на одном месте, иногда случавшееся при использовании функции поворота экрана в сеансе Wayland (Aleix Pol Gonzalez, Plasma 5.20)

В сеанс Wayland добавлена поддержка вызова действий [взмахом от краёв экрана](https://bugs.kde.org/show_bug.cgi?id=421212), а также показа скрытой панели [касанием края экрана](https://bugs.kde.org/show_bug.cgi?id=423842) (Xaver Hugl, Plasma 5.20)

В виджетах мониторинга исправлена [ошибка](https://bugs.kde.org/show_bug.cgi?id=422839), приводившая к переназначению отслеживаемых параметров при добавлении нового сетевого интерфейса (David Edmundson, Plasma 5.20)

Добавлена [процедура очистки кэша примитивов интерфейса в формате SVG](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/50), направленная на борьбу с разнообразными графическими артефактами, появляющимися после изменения коэффициента глобального масштабирования.

В подсистему индексирования и поиска файлов Baloo [внесены изменения, препятствующие бесконечным повторным попыткам проиндексировать файлы при сбое](https://bugs.kde.org/show_bug.cgi?id=425017), что приводило к излишней загрузке процессора (Stefan Brüns, Frameworks 5.74)

## Улучшения пользовательского интерфейса

В диспетчере файлов Dolphin [изменено поведение контекстного меню присвоения меток](https://bugs.kde.org/show_bug.cgi?id=424873): если в списке всего одна метка, то после её присвоения меню будет закрыто автоматически (Ismael Asensio, Dolphin 20.08.0)

В виджете «Цифровые часы» показ даты [включён по умолчанию](https://bugs.kde.org/show_bug.cgi?id=423155) (Claudius Ellsel, Plasma 5.20)

Значение скорости анимации для [элементов интерфейса](https://phabricator.kde.org/D28651) и [оформления окон](https://invent.kde.org/plasma/breeze/-/merge_requests/18) из темы Breeze теперь основывается на общесистемной скорости анимации (Martin Sandsmark и Marco Martin, Plasma 5.20)

В KRunner для выполнения операции умножения помимо символа «*» теперь также можно [использовать символ «x»](https://bugs.kde.org/show_bug.cgi?id=350066) (Alexander Lohnau, Plasma 5.20)

В KRunner [реализован показ всплывающих подсказок для отображения длинного текста](https://invent.kde.org/plasma/milou/-/merge_requests/6), к примеру, для просмотра словарных определений (Alexander Lohnau, Plasma 5.20). Также готовится изменение для показа многострочных результатов поиска:

![3](https://pointieststick.files.wordpress.com/2020/08/tooltip.png)

Сворачивание окна [больше не помещает его в конец панели задач](https://invent.kde.org/plasma/kwin/-/merge_requests/165#note_84929); теперь оно располагается в соответствии с настройками панели (Nate Graham, Plasma 5.20)

В оформление Breeze для приложений GTK внесено множество исправлений и улучшений:

* [исправлено](https://bugs.kde.org/show_bug.cgi?id=422866) отображение текста в боковых панелях диалогов, основанных на компоненте интерфейса `GtkAssistant`;
* [исключена](https://bugs.kde.org/show_bug.cgi?id=418933) прозрачность плавающей строки состояния;
* вид теней от окон приложений GTK [приведён в соответствие](https://invent.kde.org/plasma/breeze-gtk/-/merge_requests/5) с видом теней от окон приложений KDE;
* [улучшен](https://bugs.kde.org/show_bug.cgi?id=422763) внешний вид теней от всплывающих сообщений (Carson Black, Plasma 5.20).

Значки действий «Обновить» и «Удалить» в диалогах «Загрузка материалов из интернета» [заменены на более подходящие](https://bugs.kde.org/show_bug.cgi?id=424892) (Nate Graham, Frameworks 5.74)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник, и совершенно не обязательно быть программистом — занятие по душе найдётся для каждого.

И наконец, если вы находите программное обеспечение KDE полезным, окажите финансовую поддержку [фонду KDE e.V.](https://ev.kde.org/)

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Александр Яворский](https://t.me/KekcuHa)  
*Источник:* <https://pointieststick.com/2020/08/07/this-week-in-kde-window-thumbnails-on-wayland/>  

<!-- 💡: applet → виджет -->
<!-- 💡: get new [thing] → «Загрузить \[что-то\]» -->
<!-- 💡: icons → значки -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: thumbnail → миниатюра -->
