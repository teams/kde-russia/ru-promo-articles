# На этой неделе в KDE: борьба с ухудшениями

На этой неделе мы продолжили исправлять ошибки в Plasma 6.0.3, чтобы привести её в идеальное состояние. Почти все основные ошибки, обнаруженные сообществом, уже исправлены, и теперь мы вплотную занялись борьбой с ухудшениями в сеансе X11 и различными сбоями, которые фиксирует наша новая система автоматической отправки сообщений об ошибках (кстати, спасибо за эти сообщения, они реально помогают). Кроме того, мы добавили несколько автоматических тестов и, наконец, внесли парочку улучшений в пользовательский интерфейс. Сейчас идёт работа над более крутыми вещами, но о них пока рановато говорить!

> 17–24 марта, основное — прим. переводчика

## Новые возможности

Архиватор Ark теперь может открывать и извлекать самораспаковывающиеся архивы `.exe` (Kai Uwe Broulik, Ark 24.05. [Ссылка](https://invent.kde.org/utilities/ark/-/merge_requests/233)):

![0](https://pointieststick.files.wordpress.com/2024/03/screenshot_20240221_014525.png)

Системный монитор в виде гистограммы теперь можно отображать не только вертикально, но и горизонтально (Jin Liu, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/libksysguard/-/merge_requests/332))

## Улучшения пользовательского интерфейса

А вы знали, что щелчком средней кнопкой мыши по файлу в диспетчере файлов Dolphin можно открыть его в первом приложении из подменю «Открыть с помощью…»? Спорим, не знали? И я не знал. А теперь на это указывает пункт меню, который соответствует приложению (Kai Uwe Broulik, Dolphin 24.05. [Ссылка](https://invent.kde.org/system/dolphin/-/merge_requests/737)):

![1](https://pointieststick.files.wordpress.com/2024/03/image-4.png)

Анимация плавающей панели стала плавнее, особенно при использовании коэффициента масштабирования больше 100% (Fushan Wen, Plasma 6.0.3. [Ссылка 1](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2130) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=483680))

В глобальном режиме редактирования Plasma теперь можно щёлкнуть по любому месту внутри панели, чтобы отобразить диалог её настройки, вместо того чтобы целиться курсором в крошечный значок «Настроить» в конце панели (Marco Martin, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2131)):

![2](https://pointieststick.files.wordpress.com/2024/03/image-6.png)

Записи на странице «Автозапуск» Параметров системы теперь сортируются в алфавитном порядке, а не в порядке добавления (Kristen McWilliam, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4001))

Значки всплывающего меню настройки экранов теперь отображаются с учётом цвета выделения (Nicolas Fella, Frameworks 6.1. [Ссылка](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/335)):

![3](https://pointieststick.files.wordpress.com/2024/03/image-7.png)

## Исправления ошибок

Устранено недавнее ухудшение, вызывавшее сбой просмотрщика изображений Gwenview при попытке воспроизвести видео (Nicolas Fella, Gwenview 24.02.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=483242))

Решена проблема в просмотрщике документов Okular, приводившая к сбою при закрытии заметки с комментарием, когда включена проверка орфографии (Albert Astals Cid, Okular 24.02.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=483904))

Установка шрифта в Wayland теперь действительно работает, а не приводит к сбою Параметров системы (Nicolas Fella, Plasma 5.27.12. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=484273))

Устранена причина сбоя Параметров системы при применении нового оформления окон (Nicolas Fella, Plasma 6.0.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=456531))

Plasma больше не завершается аварийно при воспроизведении определённых музыкальных клипов в Spotify (Fushan Wen, Plasma 6.0.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=483818))

Устранено недавнее ухудшение Системного монитора, которое приводило к его сбою при отмене изменений после редактирования страницы (Arjen Hiemstra, Plasma 6.0.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482198))

Решена странная проблема, из-за которой некоторые окна, принадлежащие слою совместимости XWayland, непрерывно меняли свой размер при использовании определённых дробных коэффициентов масштабирования (Yifan Zhu, Plasma 6.0.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=481460))

В сеансе Wayland снято требование диспетчера окон KWin, которое заключалось в том, что содержимое буфера обмена может попадать в окно XWayland, только если на нём есть фокус ввода с клавиатуры. Такого требования не было в X11, и его применение ломало некоторые приложения, работающие через XWayland (David Edmundson, Plasma 6.0.3. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/5436))

Устранено недавнее ухудшение в Qt, влиявшее на соблюдение закона Фиттса под X11. Из-за него виджеты на панели могли не активироваться щелчком по пикселям у самого края экрана (Fushan Wen, Plasma 6.0.3 или Qt 6.6.3, [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=482580) и [ссылка 2](https://codereview.qt-project.org/c/qt/qtdeclarative/+/548356))

Устранено недавнее ухудшение, которое приводило к неправильному отображению скорости передачи данных в виджете «Сети» (Kai Uwe Broulik, Plasma 6.0.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=479230))

Устранено недавнее ухудшение в многомониторных конфигурациях, из-за которого виджеты панели задач отображали задачи не с того экрана, а в сеансе X11 уведомления появлялись не в нужном месте, а в центре одного из экранов (Fushan Wen, Plasma 6.0.3. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=482339) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=481736))

Устранено недавнее ухудшение в Системном мониторе, допущенное при портировании, когда при изменении порядка страниц их можно было перетаскивать только вверх, но не вниз (Arjen Hiemstra, Plasma 6.0.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482377))

Устранено недавнее ухудшение, связанное с цветом плиток в виджетах Системного монитора при использовании сетки цветов для визуализации (Arjen Hiemstra, Plasma 6.0.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482664))

Повышена устойчивость кода, отвечающего за перенос пользовательских глобальных комбинаций клавиш из старой службы KHotKeys в новую KGlobalAccel (Nicolas Fella, Plasma 6.0.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=484063))

Решена проблема с управлением камерой с помощью мыши в некоторых играх под Wayland (Xaver Hugl, Plasma 6.0.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=458233))

В Wayland теперь нормально отображается предпросмотр субпиксельной отрисовки на странице «Шрифты» Параметров системы (Du Yijie, Plasma 6.0.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=432320))

Компактная форма (вид кнопки) виджета меню приложения теперь отключается, если приложение не предоставляет глобального меню. Раньше она просто исчезала с экрана, приводя к перемещению содержимого панели взад-вперёд (Nate Graham, Plasma 6.0.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=435928))

Устранено недавнее ухудшение работы датчиков совокупных значений в Системном мониторе и его виджетах (Arjen Hiemstra, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=477983))

Исправлено изменение цветов под нестандартную цветовую схему многими значками Breeze, встречающимися в системном лотке (Nicolas Fella, Frameworks 6.1. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=483512) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=477289))

Устранён источник загадочных бессмысленных сообщений «Неизвестная ошибка Open Collaboration Service.(0)», которые случайно появлялись при навигации по диалогам загрузки материалов из сети (Albert Astals Cid, Frameworks 6.1. [Ссылка](https://invent.kde.org/frameworks/attica/-/merge_requests/43))

В Системном мониторе устранено недавнее ухудшение, вызванное Qt, которое приводило к его сбою при попытке открыть боковую панель сведений на странице «Приложения» (Nicolas Fella, Qt 6.6.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482169))

Другие сведения об ошибках:

* Остаётся 3 ошибки в Plasma с очень высоким приоритетом (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 34 (на прошлой неделе было 36). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 149 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-3-15&chfieldto=2024-3-22&chfieldvalue=RESOLVED&list_id=2657456&query_format=advanced&resolution=FIXED)

## Производительность и технические улучшения

Виджет Plasma утилиты для резервного копирования Kup был полностью адаптирован к Plasma 6, так что он должен снова заработать со следующим выпуском Kup (Simon Persson, [link](https://bugs.kde.org/show_bug.cgi?id=482015))

За глобальные комбинации клавиш для регулировки громкости теперь отвечает модуль системы управления службами KDED, а не последний активный экземпляр виджета «Громкость», что работало ненадёжно и ломало комбинации клавиш, когда виджета вовсе не было или было несколько его экземпляров, но один из них был удалён без перезапуска Plasma (Bharadwaj Raju, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=391578))

## Автоматизация и систематизация

Новая [инструкция по созданию эффектов для KWin](https://develop.kde.org/docs/plasma/kwineffect/) опубликована на сайте develop.kde.org (Влад Загородний и Carl Schwan, [ссылка](https://invent.kde.org/documentation/develop-kde-org/-/merge_requests/366))

Документация [«Разработка KDE для начинающих»](https://develop.kde.org/docs/getting-started/building/) была перемещена на более подходящий сайт develop.kde.org (Thiago Sueto, [ссылка](https://invent.kde.org/documentation/develop-kde-org/-/merge_requests/356))

В KWin добавлена целая куча тестов, проверяющих поведение окон X11 (Влад Загородний, [ссылка](https://invent.kde.org/plasma/kwin/-/issues/214))

Добавлена проверка работы основного пользовательского интерфейса KRunner (Fushan Wen, [ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4027))

Добавлена проверка работы комбинации клавиш для блокировки экрана (Kristen McWilliam, [ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/4840))

## Как вы можете помочь

Помогите, пожалуйста, с [сортировкой отчётов об ошибках](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging)! В Bugzilla всё ещё завал, и мы будем благодарны за помощь.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

И напоследок: KDE на 99,9% обеспечивается трудом, не оплаченным KDE e.V. Если вы хотите помочь изменить это, [рассмотрите возможность пожертвования](https://kde.org/community/donations/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2024/03/22/this-week-in-kde-4/>  
