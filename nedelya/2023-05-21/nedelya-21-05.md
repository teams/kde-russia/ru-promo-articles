# На этой неделе в KDE: начальная поддержка HDR

Один из разработчиков диспетчера окон KWin Xaver Hugl в ходе недавнего хакатона, посвящённого HDR, и прошедшей встречи команды Plasma проделал большую работу по добавлению поддержки HDR: она уже частично включена в сеанс Wayland Plasma 6.0! Вместе с этим заложен фундамент для реализации управления цветом в Wayland. Тема сложная и интересная — если хотите узнать больше, прочитайте [отличную запись в блоге Xaver](https://vk.com/wall-33239_13870).

## Новые возможности

Приложение для сканирования Skanpage теперь выводит в интерфейс опции, зависящие от используемого сканера, такие как яркость, контрастность, гамму и цветовой баланс (Skanpage 23.08. [Ссылка](https://invent.kde.org/utilities/skanpage/-/merge_requests/44))

При регулировке яркости теперь можно удерживать клавишу Shift, чтобы менять её значение с шагом в 1% — как это уже было сделано для громкости на прошлой неделе (Fushan Wen, Plasma 6.0. [Ссылка 1](https://invent.kde.org/plasma/powerdevil/-/merge_requests/175) и [ссылка 2](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2917))

Изменения приоритетов приложений в настройках привязок файлов теперь можно применять сразу к нескольким типам файлов (Marco Rebhan, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/kde-cli-tools/-/merge_requests/31)):

![0](https://pointieststick.files.wordpress.com/2023/05/image-2.png)

## Улучшения пользовательского интерфейса

Модуль отладки текстового редактора Kate получил различные улучшения: на его служебную панель были добавлены кнопки для перемещения отладчика, у него появилась страница настройки, а его название стало понятнее (Akseli Lahtinen, Kate 23.08. [Ссылка 1](https://invent.kde.org/utilities/kate/-/merge_requests/1210), [ссылка 2](https://invent.kde.org/utilities/kate/-/merge_requests/1213) и [ссылка 3](https://invent.kde.org/utilities/kate/-/merge_requests/1207))

Если вы раньше полагались на то, что просмотрщик изображений Gwenview предлагал самого себя в меню «Открыть с помощью», чтобы открывать текущее изображение в новом окне, то теперь вам доступна отдельная функция «Открыть в новом окне», которая делает то же самое поддерживаемым способом (Евгений Попов, Gwenview 23.08. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=469179))

Эффект сворачивания окон «Волшебная лампа» теперь работает и выглядит лучше с плавающими и скрывающимися панелями, а также в многомониторных конфигурациях (Влад Загородний, Plasma 5.27.6. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=466177), [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=361121) и [ссылка 3](https://bugs.kde.org/show_bug.cgi?id=463581))

При расположении окон в мозаичном режиме выбранное вами значение отступа теперь влияет и на зазоры между окнами, а не только на отступ от краёв экрана (Ismael Asensio, Plasma 5.27.6. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=469720))

Страница «Комнаты» Параметров системы была переведена на язык QML и немного визуально обновлена (Ismael Asensio, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1462)):

![1](https://pointieststick.files.wordpress.com/2023/05/image-4.png)

Теперь мы используем переключатели вместо флажков для мгновенно применяемых действий и выключателей в виджетах Plasma, например, в виджетах «Сети» и «Bluetooth» (Niccolò Venerandi и Nate Graham, Plasma 6.0. [Ссылка 1](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/654), [ссылка 2](https://invent.kde.org/plasma/plasma-nm/-/commit/a3783f7501e26d3d2a5343c4a4875172d2870bd1) и [ссылка 3](https://invent.kde.org/plasma/bluedevil/-/commit/03d2674080e605d5182b8456481791aff41f0091)):

![2](https://pointieststick.files.wordpress.com/2023/05/networks-with-switch.jpg)

Диспетчер файлов Dolphin больше не показывает индикатор свободного места для носителей, доступных только для чтения, потому что от этого было мало толку (Kai Uwe Broulik, Frameworks 6.0 [Ссылка](https://invent.kde.org/frameworks/kio/-/merge_requests/1059))

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Dolphin при запуске снова восстанавливает ранее открытые вкладки, если настроен на это; мы недавно сломали это поведение, исправляя [другую ошибку](https://bugs.kde.org/show_bug.cgi?id=408919) (Méven Car, Dolphin 23.04.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=469656))

Меню «Открыть с помощью» в Gwenview снова всегда запускает выбранное вами приложение, а не какое-то другое — мы недавно сломали это, когда убирали из этого меню сам Gwenview (Gwenview 23.04.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=469824))

В диалоговом окне «Свойства» для файлов и папок с рабочего стола теперь отображается вкладка «Сведения», как и ожидалось (Méven Car, Dolphin 23.08. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460117))

Кнопка «Источники программ» на странице настроек центра приложений Discover теперь действительно работает (Aleix Pol Gonzalez, Plasma 5.27.6. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=466709))

Discover наконец-то показывает версии обновляемых приложений Flatpak в правильном порядке, на этот раз взаправду (Aleix Pol Gonzalez, Plasma 5.27.6. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=448521))

При использовании глобального меню, приложения на основе GTK теперь правильно отображают свои меню сразу после их запуска, так что больше не нужно сначала переключать фокус на другое окно и обратно (Severin von Wnuck, Plasma 5.27.6. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=422786))

Глобальные действия «Переместить окно на следующий/предыдущий экран» снова отправляют окна на правильные экраны (Natalie Clarius, Plasma 5.27.6. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=467996))

При использовании наборов значков с разноцветными значками для действий, различные значки в приложениях на основе Kirigami больше не закрашиваются иногда полностью чёрным цветом (Александр Волков, Frameworks 5.107. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=451538))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 4 ошибки в Plasma с очень высоким приоритетом (на прошлой неделе было 3). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma остаётся 50 (на прошлой неделе было 46). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 136 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-05-12&chfieldto=2023-05-19&chfieldvalue=RESOLVED&list_id=2370898&query_format=advanced&resolution=FIXED)

## Как вы можете помочь

Если вы пользователь, обновитесь до [Plasma 5.27](https://www.kde.org/announcements/plasma/5/5.27.0/)! Если обновление в вашем дистрибутиве не доступно и не предвидится, подумайте о переходе на другой дистрибутив, поставляющий программное обеспечение ближе к графикам разработчиков.

Разработчики, пожалуйста, переходите на Plasma 6 и начинайте исправлять ошибки в ней! Она годится для повседневного использования энтузиастами (я в их числе), хоть и всё ещё очень сырая и требует много работы, чтобы стать готовой к выпуску до конца года.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

<!-- Временная ссылка! -->

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/05/20/this-week-in-kde-preliminary-hdr-support/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: directory → каталог -->
<!-- 💡: icons → значки -->
