# На этой неделе в KDE: предвкушаем Plasma 6.1

На этой неделе мы устранили пару остававшихся в Plasma 6.0 ошибок и внесли различные улучшения интерфейса в рамках работы над Plasma 6.1. Ничего принципиально нового — просто медленная, постепенная работа над стабильным выпуском!

> 28 апреля – 5 мая, основное — прим. переводчика

## Улучшения пользовательского интерфейса

В текстовом редакторе Kate файл теперь считается недавно использованным также при сохранении и закрытии, а не только при открытии. Это значит, что документы, которые долгое время оставались открытыми, пока вы работали с ними, больше не будут пропадать из списка последних файлов (Christoph Cullmann, Kate 24.05. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=486203))

Значки панели для меню запуска приложений теперь ограничены по размеру и больше не будут чрезмерно раздуваться на широких панелях (Akseli Lahtinen и Nate Graham, Plasma 6.0.5. [Ссылка 1](https://invent.kde.org/frameworks/solid/-/merge_requests/164) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=482612))

В Параметрах системы наборы значков GNOME Adwaita и HighContrast больше нельзя выбрать в качестве системных: хотя они и зарегистрированы как совместимые с FreeDesktop, однако [больше не предназначены для использования таким образом](https://gitlab.gnome.org/GNOME/adwaita-icon-theme/-/issues/288) и сломают внешний вид всех программ KDE, если попытаться их так использовать (Nate Graham, Plasma 6.0.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=486409))

«Активный» экран, на котором диспетчер окон KWin будет открывать новые окна, теперь определяется по принципу последнего взаимодействия с пользователем. К таким взаимодействиям относятся, например, движение мыши и фокус ввода с клавиатуры. Надеемся, это будет лучше соответствовать ожиданиям пользователей (Xaver Hugl, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/5625))

Представления выбора обоев сделаны безрамочными, что соответствует текущему стилю большинства других страниц настроек в Параметрах системы и Plasma (Nate Graham, Plasma 6.1. [Ссылка 1](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4271) и [ссылка 2](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2226)):

![0](https://pointieststick.files.wordpress.com/2024/05/frameless-wallpaper-chooser.jpeg)

Усовершенствован интерфейс, отображаемый при изменении оформления рабочей среды: теперь будет понятнее, что произойдёт и что может быть опасным (Nate Graham, Plasma 6.1. [Ссылка 1](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4272) и [ссылка 2](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4281#note_934301)):

<https://i.imgur.com/626VvQ2.mp4?_=1>

Если изменять профили энергопотребления с помощью инструмента командной строки `powerprofilesctl`, новое состояние теперь будет отображаться в виджете «Управление питанием и батарея» (Natalie Clarius, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=485610))

Для некоторых значков Breeze (`folder-encrypted`, `folder-decrypted` и `folder-music`) исправлены символьные версии с размерами 16 и 22 пикселя (Nate Graham, Frameworks 6.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=486316))

## Исправления ошибок

Исправлена ошибка при открытии больших изображений в просмотрщике Gwenview: теперь версия на Qt 6 может открывать файлы того же размера, что и версия на Qt 5 (Méven Car, Gwenview 24.05. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=482195))

KWin в сеансе Wayland больше не завершает работу аварийно, если не удаётся открыть сокет для слоя совместимости XWayland (Влад Загородний, Plasma 6.0.5. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/5687))

Устранён сбой Plasma при изменении списка избранных приложений в меню запуска (Fushan Wen, Plasma 6.0.5. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/4275))

Устранена ошибка, в результате которой при использовании Qt 6.7 всплывающее окно системного лотка сжималось до крошечного размера. Также нажатие на виджет Системного монитора с датчиками видеокарты больше не приводит к зависанию Plasma (Marco Martin, Plasma 6.0.5. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=485456) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=480722))

Устранена чрезвычайно странная проблема, которая могла возникнуть при открытии окон приложений интегрированной среды разработки IntelliJ и приводила к тому, что другие окна и панели Plasma пропускали через себя щелчки мыши (Вячеслав Майоров, Plasma 6.0.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=478556))

Окна, быстрым действием распахнутые на половину или четверть экрана, при выходе системы из спящего режима больше не исчезают, а распахнутые по вертикали окна больше не располагаются иногда неправильно (Xaver Hugl, Plasma 6.0.5. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=461886) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=476037))

В KWin реализован обход ошибки в драйвере видеокарт AMD, который должен снизить количество нерегулярных визуальных артефактов (Xaver Hugl, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/5635))

Исправлен ещё один вариант печально известной проблемы с некрасивыми уголками — на этот раз в меню в приложениях на основе QtWidgets (Иван Ткаченко, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/breeze/-/merge_requests/455))

Изменение размера окна с выбором обоев больше не приводит к тому, что заголовок представления сетки внезапно перемещается в середину представления (Nate Graham, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=486433))

Добавлены соответствующие значки для большего числа аудио- и видеофайлов. Если подходящий значок для формата не найден, система больше не будет возвращаться к неуместной пиктограмме динамика или киноплёнки (Kai Uwe Broulik и Nate Graham, Frameworks 6.2. [Ссылка 1](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/352) и [ссылка 2](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/359))

Исправлена ошибка в библиотеке Kirigami, когда у некоторых элементов интерфейса при использовании смешанных светло-тёмных цветовых схем были неправильные цвета (Евгений Харченко, Frameworks 6.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=486163))

Исправлена проблема, из-за которой приложения со значками в системном лотке некорректно закрывались при удалении этих значков (Tor Arne, Qt 6.7.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=484405))

Другие сведения об ошибках:

* Осталось 3 ошибки в Plasma с очень высоким приоритетом (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 35 (на прошлой неделе было 39). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 126 ошибок было исправлено на этой неделе во всём программном обеспечении KDE [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-4-26&chfieldto=2024-5-03&chfieldvalue=RESOLVED&list_id=2702314&query_format=advanced&resolution=FIXED)

## Производительность и технические улучшения

На основе отзывов специалистов по безопасности SUSE реализован ряд мер по усилению безопасности системы обработки ошибок (Harald Sitter, Plasma 6.0.5. [Ссылка](https://invent.kde.org/plasma/drkonqi/-/merge_requests/217))

## Автоматизация и систематизация

Добавлено несколько автоматических тестов для обеспечения надлежащего монтирования различных подключаемых файловых систем (Stefan Brüns, Frameworks 6.2. [Ссылка](https://invent.kde.org/frameworks/solid/-/merge_requests/164))

Добавлен автоматический тест, позволяющий убедиться, что элементы интерфейса с темой Plasma, которые должны иметь одинаковую высоту (например, текстовые поля и кнопки), сохраняют одинаковую высоту, даже если стиль Plasma изменён (Fushan Wen, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/libplasma/-/merge_requests/1111))

## Как вы можете помочь

Сообщество KDE стало важным явлением в мире — благодаря вашему труду и вложенному времени! Однако по мере роста проекта важно обеспечить стабильность работы над ним, а это значит, что труд должен *оплачиваться*. Сегодня KDE по большей части использует работу людей, которым фонд KDE e.V. не платит, и это проблема. Мы предприняли шаги, которые позволят изменить ситуацию, и наняли [платных технических подрядчиков](https://ev.kde.org/corporate/staffcontractors/), однако из-за ограниченности финансовых ресурсов этого всё ещё мало. Если вы хотите помочь, [сделайте пожертвование](https://kde.org/ru/community/donations/)!

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* коммерческий  
*Источник:* <https://pointieststick.com/2024/05/03/this-week-in-kde-looking-towards-plasma-6-1/>  
