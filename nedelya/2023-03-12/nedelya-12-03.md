# На этой неделе в KDE: Qt устойчив к падениям Wayland

Благодаря героическому труду David Edmundson, приложения на Qt 6 (а значит, в обозримом будущем, и всё программное обеспечение KDE) [теперь выживают при аварийном завершении компоновщика Wayland](https://codereview.qt-project.org/c/qt/qtwayland/+/377104)! Это очень круто! Кроме того, продолжается работа по добавлению этой функциональности в другие распространённые фреймворки вроде GTK.

Перенос Plasma на Qt 6 идёт полным ходом, и с каждым днём будущую Plasma 6 используют всё больше и больше энтузиастов. Я пока не в их числе, потому что побаиваюсь такой нестабильности, так что жду, пока там всё немного устаканится. 🙂 Но, надеюсь, уже скоро! А пока посмотрите, что ещё у нас произошло:

## Новые возможности

Эмулятор терминала Konsole теперь работает в ОС Windows! Помимо возможного распространения самого Konsole для Windows, это означает, что другие приложения KDE, совместимые с Windows и содержащие панель терминала, — например, текстовый редактор Kate, — теперь смогут встраивать именно Konsole, а не другой слабый компонент терминала (Waqar Ahmed и Christoph Cullmann, Konsole и Kate 23.04. [Ссылка 1](https://invent.kde.org/utilities/konsole/-/merge_requests/814) и [ссылка 2](https://invent.kde.org/utilities/kate/-/merge_requests/1147))

Теперь можно настроить стандартное меню запуска приложений так, чтобы оно использовало сетку для показа всех разделов, а не только «Избранного» (Tanbir Jishan, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1317)):

![0](https://pointieststick.files.wordpress.com/2023/03/grid-view-for-everything-in-kickoff.jpg)

## Улучшения пользовательского интерфейса

У новых пользователей система теперь по умолчанию будет переходить в ждущий режим через 15 минут бездействия; кроме того, для ноутбуков-трансформеров теперь генерируются правильные профили питания (Plasma 5.27.3, Nate Graham, [ссылка 1](https://invent.kde.org/plasma/powerdevil/-/merge_requests/144) и [ссылка 2](https://invent.kde.org/plasma/powerdevil/-/merge_requests/143))

На страницах приложений в Discover ряды кнопок теперь становятся столбцами в узких окнах и в мобильном интерфейсе, а их макет стал оптимальнее (Emil Velikov, Plasma 5.27.3. [Ссылка](https://invent.kde.org/plasma/discover/-/merge_requests/480)):

![1](https://pointieststick.files.wordpress.com/2023/03/image-1-1.png)

Интерфейс Помощника первого запуска теперь [совместим с мобильными устройствами](https://invent.kde.org/plasma/plasma-welcome/-/issues/13). Его содержание по-прежнему во многом рассчитано только на ПК, но это тоже скоро изменится! (Nate Graham, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=466572)):

![2](https://pointieststick.files.wordpress.com/2023/03/mobile-friendly-plasma-welcome.jpg)

Экраном входа в систему (SDDM) теперь можно управлять с помощью сенсорного экрана, если для его показа используется KWin Wayland: обрабатываются касания, открывается виртуальная клавиатура, а список раскладок клавиатуры можно пролистывать пальцем (Aleix Pol Gonzalez и Nate Graham, Plasma 5.27.3 и Frameworks 5.104. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=466721), [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=466969) и [ссылка 3](https://bugs.kde.org/show_bug.cgi?id=466968))

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Экранная линейка KRuler теперь правильно работает в сеансе Plasma Wayland (Shenleban Tongying, KRuler 23.04. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=461287))

Исправлена ошибка, из-за которой приложения могли аварийно завершать работу в сеансе Wayland, когда засыпал экран (Aleix Pol Gonzalez, Plasma 5.27.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=466674))

Ночная цветовая схема теперь работает на устройствах на основе архитектуры ARM, не поддерживающих «таблицу поиска гаммы», но поддерживающих «матрицы преобразования цвета»! Она по-прежнему не работает с видеокартами NVIDIA, потому что они не поддерживают ни то, ни другое (Влад Загородний, Plasma 5.27.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=455720))

Красный и синий цветовые каналы больше не путаются иногда при захвате экрана в сеансе Wayland (Aleix Pol Gonzalez, Plasma 5.27.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=466655))

Кнопки в виде изображений в приложениях GTK, использующих тему Breeze, теперь отображаются правильно (Janet Blackquill, Plasma 5.27.3. [Ссылка](https://invent.kde.org/plasma/breeze-gtk/-/merge_requests/72))

Исправлены два серьёзных сбоя в Plasma, связанные с отображением миниатюр окон панелью задач (Fushan Wen, Frameworks 5.104. [Ссылка](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/753))

При использовании приложений KDE вне Plasma они больше не раскрашиваются в странную смесь собственной цветовой схемы и цветовой схемы целевой платформы (Jan Grulich, Frameworks 5.105. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=447029))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Осталось 12 ошибок в Plasma с очень высоким приоритетом (на прошлой неделе было 14). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma остаётся 50 (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 101 ошибка была исправлена на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-03-03&chfieldto=2023-03-10&chfieldvalue=RESOLVED&list_id=2301328&query_format=advanced&resolution=FIXED)

## Автоматизация и систематизация

Упрощена структура [документации на портале разработчика](https://develop.kde.org/docs), чтобы нужное было легче найти (Carl Schwan. [Ссылка](https://invent.kde.org/documentation/develop-kde-org/-/merge_requests/276))

## Сетевое присутствие

Появилась страница [«KDE для учёных»](https://kde.org/for/scientists), где представлено наше программное обеспечение для использования в профессиональных научно-технических целях — берите пример с NASA и барселонского синхротрона ALBA!

![3](https://pointieststick.files.wordpress.com/2023/03/kde-for-scientists.png)

[Вводная страница о библиотеке Kirigami](https://develop.kde.org/frameworks/kirigami) была обновлена и теперь содержит актуальную информацию о применении Kirigami в разработке современных приложений:

![4](https://pointieststick.files.wordpress.com/2023/03/kirigami.png)

## Как вы можете помочь

Если вы пользователь, обновитесь до [Plasma 5.27](https://www.kde.org/announcements/plasma/5/5.27.0/)! Если обновление в вашем дистрибутиве не доступно и не предвидится, подумайте о переходе на другой дистрибутив, поставляющий программное обеспечение ближе к графикам разработчиков.

Разработчики, помогите нам исправить [известные проблемы в Plasma 5.27](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&keywords=regression%2C%20&keywords_type=allwords&list_id=2278517&query_format=advanced&version=5.26.90&version=5.27.0&version=5.27.1&version=5.27.2&version=5.27.3&version=5.27.4&version=5.27.5). Также посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этих списков — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

<!-- Временная ссылка! -->

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/03/10/this-week-in-kde-qt-apps-survive-the-wayland-compositor-crashing/>  

<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: applet → виджет -->
<!-- 💡: application launcher → меню запуска приложений -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: thumbnail → миниатюра -->
