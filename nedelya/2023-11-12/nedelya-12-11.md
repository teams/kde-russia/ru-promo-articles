# На этой неделе в KDE: Wayland по умолчанию и HDR в играх

Да-да, вы не ошиблись, мы решили дёрнуть рубильник и перейти на использование [Wayland по умолчанию](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2188)! Работа над [тремя остающимися препятствиями](https://community.kde.org/Plasma/Wayland_Showstoppers) уже идёт, и мы ожидаем, что они будут устранены в ближайшее время — но точно до итогового выпуска Plasma 6.0. Мы хотели внести это изменение как можно раньше, чтобы собрать побольше обратной связи.

Разумеется, это не всё. Это была ещё одна насыщенная неделя:

## Plasma 6

> (Сюда входит всё ПО, которое будет выпущено 28 февраля: Plasma 6, библиотеки Frameworks 6 и приложения из состава Gear 24.02)

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 118](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

Оформление приложений Breeze получило долгожданное обновление: в нём больше нет такого числа вложенных рамок! Теперь в нём, как и в приложениях на основе Kirigami, области разделяются тонкими линиями (Carl Schwan, [ссылка 1](https://invent.kde.org/plasma/breeze/-/merge_requests/342), [ссылка 2](https://invent.kde.org/frameworks/kwidgetsaddons/-/merge_requests/208), [3](https://invent.kde.org/frameworks/kio/-/merge_requests/1461), [4](https://invent.kde.org/plasma/kmenuedit/-/merge_requests/26), [5](https://invent.kde.org/system/dolphin/-/merge_requests/600), [6](https://invent.kde.org/utilities/kate/-/merge_requests/1286), [7](https://invent.kde.org/utilities/ark/-/merge_requests/191), [8](https://invent.kde.org/graphics/okular/-/merge_requests/811), [9](https://invent.kde.org/graphics/kolourpaint/-/merge_requests/36) и [10](https://invent.kde.org/plasma/plasma-integration/-/merge_requests/113)):

![0](https://pointieststick.files.wordpress.com/2023/11/frameless-dolphin.jpeg)

![1](https://pointieststick.files.wordpress.com/2023/11/kate.jpeg)

![2](https://pointieststick.files.wordpress.com/2023/11/file-dialog.jpeg)

В сеанс Plasma Wayland добавлена предварительная поддержка вывода HDR-изображения из игр на совместимые экраны (Xaver Hugl, [ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/4589))

В утилите для создания снимков экрана Spectacle появилась возможность записи прямоугольной области экрана (Noah Davis, [ссылка](https://invent.kde.org/graphics/spectacle/-/merge_requests/283))

Страница «Принтеры» Параметров системы была серьёзно переработана и теперь включает в себя функции, ранее предоставляемые запуском внешних программ. Результат выглядит намного приятнее и целостнее, чем бывший каскад диалоговых окон (Mike Noe, [ссылка](https://invent.kde.org/plasma/print-manager/-/merge_requests/63)):

![3](https://pointieststick.files.wordpress.com/2023/11/printers-1.jpeg)

![4](https://pointieststick.files.wordpress.com/2023/11/printers-2.jpeg)

Настройки панели Plasma снова были переработаны, и теперь *все* они содержатся в одном окне, без вложенных подменю. Закрыто 14 решённых отчётов об ошибках (Niccolò Venerandi и Marco Martin, [ссылка 1](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1723) и [ссылка 2](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3396)):

![5](https://pointieststick.files.wordpress.com/2023/11/image-5.png)

Архиватор Ark стал значительно быстрее сжимать файлы алгоритмами XZ и zstandard, поскольку они теперь работают в несколько потоков (Zhangzhi Hu, [ссылка](https://invent.kde.org/utilities/ark/-/merge_requests/207#note_797340))

При запуске приложений Flatpak вам больше не предлагается одобрить или запретить их «фоновую активность»; польза от этой концепции была сомнительной (David Edmundson, [ссылка](https://bugs.kde.org/show_bug.cgi?id=473881))

Добавлена простая опция для отключения звуков уведомлений во всей системе (Ismael Asensio, [ссылка 1](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3157), [ссылка 2](https://invent.kde.org/frameworks/knotifications/-/merge_requests/120) и [ссылка 3](https://invent.kde.org/plasma/kde-gtk-config/-/merge_requests/89)):

![6](https://pointieststick.files.wordpress.com/2023/11/image-6.png)

Значительно ускорен запуск Plasma — вплоть до нескольких секунд (Harald Sitter, [ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3491))

Возвращена настройка интервала двойного щелчка, теперь она находится на главной странице раздела «Поведение рабочей среды» Параметров системы. Предвосхищая вопрос «А почему не на странице „Мышь“?»: потому что она влияет и на сенсорные панели, у которых своя страница, а дублировать настройку на обеих страницах неряшливо и некрасиво (Nicolas Fella, [ссылка](https://bugs.kde.org/show_bug.cgi?id=413502))

Применение параметров Plasma к экрану входа в систему (SDDM) теперь устанавливает и желаемое состояние режима Num Lock после запуска (Chandradeep Dey, [ссылка](https://invent.kde.org/plasma/sddm-kcm/-/merge_requests/61))

В приложениях на основе QtWidgets контекстное меню выбранного объекта теперь можно открыть комбинацией клавиш Shift+F10 (Felix Ernst, [ссылка 1](https://invent.kde.org/frameworks/kxmlgui/-/merge_requests/194) и [ссылка 2](https://invent.kde.org/frameworks/kconfig/-/merge_requests/251))

Системный монитор теперь запускается комбинацией клавиш Meta+Esc (Arjen Hiemstra, [ссылка](https://invent.kde.org/plasma/plasma-systemmonitor/-/merge_requests/238))

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Решено множество проблем при работе с несколькими экранами, когда экраны вовремя не включались или застывали до перехода в другой виртуальный терминал и обратно (Xaver Hugl, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/4611))

Исправлена ​​ошибка, из-за которой расположение значков рабочего стола могло восстанавливаться неправильно, особенно если к устройству когда-либо были подключены несколько экранов (Harald Sitter, Plasma 5.27.10. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1800))

Исправлена ​​ошибка, из-за которой ночная цветовая схема могла включаться в неправильное время при использовании определённой комбинации настроек (Ismael Asensio, Plasma 5.27.10. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=469730))

Диспетчер окон KWin больше не позволяет уменьшать размер окон, забывших установить минимальный размер, до нулевого, после чего их было невозможно найти (Xaver Hugl, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=469237))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 4 ошибки в Plasma с очень высоким приоритетом (на прошлой неделе было 3). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 47 (на прошлой неделе было 52). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 146 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-11-03&chfieldto=2023-11-10&chfieldvalue=RESOLVED&list_id=2517410&query_format=advanced&resolution=FIXED)

## Как вы можете помочь

Мы проводим [сбор средств, приуроченный к выпуску Plasma 6](https://kde.org/fundraisers/plasma6member/), и нам нужна ваша помощь! Благодаря вам мы пересекли отметку в 60% от цели, но пока её не достигли. Если вам нравится наша работа, пожертвование — это отличный способ нас отблагодарить. 🙂

Разработчики, пожалуйста, помогите исправить проблемы в Qt 6, KDE Frameworks 6 и Plasma 6! Plasma 6 уже вполне годится для повседневного использования энтузиастами (я в их числе), хоть и всё ещё требует работы, чтобы стать готовой к выпуску в феврале.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/11/10/this-week-in-kde-wayland-by-default-de-framed-breeze-hdr-games-rectangle-screen-recording/>  

<!-- 💡: Escape → Esc -->
<!-- 💡: System Monitor → Системный монитор -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: get new [thing] → «Загрузить \[что-то\]» -->
<!-- 💡: icon → значок -->
