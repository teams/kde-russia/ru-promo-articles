# На этой неделе в KDE: (не)настоящее восстановление сеанса

Что ж, мегавыпуск уже по сути высечен в камне и состоится через три дня! Многие участники усердно работали над ним больше года, и мы надеемся, что он вам понравится! Впрочем, я не сомневаюсь, что наши прилежные пользователи сразу же отыщут все пропущенные нами недочёты, и вскоре мы будем усердно работать над их устранением.

Но как только мы избавимся и от них, внимание снова начнёт смещаться в сторону новых функций. И у нас *большие* идеи касательно новшеств для Plasma 6.1 и последующих версий! Благодаря архитектурной работе, проделанной за последний год, открылось множество очень интересных возможностей. Я думаю, что уже очень скоро мы будем думать о Plasma 6 как о прекрасном трамплине в будущее.

И для начала у нас есть две хорошие новые функции, которые уже появились в Plasma 6.1:

## Новые возможности

Несмотря на то, что у нас пока нет настоящего восстановления сеанса для Wayland (требуется финализация протокола), у нас теперь есть лучшее из запасных решений: притворное восстановление сеанса, которое просто заново запускает приложения, открытые до выхода из системы, и полагается на то, что они правильно сохранили своё собственное состояние. Это работает и под X11, где применяется ко всем окнам, не покрытым настоящим восстановлением сеанса. В результате теперь при входе в систему должны, как положено, возвращаться все ваши приложения, а не случайное их подмножество, поддерживающее восстановление сеанса. Эта функция управляется существующим параметром, включающим или выключающим восстановление сеанса (David Edmundson, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3523))

В эффектах «Обзор» и «Все окна» расположение окон вместо двух несовершенных способов теперь определяется одним новым алгоритмом, и он куда лучше! Окна теперь расставляются гораздо более системно и не выглядят беспорядочно разбросанными или выстроенными в ступеньки, как раньше (Yifan Zhu, Plasma 6.1. [Ссылка 1](https://invent.kde.org/plasma/kwin/-/issues/189), [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=477830), [3](https://bugs.kde.org/show_bug.cgi?id=477833), [4](https://bugs.kde.org/show_bug.cgi?id=478097), [5](https://bugs.kde.org/show_bug.cgi?id=450263)):

![0](https://pointieststick.files.wordpress.com/2024/02/screenshot_20240223_092224.jpeg)

## Улучшения пользовательского интерфейса

В диспетчере файлов Dolphin при перетаскивании объекта на папку с включённой опцией «Открывать папки при перетаскивании» эта папка теперь показывает небольшую анимацию открытия (Felix Ernst, Dolphin 24.05. [Ссылка](https://invent.kde.org/system/dolphin/-/merge_requests/715)):

<https://i.imgur.com/ZSZRoN2.mp4?_=1>

Plasma теперь использует красивый значок для показа состояния батареи беспроводных гарнитур (Severin Von Wnuck-Lipinski, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3966))

Из контекстного меню рабочего стола скрыто действие «Обновить», редко использовавшееся и не решавшее большинства проблем с отсутствующими значками, для которых его пытались использовать. Контекстное меню стало ещё компактнее, так что ни у кого больше не будет оснований говорить, что оно «раздуто»! Если вам по какой-либо причине придётся вручную обновить рабочий стол, вы можете сделать это клавишей F5 (Nate Graham, Plasma 6.1. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/2066)):

![2](https://pointieststick.files.wordpress.com/2024/02/image-6.png)

## Исправления ошибок

Проводное подключение к вашему компьютеру iPhone или другого мобильного устройства от Apple теперь работает, если в названии устройства есть апостроф (например, «Konqi's iPhone») (Kai Uwe Broulik, kio-extras 24.02. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=462381))

Устранён наиболее распространённый сбой диспетчера окон KWin под X11, который часто наблюдался при изменении расположения экранов (Xaver Hugl, Plasma 5.27.11. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=473602))

Решена ​​проблема, из-за которой на устройствах с некоторыми видеокартами экран мог стать полностью чёрным, за исключением движущегося курсора, после переключения между виртуальными терминалами (Jakob Petsovits, Plasma 5.27.11. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=477916))

Исправлена ​​ошибка, из-за которой могло переставать работать перетаскивание значков в панели задач (Fushan Wen, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=465377))

Эффект KWin «Масштаб» (вариант экранной лупы) теперь может приближать все области сложных многомониторных конфигураций (Michael VanOverbeek, KWin 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=467182))

Процесс с названием `ksmserver-logout-greeter` больше не отображается в панели задач, когда виден экран выхода из системы (Akseli Lahtinen, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=403243))

Исправлен визуальный дефект, из-за которого обводка окон могла неплотно прилегать к окнам некоторых размеров при использовании определённых дробных коэффициентов масштабирования (Akseli Lahtinen, Plasma 6.0.1 [Ссылка](https://bugs.kde.org/show_bug.cgi?id=462005))

Исправлен случай, когда KWin мог аварийно завершить работу при использовании старых проприетарных драйверов NVIDIA 340-й серии (Влад Загородний, Plasma 6.0.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=477854))

Исправлена ​​ошибка, из-за которой Plasma могла аварийно завершить работу при перезапуске вручную через systemd (Harald Sitter, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=477165))

Средство выбора комбинации клавиш в диалоговом окне настройки панели теперь учитывает цветовую схему вашего оформления Plasma, как и ожидалось (Marco Martin, Frameworks 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=479015))

Встроенные уведомления в приложениях на основе библиотеки Kirigami больше не переполняются визуально, если в них содержится много текста (Jack Hill, Frameworks 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=476021))

Другие сведения об ошибках:

* Остаётся 3 ошибки в Plasma с очень высоким приоритетом (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma остаётся 30 (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 169 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-2-16&chfieldto=2024-2-23&chfieldvalue=RESOLVED&list_id=2621225&query_format=advanced&resolution=FIXED)

## Производительность и технические улучшения

Время запуска Dolphin сокращено на 2–17% (Felix Ernst, Dolphin 24.05. [Ссылка](https://invent.kde.org/system/dolphin/-/merge_requests/716))

## Как вы можете помочь

Спасибо всем, кто поучаствовал в [сборе средств, приуроченном к выпуску Plasma 6](https://kde.org/fundraisers/plasma6member/) — он крайне успешен! Я думал, что 500 новых спонсоров — это слишком оптимистичная цель для фонда KDE e.V., но вы показали мне, что я, к счастью, ошибался. Сейчас их **850**. Благодарю всех, кто поверил в нас, — мы постараемся не подвести! Если вам нравится наша работа, пожертвование — это отличный способ нас отблагодарить. 🙂

Разработчики…. да просто отдохните несколько дней. Вы заслужили!

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2024/02/23/this-week-in-kde-real-fake-session-restore/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: icons → значки -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: task manager → панель задач -->
