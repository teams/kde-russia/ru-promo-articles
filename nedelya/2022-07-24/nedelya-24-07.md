# На этой неделе в KDE: горы улучшений интерфейса

> 17–24 июля, основное — прим. переводчика

На этой неделе мы добились больших успехов в решении проблем пользовательского интерфейса, и я уверен, что среди них есть хотя бы одна проблема, которая вас раздражала!

## Исправленные «15-минутные ошибки»

Текущее число ошибок: **51, вместо бывших 52.** 1 добавлена и 2 исправлены:

Центр приложений Discover [больше не испытывает иногда проблем с получением отзывов о приложениях, особенно сразу после его запуска](https://bugs.kde.org/show_bug.cgi?id=426270) (Aleix Pol Gonzalez, Plasma 5.24.6)

Комбинация клавиш эффекта «Обзор» [больше не перестаёт работать в случайные моменты](https://bugs.kde.org/show_bug.cgi?id=448435) (Marco Martin, Plasma 5.26)

[Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)

## Новые возможности

Текстовые метаданные (не EXIF), хранящиеся внутри изображений PNG, [теперь извлекаются и отображаются в диалоговом окне «Свойства»](https://invent.kde.org/frameworks/kfilemetadata/-/merge_requests/61) (Kai Uwe Broulik, Frameworks 5.97):

![0](https://pointieststick.files.wordpress.com/2022/07/screenshot_20220707_223635.png)

## Wayland

В сеансе Plasma Wayland [исправлена ошибка, из-за которой диспетчер окон KWin мог аварийно завершать работу при нажатии физических кнопок на подключённом планшете для рисования](https://bugs.kde.org/show_bug.cgi?id=456817) (Aleix Pol Gonzalez, Plasma 5.25.4)

В сеансе Wayland анимация курсора, воспроизводимая при запуске приложений с помощью слоя совместимости XWayland, [теперь перестаёт воспроизводиться, как только приложение запустилось](https://bugs.kde.org/show_bug.cgi?id=455265) (Aleix Pol Gonzalez, Plasma 5.25.4)

В сеансе Wayland по подменю в меню запуска приложений по умолчанию [теперь можно полноценно перемещаться с помощью клавиатуры](https://bugs.kde.org/show_bug.cgi?id=447451) (Plasma 5.26)

## Улучшения пользовательского интерфейса

Диалог «Добавить правило» на странице «Брандмауэр» Параметров системы [теперь полностью читаем и выглядит лучше](https://bugs.kde.org/show_bug.cgi?id=456603) (Nate Graham, Plasma 5.25.4)

Рамка, выделяющая окна под курсором в новых эффектах «Все окна» и «Все рабочие столы», [теперь больше, а значит, заметнее](https://bugs.kde.org/show_bug.cgi?id=454842) (Nate Graham, Plasma 5.26):

![1](https://pointieststick.files.wordpress.com/2022/07/bigger-highlight.jpeg)

Маленькие перетаскиваемые маркеры в режиме редактирования панели [теперь отображают всплывающие подсказки при наведении, а ещё вы можете дважды щёлкнуть по ним, чтобы сбросить их в исходное состояние](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1055) (Иван Ткаченко, Plasma 5.26):

![3](https://pointieststick.files.wordpress.com/2022/07/tooltip.jpeg)

*«Перетащите для изменения минимальной высоты. Двойной щелчок для отмены»*

В диалоговом окне выбора экрана для записи через изолированные приложения (например, OBS Studio в формате Snap или Flatpak) [элементы списка теперь ведут себя адекватнее при двойном щелчке по ним](https://bugs.kde.org/show_bug.cgi?id=451240) (Aleix Pol Gonzalez, Plasma 5.26)

Когда изолированное приложение записывает экран, а в системном лотке отображается значок для принудительной остановки записи, [нажатие на него теперь открывает контекстное меню с действием «Остановить запись», а не просто сразу прерывает запись, прежде чем вы поймёте, что этот значок делает](https://bugs.kde.org/show_bug.cgi?id=446041) (Harald Sitter, Plasma 5.26)

Изменение геометрии окна (например, при перемещении или распахивании) [теперь сопровождается анимацией в виджете «Переключение рабочих столов»](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1038) (Иван Ткаченко, Plasma 5.26):

<https://i.imgur.com/WpM2Py8.mp4?_=1>

В диалоговом окне свойств файла, в метаданных которого есть GPS-координаты, [эта информация теперь отображается в виде ссылки](https://invent.kde.org/libraries/baloo-widgets/-/merge_requests/21) (Kai Uwe Broulik, Frameworks 5.97):

![5](https://pointieststick.files.wordpress.com/2022/07/screenshot_20220707_200327.png)

Значок приложения «Центр справки» [теперь всегда цветной, как и другие значки приложений, при использовании набора значков Breeze](https://bugs.kde.org/show_bug.cgi?id=456737) (Nate Graham, Frameworks 5.97)

## Исправления ошибок и улучшения производительности

В сеансе Plasma X11 [виджет «Выбор цвета» снова может брать цвета с экрана](https://bugs.kde.org/show_bug.cgi?id=454974) (Иван Ткаченко, Plasma 5.24.7)

Отправка оценок полезности отзывов в Discover [снова работает](https://invent.kde.org/plasma/discover/-/commit/7ab3bfe4ec6a6767aa1410aa448faed77c934069) (Aleix Pol Gonzalez, Plasma 5.24.7)

Вы снова можете [перемещаться между окнами и рабочими столами с помощью клавиатуры в эффекте «Все рабочие столы»](https://bugs.kde.org/show_bug.cgi?id=456068) (Влад Загородний, Plasma 5.25.4)

Предварительный просмотр цветовых схем на странице «Цвета» Параметров системы [теперь на 100% точно отражает цвета](https://bugs.kde.org/show_bug.cgi?id=456648) (Jan Blackquill, Plasma 5.25.4)

Фон строк меню в приложениях на основе QtQuick [теперь правильного цвета при использовании цветовых схем с цветами заголовка](https://bugs.kde.org/show_bug.cgi?id=456729), например, «Breeze, светлый вариант» и «Breeze, тёмный вариант» (Kartikey Subramanium, Frameworks 5.97)

Утилита для создания снимков экрана Spectacle и другие приложения [теперь правильно определяют наличие OBS Studio, Vokoscreen и других сторонних приложений](https://bugs.kde.org/show_bug.cgi?id=417575) в меню, где предлагается их установка (Nicolas Fella, Frameworks 5.97)

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2022/07/22/this-week-in-kde-tons-of-ui-improvements-and-bugfixes/>  

<!-- 💡: Help Center → Центр справки -->
<!-- 💡: Kicker → классическое меню запуска приложений -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: icon theme → набор значков -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: system tray → системный лоток -->
