# На этой неделе в KDE: грядёт заморозка Plasma 6

На сегодняшний день почти все запланированные для Plasma 6 новшества реализованы (наступает «заморозка функций»), и всеобщее внимание переключается на устранение недочётов. Пользователи, принимающие участие в раннем тестировании, присылают нам много отчётов об ошибках (в основном небольших), и мы исправляем их так быстро, как только можем! Были и другие, более заметные, изменения:

## Мегавыпуск KDE 6

> (Сюда входит всё ПО, которое будет выпущено 28 февраля: Plasma 6, библиотеки Frameworks 6 и приложения из состава Gear 24.02)

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 162](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

При выключении или перезагрузке компьютера из сеанса Plasma Wayland приложения с несохранёнными изменениями теперь предлагают пользователю сохранить их, а не просто немедленно завершают работу и теряют изменения. В итоге для этого не потребовалось никакого дополнительного протокола! Это была одна из трёх последних [проблем, блокирующих переход на Wayland](https://community.kde.org/Plasma/Wayland_Showstoppers) (David Redondo, [ссылка](https://bugs.kde.org/show_bug.cgi?id=461176))

«Прыгающие» клавиши (Специальные возможности > Фильтры клавиатуры) теперь полноценно работают в сеансе Plasma Wayland. Это была вторая из остававшихся [проблем Wayland](https://community.kde.org/Plasma/Wayland_Showstoppers)! Теперь [осталась всего одна](https://bugs.kde.org/show_bug.cgi?id=444335), и над её решением тоже работают! (Nicolas Fella, [ссылка](https://bugs.kde.org/show_bug.cgi?id=474752))

Диспетчер разделов больше не позволит создавать в файле `fstab` записи без точек монтирования, которые в будущем могут помешать подключению разделов (Arjen Hiemstra, [ссылка 1](https://invent.kde.org/system/kpmcore/-/merge_requests/48) и [ссылка 2](https://invent.kde.org/system/partitionmanager/-/merge_requests/41))

Файлы и папки, созданные в каталоге `~/Рабочий стол` в обход Plasma, теперь должны всегда немедленно появляться на рабочем столе (Harald Sitter, [ссылка](https://bugs.kde.org/show_bug.cgi?id=469260))

Изменение аватара пользователя через Параметры системы теперь отражается в меню запуска приложений сразу же, а не только после перезапуска Plasma (Akseli Lahtinen, [ссылка](https://bugs.kde.org/show_bug.cgi?id=384107))

Исправлен визуальный сбой во всплывающем окне автодополнения в текстовом редакторе Kate и других приложениях на основе компонента редактирования KTextEditor (Waqar Ahmed, [ссылка](https://bugs.kde.org/show_bug.cgi?id=445940))

Поскольку в Plasma 6 изменился программный интерфейс виджетов Plasma, виджеты, установленные для Plasma 5, не будут работать в Plasma 6. Теперь на их месте будет показываться сообщение об ошибке, предлагающее пользователям установить совместимую версию виджета (Marco Martin, [ссылка 1](https://invent.kde.org/plasma/plasma-framework/-/merge_requests/977), [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=471123))

После загрузки автономного обновления теперь можно пропустить его применение при следующей перезагрузке устройства. Мы рассматриваем возможность добавления этой опции и для выключения (Kai Uwe Broulik, [ссылка](https://bugs.kde.org/show_bug.cgi?id=435845))

Виджет «Батарея и яркость» был разделён на два новых виджета: «Яркость и цвет» и «Питание и батарея». В первый было также перенесено управление ночной цветовой схемой, так что общее число виджетов в системном лотке не изменилось — просто они теперь уместнее организованы (Natalie Clarius, [ссылка](https://invent.kde.org/plasma/plasma-workspace/-/issues/93)):

![0](https://pointieststick.files.wordpress.com/2023/11/color.jpeg)

![1](https://pointieststick.files.wordpress.com/2023/11/power.jpeg)

Почтовый клиент KMail получил поддержку нового безрамочного стиля Breeze из Plasma 6 (Carl Schwan, [ссылка](https://invent.kde.org/pim/kmail/-/merge_requests/109)):

![2](https://pointieststick.files.wordpress.com/2023/11/kmail.png)

Диалог «Приложение не отвечает» был обновлён до XXI-го века: теперь он работает в сеансе Plasma Wayland, а его интерфейс красивее и понятнее (Kai Uwe Broulik, [ссылка](https://blog.broulik.de/2023/11/freezing-in-style/)):

![3](https://pointieststick.files.wordpress.com/2023/11/screenshot_20231124_210752.png)

Утилиту для создания снимков экрана Spectacle теперь можно запускать также комбинацией клавиш Meta+Shift+S, что пригодится тем, у кого на клавиатуре нет клавиши Print Screen (Noah Davis, [ссылка](https://bugs.kde.org/show_bug.cgi?id=447550))

В Помощник первого запуска добавлена страница, показываемая только пользователям предварительных версий Plasma (Oliver Beard, [ссылка](https://invent.kde.org/plasma/plasma-welcome/-/merge_requests/119)):

![4](https://pointieststick.files.wordpress.com/2023/11/image-13.png)

## Другие сведения об ошибках

Этот раздел такой короткий, потому что все заметные исправления ошибок нынче включаются в мегавыпуск, а не в стабильные ветки — и чем ближе 28 февраля, тем заметнее это будет становиться.

* Остаётся 4 ошибки в Plasma с очень высоким приоритетом (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 40 (на прошлой неделе было 44). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 221 (!!!) ошибка была исправлена на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-11-17&chfieldto=2023-11-24&chfieldvalue=RESOLVED&list_id=2529440&query_format=advanced&resolution=FIXED)

## Как вы можете помочь

Мы проводим [сбор средств, приуроченный к выпуску Plasma 6](https://kde.org/fundraisers/plasma6member/), и нам нужна ваша помощь! Благодаря вам мы пересекли отметку в 75% от цели, но пока её не достигли. Если вам нравится наша работа, пожертвование — это отличный способ нас отблагодарить. 🙂

Разработчики, пожалуйста, помогите исправить проблемы в Qt 6, KDE Frameworks 6 и Plasma 6! Plasma 6 уже вполне годится для повседневного использования энтузиастами (я в их числе), хоть и всё ещё требует работы, чтобы стать готовой к выпуску в феврале.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/11/24/this-week-in-kde-the-plasma-6-feature-freeze-approaches/>  

<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: Partition Manager → Диспетчер разделов -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: system tray → системный лоток -->
