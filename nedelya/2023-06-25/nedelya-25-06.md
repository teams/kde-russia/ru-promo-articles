# На этой неделе в KDE: SDDM

На этой неделе SDDM — экран входа в систему, используемый KDE и некоторыми другими проектами, — спустя два с половиной года наконец-то [получил новую версию](https://github.com/sddm/sddm/releases/tag/v0.20.0)! Над ней работали многие участники KDE, в частности, Aleix Pol Gonzalez, Fabian Vogt, David Edmundson и Harald Sitter. Сейчас можно говорить о том, что они вносят основной вклад в развитие проекта и по сути возглавляют его. По этой причине мы хотели бы взять будущее SDDM в свои руки и к Plasma 6 [забрать его в сообщество KDE](https://invent.kde.org/plasma/plasma-desktop/-/issues/91). Если у нас это получится, новые выпуски SDDM будут публиковаться вместе с Plasma и использовать технологии Plasma для реализации дополнительных функций, таких как управление сетевыми подключениями и Bluetooth-устройствами до входа в сеанс, а также для более тесной интеграции с предпочтениями пользователей.

## Plasma 6

Из других новостей Plasma 6: всё было [переведено на использование KSvg](https://invent.kde.org/frameworks/ksvg/-/issues/1), новой библиотеки для работы с SVG, которую можно применять и вне Plasma. В основном над этим трудился Marco Martin, а я немного помогал и тестировал.

Кроме того, Xaver Hugl значительно [улучшил производительность графики](https://invent.kde.org/plasma/kwin/-/merge_requests/4177) на устройствах с несколькими видеокартами, где карте NVIDIA отведена вторичная роль. Также ведётся работа над солидным увеличением производительности для некоторых пользователей графических чипов Intel; больше об этом на следующей неделе.

Наконец, в Plasma 6 [собственное масштабирование Qt будет использоваться и в сеансе X11](https://bugs.kde.org/show_bug.cgi?id=356446), что улучшит опыт использования глобального масштабирования для пользователей, не работающих в многомониторных конфигурациях с разными коэффициентами масштаба у экранов. Последних мы, как и раньше, зовём на Wayland: в X11 этот сценарий использования в любом случае не поддерживается. Этим изменением тоже занимался Marco.

Как обычно, было много и других самых разных исправлений; так, David Edmundson и Xaver Hugl исправили все существенные ошибки, с которыми я сталкивался в сеансе Wayland из Plasma 6. В итоге я с радостью снова использую именно его, а не развалюху X11. 🙂

## Новые возможности

В Помощник первого запуска добавлен режим Live-среды, [позволяющий](https://invent.kde.org/plasma/plasma-welcome#for-live-distributions) дистрибутивам при загрузке системы со сменного носителя отображать специальную страницу с кнопкой для запуска установщика (Oliver Beard, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=466282)):

![0](https://pointieststick.files.wordpress.com/2023/06/image-17.png)

Виджет «Веб-браузер» теперь по умолчанию использует Favicon сайта в качестве своего значка на панели, а при желании вы можете выбрать собственный значок (Fushan Wen, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/407)):

![1](https://pointieststick.files.wordpress.com/2023/06/image-19.png)

Диспетчер разделов теперь можно запустить из контекстного меню съёмных устройств в диспетчере файлов Dolphin и других приложениях с панелью «Точки входа», чтобы, в частности, переформатировать выбранный носитель (Harald Sitter, Frameworks 6.0. [Ссылка](https://invent.kde.org/frameworks/kio/-/merge_requests/1320)):

![2](https://pointieststick.files.wordpress.com/2023/06/image-18.png)

## Улучшения пользовательского интерфейса

Dolphin теперь скрывает временные файлы приложений и файлы резервных копий (вроде тех бесполезных файлов с именами, заканчивающимися на `~` или `#`), если только не включён показ скрытых файлов (Méven Car, Dolphin 23.08. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=3212)). Это пожелание впервые было зарегистрировано 23 года назад!

В Системном мониторе названия элементов списка без значков теперь выровнены с названиями элементов со значками (Oliver Beard, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/plasma-systemmonitor/-/merge_requests/217)):

![3](https://pointieststick.files.wordpress.com/2023/06/image-16.png)

## Другие заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Дистрибутивы, основанные на rpm-ostree (например, [Fedora Kinoite](https://fedoraproject.org/kinoite/)), снова получают обновления через центр приложений Discover (Aleix Pol Gonzalez, Plasma 5.27.6. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=470372))

На странице «Thunderbolt» Параметров системы текст больше не уезжает за край окна при большом его размере (Nate Graham, Plasma 5.27.6. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=461102))

Диспетчер окон KWin больше не падает иногда, когда вы случайно вместо кнопки в заголовке окна нажимаете на её всплывающую подсказку (Xaver Hugl, Plasma 5.27.6. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=471285))

При использовании дробного коэффициента масштабирования флажки на странице настроек Discover больше не искажаются при прокрутке страницы с помощью сенсорной панели (Akseli Lahtinen, Plasma 5.27.6. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=471067))

Исправлено несколько причин аварийного завершения работы KWin сразу после входа в сеанс (David Edmundson, Plasma 5.27.7. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=444665) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=471139))

У большего числа приложений в формате Snap теперь отображаются уведомления, в том числе у мессенджера Slack (Kai Uwe Broulik, Plasma 5.27.7. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2577))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 3 ошибки в Plasma с очень высоким приоритетом (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma остаётся 59 (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 73 ошибки были исправлены на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-06-17&chfieldto=2023-06-24&chfieldvalue=RESOLVED&list_id=2400936&query_format=advanced&resolution=FIXED)

## Как вы можете помочь

Разработчики, пожалуйста, **переходите на Plasma 6** и начинайте исправлять ошибки в ней! Она вполне годится для повседневного использования энтузиастами (я в их числе), хоть и всё ещё требует работы, чтобы стать готовой к выпуску до конца года.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/06/23/this-week-in-kde-sddm/>  

<!-- 💡: GPU → видеокарта / графический ускоритель -->
<!-- 💡: Partition Manager → Диспетчер разделов -->
<!-- 💡: System Monitor → Системный монитор -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: venerable → древний -->
