# На этой неделе в KDE: всё, везде и сразу

На этой неделе у нас есть всё: и исправления ошибок в мегавыпуске, и новые функции на будущее, и правки для Frameworks 5, а также улучшения интерфейса, производительности, документации и экосистемы. Поехали!

## После Мегавыпуска

Калькулятор KCalc теперь показывает не только результат вычисления, но и введённое вами выражение (Gabriel Barrantes, KCalc 24.05, [ссылка](https://invent.kde.org/utilities/kcalc/-/merge_requests/67)):

![0](https://pointieststick.files.wordpress.com/2024/01/image-1.png)

Утилита для создания снимков экрана Spectacle теперь находит на снимках QR-коды и ​​предлагает вам открыть содержащиеся в них ссылки (Dinesh Manajipet, Spectacle 24.05. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=445487)):

![1](https://invent.kde.org/graphics/spectacle/uploads/d98c96d9e9a1c856ca5ae086522f24d8/image.png)

Виджет «Прогноз погоды» теперь показывает предупреждения о погоде для местоположений в США с источником данных NOAA Weather (Ismael Asensio, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=370226)):

![2](https://invent.kde.org/plasma/plasma-workspace/uploads/da54136c1b5fe38c32653c23227d8a42/noaa-alerts-full.png)

Страница «Планшет для рисования» Параметров системы теперь позволяет назначать на кнопки пера или планшета клавиши-модификаторы (Tino Lorenz, Plasma 6.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=461259))

## Мегавыпуск KDE 6

> (Сюда входит всё ПО, которое будет выпущено 28 февраля: Plasma 6, библиотеки Frameworks 6 и приложения из состава Gear 24.02)

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 231](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

### Улучшения пользовательского интерфейса

При перетаскивании приложения или окна из списка задач на другую часть панели Plasma больше не будет случайно создаваться виджет запуска (Niccolò Venerandi, [ссылка](https://bugs.kde.org/show_bug.cgi?id=416927))

Прокрутка колеса мыши над ползунками громкости в виджете «Громкость» и на странице «Настройка звука» Параметров системы теперь изменяет громкость с настроенным пользователем шагом (Yifan Zhu, [ссылка 1](https://bugs.kde.org/show_bug.cgi?id=409325) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=459639))

### Исправления ошибок

> **Важное примечание:** я не упоминаю здесь исправления ошибок, не успевших дойти до пользователей; посреди большого цикла разработки Plasma их слишком много, чтобы про них писать (а вам — читать), да и вообще мало кто их видел. Тем не менее огромное спасибо всем, кто плотно занимается их исправлением!

Критически важный эффект «Колышущиеся окна» снова работает во время использования эффекта масштабирования (Влад Загородний, [ссылка](https://bugs.kde.org/show_bug.cgi?id=480216))

На странице «Курсоры мыши» Параметров системы предпросмотр доступных размеров курсора теперь правильного размера при использовании коэффициента масштабирования экрана (Yifan Zhu, [ссылка 1](https://bugs.kde.org/show_bug.cgi?id=478089) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=479968))

Изменение названия, значка, команды запуска и других свойств приложения, добавленного в избранное в меню запуска приложений, теперь вступает в силу сразу же, а не только после перезапуска Plasma (Marco Martin, [ссылка](https://bugs.kde.org/show_bug.cgi?id=405790))

Исправлена ​​ошибка, из-за которой панели в режимах видимости «Автосокрытие» и «Скрывать под окнами» могли появляться и застревать в нескрытом состоянии при изменении настроек экрана (Yifan Zhu, [ссылка](https://bugs.kde.org/show_bug.cgi?id=478256))

Исправлено несколько проблем, из-за которых сочетания клавиш, использующие клавиши цифрового блока, регистрировались неправильно в сеансах X11 и Wayland (Nicolas Fella и Евгений Попов, [ссылка 1](https://bugs.kde.org/show_bug.cgi?id=453423), [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=446389), [ссылка 3](https://invent.kde.org/plasma/kwin/-/merge_requests/5039) и [ссылка 4](https://bugs.kde.org/show_bug.cgi?id=480169))

Всплывающее окно «Варианты виджета» больше нельзя открыть в нескольких экземплярах (Niccolò Venerandi, [ссылка](https://bugs.kde.org/show_bug.cgi?id=454526))

Другие сведения об ошибках:

* Остаётся 4 ошибки в Plasma с очень высоким приоритетом (на прошлой неделе было 3, но две из них, видимо, про одно и то же). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 34 (на прошлой неделе было 35). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 150 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2024-1-19&chfieldto=2024-1-26&chfieldvalue=RESOLVED&list_id=2592377&query_format=advanced&resolution=FIXED)

### Производительность и технические улучшения

Чтение различных параметров из файлов конфигурации, часто выполняемое программным обеспечением KDE, было ускорено на 35–40% (Friedrich Kossebau, [ссылка](https://invent.kde.org/frameworks/kconfig/-/merge_requests/271))

Улучшены скорость работы и отзывчивость интерфейса Параметров системы (David Edmundson, [ссылка](https://invent.kde.org/plasma/systemsettings/-/merge_requests/280))

Немного улучшено время запуска центра приложений Discover (Aleix Pol Gonzalez, [ссылка 1](https://invent.kde.org/plasma/discover/-/merge_requests/744) и [ссылка 2](https://invent.kde.org/plasma/discover/-/merge_requests/745#note_856053))

Захват видео с экрана диспетчером окон KWin для записи, демонстрации или показа миниатюр окон в сеансе Wayland теперь работает с видеокартами NVIDIA (Xaver Hugl, [ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/5037))

## Исправления для Frameworks 5

Функция удаления файлов конфигурации более не установленного приложения Flatpak в центре программ Discover снова работает (Иван Ткаченко, Plasma 5.27.10.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=478704))

Исправлен сбой в диспетчере файлов Dolphin при использовании kio-admin для выполнения операции с правами администратора (Harald Sitter, Frameworks 5.115. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=474909))

## Автоматизация и систематизация

Подготовлено [руководство](https://develop.kde.org/docs/getting-started/python/) по использованию PyQt для разработки программного обеспечения KDE с использованием Python, а не C++ (Thiago Sueto, Helio Loureiro и Dimitris Kardarakos, [ссылка](https://invent.kde.org/documentation/develop-kde-org/-/merge_requests/343))

## Изменения вне KDE, затрагивающие KDE

Участники KDE также внесли два заметных улучшения в мир за пределами KDE, что пригодится как KDE, так и другим проектам:

В стандартный портал настроек, используемый различными графическими окружениями и приложениями, добавлена опция «пользователь предпочитает высокую контрастность». Ожидайте появления её поддержки в программном обеспечении KDE в ближайшее время! (Dominic Hayes, [ссылка](https://github.com/flatpak/xdg-desktop-portal/pull/1175))

Браузеры на основе Chromium (Google Chrome, Opera, Яндекс Браузер) и Electron-приложения получили поддержку протокола Wayland `cursor-shape-v1`, что позволит им показывать курсоры правильного размера из настроенного пользователем набора в сеансе Plasma 6 Wayland (Илья Бизяев, Chromium 122. [Ссылка](https://chromium-review.googlesource.com/c/chromium/src/+/5196950))

## Как вы можете помочь

Спасибо всем, кто поучаствовал в [сборе средств, приуроченном к выпуску Plasma 6](https://kde.org/fundraisers/plasma6member/) — он крайне успешен! Я думал, что 500 новых спонсоров — это слишком оптимистичная цель для фонда KDE e.V., но вы показали мне, что я, к счастью, ошибался. Сейчас их уже целых **695**, и это число продолжает расти. Благодарю всех, кто поверил в нас, — мы постараемся не подвести! Если вам нравится наша работа, пожертвование — это отличный способ нас отблагодарить. 🙂

Разработчики, пожалуйста, помогите исправить проблемы в Qt 6, KDE Frameworks 6 и Plasma 6! Plasma 6 уже вполне годится для повседневного использования, хоть и всё ещё требует работы, чтобы стать готовой к выпуску в феврале.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2024/01/26/this-week-in-kde-everything-everywhere-all-at-once-edition/>  

<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: locations → расположения (в файловой системе) / местоположения (на карте) -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: thumbnail → миниатюра -->
