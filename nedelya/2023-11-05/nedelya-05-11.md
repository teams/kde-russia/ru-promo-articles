# На этой неделе в KDE: альфа-версия Plasma 6

Время летит, и уже через пару дней нам нужно выпустить альфа-версию Plasma 6. Все спешат протащить свои изменения в выпуск до мягкой заморозки функций (в понедельник) или хотя бы до жёсткой (через несколько недель). Начиная с понедельника, мы будем отдавать приоритет исправлению ошибок и доработке пользовательского интерфейса, лишь иногда отвлекаясь на завершение работы над новыми функциями.

## Мегавыпуск KDE 6

> (Сюда входит всё ПО, которое будет выпущено 28 февраля: Plasma 6, библиотеки Frameworks 6 и приложения из состава Gear 24.02)

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 113](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

Улучшено представление оценок в центре приложений Discover: теперь он показывает большой блок сводки по всем оценкам с выдержками из наиболее полезных, а все отзывы, как и раньше, открываются во всплывающем окне. «Полезность» отзывов, по которой они теперь сортируются, определяется на основе голосов пользователей, новизны и соответствия доступной версии (Marco Martin, [ссылка 1](https://invent.kde.org/plasma/discover/-/merge_requests/680) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=436057)):

![0](https://pointieststick.files.wordpress.com/2023/11/image-3.png)

Поиск в Discover был значительно улучшен и теперь практически всегда находит то, что вы ищете, если оно доступно (Marco Martin, [ссылка](https://invent.kde.org/plasma/discover/-/merge_requests/683)):

![1](https://pointieststick.files.wordpress.com/2023/11/image-2.png)

*Прошу прощения за отсутствие значков приложений, это неправильная настройка на моём компьютере, а не ошибка в Discover*

Страница «Энергосбережение» Параметров системы была заменена на новую версию на QML, которая понятнее и приятнее выглядит и не страдает ни от одной из ранее известных ошибок (Jakob Petsovits, [ссылка](https://invent.kde.org/plasma/powerdevil/-/merge_requests/259)):

![2](https://pointieststick.files.wordpress.com/2023/11/image-4.png)

Оформления рабочего стола Plasma, не содержащие SVG-стиль индикатора сгруппированных задач, — а в их числе теперь и Breeze — будут использовать новый интересный способ отображения групп в панели задач (Nate Graham. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/745)):

![3](https://pointieststick.files.wordpress.com/2023/11/image-1.png)

*Он ещё может поменяться на основе отзывов*

У линейных графиков с ограниченным по вертикали размером в Системном мониторе и его виджетах больше не обрезается легенда (Arjen Hiemstra, [ссылка](https://bugs.kde.org/show_bug.cgi?id=463498)):

![4](https://pointieststick.files.wordpress.com/2023/11/image.png)

Область ввода графического планшета теперь можно переназначить на комбинированную площадь всех экранов (Aki Sakurai, [ссылка](https://bugs.kde.org/show_bug.cgi?id=450874))

Временные диалоговые окна (то есть окна, полностью принадлежащие родительскому окну, живущие меньше него и закрываемые клавишей Esc, вроде диалогов настроек или сообщений об ошибках) теперь обрабатываются в сеансе Plasma Wayland так же, как и под X11: они больше не отображаются в панели задач как отдельные окна, передают состояние «требует внимания» своим родителям и так далее (Kai Uwe Broulik, [ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/3126))

Архиватор Ark научился распаковывать многотомные ZIP-архивы (Илья Поминов, Ark 24.02. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=399949))

## Другие заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Устранён один из наиболее распространённых случайных сбоев в Plasma и в Параметрах системы при изменении настроек аудиоустройств (David Redondo, Plasma 5.27.9. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=468791))

Блокировщик экрана больше не может сломаться, если очень много приложений было восстановлено из прошлого сеанса или какое-либо из восстановленных приложений незаметно исчерпало ресурсы на восстановление. Теперь Plasma предупредит вас об этом и предотвратит истощение ресурсов (Harald Sitter, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=475506))

При использовании службы управления сетями NetworkManager версии 1.44 её перезапуск, который иногда происходит автоматически после спящего режима, больше не будет приводить к исчезновению виджета сетей или прекращению отображения каких-либо сетей в нём (Илья Кацнельсон, Frameworks 5.112. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=471870))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 3 ошибки в Plasma с очень высоким приоритетом (на прошлой неделе было 2). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 52 (на прошлой неделе было 55). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 98 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-10-27&chfieldto=2023-11-02&chfieldvalue=RESOLVED&list_id=2511392&query_format=advanced&resolution=FIXED)

## Как вы можете помочь

Мы проводим [сбор средств, приуроченный к выпуску Plasma 6](https://kde.org/fundraisers/plasma6member/), и нам нужна ваша помощь! Если вам нравится наша работа, пожертвование — это отличный способ нас отблагодарить. 🙂

Разработчики, пожалуйста, помогите исправить проблемы в Qt 6, KDE Frameworks 6 и Plasma 6! Plasma 6 уже вполне годится для повседневного использования энтузиастами (я в их числе), хоть и всё ещё требует работы, чтобы стать готовой к выпуску в феврале.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/11/03/this-week-in-kde-plasma-6-alpha-approaches/>  

<!-- 💡: Escape → Esc -->
<!-- 💡: System Monitor → Системный монитор -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icons → значки -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
