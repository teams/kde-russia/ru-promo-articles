# На этой неделе в KDE: новинки в Plasma 6

Пишу по горячим следам конференции Akademy 2023, которая послужила плодотворной площадкой для совместной работы. Итак, помимо фонового труда над стабилизацией Plasma 6, мы добавили кое-что новенькое!

## Plasma и Frameworks 6

[Общие сведения](https://community.kde.org/Plasma/Plasma_6) – [Отслеживаемые проблемы: 59](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&classification=Applications&classification=Plasma&j_top=OR&keywords=qt6&keywords_type=allwords&list_id=2406607&o3=equals&o4=equals&o5=equals&o6=equals&order=changeddate%20DESC%2Cbug_severity%2Cdupecount%20DESC%2Cbug_id&product=Active&product=Active%20Window%20Control&product=Bluedevil&product=Breeze&product=colord-kde&product=Discover&product=homerun&product=kactivitymanagerd&product=kde-cli-tools&product=kde-gtk-config&product=kde-inotify-survey&product=kded-appmenu&product=kdeplasma-addons&product=Keyboard%20status%20applet&product=kgamma&product=khotkeys&product=kicker&product=kinfocenter&product=kiosk&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksplash&product=kstart&product=kwayland-integration&product=kwin&product=kwin4&product=kwintv&product=lattedock&product=Mangonel&product=Oxygen&product=PicoWizard&product=Plasma%20Bigscreen&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-browser-integration&product=plasma-disks&product=plasma-integration&product=plasma-mediacenter&product=plasma-mobile&product=plasma-mycroft&product=plasma-nm&product=plasma-pa&product=plasma-pass&product=plasma-pk-updates&product=plasma-redshift-control&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasma-wayland-protocols&product=plasma4&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=printer-applet&product=pulseaudio-qt&product=system-config-printer-kde&product=Systemd%20KCM&product=systemdgenie&product=systemsettings&product=systemsettings-kde3&product=Touchpad-KCM&product=user-manager&product=wacomtablet&product=Welcome%20Center&product=xdg-desktop-portal-kde&query_format=advanced&v3=systemsettings&v4=Discover&v5=plasma-systemmonitor&v6=kinfocenter)

Plasma теперь воспроизводит звук из активной звуковой темы (о них чуть позже), когда подключается или извлекается USB-устройство. И это, конечно же, можно настроить! (Kai Uwe Broulik, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=435509))

На совместимых устройствах с достаточно современными версиями ядра и прошивки теперь можно переключать профиль энергопотребления через всплывающее меню, подобное тому, что используется для настройки экранов; оно вызывается комбинацией клавиш Meta+B (Natalie Clarius, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=469487)):

![0](https://pointieststick.files.wordpress.com/2023/07/image-1.png)

Виджет «Сети» теперь показывает встроенное сообщение, когда вы подключаетесь к сети, требующей входа через страницу captive portal — например, в аэропорту (Kai Uwe Broulik, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=411856)):

![1](https://pointieststick.files.wordpress.com/2023/07/captive-portal-message.png)

В библиотеке KNotifications из состава Frameworks 6 появилась поддержка воспроизведения звуков из звуковых тем, совместимых с соответствующим [стандартом](https://freedesktop.org/wiki/Specifications/sound-theme-spec/) FreeDesktop (Ismael Asensio, Frameworks 6.0. [Ссылка](https://invent.kde.org/frameworks/knotifications/-/merge_requests/105))  
Это необходимо для перехода Plasma 6 на [новую звуковую тему](https://invent.kde.org/raploz/blue-ocean-sound-theme), подготовленную Guilherme Silva. Кстати, текущую звуковую тему, Oxygen, мы тоже сделали совместимой! Вы ещё услышите об этом изменении в ближайшие недели и месяцы, когда работа будет завершена.

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

При масштабировании жестом щипка в просмотрщике документов Okular документ больше не прокручивается с бешеной скоростью (Nicolas Fella, Okular 23.08. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=421755))

Заработала функция перемещения фокуса ввода на конкретную панель Plasma по нажатию пользовательской комбинации клавиш (Fushan Wen, Plasma 5.27.7. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=455398))

Различные опции теперь всегда правильно отображаются на странице настройки сенсорной панели в Параметрах системы, независимо от того, как она была открыта (Méven Car, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=408116))

На многих устройствах с двумя видеокартами обе из них теперь должны правильно определяться, что позволит указывать системе использовать более мощную для определённых приложений (Dave Vasilevsky, Frameworks 5.109. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=449106))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 4 ошибки в Plasma с очень высоким приоритетом (на одну больше, чем две недели назад). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma стало 60 (две недели назад была 61) [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 138 ошибок было исправлено за две недели во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-07-07&chfieldto=2023-07-21&chfieldvalue=RESOLVED&list_id=2424179&query_format=advanced&resolution=FIXED)

## Как вы можете помочь

Разработчики, пожалуйста, помогите исправить проблемы в [Qt 6, KDE Frameworks 6 и Plasma 6](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&f1=keywords&f2=product&list_id=2398791&o1=allwordssubstr&o2=notequals&query_format=advanced&v1=qt6&v2=neon)! Plasma 6 вполне годится для повседневного использования энтузиастами (я в их числе), хоть и всё ещё требует работы, чтобы стать готовой к выпуску до конца года.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/07/21/this-week-in-kde-plasma-6-features/>  

<!-- 💡: GPU → видеокарта / графический ускоритель -->
<!-- 💡: OSD → экранное уведомление -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
