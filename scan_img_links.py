#!/usr/bin/env python3
import os
import re
import requests
from pathlib import Path
from urllib.parse import urlparse
from PIL import Image, UnidentifiedImageError

# Match only links that are fully qualified with http(s)
LINK_REGEX_ONLY_FULL = re.compile('\[([^\]]*)\]\((https?:\/\/[^\)]+)\)')

# Match full links and relative paths
LINK_REGEX = re.compile('\[([^\]]*)\]\(([^\)]+)\)')

# Match only images with links that are fully qualified with http(s)
IMAGE_REGEX_ONLY_FULL = re.compile('!\[([^\]]*)\]\((https?:\/\/[^\)]+)\)')

# Match images with both full links and relative paths
IMAGE_REGEX = re.compile('!\[([^\]]*)\]\(([^\)]+)\)')

# Direct video links
VIDEO_REGEX = re.compile('<(.*?\\.(?:mp4|webm|mkv)(?:\\?.+?)*)>')

THUMBNAIL_NAMES = ["thumb.jpg", "thumb.png"]

def get_md_files(dirname: str):
    result = []
    for file_path in Path(dirname).rglob('*.md'):
        if file_path == Path(dirname) / "README.md":
            continue
        if file_path.name == "cheburnet.md":
            continue
        filename = str(file_path)
        result.append(filename)
    return sorted(result)


def get_regex_matches_in_file(filename: str, regex):
    with open(filename, 'r') as f:
        lines = f.readlines()

    result = []
    for l in lines:
        result += re.findall(regex, l)
    
    return result


def get_links_in_md_file(filename: str, only_full_links: bool):
    regex = LINK_REGEX_ONLY_FULL if only_full_links else LINK_REGEX
    return get_regex_matches_in_file(filename, regex)


def get_images_in_md_file(filename: str, only_full_links: bool):
    regex = IMAGE_REGEX_ONLY_FULL if only_full_links else IMAGE_REGEX
    return get_regex_matches_in_file(filename, regex)


def get_videos_in_md_file(filename: str):
    return get_regex_matches_in_file(filename, VIDEO_REGEX)


def get_filename_from_url(url: str):
    url_parsed = urlparse(url)
    return os.path.basename(url_parsed.path)


def is_integer(s: str):
    return s.isdigit()


def is_link_available(url: str):
    r = requests.get(url)
    return r.ok


def validate_thumbnail(path: Path):
    try:
        with Image.open(path) as im:
            width, height = im.size
            expected_width = round(height / 9 * 16)
            if abs(expected_width - width) > 2:
                print(f"ERROR: {path} is not 16:9 with resolution {width}x{height}")
            elif height < 300:
                print(f"ERROR: {path} is too small at {width}x{height}")
    except UnidentifiedImageError as e:
        print(f"ERROR: cannot open {path}: {e}")


def validate_image(path: Path):
    try:
        Image.open(path)
    except UnidentifiedImageError as e:
        print(f"ERROR: cannot open {path}: {e}")


def main():
    cwd = Path().resolve()
    files = get_md_files(".")

    all_full_links = set([link[1] for f in files for link in get_links_in_md_file(f, only_full_links=True)])
    all_non_full_links = set([link[1] for f in files for link in get_links_in_md_file(f, only_full_links=False)])
    all_non_full_links = all_non_full_links.difference(all_full_links)

    for f in files:
        f_location = Path(f).parent.resolve()
        f_siblings = set([x for x in f_location.iterdir() if x != Path(f).resolve()])
        f_siblings.discard(f_location / ".directory")

        full_links = set([link[1] for link in get_links_in_md_file(f, only_full_links=True)])
        non_full_links = set([link[1] for link in get_links_in_md_file(f, only_full_links=False)])
        non_full_links = non_full_links.difference(full_links)
        image_links = get_images_in_md_file(f, only_full_links=False)
        image_links_set = set([link[1] for link in image_links])
        video_links = get_videos_in_md_file(f)

        thumbnail_found = False
        thumbnail = None
        for t_name in THUMBNAIL_NAMES:
            t_path = f_location / t_name
            if t_path in f_siblings:
                thumbnail_found = True
                thumbnail = t_path
                f_siblings.remove(t_path)
                break
        if thumbnail_found:
            validate_thumbnail(thumbnail)
        else:
            warning_message = f"WARNING: no thumbnail found in {f_location.relative_to(cwd).as_posix()}"
            if not f_siblings:
                warning_message += ", and nothing to make it from!"
            elif not image_links and not video_links:
                warning_message += ", and no media to make it from!"
            print(warning_message)

        vk_link_txt_path = f_location / "vk_link.txt"
        if vk_link_txt_path in f_siblings:
            f_siblings.remove(vk_link_txt_path)
        else:
            print(f"WARNING: no vk_link.txt found in {f_location.relative_to(cwd).as_posix()}")

        for (link_name, link_url) in image_links:
            if link_url.startswith("http"):
                if not is_integer(link_name):
                    print(f"Non-integer image link text: {link_name}")
                    expected_img_filename = f"{get_filename_from_url(link_url)}"
                else:
                    expected_img_filename = f"{link_name}_{get_filename_from_url(link_url)}"
            else:
                # Probably relative.
                expected_img_filename = link_url
            expected_img_path = f_location / expected_img_filename
            if expected_img_path not in f_siblings:
                print(f"ERROR: cannot find {expected_img_path.relative_to(cwd).as_posix()}")
            else:
                validate_image(expected_img_path)
                f_siblings.remove(expected_img_path)

        video_names = set([get_filename_from_url(x) for x in video_links])

        f_siblings_after_videos = set()
        for sibling in f_siblings:
            found = False
            for vn in video_names:
                if "_" + vn in sibling.name:
                    found = True
                    video_names.remove(vn)
                    break
            if not found:
                f_siblings_after_videos.add(sibling)
        
        for vn in video_names:
            print(f"ERROR: cannot find {vn} in {f_location.relative_to(cwd).as_posix()}")
        f_siblings = f_siblings_after_videos

        for link in non_full_links:
            link_ext = link.split(".")[-1]
            link_local_file: Path = f_location / link
            link_local_file_pretty = link_local_file.relative_to(cwd).as_posix()
            if link_ext in ["mp4", "webm"]:
                if link_local_file.is_file():
                    # Relative link to a custom video.
                    f_siblings.remove(link_local_file)
                else:
                    print(f"ERROR: link to non-existent local video: {link_local_file_pretty}")
            elif link_ext in ["jpg", "jpeg", "png", "gif", "webp"]:
                if link not in image_links_set:
                    print(f"ERROR: link to image instead of embed: {link}")
            else:
                print(f"ERROR: Relative link to unknown file type: {link}")

        for sibling in sorted(f_siblings):
            print(f"WARNING: Unknown/unused file: {sibling.relative_to(cwd).as_posix()}")


main()
