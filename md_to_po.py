#!/usr/bin/env python3
import subprocess
import sys

from io import StringIO
from pathlib import Path
from pprint import pprint
from typing import List, Tuple, Optional, Dict
from collections.abc import Generator

import markdown
import regex
from lxml import etree


PO_HEADER = """
msgid \"\"
msgstr \"\"
\"Language: ru\\n\"
\"Language-Team: Russian <kde-russian@lists.kde.ru>\\n\"
\"MIME-Version: 1.0\\n\"
\"Content-Type: text/plain; charset=UTF-8\\n\"
\"Content-Transfer-Encoding: 8bit\\n\"
\"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\\n\"
"""
NAMESPACE = "{http://www.w3.org/XML/1998/namespace}"

CYRILLIC_ALPHABET = set("абвгдеёжзийклмнопрстуфхцчшщъыьэюя")
LATIN_ALPHABET = set("abcdefghijklmnopqrstuvwxyz")

HTML_COMMENT_RE = regex.compile(r'<!-- .*? -->')
NOTABLE_KDE_URL_RE = regex.compile(r'http(s)?://(bugs|phabricator|invent)\.kde\.org/[^)]+')
CHANGE_AUTHOR_RE = regex.compile(r" \([\p{L} ]+, [\p{L}\d. ]+([Ll]ink( \d)?(,| and)? ?)*\):?$")

def unmark_element(element, stream=None):
    if stream is None:
        stream = StringIO()
    if element.text:
        stream.write(element.text)
    for sub in element:
        unmark_element(sub, stream)
    if element.tail:
        stream.write(element.tail)
    return stream.getvalue()


markdown.Markdown.output_formats["plain"] = unmark_element
# noinspection PyTypeChecker
__md_plain = markdown.Markdown(output_format="plain")
__md_plain.stripTopLevelTags = False


def md_to_plain(text: str) -> str:
    return __md_plain.convert(text)


def chunks(l: list, chunk_size: int) -> Generator[list]:
    for i in range(0, len(l), chunk_size):
        yield l[i:i + chunk_size]


class Revision:
    def __init__(self, sha: str, subject: str, path: Path):
        self.sha = sha
        self.subject = subject
        self.path = path
    
    def __str__(self):
        return f"{self.path}: {self.sha}   {self.subject}"
    

class TranslationEntry:
    def __init__(self, source: str, target: str, file_path: Path, context: str, comments: List[str]):
        self.source = source
        self.target = target
        self.file_path = file_path
        self.context = context
        self.comments = comments
    
    def __str__(self):
        return f"[{self.file_path}]\n{self.source}\n->\n{self.target}\n<{self.context}>  # " + " # ".join(self.comments)


def get_file_revisions(file_path: Path) -> List[Revision]:
    p = subprocess.run(["git", "log", "--pretty=format:%h %s", "--name-only", "--follow", str(file_path)], check=True, capture_output=True)
    lines = p.stdout.decode().splitlines()
    lines = [l.strip() for l in lines]
    lines = [l for l in lines if l]

    revisions = []
    for rev_info in chunks(lines, 2):
        fmt, file_path = rev_info
        sha, subject = fmt.split(" ", 1)
        revisions.append(Revision(sha, subject, Path(file_path)))

    return revisions


def get_file_content_at_revision(revision: Revision) -> Optional[str]:
    try:
        p = subprocess.run(["git", "show", f"{revision.sha}:{revision.path}"], check=True, capture_output=True)
    except subprocess.CalledProcessError as e:
        print(f"Error getting content of {revision.path} at revision {revision.sha}: {e}", file=sys.stderr)
        return None
    content = p.stdout.decode()
    if not content:
        print(f"Empty content of {revision.path} at revision {revision.sha}", file=sys.stderr)
        return None
    return content


def is_russian_text(text: str) -> bool:
    text = md_to_plain(text)
    text = text.lower()
    text = regex.sub(HTML_COMMENT_RE, "", text)
    cyrillic_letter_count = sum(1 for c in text if c in CYRILLIC_ALPHABET)
    latin_letter_count = sum(1 for c in text if c in LATIN_ALPHABET)
    cyrillic_percentage = cyrillic_letter_count / (cyrillic_letter_count + latin_letter_count) * 100
    return cyrillic_percentage > 50


def choose_russian_revision(revisions: List[Revision]) -> Optional[Revision]:
    latest_revision = revisions[0]
    latest_content = get_file_content_at_revision(latest_revision)
    if latest_content is None:
        return None
    if is_russian_text(latest_content):
        return latest_revision
    else:
        return None


def choose_english_revision(revisions: List[Revision]) -> Optional[Revision]:
    for rev in revisions:
        content = get_file_content_at_revision(rev)
        if content is None:
            continue
        if not is_russian_text(content):
            return rev
    return None


def get_link_mapping(text: str) -> Dict[str, List[str]]:
    lines = text.splitlines()
    lines = [l.strip() for l in lines]
    lines = [l for l in lines if l]

    result = {}
    for l in lines:
        l_plain = md_to_plain(l).strip()
        if not l_plain:
            continue
        links = NOTABLE_KDE_URL_RE.finditer(l)
        links = [x.group(0) for x in links]
        links = [x for x in links if "buglist" not in x]
        links = sorted(list(set(links)))
        for link in links:
            result.setdefault(link, [])
            result[link].append(l_plain)
        
    return result


def split_out_author(line_plain: str) -> Tuple[str, str]:
    # Do not translate trailing parentheses in a changelog:
    # "This was finally [done](https://example.com) (Sam Smith, Plasma 5.25)"
    change_author_matches = CHANGE_AUTHOR_RE.finditer(line_plain)
    for match in change_author_matches:
        change_author = match.group(0)
        return line_plain[:match.start()], change_author
    return line_plain, ""


def match_mappings(russian_mapping: Dict[str, List[str]], english_mapping: Dict[str, List[str]], file_path: Path) -> List[TranslationEntry]:
    result = []
    memory = set()

    for context, sources in english_mapping.items():
        if context not in russian_mapping:
            continue

        if len(sources) > 1:
            print(f"Multiple sources for {context}: {sources}", file=sys.stderr)
            continue
        source = sources[0]
        source, source_author = split_out_author(source)

        targets = russian_mapping[context]
        if len(targets) > 1:
            print(f"Multiple targets for {context}: {targets}", file=sys.stderr)
            continue
        target = targets[0]
        target, target_author = split_out_author(target)

        if (source, target) in memory:
            continue
        memory.add((source, target))

        if source_author or target_author:
            if source_author == target_author:
                comments = [source_author]
            else:
                comments = [f"{source_author} | {target_author}"]
        else:
            comments = []

        result.append(TranslationEntry(source, target, file_path, context, comments))
    
    return result


def process_dir(dir_path: Path) -> List[TranslationEntry]:
    result = []
    for file_path in sorted(dir_path.glob("**/*.md")):
        if str(file_path) not in ["README.md", "web/cheburnet/cheburnet.md"]:
            result += process_file(file_path)
    return result


def process_file(file_path: Path) -> List[TranslationEntry]:
    # print(f"Processing {file_path}")
    revisions = get_file_revisions(file_path)
    if not revisions:
        print(f"Skipping {file_path}: No revisions; uncommitted?", file=sys.stderr)
        return []
    russian_revision = choose_russian_revision(revisions)
    if russian_revision is None:
        print(f"Skipping {file_path}: No Russian revision", file=sys.stderr)
        return []
    english_revision = choose_english_revision(revisions)
    if english_revision is None:
        print(f"Skipping {file_path}: No English revision", file=sys.stderr)
        return []

    russian_content = get_file_content_at_revision(russian_revision)
    english_content = get_file_content_at_revision(english_revision)

    russian_mapping = get_link_mapping(russian_content)
    english_mapping = get_link_mapping(english_content)

    translation_entries = match_mappings(russian_mapping, english_mapping, file_path)
    return translation_entries


def deduplicate_entries(entries: List[TranslationEntry]) -> List[TranslationEntry]:
    result = []
    entry_indices = {}
    for entry in entries:
        if entry.source in entry_indices:
            past_index = entry_indices[entry.source]
            if entry.target != result[past_index].target:
                result[past_index].comments.append(f"Alt translation: {entry.target}")
            continue
        result.append(entry)
        entry_indices[entry.source] = len(result) - 1
    return result


def write_po_file(translation_entries: List[TranslationEntry], po_file_path: Path):
    with po_file_path.open("w") as f:
        f.write(PO_HEADER)
        f.write("\n\n")
        for entry in translation_entries:
            f.write(f"#: {entry.file_path}\n")
            for c in entry.comments:
                f.write(f"# {c}\n")
            f.write(f"msgctxt \"{entry.context}\"\n")
            f.write(f"msgid \"{entry.source}\"\n")
            f.write(f"msgstr \"{entry.target}\"\n")
            f.write("\n")


def write_tmx_file(translation_entries: List[TranslationEntry], tmx_file_path: Path):
    root = etree.Element('tmx')
    header = etree.SubElement(root, 'header', creationtool="md_to_po",
                              creationtoolversion="1.0",
                              segtype="paragraph")
    header.set('o-encoding', 'UTF-8')
    body = etree.SubElement(root, 'body')
    
    for entry in translation_entries:
        tu_node = etree.SubElement(body, 'tu')
        
        tuv_en = etree.SubElement(tu_node, 'tuv')
        tuv_en.set(NAMESPACE + "lang", "en")
        tuv_en_seg = etree.SubElement(tuv_en, 'seg')
        tuv_en_seg.text = entry.source
        
        tuv_ru = etree.SubElement(tu_node, 'tuv')
        tuv_ru.set(NAMESPACE + "lang", "ru")

        tuv_ru_context = etree.SubElement(tuv_ru, 'prop')
        tuv_ru_context.set("type", "x-context")
        tuv_ru_context.text = entry.context

        tuv_ru_file = etree.SubElement(tuv_ru, 'prop')
        tuv_ru_file.set("type", "x-file")
        tuv_ru_file.text = entry.file_path.as_posix()

        tuv_ru_seg = etree.SubElement(tuv_ru, 'seg')
        tuv_ru_seg.text = entry.target

        for c in entry.comments:
            tuv_ru_comment = etree.SubElement(tuv_ru, 'note')
            tuv_ru_comment.text = c

    tree = etree.ElementTree(root)
    etree.indent(tree, space="    ")
    tree.write(tmx_file_path.as_posix(), pretty_print=True, xml_declaration=True, encoding="utf-8")


def main():
    if not Path(".git").is_dir():
        print("This script should be run from a Git repository", file=sys.stderr)
        sys.exit(1)

    dir_or_file_path = Path(sys.argv[1])
    result = None
    if dir_or_file_path.is_dir():
        result = process_dir(dir_or_file_path)
    elif dir_or_file_path.is_file() and dir_or_file_path.suffix == ".md":
        result = process_file(dir_or_file_path)
    else:
        print(f"No such Markdown file or directory: {dir_or_file_path}", file=sys.stderr)
        sys.exit(1)

    result = deduplicate_entries(result)
    write_po_file(result, Path("ru-promo-articles.po"))
    write_tmx_file(result, Path("ru-promo-articles.tmx"))


if __name__ == "__main__":
    main()
