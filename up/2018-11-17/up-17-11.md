# Удобство и продуктивность, ч. 45

Не желаете взглянуть на новый выпуск Удобства и продуктивности? У нас много крутых улучшений для основных приложений KDE — часть для грядущего релиза KDE Applications 18.12, а некоторые для следующего. И ещё много полезного, конечно же!

## Новые возможности

[Теперь можно помечать сетевые соединения как тарифицируемые](https://bugs.kde.org/show_bug.cgi?id=401020) (Jan Grulich, KDE Plasma 5.15.0)

[Диспетчер файлов Dolphin теперь показывает список всех активных меток в панели «Точки входа»](https://bugs.kde.org/show_bug.cgi?id=182367) (Nicolas Fella, KDE Applications 19.04.0):

![0](https://i.imgur.com/xjjr1e4.png)

Через контекстное меню файлов и папок в Dolphin [теперь можно добавлять и удалять метки](https://phabricator.kde.org/D16872) (Nicolas Fella, KDE Applications 19.04.0):

![1](https://phabricator.kde.org/file/data/7ptov3nvkape2aywhgkk/PHID-FILE-2c256gwecviafkyyzxez/Screenshot_20181114_155822.png)

## Исправления ошибок

Перекрёстные курсоры в наборах курсоров Breeze и Breeze Snow [теперь выглядят гораздо лучше, и их легче различить на тёмном и светлом фоне соответственно](https://bugs.kde.org/show_bug.cgi?id=400110) (Noah Davis, KDE Plasma 5.12.8):

![2](https://phabricator.kde.org/file/data/ue4hbx5ub6i3m4thzhmg/PHID-FILE-ihpc72ll4pyidqwbekfd/Screenshot_20181114_151804.png)

Центр приложений Discover [снова показывает ход выполнения для задачи обновления](https://bugs.kde.org/show_bug.cgi?id=400891) (Aleix Pol Gonzalez, KDE Plasma 5.14.4)

[Интерфейс настройки VPN теперь можно использовать на маленьких экранах](https://bugs.kde.org/show_bug.cgi?id=401004) (Jan Grulich, KDE Plasma 5.14.4)

Анимации открытия и закрытия окон «Скольжение», «Растворение» и «Масштабирование» [теперь взаимоисключающие](https://phabricator.kde.org/D16858) (Влад Загородний, KDE Plasma 5.15.0)

[Если в результате изменения размера шрифта заголовок окна должен стать выше, то это теперь происходит сразу, а не после перезагрузки](https://bugs.kde.org/show_bug.cgi?id=400980) (Влад Загородний, KDE Plasma 5.15.0)

[Улучшено поведение эмулятора терминала Konsole в GNOME Dash](https://bugs.kde.org/show_bug.cgi?id=372441) (David Hallas, KDE Applications 18.12.0)

[В Dolphin при размонтировании тома из панели «Точки входа» теперь можно примонтировать его снова](https://bugs.kde.org/show_bug.cgi?id=400992) (Thomas Surrel, KDE Applications 18.12.0)

В Konsole при расширении выделения, которое изначально было создано двойным или тройным щелчком по тексту, с помощью щелчка с зажатым Shift, [оно теперь расширяется по словам в первом случае или по целым строкам во втором](https://bugs.kde.org/show_bug.cgi?id=374454) (Glenn Coombs, KDE Applications 18.12)

В Discover (и в других приложениях на основе библиотеки Kirigami, использующих многостолбцовую навигацию) [теперь можно взаимодействовать с элементами пользовательского интерфейса в столбцах, которые видимы, но не сфокусированы](https://bugs.kde.org/show_bug.cgi?id=400518) (Marco Martin, KDE Frameworks 5.53)

## Улучшения пользовательского интерфейса

Строка поиска и запуска KRunner [больше не показывает дубликаты файлов, относящихся к нескольким категориям](https://phabricator.kde.org/D16690) (Kai Uwe Broulik, KDE Plasma 5.14.4)

Discover [теперь поддерживает расширения для изолированных приложений Flatpak и позволяет выбрать, какие из них установить](https://bugs.kde.org/show_bug.cgi?id=400795) (Aleix Pol Gonzalez, KDE Plasma 5.15.0)

[Добавлены новые значки для сортировки](https://bugs.kde.org/show_bug.cgi?id=393318), один из которых теперь [используется в новой кнопке выбора критерия сортировки в диалогах открытия и сохранения файлов](https://phabricator.kde.org/D12337) (Rafael Brandmaier и Nate Graham, KDE Frameworks 5.53):

![3](https://phabricator.kde.org/file/data/6wh7avlpmyha5inx337e/PHID-FILE-r2roksziq7rjxxmuhn6i/Sort_order_chooser.png)

В Dolphin [появился новый пункт меню, который можно выбрать для показа всех скрытых пунктов в панели «Точки входа»](https://bugs.kde.org/show_bug.cgi?id=400860) (Chris Rizzitello, KDE Applications 18.12.0):

![4](https://i.imgur.com/nOhBy1i.png)

[Встроенный в текстовый редактор Kate терминал теперь автоматически синхронизирует текущий каталог с расположением активного файла](https://phabricator.kde.org/D16650) и [может быть сфокусирован и расфокусирован с помощью почти уже стандартной клавиши F4](https://phabricator.kde.org/D16652) (Gregor Mi, KDE Applications 18.12.0)

[В Kate при открытой странице Быстрого перехода попытка снова открыть её приведёт к возврату к открытому документу](https://phabricator.kde.org/D16378) (Loh Tar, KDE Applications 18.12.0)

[Kate, KDevelop и другие приложения, использующие библиотеку KSyntaxHighlighting, теперь поддерживают подсветку синтаксиса в файлах BrightScript](https://phabricator.kde.org/D16677) (Daniel Levin, KDE Frameworks 5.53)

[У приложения Sublime Text появился неплохой новый значок в Breeze](https://phabricator.kde.org/D16815) (Rafael Brandmaier, KDE Frameworks 5.53):

![5](https://phabricator.kde.org/file/data/fj56xyqznsjbkio5jfm7/PHID-FILE-meysex7n2klyeu7trqov/Sublime_New_Icon.png)

На следующей неделе ваше имя может появиться в этом списке! Не знаете, с чего начать? Просто спросите! Я недавно смог помочь нескольким новым участникам, буду рад помочь и вам! Вы также можете прочитать статью по ссылке <https://kde.ru/join> и выяснить, как стать частью того, что действительно имеет значение. Вам не требуется быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Если мои усилия производить, направлять и документировать эту работу кажутся вам полезными и вы хотели бы увидеть больше, можете пожертвовать средств мне на [Patreon](https://www.patreon.com/ngraham), [Liberapay](https://liberapay.com/ngraham/donate) или [PayPal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=Y279422B3LMRE).
Также рассмотрите возможность [пожертвования средств в фонд KDE e.V.](https://kde.org/ru/community/donations/)

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Денис Карповский](https://vk.com/geext)  
*Источник:* <https://pointieststick.com/2018/11/17/this-week-in-usability-productivity-part-45/>  

<!-- 💡: Places panel → панель «Точки входа» -->
<!-- 💡: directory → каталог -->
<!-- 💡: icon → значок -->
<!-- 💡: location → расположение (в файловой системе) / местоположение (на карте) -->
<!-- 💡: screenshot → снимок экрана -->
