# Да кто такой этот ваш Wayland?

Wayland. Его часто упоминают: «Исправлена ошибка такая-то в сеансе Plasma Wayland», «Сеанс Wayland получил поддержку такой-то функции». А в последнее время он мелькает в новостях о [предложении Fedora KDE в 40-й версии отказаться от сеанса Plasma X11 и оставить только Wayland](https://fedoraproject.org/wiki/Changes/KDE_Plasma_6#Feedback). Я видел много волнений и испугов по этому поводу.

Что ж, давайте обсудим!

## Что такое Wayland?

Wayland — это [набор протоколов](https://wayland.app/protocols/), определяющих, как компоновщик (диспетчер окон) рисует что-либо на экране и как приложения говорят компоновщику, что нужно нарисовать. Это отчасти похоже на то, как веб-браузеры используют протокол HTTP для загрузки веб-страниц, а почтовые клиенты обращаются к серверу по протоколу SMTP для отправки писем.

К Wayland также прилагается реализация передачи сообщений по этим протоколам в виде легковесных библиотек `libwayland-client` и `libwayland-server` со стабильными версионированными программными интерфейсами. Различные приложения и компоновщики вроде KWin (KDE) и Mutter (GNOME) используют эти интерфейсы.

## Зачем Wayland нужен?

Вкратце, он нужен, потому что X11 — старая оконная система, которую он заменяет, — мертва.

Код X11 годами не развивался, а в последнее время и вовсе [не получал никаких изменений](https://gitlab.freedesktop.org/xorg/xserver/-/commits/master), кроме как в слое совместимости XWayland, позволяющем приложениям, написанным для X11, работать в сеансе Wayland. Прекращение поддержки настолько центрального компонента, как сервер оконной системы, — большая проблема, так как означает отсутствие исправлений ошибок и уязвимостей, а также отставание от меняющихся потребностей окружающих.

## Почему X11 умерла?

Базовая модель разработки X11 заключалась в развитии тяжеловесного сервера оконной системы, Xorg, отвечающего за всё и сразу и используемого всеми. В теории у него могло быть несколько реализаций, и в разные моменты времени [они существовали](https://ru.wikipedia.org/wiki/XFree86), но на практике написать новый сервер с нуля, не основываясь на старом, просто невозможно. Все сильно предпочитали договариваться об использовании единственного X-сервера и разом мигрировали с одного на другой, когда появлялось лучшее ответвление. Это было удобно, потому что экономило ограниченные ресурсы на разработку, и когда в X-сервер добавлялась новая функция, все автоматически получали к ней доступ.

Но был и недостаток: так как все использовали Xorg, любая добавленная ради одного применения функция могла сломать все остальные сценарии в других проектах, и это частенько случалось. Исправления одних ошибок часто ухудшали малоизвестный функционал, используемый другими программами.

В итоге Xorg стал слишком большим, слишком сложным и слишком хрупким, чтобы его можно было трогать без риска сломать всю экосистему Linux. Сегодня он стабилен, но не потому, что хорош, а потому что уже много лет как заморожен. Стабильность пришла рука об руку с застоем, а в мире технологий, как известно, что не адаптируется, то умирает — вместе со всем, что на это полагалось.

## А чем Wayland лучше?

Wayland был задуман разработчиками X11, стремившимися избежать повторения своих ошибок. Помимо множества технических отличий, Wayland, будучи минимальным набором протоколов и двумя крайне легковесными библиотеками, перекладывает всю тяжёлую работу на плечи диспетчеров окон (теперь именуемых компоновщиками Wayland), которые становятся серверами оконной системы в своих графических окружениях. Новая функция, добавленная в один компоновщик, не нарушает работу других. Компоновщик может и вовсе помещать новые функции в собственные приватные протоколы, и только приложения, поддерживающие работу конкретно с ним, смогут их использовать.

## Постойте, звучит так себе

Да, Wayland не лишён и собственных изъянов. Травмированные разработчики X11, на мой взгляд, перестарались и слишком далеко ушли в другую крайность. Минимальный набор протоколов Wayland лишён большинства функций, необходимых для работы сложных приложений и графических окружений: там нет ничего про блокировку экрана, удалённый доступ, передачу фокуса окнам других программ, масштабирование с дробным коэффициентом и так далее. Авторам каждого компоновщика пришлось самостоятельно придумывать, как решить эти задачи, что фрагментировало ресурсы на разработку и стало неподъёмной задачей для маленьких команд без опыта суровой работы с графическим стеком. Это реальные проблемы, и мы не должны их замалчивать.

## Да, но есть решения

Со временем минимальный набор основных протоколов был дополнен всем необходимым для работы рабочего стола Linux и сложных приложений. Больша́я часть этой работы была проделана совсем недавно усилиями KDE и на средства Blue Systems и Valve. Так что большинство старых жалоб в интернете на отсутствие в Wayland какой-либо функции (дробного масштабирования, удалённого доступа к экрану или глобальных комбинаций клавиш), скорее всего, уже неактуальны.

В свою очередь, проблему фрагментации ресурсов на разработку стремится решить [wlroots](https://gitlab.freedesktop.org/wlroots/wlroots), библиотека реализаций Wayland, которую можно использовать для создания собственного компоновщика. Мы не используем её в KWin, потому что основную часть этой работы мы проделали сами ещё до её появления, но она очень пригодится тем, кто сейчас захочет написать компоновщик с нуля. Кто знает, может, в будущем и KWin на неё перепишут.

## Почему переход на Wayland такой долгий?

Недостаточность основного набора протоколов Wayland для полноценной замены X11 — плохое архитектурное решение со стороны его авторов, которое с самого выпуска в 2008 году ограничило его шансы на быстрое внедрение. Мы не видели таких сложностей с другими новыми технологиями вроде Systemd и PipeWire — они были приняты куда быстрее.

И, к сожалению, проводить новые протоколы через процесс одобрения, чтобы исправить эту проблему, — то ещё политическое упражнение. Это требует сопереживания и компромиссов с людьми из других проектов, которые подходят к решению той же проблемы совершенно иным образом. А кто-то может и вовсе не соглашаться, что эту проблему вообще стоит решать. Бесконечные споры о мелочах сбивают обсуждение с толку, все теряют мотивацию и перестают работать. Долгое время срочность принятия таких решений была низкой, потому что X11 ещё не была окончательно мертва.

В общем, это «удовольствие» растянулось на пятнадцать лет, и всё это время Wayland был всеобщим посмешищем. Приятного мало, но что поделать… зато теперь мы готовы. Стандартные протоколы для более-менее всего, что нужно, появились. Над несколькими оставшимися очевидными пробелами (вроде калибровки цветов экрана) активно работают в приоритетном порядке.

> Разработчик KWin Xaver Hugl считает, что главной причиной медленного развития Wayland стали графические драйверы: «В одно время с появлением Wayland появился и новый API ядра Linux для работы с видеокартами, Direct Rendering Manager (DRM). Он был необходим для реализации Wayland, но его доработка и создание совместимых с ним драйверов заняли несколько лет. Разумеется, стоит отдельно упомянуть NVIDIA: из пятнадцати лет существования Wayland, первые одиннадцать было невозможно написать компоновщик, работающий с видеокартами NVIDIA, ещё два это можно было сделать только используя специфичный для NVIDIA интерфейс EGLStreams, и лишь в последние два года — через стандартный GBM, но со сломанной синхронизацией».

## Ну когда уже?

Plasma и приложения KDE отлично работают под Wayland, *особенно* в предстоящем выпуске Plasma 6. Как я уже упомянул, есть пробелы, но они нынче довольно быстро устраняются.

Большинство сторонних приложений, не адаптированных для Wayland, работают нормально через слой совместимости XWayland. Встречаются и исключения: некоторые приложения особенно сильно зависят от X11 — их нужно доработать для использования новых программных интерфейсов Wayland.

Многие разработчики привыкли игнорировать новости о Wayland, когда он был ещё совсем игрушечным, и не занимались адаптацией своих приложений. Что ж, теперь он не игрушка, и они вдруг начали осознавать срочность подготовки приложений к работе с Wayland. Их можно понять. Но теперь всё серьёзно, и настало время действовать. Любым протоколам, ещё находящимся в работе, пойдёт на пользу обратная связь от разработчиков приложений; можно предлагать и новые. Этот процесс занимает время, и чем раньше он начнётся, тем лучше — потребность в нём сама никуда не исчезнет.

## Возвращаясь к Fedora KDE

Дистрибутив Fedora всегда был в первых рядах прогресса в Linux, внедряя новые технологии, как только они становились уже *почти* готовыми, и тем самым заставляя их быстро совершенствоваться. Fedora первой внедрила Systemd, PulseAudio и PipeWire. Она была первой, кто перешёл на сеанс Plasma Wayland по умолчанию. Теперь же Fedora KDE хочет первой полностью отказаться от сеанса X11 и перевести всех на Wayland.

Целевая аудитория Fedora — люди, которым нравятся перемены. Если это про вас, то все эти проекты должны вам казаться интересными и крутыми! Если нет… то Fedora просто не для вас. Это нормально. Переустановка ОС — неприятное занятие, но важно выбрать ту, которая соответствует вашим потребностям и предпочтениям. Свобода выбора приходит с ответственностью за выбор.

Может быть, вы боитесь, что Fedora — это «первый звоночек», и в том же направлении двинутся все остальные. Что ж, резонно, но на любые изменения требуется время. Дистрибутивы, специально поставляющие старое ПО, вроде Ubuntu LTS и Debian Stable, позволят вам пользоваться X11 ещё многие годы. В Arch пакеты X11, вероятно, тоже будут доступны. У вас есть выбор. К тому времени, когда даже Ubuntu LTS откажется от X11, в Wayland уже всё будет полностью готово.

## Подведём итоги

Wayland — это замена мёртвой оконной системы X11. Несмотря на ухабистый путь развития, он вполне готов для Plasma и приложений KDE, поэтому Fedora KDE активно его продвигает. Многие сторонние приложения уже нативно работают под Wayland, а авторам тех, которые ещё не работают, нужно приложить усилия по переходу на Wayland. Если им всё ещё чего-то не хватает, они должны помочь добавить это. Работа идёт, и движение в этом направлении никуда не денется. Нам нужно сотрудничать, чтобы добиться результатов быстрее и комфортнее.

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/09/17/so-lets-talk-about-this-wayland-thing/>  
